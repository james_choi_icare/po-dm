﻿/* =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description:	Claim Management Domain - ccst_medicaltreatment
-- Version: v24 initial build
-- =============================================*/
CREATE PROCEDURE [dbo].[SP_REC_XFM_MEDICALTREATMENT]
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_medicaltreatment','Claim Management - v24 12/06/2019'

Select 
	app.Create_date as ActionDate,
	'PT' as ApprovedTreatment,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':INJURED' as ClaimContactID,
	'EMICS:' + RTRIM(cd.claim_number) as ClaimID,
	ICD_Code1 as ICD1,
	ICD_Code2 as ICD2,
	ICD_Code3 as ICD3,
	NULL as ICD4,
	app.claim_no as LUWID,
	'EMICS:' + RTRIM(convert(varchar, app.Claim_no)) + '_' + convert(varchar, app.ID) + ':MEDICALTREATMENT' as PublicID,
	app.No_of_Treatments as TreatmentQuality
	
Into ccst_medicaltreatment

From DM_STG_EMICS.dbo.Approval app 
Inner join DM_STG_EMICS.dbo.Claim_Detail_Extra cde on app.claim_no = cde.claim_no
Inner join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number = app.claim_no
--Inner Join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.claim_number
Where cd.is_null = 0
END