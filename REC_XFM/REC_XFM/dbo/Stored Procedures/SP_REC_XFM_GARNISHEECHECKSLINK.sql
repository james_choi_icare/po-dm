﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_GARNISHEECHECKSLINK]
AS
BEGIN
-- ===============================
-- Author: Ann
-- Domain: PAYMENTS AND Recoveries  ccst_garnisheecheckslink_icare
-- Version: v42
--			14/05 checked against v45.6 no change
--			04/09 updated checkid xform, publicid xform, SC to include cpc 

-- =====================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_garnisheecheckslink_icare','Payments and Recoveries - v47.2 04/09/2019'
--drop table ccst_garnisheecheckslink_icare
	SELECT 
	CASE WHEN CHEQUE_NO IS  NULL THEN  
		'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+isnull(convert(varchar, cpc.ID),'00')+':CHECK'
		ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+isnull(convert(varchar,cpc.ID),'00')+':CHECK' END AS CheckID,
	'EMICS:'+RTRIM(GRD.Claim_No)+'_'+RTRIM(GRD.Garnishee_No)+'_'+convert(varchar,GRD.ID)+':GARNISHEE' as GarnisheeID,
	pr.Claim_No AS LUWID,
	CASE WHEN CHEQUE_NO IS  NULL THEN  
		'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+RTRIM(GRD.Garnishee_No)+'_'+convert(varchar, GRD.ID)
		ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+RTRIM(GRD.Garnishee_No)+ '_' + convert(varchar, GRD.ID)
		END As PublicID,
	Cheque_No as ChequeNo_SRC_VALUE

	Into ccst_garnisheecheckslink_icare

	FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	ON 
	CD.Claim_Number=CPR.Claim_number
	INNER JOIN (select * from DM_STG_EMICS.dbo.Payment_Recovery) PR
	on CPR.Claim_number=PR.Claim_No
	AND CPR.Payment_no=PR.Payment_no
	INNER JOIN (select * from DM_STG_EMICS.dbo.Garnishees GR
	where  not exists (select 1 from DM_STG_EMICS.dbo.Garnishees GR1 
	where Cancelled_Date is not null and GR.Claim_No=GR1.Claim_No
	and GR.Garnishee_No=GR1.Garnishee_No and  
	 GR.Start_Date<=isnull(GR1.End_Date,'1-Jan-2079') 
	and isnull(GR.End_Date,'1-Jan-2079')>=GR1.Start_Date
	)
	union all
	select * from DM_STG_EMICS.dbo.Garnishees GR
	where  GR.Cancelled_Date is null and  exists (select 1 from DM_STG_EMICS.dbo.Garnishees GR1 
	where Cancelled_Date is not null and GR.Claim_No=GR1.Claim_No
	and GR.Garnishee_No=GR1.Garnishee_No and  
	 GR.Start_Date<=isnull(GR1.End_Date,'1-Jan-2079') 
	and isnull(GR.End_Date,'1-Jan-2079')>=GR1.Start_Date
	)
	union all
	select * from DM_STG_EMICS.dbo.Garnishees GR
	where  GR.Cancelled_Date is not null and  not exists (select 1 from DM_STG_EMICS.dbo.Garnishees GR1 
	where Cancelled_Date is  null and GR.Claim_No=GR1.Claim_No
	and GR.Garnishee_No=GR1.Garnishee_No and  
	 GR.Start_Date<=isnull(GR1.End_Date,'1-Jan-2079') 
	and isnull(GR.End_Date,'1-Jan-2079')>=GR1.Start_Date
	)) GRD
	ON PR.Claim_No=GRD.Claim_No
	AND PR.Garnishee_No=GRD.Garnishee_No
	LEFT OUTER JOIN (
	SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_STG_CC..CCX_paycodepaymenttype_icare A,
	 (select * from DM_STG_CC..CCX_paycode_icare where Retired=0) B,DM_STG_CC..cctl_paymenttype_icare C
	WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC
	ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)

	WHERE CD.is_Null=0 and   PR.Trans_Amount>0 and PR.Period_Start_Date<=GRD.End_Date and  PR.Period_End_Date>=GRD.Start_Date and PR.Reversed<>1

END