﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_EMPLOYMENTCAPACITY]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 15/05
-- Description:	Work Capacity ccst_EmploymentCapacity_icare
-- Version: 15/05 v7.3 initial build
--			17/05 checked against v8.0 no change
--			05/07/2019 checked against v12 no change
-- =============================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_employmentcapactiy_icare','Work Capacity - v12 04/07/2019'
-- drop table ccst_employmentcapacity_icare

SELECT 
	Case when PATINDEX('4*%',mc.Restriction_List)>0 or PATINDEX('%*4',mc.Restriction_List)>0  or PATINDEX('%*4*%',mc.Restriction_List)>0 
		or PATINDEX('7*%',mc.Restriction_List)>0 or PATINDEX('%*7',mc.Restriction_List)>0 or PATINDEX('%*7*%',mc.Restriction_List)>0 
		or PATINDEX('12*%',mc.Restriction_List)>0  or PATINDEX('%*12',mc.Restriction_List)>0 or PATINDEX('%*12*%',mc.Restriction_List)>0 then 'Limited ' + 
			Case when  PATINDEX('4*%',mc.Restriction_List)>0 or PATINDEX('%*4',mc.Restriction_List)>0  or PATINDEX('%*4*%',mc.Restriction_List)>0 then 
				Case when (PATINDEX('7*%',mc.Restriction_List)>0 or PATINDEX('%*7',mc.Restriction_List)>0  or PATINDEX('%*7*%',mc.Restriction_List)>0) or (PATINDEX('12*%',mc.Restriction_List)>0  or PATINDEX('%*12',mc.Restriction_List)>0 or PATINDEX('%*12*%',mc.Restriction_List)>0)
					then 'bending, '
					else 'bending ' 
					end
			ELSE '' end +  
			Case When PATINDEX('7*%',mc.Restriction_List)>0 or PATINDEX('%*7',mc.Restriction_List)>0  or PATINDEX('%*7*%',mc.Restriction_List)>0  then
				Case When PATINDEX('12*%',mc.Restriction_List)>0  or PATINDEX('%*12',mc.Restriction_List)>0 or PATINDEX('%*12*%',mc.Restriction_List)>0 then 'twisting, ' 
					else 'twisting ' 
					end
			ELSE '' end + 
			Case When PATINDEX('12*%',mc.Restriction_List)>0  or PATINDEX('%*12',mc.Restriction_List)>0 or PATINDEX('%*12*%',mc.Restriction_List)>0 then 'squatting ' ELSE '' end + 'capacity'
		End as BendingCapacity,
	'Restriction List: ' +	mc.Restriction_list as BendingCapacity_SRC_VALUE,
	'EMICS:'+ltrim(rtrim(cd.Claim_Number))+':WORKCOMP' as ClaimWC,
	mc.Diagnosis as Comment,
	mc.Examination_date as ConsultationDate,
	CASE WHEN NewEstimatedTime.new_EstimatedWorkDays is not null then convert(decimal(18,1),NewEstimatedTime.New_EstimatedWorkDays)
		else case when mc.Hours_per_week = 0 then 0.0 
		else null 
		end 
		--end as Estimated_work_days 
		End as DaysPerWeek,
	CASE WHEN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(MC.Diagnosis,'  ',' '),CHAR(13) + CHAR(10),''),CHAR(13),''))) = '' THEN NULL 
		WHEN LEN(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(MC.Diagnosis,'  ',' '),CHAR(13) + CHAR(10),''),CHAR(13),'')))) = 1 THEN NULL 
		ELSE LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(MC.Diagnosis,'  ',' '),CHAR(13) + CHAR(10),''),CHAR(13),''))) 
		END AS Diagnosis,
	NULL as DocumentLinkableID,
	Case when Restriction_List like '%20%' then 'Limited driving capacity'
		End as DrivingCapacity,
	mc.Date_To as EndDate,
	Case when Full_Part_Ind =1 or Full_Part_Ind =2 then 1 else 0
		End as EngagedInPaidEmployment,
	UR1.Answer as FactorsDelayingRecovery,
	CASE WHEN NewEstimatedTime.new_EstimatedWorkHoursPerDay is not null then ROUND(NewEstimatedTime.New_EstimatedWorkHoursPerDay,2)
		else case when mc.Hours_per_week = 0 then 0 
		else null end 
		end as HoursPerDAY,
	case when InitialCert.minID is not null then 1 else 0 end InitialCertificate,
	Case when UR2.Option_Answer = 'N' then 0
		When UR2.Option_Answer = 'Y' then 1
		End as InjuryConsistentWithCause,
	Case when Restriction_List like '%3%' then 'Limited lifting and carrying capacity'
		End as LiftingCapacity,
	cd.Claim_Number as LUWID,
	'EMICS:'+rtrim(ltrim(cd.Claim_Number))+'_'+cd.nom_treating_doctor+':VENDORS' MedicalProvider,
	CASE WHEN DD.DueDate < mc.date_to and dd.duedate > mc.date_from then dd.duedate else null end as NextReviewDate,
	CASE WHEN LTRIM(RTRIM(REPLACE(REPLACE(Restrictions,'  ',' '),CHAR(13) + CHAR(10),''))) = '' THEN NULL 
		WHEN LEN(LTRIM(RTRIM(REPLACE(REPLACE(MC.Restrictions,'  ',' '),CHAR(13) + CHAR(10),'')))) = 1 THEN NULL 
		ELSE LTRIM(RTRIM(REPLACE(REPLACE(Restrictions,'  ',' '),CHAR(13) + CHAR(10),''))) 
		END AS OtherCapacity,
	Case when UR1.Option_Answer = 'N' then 0 
		When UR1.Option_Answer = 'Y' then 1 
		end as PreExistingConditions,
	'EMICS:'+rtrim(ltrim(cd.Claim_Number))+'_'+rtrim(ltrim(mc.ID))+':EMPLOYMENTCAPACITY' as PublicID,
	Case when Restriction_List like '%5%' then 'Limited pushing and pulling capacity' 
		End as PushingPullingCapacity,
	RRP.ReferralToRehabProvider as ReferralToRehabProvider,
	Case when Restrictions IS NOT NULL then 1  else  0
		End as RequestForPositionDescription,
	'Restrictions: ' + convert(varchar, Restrictions) as RequestForPositionDescription_SRC_VALUE,
	NULL as SignatureProvided,
	Case when Restriction_List like '%8%' then 'Limited sitting capacity'
		End as SittingCapacity,
	Case when Restriction_List like '%9%' then 'Limited standing capacity'
		End as StandingCapacity,
	mc.Date_From as StartDate,
	MC.HOURS_PER_WEEK as TotalHoursPerWeek,
	UR3.Answer as TreatmentComments1,
	UR4.Answer as TreatmentComments2,
	UR5.Answer as TreatmentComments3,
	Case when UR3.Option_Answer = 'UNC' then 'uncertain'
		when UR3.Option_Answer = 'W0T2' then '0_2_weeks'
		when UR3.Option_Answer = 'W2T4' then '2_4_weeks'
		when UR3.Option_Answer = 'W4T6' then '4_6_weeks'
		when UR3.Option_Answer = 'W6P' then '6_plus_weeks'
		End as TreatmentDuration1,
	Case when UR4.Option_Answer = 'UNC' then 'uncertain'
		when UR4.Option_Answer = 'W0T2' then '0_2_weeks'
		when UR4.Option_Answer = 'W2T4' then '2_4_weeks'
		when UR4.Option_Answer = 'W4T6' then '4_6_weeks'
		when UR4.Option_Answer = 'W6P' then '6_plus_weeks'
		End as TreatmentDuration2,
	Case when UR5.Option_Answer = 'UNC' then 'uncertain'
		when UR5.Option_Answer = 'W0T2' then '0_2_weeks'
		when UR5.Option_Answer = 'W2T4' then '2_4_weeks'
		when UR5.Option_Answer = 'W4T6' then '4_6_weeks'
		when UR5.Option_Answer = 'W6P' then '6_plus_weeks'
		End as TreatmentDuration3
	
into ccst_employmentcapacity_icare

from DM_STG_EMICS.dbo.medical_cert MC join 
	DM_STG_EMICS.dbo.claim_detail CD on mc.claim_no = cd.claim_number
	--Inner join [DM_XFM].dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
	left outer join (select * from DM_STG_EMICS.dbo.user_response where question_id = '28') as UR1 ON MC.CLAIM_NO = UR1.REF_NO
	left outer join (select * from DM_STG_EMICS.dbo.user_response where question_id = '20') as UR2 ON MC.CLAIM_NO = UR2.REF_NO
	left outer join (select * from DM_STG_EMICS.dbo.user_response where question_id = '25') as UR3 ON MC.CLAIM_NO = UR3.REF_NO
	left outer join (select * from DM_STG_EMICS.dbo.user_response where question_id = '26') as UR4 ON MC.CLAIM_NO = UR4.REF_NO
	left outer join (select * from DM_STG_EMICS.dbo.user_response where question_id = '27') as UR5 ON MC.CLAIM_NO = UR5.REF_NO
	LEFT OUTER JOIN(  SELECT max(DUEDATE) as DUEDATE , CLAIMNUMBER FROM DM_STG_TASKMGR.DBO.TM_TASK_INCOMPLETE WHERE SUBJECT LIKE '%Review%' and reportid like 'ncmm%'  group by claimnumber) as DD  on MC.CLAIM_NO = DD.CLAIMNUMBER
	left outer join (SELECT distinct Claim_No,ReferralTorEHABpROVIDER = 1 from DM_STG_EMICS.dbo.Medical_Cert where Claim_No in
	( select distinct Claim_no from DM_STG_EMICS.dbo.Rehabilitation_detail)) as RRP on MC.Claim_no = RRP.Claim_no
	left outer join ( select claim_no,min(id) as minID from DM_STG_EMICS.dbo.medical_cert group by claim_no) as InitialCert on mc.Claim_no = InitialCert.Claim_no and mc.id = initialcert.minID
	LEFT OUTER JOIN
	(
	SELECT 
	 cd.Claim_Number
	 ,mc.ID
	 ,CD.WORK_DAYS
	 ,(LEN(CD.WORK_DAYS) - LEN(REPLACE(CD.WORK_DAYS,'1',''))) AS CURRENT_WORKDAYS
	 ,mc.HOURS_PER_WEEK AS EstimatedHoursPerWeek
	 ,mc.Hours_Per_Week / Replace((len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) as current_estimatedWorkHoursPerDay
	 ,CASE 
	  WHEN mc.Hours_Per_Week BETWEEN 35 AND 40 AND (LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))) < 5 THEN 5

	  WHEN mc.Hours_Per_Week / REPLACE((LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))),0,1) > 12 
	   THEN CASE WHEN CEILING(mc.Hours_Per_Week / 12) > 7 THEN 7 ELSE CEILING(mc.Hours_Per_Week / 12) END

	  ELSE REPLACE((LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))),0,1)
	 END AS New_EstimatedWorkDays

	 ,CASE 
	  WHEN mc.Hours_Per_Week BETWEEN 35 AND 40 AND (LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))) < 5 THEN mc.Hours_Per_Week / 5

	  WHEN mc.Hours_Per_Week / REPLACE((LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))),0,1) > 12 
	   THEN CASE WHEN CEILING(mc.Hours_Per_Week / 12) > 7 THEN mc.Hours_Per_Week / 7 ELSE mc.Hours_Per_Week /  CEILING(mc.Hours_Per_Week  / 12) END

	  ELSE mc.Hours_Per_Week / REPLACE((LEN(cd.Work_Days) - LEN(REPLACE(cd.Work_Days,'1',''))),0,1)
	 END AS New_EstimatedWorkHoursPerDay

	FROM DM_STG_EMICS.DBO.MEDICAL_CERT MC left outer join DM_STG_EMICS.dbo.CLAIM_DETAIL cd ON MC.CLAIM_NO = CD.Claim_Number
	WHERE mc.Hours_Per_Week > 0
	) as NewEstimatedTime on MC.Claim_No = NewEstimatedTime.Claim_Number and MC.ID = NewEstimatedTime.ID
	WHERE CD.IS_NULL = 0
	ORDER BY CD.CLAIM_NUMBER, MC.ID
END