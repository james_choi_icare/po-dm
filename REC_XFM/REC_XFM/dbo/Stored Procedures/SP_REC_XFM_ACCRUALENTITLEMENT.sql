﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_ACCRUALENTITLEMENT]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 09/05
-- Description: BENEFITS domain ccst_accrualentitlement
-- Version: 
-- =============================================

	--Select 
	--	pr.claim_no as LUWID,
	--	'EMICS:' + RTRIM(pr.claim_no) + '_' + convert(varchar, ID) + '_' +  --day number out of total days?
	--	 ':ACCRUALENTITLEMENT' PublicID, 

	--	'EMICS:' + RTRIM(pr.Claim_no) + ':BENEFITSACCRUAL' as BenefitsAccrual_icareID, 

	--	--Trans_amount/ DATEDIFF(day,Period_Start_Date,Period_End_Date)  as DayAmount, --exclude if divided by 0
	
	--	DATEDIFF(day,Period_Start_Date,Period_End_Date) as datedifference,
	--	Period_Start_Date,
	--	Period_End_Date,
	--	convert(date, (Period_End_Date - Period_Start_Date)) as DayPaid,
	--	Case when (Period_End_Date-Period_Start_Date) <14 then 'FirstEntitlement'
	--		When (Period_End_Date-Period_Start_Date) >14 and 
	--		(Period_End_Date-Period_Start_Date) < 131 then 'SecondEntitlement'
	--		End as EntitlementType

	--into ccst_accrualentitlement_icare

	--From DM_STG_EMICS.dbo.Payment_REcovery pr
	--Where estimate_type = 50 and trans_amount <> 0
	--and DATEDIFF(day,Period_Start_Date,Period_End_Date) = 0
	--order by Claim_no, payment_No

	--drop table ccst_accrualentitlement_icare

	IF OBJECT_ID('tempdb..#temp_DateTable') is not null
		drop table #temp_DateTable
	IF OBJECT_ID('tempdb..#temp_DailyPayments') is not null
		drop table #temp_DailyPayments

	DECLARE @sDate datetime, @eDate datetime;

	SELECT @sDate = min(period_start_date), @eDate = max(period_end_date)
	FROM DM_STG_EMICS.dbo.Payment_Recovery  x
	WHERE estimate_type = 50 and trans_amount <> 0  and Reversed <> 1

	SELECT top (datediff(dd, @sDate, @eDate)) ROW_NUMBER() over(order by a.name) as SiNo, 
		Dateadd(dd, ROW_NUMBER() over(order by a.name) , @sDate) as Dt into #temp_DateTable
	FROM sys.all_objects a

	;WITH PayRec AS
	(
		Select x.claim_no, period_start_date, period_end_date, trans_amount, transaction_date, 
			datediff(DAY, period_start_date, period_end_date) + 1 dateDif
		From DM_STG_EMICS.dbo.Payment_Recovery  x
			inner join  (select Claim_no, min(period_start_date) First_Date
							FROM DM_STG_EMICS.dbo.Payment_Recovery 
							Where  estimate_type = 50 and trans_amount <> 0  and Reversed <> 1
							GROUP BY Claim_No) y ON x.Claim_No = y.Claim_No
		Where  estimate_type = 50 and trans_amount <> 0  and Reversed <> 1 --and x.Claim_No in ( '1179134')
	)
	SELECT Claim_No, DayPaid, sum(Trans_Amount) Trans_Amount INTO #temp_DailyPayments
	FROM (select Claim_No, dt DayPaid, cast(cast(Trans_Amount as decimal(12,7))/cast(dateDif as decimal(12,7)) as decimal(12,7)) Trans_Amount
		from PayRec a
			inner join #temp_DateTable b ON b.Dt between a.Period_Start_Date and a.Period_End_Date
	) x
	GROUP BY Claim_No, DayPaid

	;With CTEx as (
		select claim_no,min_period_start_date,max_period_end_date, row_number() OVER (partition by claim_no order by countweeks) CountWeeks
		FROM (SELECT claim_no, CountWeeks, min(period_start_date) min_period_start_date, max(period_end_date) max_period_end_date
				FROM (Select x.claim_no, period_start_date, period_end_date, trans_amount, transaction_date, 
							datediff(week, First_Date, period_start_date) CountWeeks
						From DM_STG_EMICS.dbo.Payment_Recovery  x
							inner join  (select Claim_no, min(period_start_date) First_Date
											FROM DM_STG_EMICS.dbo.Payment_Recovery 
											Where  estimate_type = 50 and trans_amount <> 0  and Reversed <> 1 AND Period_Start_Date is not null  
											GROUP BY Claim_No) y ON x.Claim_No = y.Claim_No
						Where  estimate_type = 50 and trans_amount <> 0  							
								and Reversed <> 1) xx
				group by Claim_No, CountWeeks) a
	),FirstEntitlement as (
		Select claim_no, count(distinct countweeks) as FirstEntitlementWeeks, min(min_period_start_date) min_period_start_date, max(max_period_end_date) max_period_end_date
		From CTEx
		where countweeks between 1 and 13
		Group by claim_no
	),SecondEntitlement as
	(
		Select claim_no, count(DISTINCT countweeks)  as SecondEntitlementWeeks, min(min_period_start_date) min_period_start_date, max(max_period_end_date) max_period_end_date
		From CTEx
		where CountWeeks between 14 and 130
		Group by claim_no
	)
	select a.Claim_No as LUWID, 
			'EMICS:' + RTRIM(a.Claim_no) + ':BENEFITSACCRUAL' as BenefitsAccrual_icareID, 
			'EMICS:' + RTRIM(a.claim_no) + '_' + cast(datepart(YYYY,DayPaid) as varchar) 
						+ '_' + right('00' + cast(datepart(MM,DayPaid) as varchar),2)
						+ '_' + right('00' + cast(datepart(DD,DayPaid) as varchar),2) +  ':ACCRUALENTITLEMENT' PublicID, 
			DayPaid, 
			a.Trans_Amount DayAmount,	
		CASE WHEN a.DayPaid BETWEEN b.min_period_start_date AND b.max_period_end_date THEN 'FirstEntitlement'
			 WHEN a.DayPaid BETWEEN c.min_period_start_date AND c.max_period_end_date THEN 'SecondEntitlement'
			ELSE NULL END EntitlementType into ccst_accrualentitlement_icare
	from #temp_DailyPayments a 
		left join FirstEntitlement b ON a.Claim_No = b.Claim_No
		left join SecondEntitlement c ON a.Claim_No = c.Claim_No

	DROP TABLE #temp_DateTable
	DROP TABLE #temp_DailyPayments

END