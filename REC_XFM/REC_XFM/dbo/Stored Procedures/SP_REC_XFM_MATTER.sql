﻿
-- =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Legal domain - ccst_matter
-- Version: Initial build v32 18/06/2019
--			19/06/2019 v34 updated rules to Name after clarification
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_MATTER]
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_matter','Legal - v34'
-- drop table ccst_matter
Select 
	Case when l.Hearing_Outcome = 'AA' or l.Hearing_Type_code = 'AR' then 1 else 0 end as Arbitration,
	'Hearing_outcome = ' + ISNULL(convert(Varchar, Hearing_outcome),'') as Arbitration_SRC_VALUE, 
	Case when Hearing_outcome = 'AA' or Hearing_type_code = 'AR' then Date_Hearing else NULL end as ArbitrationDate,
	NULL as ArbitrationRoom,
	
	Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user2.PublicID, cc_user_sm.PublicID) 
		End as AssignedByUserID, --v1.2
	case when cc_user.publicid is null then cc_group2.publicID
		else cc_group.publicid end as AssignedGroupID, --v1.2	NULL as AssignedQueueID, --v1.2
	Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID,  cc_user2.PublicID, cc_user_sm.PublicID) 
		End as AssignedUserID, --v1.2
	
	
	l.Create_date as AssignmentDate,
	Case when l.Owner is not null then 'assigned' else 'unassigned' End as AssignmentStatus,
	NULL as CaseNumber,
	'EMICS:' + rtrim(Cd.claim_number) as ClaimID,
	Case when l.Type = 5 then Status_Date else NULL end as CloseDate,
	Case when l.Type = 1 or l.Common_Law_Action_Date is not null then 'EMICS:' + RTRIM(l.Claim_no) + '_' + convert(varchar, l.ID) + ':Litigation' End as CommonLawID,
	NULL as DeclaratoryJgmt,
	NULL as DefenseApptDate,
	NULL as DocketNumber,
	Case when l.Hearing_Type_Code = 'AR' then 'arbitration_icare'
		when l.Hearing_Type_Code in ('AP','DP','DR','CA') then 'hearing_icare'
		when l.Hearing_Type_Code = 'MD' then 'mediation_icare'
		when l.Hearing_Type_Code = 'TC' then 'teleconference_icare'
		when (l.Hearing_Type_Code is NULL or l.Hearing_Type_Code = '') then NULL
		End as EventType_icare,
	a.FinalLegalCost as FinalLegalCost,
	NULL as FinalSettleCost,
	l.Date_Settlement as FinalSettleDate,
	l.Date_Hearing as HearingDate,
	cm.data as Keyissues_icare,
	NULL as LegalSpecialty,
	NULL as LitigationStrategy_icare,
	cd.claim_number as LUWID,
	Case when l.Type in (1,2,5) then 
		Case when l.Common_Law_Action_Date is not null then 'commonlaw_icare'
			Else 'statutoryworkerscompensation_icare' End
		Else 'other_icare'
		End as MatterType,
	Case when l.Type in (1,2,5) Then
		Case when l.Common_Law_Action_Date is not NULL then 'EML: Common Law - '+ CAST(l.ID as varchar(10))
			Else 'EML: Compensation - ' + CAST(l.ID as varchar(10))
			End
		Else 'EML: Other - ' + CAST(l.ID as varchar(10))
		End as Name,
	Case when l.Type not in (1,2,5) then r.Description
		End as OtherMatterSubTypeTest_icare,
	'EMICS:' + l.Claim_no + '_' + convert(varchar,l.ID) + ':Litigation' as PublicID,
	CAST(l.Create_date as date) as RaisedDate_icare,
	Case when l.Type = 5 OR (l.Type = 1 and l.Common_Law_Action_Outcome IS NOT NULL) Then 
		Case When l.Type = 1 AND l.Common_Law_Action_Outcome is not null then 
			Case when l.Common_Law_Action_Outcome = '1' then '01'
				when l.Common_Law_Action_Outcome = '2' then '02'
				when l.Common_Law_Action_Outcome = '3' then '03'
				when l.Common_Law_Action_Outcome = '4' then '04'
				when l.Common_Law_Action_Outcome = '5' then '05'
				when l.Common_Law_Action_Outcome = '6' then '06'
				when l.Common_Law_Action_Outcome = '7' then '07'
				when l.Common_Law_Action_Outcome = '8' then '08'
			END
		When l.type = 5 then 
			Case when l.Hearing_Outcome = 'AA' then NULL
				when l.Hearing_Outcome = 'AE' then 'awardfortheemployer_icare'
				when l.Hearing_Outcome = 'AH' then NULL
				when l.Hearing_Outcome = 'AJ' then NULL
				when l.Hearing_Outcome = 'AW' then 'awardfortheworker_icare'
				when l.Hearing_Outcome = 'CH' then NULL
				when l.Hearing_Outcome = 'CO' then NULL
				when l.Hearing_Outcome = 'DA' then 'discontinued_icare'
				when l.Hearing_Outcome = 'OE' then 'IA_icare'
				when l.Hearing_Outcome = 'RA' then NULL
				when l.Hearing_Outcome = 'SC' then NULL
				when l.Hearing_Outcome = 'SD' then 'settled_icare'
				when l.Hearing_Outcome = 'SO' then 'dismissed_icare'
				when l.Hearing_Outcome = 'TA' then NULL
				when l.Hearing_Outcome is null or l.Hearing_Outcome = '' then 'other_icare'
			END
		Else NULL
		END
		End as Resolution,
	Case when sl.Claim_no is not null then 'yes_icare' else 'no_icare' 
		End as SignificantLitigation_icare,
	Case when l.Hearing_Type_code = 'TC' then l.Date_Hearing
		Else NULL End as TeleconferenceDate_icare,
	'loadsave' as ValidationLevel,
	Case when l.Type_Of_Legal_Action in ('WCP','WCI','LDI') then 1 else 0
		End as WPI_icare

into ccst_matter 

From DM_STG_EMICS.dbo.Litigation l
Inner Join DM_STG_EMICS.dbo.Claim_detail cd on l.claim_no = cd.claim_number
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 


Left outer join DM_STG_EMICS.dbo.Claim_activity_detail ca on l.Claim_no = ca.claim_no
Left outer join DM_STG_EMICS.dbo.CLAIMS_OFFICERS co on co.Alias = l.Owner --litigation Owner

Left outer join DM_STG_EMICS.dbo.CLAIMS_OFFICERS co2 on co2.Alias = ca.claims_officer--co.Alias owner
LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - 
		CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
		ON (co2.Alias = cntrl2.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID)

Left outer join (Select id, data = STUFF((Select ' ' + data From DM_STG_EMICS.dbo.CLM_MEMO cm Where cm.id = cm2.id For XML PATH('')),1,1,'')
  From DM_STG_EMICS.dbo.CLM_MEMO cm2
  group by ID) cm on l.Text_ptr = cm.Id 
Left outer join DM_STG_EMICS.dbo.Ref_Look_Up r on l.Type_Of_Legal_Action = r.Code and r.Name = 'TypeOfLegalAction'
Left outer join (Select LT.claim_no, sum(isnull(trans_amount,0.0) - ISNULL(ITC,0.0) - ISNULL(DAM,0.0)) as FinalLegalCost 
	from DM_STG_EMICS.dbo.Litigation LT 
Left outer join DM_STG_EMICS.dbo.PAYMENT_RECOVERY pr on lt.claim_no = pr.claim_no and pr.estimate_Type = '61' Group by LT.Claim_no) a on a.claim_no = cd.claim_number
Left outer join DM_STG_EMICS.dbo.Significant_Legal_issue sl on l.claim_no = sl.claim_no

LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)
left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups on (lkp_eml_team_gwcc_groups.EML_TEAM = co.grp) -- litigation owner group
left join DM_STG_CC.[dbo].[cc_group] cc_group on ( cc_group.Name = lkp_eml_team_gwcc_groups.gwcc_group )

left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups2 on (lkp_eml_team_gwcc_groups2.EML_TEAM = co2.grp) -- co.Alias owner group
left join DM_STG_CC.[dbo].[cc_group] cc_group2 on ( cc_group2.Name = lkp_eml_team_gwcc_groups2.gwcc_group )


left join DM_XFM.[dbo].[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on ( cntrl.EML_Email = lkp_eml_gwcc_queues.EMLEmailAddress 
			 and ca.Claims_Officer = lkp_eml_gwcc_queues.EMLQueueUser)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_user_queue on (lkp_eml_gwcc_queues.GWCC_Claim_User = cc_credential_user_queue.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_queue on (cc_user_queue.CredentialID = cc_credential_user_queue.ID)

Where l.ID <> 101 --255
and cd.is_null = 0

END