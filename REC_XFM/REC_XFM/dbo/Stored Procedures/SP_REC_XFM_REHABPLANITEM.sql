﻿/*- =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description:	Claim Management Domain - ccst_rehabplanitem_ext
-- Version: v24 initial build
-- =============================================*/
CREATE PROCEDURE [dbo].[SP_REC_XFM_REHABPLANITEM]
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_rehabplanitem_ext','Claim Management - v24 12/06/2019'


Select
	Case when rtwp.Date_achieved is null then 'inprogress' else 'complete' 
		End as ActionStatus_icare,
	Case when rtwp.Date_achieved is null then 0 else 1 
		End as CompletedFlag,
	rtwd.Duties as Description,
	'low' as Difficulty,
	Case when rtwp.Date_achieved is null then 'inprogress' else 'complete'
		End as Goal_icare,
	'low' as Importance,
	0 as IncludeOnImp_icare,
	rtwp.Claim_no as LUWID,
	'EMICS:' + RTRIM(cd.claim_number) + ':INJURED' as OwnerContactID,
	'EMICS:' + RTRIM(rtwp.claim_no) + '_' + convert(varchar, rtwp.rtwplan_no) + ':PLANDURATION' as PublicID,
	0 as ReferralRequired_icare,
	'EMICS:' + RTRIM(rtwp.claim_no) + '_' + convert(varchar, rtwp.rtwplan_no) + ':RTWPLAN' as RehabPlanID,
	0 as Relevant_icare,
	'rtw_icare' as StrategyGoalType,
	'RehabPlanGoal_Ext' as Subtype,
	rtwd.Duration_To as TargetDate

into ccst_rehabplanitem_ext

From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration rtwd --691
Inner join DM_STG_EMICS.dbo.Claim_RTW_Plan rtwp on rtwd.claim_no = rtwp.claim_no and rtwd.RTWplan_no = rtwp.rtwplan_no
Inner join (Select claim_no, RTWPlan_no, max(duration_no) as DUration_no From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration as ClaimRTW_Plan_Duration_1
		Group by Claim_no, RTWPlan_no) as Max_Duration on rtwd.Claim_No = max_duration.claim_no
		And rtwd.RTWplan_no = Max_Duration.RTWplan_no
		And rtwd.Duration_no = Max_Duration.DUration_no
Inner join DM_STG_EMICS.dbo.claim_detail cd on rtwp.claim_no = cd.claim_number
--Inner Join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.claim_number
Where cd.is_Null = 0

END