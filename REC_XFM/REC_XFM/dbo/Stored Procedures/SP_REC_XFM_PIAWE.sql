﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_PIAWE]
AS
BEGIN
	/*-- =================================
	-- Author: Ann
	-- Modified date: 09/05
	-- Domain: BENEFITS ccst_piawe_icare
	-- Comments: Initial build v39 
	--			 Updated v44 mapping 7/05: updated PublicID;
	--			Updated v45 09/05 .. looks up dm_xfm_benefitsaccrual_icare
	--			Updated v58.1 new SC and xforms 25/06/2019
	--			reformatted sc and updated publicID 08/07/2019
	--			03/09/2019 updated piawe_audit to include verified date
	-- =================================*/
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_piawe_icare','Benefits - v73 3/09/2019'
--drop table ccst_piawe_icare



;With PIAWE_mstr as (   ----PIAWE 'WITH' TABLE
	 select distinct cd.id
	  ,cd.claim_no
	  ,cd.weekly_wage
	  ,cd.SHIFT_ALLOWANCES
	  ,cd.OT_Allowances
	  ,cd.PIAWE_VERIFIED_DATE
	  ,cd.PIAWE
	  ,cd.create_date
	  ,c.Date_of_Injury
	  ,cd.ispiaweedited 
	from DM_STG_EMICS.[dbo].[cd_audit] cd 
	 inner join  DM_STG_EMICS.dbo.CLAIM_DETAIL c
	 on c.claim_number=cd.claim_no
	  and c.is_null=0
	 where cd.PIAWE<>0 
	  and (cd.PIAWE_VERIFIED_DATE is not null 
	   or (cd.PIAWE_VERIFIED_DATE is null 
		and  exists (select is_exempt from  DM_STG_EMICS.dbo.amendment_exemptions ae where ae.claim_no = cd.Claim_No and is_exempt =  1)))
	)
,pr as (SELECT * FROM DM_STG_EMICS.dbo.payment_recovery WHERE  Reversed<>1 and estimate_type in (50,54,58,59,60,65))
,PIAWE_Paid as ( select m.claim_no,m.create_date ,max(m.id) as max_paid_PIAWE_id from PIAWE_mstr m
   inner join pr pr on m.claim_no=pr.Claim_No 
   and m.create_date<=pr.Transaction_date
   group by m.claim_no,m.create_date)
,PIAWE_unpaid as (select mp.claim_no,mp.create_date,(mp.id) as max_unpaid_PIAWE_id from PIAWE_mstr mp
   left outer join  pr pr
   on mp.claim_no=pr.Claim_No 
    and mp.create_date<=pr.Transaction_date 
   WHERE  pr.Claim_No is null )
,PIAWE_UNION as (SELECT distinct m.* FROM PIAWE_mstr M
  inner JOIN PIAWE_PAid PP
  ON PP.claim_no=M.claim_no
   and m.id=pp.max_paid_PIAWE_id
  
  UNION 
  
  SELECT distinct MU.* FROM PIAWE_mstr MU
  INNER JOIN PIAWE_unpaid PU
  ON PU.claim_no=MU.claim_no
   and mu.id=pu.max_unpaid_PIAWE_id
	)
,PIAWE_HIST as (  --remove duplicates
	select * from (
	  select *
	  ,rank() over (partition by (claim_no), cast(create_date as date) order by create_date desc) rn
	 from PIAWE_UNION
	  )a
	  where rn =1) 

,PIAWE_AUDIT as (
	select  --PIAWE_AUDIT
	Claim_Number
	,PIAWE_START_DATE
	,OE
	,OTA
	,SA
	,EMICS_PIAWE
	,ltrim(rtrim(Claim_Number)) as CD_LUWID
	,cast(row_number() over (partition by Claim_Number order by PIAWE_START_DATE asc, PIAWE_VERIFIED_DATE) as int) as RN_PublicID
	,ispiaweedited
	,Date_of_Injury
	from (
	SELECT DISTINCT
	 ltrim(rtrim(mer.Claim_No)) as Claim_Number
	,cd.Date_of_Injury
	,mer.PIAWE_START_DATE
	  ,mer.ispiaweedited
	  ,isnull(mer.Weekly_Wage,0) as OE
	,isnull(mer.OT_Allowances,0) as OTA
	,isnull(mer.SHIFT_ALLOWANCES,0) as SA
	,mer.piawe as EMICS_PIAWE
	,mer.PIAWE_VERIFIED_DATE
	FROM (
	select claim_no,create_date as PIAWE_START_DATE,PIAWE,weekly_wage,ispiaweedited,
	SHIFT_ALLOWANCES,OT_Allowances, PIAWE_VERIFIED_DATE from  PIAWE_HIST /*use SC for PIAWE_HIST selection criteria in mapping sheet*/
	 ) mer
	  LEFT JOIN DM_STG_EMICS.dbo.Payment_Recovery PR ON mer.claim_no = PR.Claim_no
	  LEFT JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON mer.claim_no = CAD.Claim_no
	  LEFT JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd on cd.Claim_Number = mer.claim_no
	WHERE
	pr.reversed = 0
	AND CAD.Claim_Liability_Indicator <> 12 
	 AND (PR.Estimate_type IN (50,72,54,58,59,60,65) OR CD.is_Time_Lost = 1 OR cd.Date_Deceased IS NOT NULL)
	GROUP BY mer.Claim_No
	,cd.Date_of_Injury
	,mer.PIAWE_START_DATE
	,mer.ispiaweedited
	,mer.Weekly_Wage
	,mer.OT_Allowances
	,mer.SHIFT_ALLOWANCES
	,mer.PIAWE
	,mer.PIAWE_VERIFIED_DATE
	) dat )

--drop table ccst_piawe_icare
Select  distinct
	pa.Claim_Number as LUWID,

	-- JCFIX ----------------
	Case when pa.ispiaweedited = 1 then 'EMICS:' + RTRIM(pa.Claim_Number) + '_' + convert(varchar, RN_PublicID) +
		':indexed_PIAWE' 
		Else 'EMICS:' + RTRIM(pa.Claim_Number) + '_' +  
		convert(varchar, RN_PublicID) + ':Migrated_PIAWE' 
		End as PublicID,
	-- JCFIX ----------------
	--Case when pa.ispiaweedited = 1 then 'EMICS:' + RTRIM(cd.Claim_Number) + '_' +  CONVERT(varchar, ROW_NUMBER() OVER (ORDER BY CD.claim_number, pa.PIAWE_START_DATE asc)) +
	--	':indexed_PIAWE' 
	--	Else 'EMICS:' + RTRIM(cd.Claim_Number) + '_' +  CONVERT(varchar, ROW_NUMBER() OVER (ORDER BY CD.claim_number, pa.PIAWE_START_DATE asc)) + ':Migrated_PIAWE' 
	--	End as PublicID,
	'EMICS:' + RTRIM(pa.Claim_number) + ':EMPLOYMENT_DATA' as EmploymentDataID,
	'EMICS:' + RTRIM(pa.Claim_Number) + ':INDEMNITY' as ExposureID,
	'Detailed information held in notes' as Comments,
	NULL as DeflatedPIAWEPost52,
	NULL as DeflatedPIAWEPre52,
	'FALSE' as Draft,
	convert(datetime2,pa.PIAWE_START_DATE) as EffectiveDate_icare,
	NULL as HoursWorked,
	NULL as LeaveWageRecordsRecd,
	pa.OE as OrdinaryEarnings_icare,
	ISNULL(pa.SA,0) + isnull(pa.OTA,0) as OvertimeAllowance_icare,
	'Please see Claim File Notes for more information' as PIAWEComment,
	0 as PIAWEDeactivated_icare,
	Case when pa.ispiaweedited = 1 then pa.EMICS_PIAWE
		Else  ISNULL(pa.OE,0) + ISNULL(pa.SA,0) + ISNULL(pa.OTA,0) End as PIAWEFirst52_icare,
	Case when pa.ispiaweedited = 1 then pa.EMICS_PIAWE
		When pa.Date_of_Injury < 26/10/2018 then pa.OE
		when pa.Date_of_Injury >=36/10/2018 then isnull(pa.OE,0) + isnull(pa.SA, 0) + isnull(pa.OTA,0) else null
		end as PIAWELatest52_icare,
	'Please see Claim File Notes for more information' as PIAWEReason_icare,
	Case when pa.ispiaweedited = 1 then 'indexed_icare' else 'migrated_icare' end as PIAWEType_icare,
	52 as RelevantPeriod,
	NULL as RPEndDate,
	NULL as RPStartDate,
	0 as Schedule3Applicable,
	isnull(pa.OE,0) as TotalOEWeekOverRP,
	ISNULL(pa.SA,0) + ISNULL(OTA,0) as TotalShiftOTAllowanceWeekRP --pa.OTA + pa.SA as TotalShiftOTAllowanceWeekRP --cd.SHIFT_ALLOWANCES + cd.OT_Allowances 

	into ccst_piawe_icare

From PIAWE_AUDIT pa

 
END