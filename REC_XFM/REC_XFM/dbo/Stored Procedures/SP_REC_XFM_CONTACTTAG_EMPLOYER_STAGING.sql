﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 29/05/2019
-- Description:	Contacts - Named Insured V003.7
-- 1. This is the child Stored Proc for EMPLOYER contacttag that will called by the master stored proc SP_REC_XFM_CONTACTTAG
-- 2. <10-Jul-2019><Satish>: Added INSCOPE table to the joins
-- 3. <17-Jul-2019><Satish>: Commented out INSCOPE table
-- 4. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 5. <23-Jul-2019><RM>: Updated the code as per mapping v004.01 - updated reference from CM_C to CM_CT for the joins.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CONTACTTAG_EMPLOYER_STAGING]
	
AS
BEGIN

IF OBJECT_ID('ContacttagREF_temptable_employer_verifiedPC') is not null
drop table dbo.ContacttagREF_temptable_employer_verifiedPC;

IF OBJECT_ID('ContacttagREF_temptable_employer_verifiedCDR') is not null
drop table dbo.ContacttagREF_temptable_employer_verifiedCDR;

IF OBJECT_ID('temptable_Contacttag_EMPLOYER_VerifiedPC') is not null
drop table temptable_Contacttag_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_Contacttag_EMPLOYER_VerifiedCDR') is not null
drop table temptable_Contacttag_EMPLOYER_VerifiedCDR;


SELECT * INTO temptable_Contacttag_EMPLOYER_VerifiedPC
FROM
(
select 
--distinct
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
--CM_C.PublicID as ContactID,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
--c.CRM_ID as LinkID,
CM_CT.LinkId as AddressBookUID, -- 5. <23-Jul-2019><RM>: Updated the code as per mapping v004.01 - updated reference from CM_C to CM_CT for the joins.
NULL as ExternalLinkID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':CLAIMPARTY' as PublicID,
'claimparty' as Type
--CM_CT.Type as Type_CM
from 
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
  LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
   left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
   left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID --08/02/2019: Added to get ACN and ABN
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
--INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
--   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontacttag] CM_CT on CM_CT.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS' -- 5. <23-Jul-2019><RM>: Updated the code as per mapping v004.01 - updated reference from CM_C to CM_CT for the joins.
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] CM_CT on CM_CT.ABContactId = CM_C.PublicID
where CD.is_Null = 0
--and CD.Claim_Number=1333632
UNION
select 
--distinct
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
--CM_C.PublicID as ContactID,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
--c.CRM_ID as LinkID,
CM_CT.LinkId as AddressBookUID,
NULL as ExternalLinkID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':CLIENT' as PublicID,
'client' as Type
--CM_CT.Type as Type_CM
from 
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
  LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
   left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
   left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID --08/02/2019: Added to get ACN and ABN
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
--INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
--   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontacttag] CM_CT on CM_CT.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS' -- 5. <23-Jul-2019><RM>: Updated the code as per mapping v004.01 - updated reference from CM_C to CM_CT for the joins.
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] CM_CT on CM_CT.ABContactId = CM_C.PublicID
where CD.is_Null = 0
--and CD.Claim_Number=1333632
)X

SELECT * INTO temptable_Contacttag_EMPLOYER_VerifiedCDR
FROM
(
select 
--distinct
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
--CM_C.PublicID as ContactID,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
--c.CRM_ID as LinkID,
CAST (NULL as varchar) as addressbookUID,
CAST (NULL as varchar) as ExternalLinkID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':CLAIMPARTY' as PublicID,
'claimparty' as Type
--CM_CT.Type as Type_CM
from 
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
left outer join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
  LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
   left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
   left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID --08/02/2019: Added to get ACN and ABN
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
--INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
--   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontacttag] CM_CT on CM_CT.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS' -- 5. <23-Jul-2019><RM>: Updated the code as per mapping v004.01 - updated reference from CM_C to CM_CT for the joins.
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] CM_CT on CM_CT.ABContactId = CM_C.PublicID
where CD.is_Null = 0
and cons_pp.LUWID IS NULL -- i.e not Verified PC
--and CD.Claim_Number=1333632
UNION
select 
--distinct
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
--CM_C.PublicID as ContactID,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
--c.CRM_ID as LinkID,
CAST (NULL as varchar) as addressbookUID,
CAST (NULL as varchar) as ExternalLinkID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':CLIENT' as PublicID,
'client' as Type
--CM_CT.Type as Type_CM
from 
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
left outer join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
where CD.is_Null = 0
and cons_pp.LUWID IS NULL -- i.e not Verified PC
--and CD.Claim_Number=1333632
)Y
--,

--Use [REC_XFM]
--Go


;

select
*
into dbo.ContacttagREF_temptable_employer_verifiedPC
from
(
select * 
from temptable_Contacttag_EMPLOYER_VerifiedPC
)x
;

select
*
into dbo.ContacttagREF_temptable_employer_verifiedCDR
from
(
select * 
from temptable_Contacttag_EMPLOYER_VerifiedCDR
)x
;

IF OBJECT_ID('temptable_Contacttag_EMPLOYER_VerifiedPC') is not null
drop table temptable_Contacttag_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_Contacttag_EMPLOYER_VerifiedCDR') is not null
drop table temptable_Contacttag_EMPLOYER_VerifiedCDR;

END