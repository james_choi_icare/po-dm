﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_GUARDIANEDGETABLE]
AS
BEGIN
	/*========================================
		Author: Ann
		Domain: PAYMENTS AND RECOVERIES DOMAIN - ccst_guardianedgetable
		Comments:  v42 
				 v45.4 underscore fix for PublicID 10/05 ag
				 14/05 checked v45.6 - no changes
				 11/07/2019 checked 11/07 no changes
				 03/09/2019 updated SC to add CPC v47.2 
	========================================*/
	--drop table ccst_guardianedgetable
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_guardianedgetable','Payments and Recoveries - v47.2 03/09/2019'

SELECT distinct 
	CASE WHEN CPR.PAYEE_TYPE =1 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'
		WHEN CPR.PAYEE_TYPE in (3,4) THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,CPR.Payee_Code)+':VENDOR'
		WHEN PAYEE_TYPE=2 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INSURED'
		WHEN PAYEE_TYPE=23 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,DP.ID) + ':DEPENDENT'
		ELSE NULL
		End AS ForeignEntityID,

	RTRIM(CD.CLAIM_NUMBER) AS LUWID,
	CASE WHEN CHEQUE_NO IS  NULL THEN 'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+ISNULL(convert(varchar,cpc.id),'00') + ':CHECK'
		ELSE
		'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+ISNULL(convert(varchar,cpc.ID),'00') + ':CHECK' 
		END as OwnerID,
	'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,cpr.Payee_type)+'_'+ISNULL(convert(varchar,cpc.ID),'00')+':CHKCNTCT' as PublicId
 
Into ccst_guardianedgetable

FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
INNER JOIN (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no, Invoice_Date,period_start_date,period_end_date,Estimate_type ) as pr_id,* 
	from DM_STG_EMICS.dbo.Payment_Recovery) PR ON PR.Claim_No=CPR.Claim_number
	AND PR.Payment_no=cpr.Payment_no
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON CD.Claim_Number=CPR.Claim_number
LEFT OUTER JOIN DM_STG_EMICS.dbo.DEPENDENTS DP ON CPR.Payee_Code=DP.Creditor_no
LEFT OUTER JOIN (SELECT B.paycode, c.typecode as paymenttype_icare, c.ID from DM_STG_CC.dbo.ccx_paycodepaymenttype_icare A,
	(Select * from DM_STG_CC.dbo.ccx_paycode_icare where Retired =0) B, DM_STG_CC.dbo.cctl_paymenttype_icare C
	Where a.paycode= b.ID and a.paymenttype= c.ID) CPC
	on CPC.paycode = (case when pr.payment_type like 'OR%' then PR.WC_Payment_Type else PR.payment_type end)
	--INNER JOIN [DM_XFM].dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number

WHERE CPR.Payee_Code is not null
--#DMIG- 3815
and PR.Estimate_type<70 and PR.Reversed<>1 
and isnull(CPR.Cheque_status,'00') not in (3,6)


END