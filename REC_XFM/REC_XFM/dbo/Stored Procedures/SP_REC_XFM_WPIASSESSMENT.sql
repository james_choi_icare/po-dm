﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_WPIASSESSMENT]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 17/05
-- Description: Work Capacity drop table ccst_WPIAssessment_icare
-- Version: 17/05 initial build v8.0 mapping spec
--			24/06/2019 v9.2 updated SC and rules
--			09/07/2019 v12.1 no changes
--			19/07/2019 updated to latest sc and rules
-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_WPIassessment_icare','Work Capacity - v15 19/07/2019'

SELECT
	cd.Claim_Number AS LUWID
	,CASE	
		WHEN cad.WPI > 30 THEN '07'			-- Greater Than 30%
		WHEN cad.WPI BETWEEN 21 AND 30 THEN '06'	-- Greater Than 20% & Less Than or Equal to 30%
		WHEN cad.WPI BETWEEN 15 AND 20 THEN '05'	-- Greater Than 14% & Less Than or Equal to 20%
		WHEN cad.WPI BETWEEN 11 AND 14 THEN '04'	-- Greater Than 10% & Less Than or Equal to 14%
		WHEN cad.WPI BETWEEN  0 AND 10 THEN '03'	-- Less Than or Equal to 10%
		ELSE '02'					-- Not Yet Estimated
	 END AS EstPermImpairment_icare			
	,CASE 
		WHEN (SELECT DISTINCT CD2.Claim_Number
				FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD2
					INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD2 ON CD2.Claim_Number = CAD2.Claim_no
					LEFT OUTER JOIN (SELECT * FROM DM_STG_EMICS.dbo.PAYMENT_RECOVERY WHERE Reversed = 0) PR ON PR.Claim_No = CD2.Claim_Number
					LEFT OUTER JOIN (SELECT DISTINCT Claim_No FROM DM_STG_EMICS.dbo.MEDICAL_CERT WHERE is_Deleted = 0) MC ON CAD2.Claim_no = MC.Claim_No
				WHERE CD2.is_Null = 0 
					AND CAD2.Claim_Liability_Indicator <> 12
					AND (PR.Estimate_Type IN (51,52,53,55,56,63,64) OR CD2.is_Medical_Only = 1 OR MC.Claim_No IS NOT NULL)
					AND CD2.Claim_Number = cd.Claim_Number) > 0
		THEN 'EMICS:' + RTRIM(cd.Claim_Number) + ':MEDONLYEXP' 
		ELSE NULL
	 END AS ExposureID
	--,'EMICS:' + RTRIM(cd.Claim_Number) + ':MEDONLYEXP' AS ExposureID
	,CASE 
		WHEN cad.WPI > 20 THEN (SELECT Transaction_Date 
								FROM DM_STG_EMICS.dbo.CAD_AUDIT cadt
								WHERE cadt.ID = (SELECT MIN(ID)
												 FROM DM_STG_EMICS.dbo.CAD_AUDIT
												 WHERE Claim_no = cad.Claim_no
													AND WPI > 20))  -- Change 18/07/2019
		ELSE NULL
	 END AS HHNApprovalDate
	,CASE
		WHEN cad.WPI > 20 THEN 'Please refer to Claim File Notes for more info' 
		ELSE NULL
	 END AS HHRReason
	,CASE
		WHEN cad.WPI BETWEEN 21 AND 30 THEN 'HighNeeds_icare'
		WHEN cad.WPI > 30 THEN 'HighestNeeds_icare'
		ELSE NULL
	 END AS HighHighestNeeds
	,'EMICS:' + RTRIM(cd.Claim_Number) + ':WPIASSESSMENT' AS PublicID
into ccst_WPIAssessment_icare
FROM DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd ON cad.Claim_No = cd.Claim_Number

WHERE cd.is_Null = 0
	AND cd.Result_of_Injury_Code IN ('2','3')


END