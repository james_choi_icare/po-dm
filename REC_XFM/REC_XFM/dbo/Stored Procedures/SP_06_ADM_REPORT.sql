﻿CREATE PROC [dbo].[SP_06_ADM_REPORT]
AS
BEGIN
	SELECT *, 'MISSING_LUWID_TABLES'
	FROM [dbo].[MISSING_LUWID_TABLES]

	SELECT *,'MISSING_MAPPING_TABLES'
	FROM [dbo].[MISSING_MAPPING_TABLES]

	SELECT *
	FROM ADM_XFM_TABLE_MAPPING
	WHERE Process = 0

	SELECT Cohort, count(*) Claim_Count
	FROM ADM_Claim_Cohort
	group by Cohort

	SELECT *
	FROM [dbo].[MISSING_TABLES]

	SELECT *
	FROM [dbo].[LATEST_ERRORS]
	order by 2

	SELECT *
	FROM [dbo].[LATEST_LOG_DETAILS]
	order by 3 desc

	select *
	from ADM_RECON_SUMMARY_COHORT a
		inner join ADM_XFM_TABLE_MAPPING b ON a.XFM_TABLE = replace(b.xfm_table,'DM_XFM_POST','DM_POST') and Process = 1
	order by 2,3 desc,1
END