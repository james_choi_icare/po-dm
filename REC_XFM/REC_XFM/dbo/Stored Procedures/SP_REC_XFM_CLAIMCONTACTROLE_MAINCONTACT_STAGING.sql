﻿


-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 24/06/2019
-- Description:	Contacts - Main Contact V002.17
-- 1. This is the child Stored Proc for MAINCONTACT claimcontactrole that will called by the master stored proc SP_REC_XFM_CLAIMCONTACTROLE
-- 2. <09Aug2019><Satish>:Added more code to account for ER_unmatched and Verified CDR main contacts
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMCONTACTROLE_MAINCONTACT_STAGING]
	
AS
BEGIN


IF OBJECT_ID('ClaimContactRoleREF_temptable_maincontact_matched') is not null
drop table dbo.ClaimContactRoleREF_temptable_maincontact_matched;

IF OBJECT_ID('ClaimContactRoleREF_temptable_maincontact_unmatched_ER') is not null
drop table dbo.ClaimContactRoleREF_temptable_maincontact_unmatched_ER;

IF OBJECT_ID('ClaimContactRoleREF_temptable_maincontact_verifiedCDR') is not null
drop table dbo.ClaimContactRoleREF_temptable_maincontact_verifiedCDR;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_matched_WOP') is not null
drop table dbo.temptable_ClaimContactRole_MAINCONTACT_matched_WOP;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_matched_ER') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_matched_ER;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_unmatched_ER') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_unmatched_ER;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR;

SELECT * INTO temptable_ClaimContactRole_MAINCONTACT_matched_ER
FROM
(
select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_MAINCONTACT' as PublicID,
0 as Retired,
'maincontact' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
)X
) QA_contact_ref_staging

UNION

select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_REPORTER' as PublicID,
0 as Retired,
'reporter' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
--select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
--where AddressBookUID is not null
)X
) QA_contact_ref_staging

)X

SELECT * INTO temptable_ClaimContactRole_MAINCONTACT_matched_WOP
FROM
(
select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_MAINCONTACT' as PublicID,
0 as Retired,
'maincontact' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null
)X
) QA_contact_ref_staging
)X

--Added this on 09Aug2019
SELECT * INTO temptable_ClaimContactRole_MAINCONTACT_unmatched_ER
FROM
(
select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_MAINCONTACT' as PublicID,
0 as Retired,
'maincontact' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_VerifiedPC_unmatched]
)X
) QA_contact_ref_staging

UNION

select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_REPORTER' as PublicID,
0 as Retired,
'reporter' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_VerifiedPC_unmatched]
--select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
--where AddressBookUID is not null
)X
) QA_contact_ref_staging

)X

--Added this on 09Aug2019
SELECT * INTO temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR
FROM
(
select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_MAINCONTACT' as PublicID,
0 as Retired,
'maincontact' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_rest_of_all_VerifiedCDR]
)X
) QA_contact_ref_staging

UNION

select 
--distinct
1 as Active,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC' as ClaimContactID,
NULL as Comments,
NULL as CoveredPartyType,
NULL as EvaluationID,
NULL as ExposureID,
NULL as IncidentID,
QA_contact_ref_staging.LUWID as LUWID,
NULL as MatterID,
NULL as NegotiationID,
NULL as PartyNumber,
NULL as PolicyID,
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC_REPORTER' as PublicID,
0 as Retired,
'reporter' as  Role,
NULL as WitnessPerspective,
NULL as WitnessPosition,
NULL as WitnessStatementInd
FROM
(
select LUWID--,AddressBookUID
from
(
select LUWID--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_rest_of_all_VerifiedCDR]
--select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
--where AddressBookUID is not null
)X
) QA_contact_ref_staging

)X

select
*
into dbo.ClaimContactRoleREF_temptable_maincontact_matched
from
(
select * 
from temptable_ClaimContactRole_MAINCONTACT_matched_ER
UNION
select * 
from temptable_ClaimContactRole_MAINCONTACT_matched_WOP
)x
;

select
*
into dbo.ClaimContactRoleREF_temptable_maincontact_unmatched_ER
from
(
select * 
from temptable_ClaimContactRole_MAINCONTACT_unmatched_ER
)x
;

select
*
into dbo.ClaimContactRoleREF_temptable_maincontact_verifiedCDR
from
(
select * 
from temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR
)x
;


IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_matched_ER') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_matched_ER;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_matched_WOP') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_matched_WOP;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_unmatched_ER') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_unmatched_ER;

IF OBJECT_ID('temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR') is not null
drop table temptable_ClaimContactRole_MAINCONTACT_rest_of_all_VerifiedCDR;

END