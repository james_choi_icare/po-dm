﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CHECKPORTION]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Create date: 23/05/2019
-- Description:	Payments and recoveries ccst_checkportion
-- Version: 23/05/2019 v45.10 initial build
--			20/06/2019 v46.3.6 no change
--			07/11/2019 v46.3.9 checked against latest version no change
--			04/09/2019 v47.2 updated Publicid xform, added cpc SC
-- ============================================= */

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_checkportion','Payments and Recoveries - v47.2 04/09/2019'
--drop table ccst_checkportion
select distinct
	null  AS FixedClaimAmount,
	null AS FixedReportingAmount,
	null  as FixedReservingAmount,
	null  as FixedTransactionAmount,
	RTRIM(cpr.claim_number) AS LUWID,
	null AS Percentage,
	'EMICS:'+RTRIM(CPR.Claim_Number)+'_'+CONVERT(VARCHAR,CPR.Payment_No)+isnull('_'+CPR.cheque_No,'')+'_'+isnull(convert(varchar,cpc.ID),'00') +':PRICHECKPORTION' as PublicID,
	NULL AS Reissued,
	0 AS Retired

into ccst_checkportion

from DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr
inner join (select distinct PR_Adj.Claim_No,PR_Adj.Payment_no,PR_gar.Payment_no as Garnishee_payment_no from 
	(select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
inner join (select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1)PR_Gar on PR_Adj.Claim_No=PR_Gar.Claim_No
	and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
	and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
	and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
	and pr_adj.Garnishee_Payment_id=PR_Gar.id) gar on cpr.Payment_no=gar.Payment_no
inner join DM_STG_EMICS.dbo.Payment_Recovery PR on PR.Payment_no=CPR.Payment_no and pr.Claim_No=cpr.Claim_number
LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_XFM_LD..DM_LKP_GW_CCX_paycodepaymenttype_icare A,
	 (select * from DM_XFM_LD..DM_LKP_GW_CCX_paycode_icare where Retired=0) B,DM_XFM_LD..DM_LKP_GW_cctl_paymenttype_icare C
	WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC
	ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)

UNION ALL

select distinct 
	cpr.Cheque_Amount as FixedClaimAmount,
	cpr.Cheque_Amount as FixedReportingAmount,
	cpr.Cheque_Amount as FixedReservingAmount,
	cpr.Cheque_Amount as FixedTransactionAmount,
	RTRIM(cpr.claim_number) AS LUWID,
	null AS Percentage,
	'EMICS:'+RTRIM(CPR.Claim_Number)+'_'+CONVERT(VARCHAR,CPR.Payment_No)+isnull('_'+CPR.cheque_No,'')+'_'+isnull(convert(varchar,cpc.ID),'00')+':SECCHECKPORTION' as PublicID,
	NULL AS Reissued,
	0 AS Retired
from DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr
inner join (select distinct PR_Adj.Claim_No,PR_Adj.Payment_no,PR_gar.Payment_no as Garnishee_payment_no from 
	(select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
	inner join (select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1)PR_Gar on PR_Adj.Claim_No=PR_Gar.Claim_No
	and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
	and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
	and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
	and pr_adj.Garnishee_Payment_id=PR_Gar.id) gar on cpr.Payment_no=gar.Garnishee_payment_no
inner join DM_STG_EMICS.dbo.Payment_Recovery PR on PR.Payment_no=CPR.Payment_no and pr.Claim_No=cpr.Claim_number
LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_XFM_LD..DM_LKP_GW_CCX_paycodepaymenttype_icare A,
 (select * from DM_XFM_LD..DM_LKP_GW_CCX_paycode_icare where Retired=0) B,DM_XFM_LD..DM_LKP_GW_cctl_paymenttype_icare C
WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)
--isnull=0?

END