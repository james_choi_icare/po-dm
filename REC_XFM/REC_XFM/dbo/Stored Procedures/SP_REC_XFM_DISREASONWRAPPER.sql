﻿-- =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Legal domain - ccst_disreasonwrapper_icare
-- Version: Initial build v30
-- =============================================
CREATE PROCEDURE SP_REC_XFM_DISREASONWRAPPER
AS
BEGIN
--ccst_disreasonwrapper_icare
Select 

	Case when Dispute_type =  0 then null
		when Dispute_type =  1 then 01
		when Dispute_type =  2 then 02
		when Dispute_type =  3 then 03
		when Dispute_type =  4 then 04
		when Dispute_type =  5 then 05
		when Dispute_type =  6 then 06
		when Dispute_type =  7 then 07
		when Dispute_type =  8 then 08
		when Dispute_type =  9 then 09
		when Dispute_type =  10 then 10
		when Dispute_type =  11 then 11
		when Dispute_type =  12 then 12
		when Dispute_type =  13 then 13
		when Dispute_type =  14 then 14
		when Dispute_type =  15 then 15
		when Dispute_type =  16 then 16
		when Dispute_type =  17 then 16
		End as DisputeReason,
	'EMICS:' + RTRIM(cada.ID) + 'LIABILITYSTATUSHISTORY' as LiabilityStatusID,
	cada.Claim_no as LUWID,
	'EMICS:' + RTRIM(cada.claim_no) + '_' + convert(varchar, cada.ID) + ':DISREASONWRAPPER' as PublicID
Into ccst_disreasonwrapper_icare
From (Select claim_no, dispute_type, max(ID) as 'ID' 
	from DM_STG_EMICS.dbo.CAD_AUDIT where dispute_type is not null and dispute_type != 0
	Group by claim_no, dispute_type, claim_liability_indicator) cada --57 records
END