﻿CREATE PROC [dbo].[SP_02b_PROCESS_RECON_XFM_MISSING_ROWS] 
		@runID int,
		@xfm_table varchar(100),
		@xfm_recon_table varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @columns varchar(max) = '',  @query nvarchar(max);

	BEGIN TRY	
		SET @query = 'DELETE FROM [dbo].[ADM_RECON_RESULT] WHERE Tgt_Table = ''' + @xfm_table + ''' AND Tgt_Column = ''PublicID''';
		exec sp_executesql @query

		SET @query = 'INSERT INTO [dbo].[ADM_RECON_RESULT]
						SELECT DISTINCT ''' + @xfm_table + ''' Tgt_Table, ''PublicID'' Tgt_Column, a.PublicID XFM_Key, b.PublicID Expected_Key,  a.PublicID XFM_Value, b.PublicID Expected_Value,null,' + cast(@runID as varchar(10)) + ', ISNULL(a.LUWID,b.LUWID)
							FROM [DM_XFM].dbo.' + @xfm_table + ' a
								FULL JOIN ' + @xfm_recon_table + ' b ON a.PublicID = b.PublicID
							WHERE a.PublicID is null OR b.PublicID is null';
		exec sp_executesql @query
	END TRY
	BEGIN CATCH
		IF ERROR_NUMBER() is not null
		BEGIN
			INSERT INTO [dbo].[ADM_RECON_RUN_ERROR]
			SELECT @runID, @query + ' ' + ERROR_MESSAGE()
		END
	END CATCH

END