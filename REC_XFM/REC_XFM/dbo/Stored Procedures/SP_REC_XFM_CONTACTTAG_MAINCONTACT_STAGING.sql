﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 21/06/2019
-- Description:	Contacts - Main Contacts v2.17
-- 1. This is the child Stored Proc for MAINCONTACT contacttag that will called by the master stored proc SP_REC_XFM_CONTACTTAG
-- 2. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 3. RM - 06-Aug - Updated 'client' as Type to 'claimparty' in temptable_Contacttag_maincontact_CM_unmatch
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CONTACTTAG_MAINCONTACT_STAGING]
	
AS
BEGIN

IF OBJECT_ID('ContacttagREF_temptable_MC_matched') is not null
drop table dbo.ContacttagREF_temptable_MC_matched;

IF OBJECT_ID('temptable_Contacttag_maincontact_CM_match') is not null
drop table temptable_Contacttag_maincontact_CM_match;

IF OBJECT_ID('temptable_Contacttag_maincontact_CM_unmatch') is not null
drop table temptable_Contacttag_maincontact_CM_unmatch;


SELECT * INTO temptable_Contacttag_maincontact_CM_match
FROM
(
select 
--distinct
--concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
concat('EMICS:',QA_contact_ref_staging.LUWID,':INSURED_MC') as ContactID,
--CM_CT.ContactLinkID as ContactID,
--CM_C.PublicID as ContactID,
--ltrim(rtrim(CD.Claim_Number)) as LUWID,
QA_contact_ref_staging.LUWID as LUWID,
--c.CRM_ID as LinkID,
--CM_C.LinkId as AddressBookUID,
CM_CT.LinkId as AddressBookUID,
NULL as ExternalLinkID,
CASE 
WHEN CM_CT.Type='client'     THEN 'EMICS:'+QA_contact_ref_staging.LUWID+':INSURED_MC'+':CLIENT'
WHEN CM_CT.Type='claimparty' THEN 'EMICS:'+QA_contact_ref_staging.LUWID+':INSURED_MC'+':CLAIMPARTY'
END as PublicID,
CASE 
WHEN CM_CT.Type='client' THEN 'client'
WHEN CM_CT.Type='claimparty' THEN 'claimparty'
END as Type,
0 as Retired
--CM_CT.Type as Type_CM
FROM
(
select LUWID,AddressBookUID
from
(
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null
)X
) QA_contact_ref_staging
--LEFT OUTER JOIN [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = QA_contact_ref_staging.AddressBookUID
LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= QA_contact_ref_staging.AddressBookUID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] CM_CT on CM_CT.ABContactId = CM_C.PublicID
LEFT OUTER JOIN 
(
select CM_CT1.ID,CM_CT1.ABContactId,CM_CT1.LinkID, ABTL_CT.TYPECODE as Type  
from [DM_STG_CM_LD].[dbo].[ab_abcontacttag] CM_CT1
inner join [DM_STG_CM_LD].[dbo].[abtl_contacttagtype] ABTL_CT
on CM_CT1.Type=ABTL_CT.ID
)CM_CT --23Jul2019 added this to ensure we dont lookup [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] 
on CM_CT.ABContactId = CM_C.ID
--select PublicID,ID,* from [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C 
--where LinkID='0032O000005Bm5AQAS'
--select * from [DM_STG_CM_LD].[dbo].[ab_abcontacttag] CM_CT
--where ABContactId=2554
--SELECT 
--  C1.2.*, GWCM.ab_Contact.*, GWCM.ab_ContactTag.*, GWCM.ab_Address.*
--FROM C1.2 
--   INNER JOIN GWCM.ab_Contact ON GWCM.ab_Contact.LinkID =  c1.2.Contact.LinkId (ie the GWPC.ContactLinkID)
--      INNER JOIN GWCM.ab_ContactTag ON GWCM.ab_Contact.ID = GWCM.ab_ContactTag.abContactID
--      INNER JOIN GWCM.ab_Address ON GWCM.ab_Contact.PrimaryAddressID = GWCM.ab_Address.ID
--where CD.is_Null = 0
--and CD.Claim_Number=1333632
where CM_CT.Type is not null --ensuring we pick matched records only
)X

SELECT * INTO temptable_Contacttag_maincontact_CM_unmatch
FROM
(
select 
--CM_CT.*
------distinct
--------concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as ContactID,
concat('EMICS:',QA_contact_ref_staging.LUWID,':INSURED_MC') as ContactID,
-------CM_CT.ContactLinkID as ContactID,
-------CM_C.PublicID as ContactID,
-------ltrim(rtrim(CD.Claim_Number)) as LUWID,
QA_contact_ref_staging.LUWID as LUWID,
-------c.CRM_ID as LinkID,
--------CM_C.LinkId as AddressBookUID,
--CM_CT.LinkId as AddressBookUID
'EMICS:'+QA_contact_ref_staging.LUWID+':INSURED_MC'+':CLAIMPARTY' as AddressBookUID,
NULL as ExternalLinkID,
'EMICS:'+QA_contact_ref_staging.LUWID+':INSURED_MC'+':CLAIMPARTY' as PublicID,
'claimparty' as Type, --RM - 06-Aug - Updated 'client' as Type to 'claimparty' in temptable_Contacttag_maincontact_CM_unmatch
0 as Retired
-------CM_CT.Type as Type_CM
FROM
(
select LUWID,AddressBookUID
from
(
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null
)X
) QA_contact_ref_staging
--LEFT OUTER JOIN [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = QA_contact_ref_staging.AddressBookUID
LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= QA_contact_ref_staging.AddressBookUID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] CM_CT on CM_CT.ABContactId = CM_C.PublicID and CM_CT.Type='claimparty'
LEFT OUTER JOIN 
(
select CM_CT1.ID,CM_CT1.ABContactId, ABTL_CT.TYPECODE as Type  
from [DM_STG_CM_LD].[dbo].[ab_abcontacttag] CM_CT1
inner join [DM_STG_CM_LD].[dbo].[abtl_contacttagtype] ABTL_CT
on CM_CT1.Type=ABTL_CT.ID
)CM_CT --23Jul2019 added this to ensure we dont lookup [DM_XFM_LD].[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] 
on CM_CT.ABContactId = CM_C.ID and CM_CT.Type='claimparty'
left outer join temptable_Contacttag_maincontact_CM_match temp_CT_match on temp_CT_match.LUWID=QA_contact_ref_staging.LUWID and temp_CT_match.Type='claimparty'
--SELECT 
--  C1.2.*, GWCM.ab_Contact.*, GWCM.ab_ContactTag.*, GWCM.ab_Address.*
--FROM C1.2 
--   INNER JOIN GWCM.ab_Contact ON GWCM.ab_Contact.LinkID =  c1.2.Contact.LinkId (ie the GWPC.ContactLinkID)
--      INNER JOIN GWCM.ab_ContactTag ON GWCM.ab_Contact.ID = GWCM.ab_ContactTag.abContactID
--      INNER JOIN GWCM.ab_Address ON GWCM.ab_Contact.PrimaryAddressID = GWCM.ab_Address.ID
--where CD.is_Null = 0
--and CD.Claim_Number=1333632
where temp_CT_match.LUWID is null --ensuring we pick those contacts for which client record is not there already in CM
)X


;

select
*
into dbo.ContacttagREF_temptable_MC_matched
from
(
select * 
from temptable_Contacttag_maincontact_CM_match
union
select * 
from temptable_Contacttag_maincontact_CM_unmatch
)x
;


IF OBJECT_ID('temptable_Contacttag_maincontact_CM_match') is not null
drop table temptable_Contacttag_maincontact_CM_match;

IF OBJECT_ID('temptable_Contacttag_maincontact_CM_unmatch') is not null
drop table temptable_Contacttag_maincontact_CM_unmatch;

END