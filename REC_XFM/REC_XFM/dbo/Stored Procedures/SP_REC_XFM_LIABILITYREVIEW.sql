﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_LIABILITYREVIEW]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Modified date: 09/05
-- Description: liability assessment ccst_liabilityreview
-- Version:	v22 initial build
--			17/05/2019 v24 - updated selection criteria, added ReviewerID, RequestDate, ReviewDate xform rules
			10/06/2019 v25 - v1.2 of users and groups fields updated for RequesterID, ReviewerID
-- ============================================= */

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_liabilityreview','Liability Assessment - v25 10/06/2019'

Select
	RTRIM(cd.claim_number) as LUWID,
	'EMICS:' + Ltrim(rtrim(cada.Claim_no)) + '_'+ convert(varchar, cada.ID) + ':LIABILITYREVIEW' as PublicID,
	'EMICS:' + ltrim(rtrim(cada.claim_no)) + ':WORKCOMP' ClaimWCID, 
	coalesce (cc_user.PublicID, cc_user_sm.PublicID) as RequesterID, -- v1.2
	coalesce (cc_user.PublicID, cc_user_sm.PublicID) as ReviewerID, -- v1.2
	'See Notes' as Comment,
	'DecisionSupported' as Outcome,
	Convert(date, cada.Transaction_Date, 103) as RequestDate,
	Convert(Date, cada.Transaction_Date, 103) as ReviewDate,	
	Case when cd.Date_Deceased is not null and cada.Claim_liability_indicator in (2,11,8) then 'AcceptFatalityDecision'
		When cd.Date_Deceased is not null and cada.Claim_liability_indicator in (7,9,10) then 'DeclineFatalityDecision'
		When cada.Claim_Liability_Indicator = 9 then 'ApplyReasonableExcuse'
		When cada.Claim_Liability_Indicator = 7 OR cada.Claim_Liability_Indicator = 11 then 'DeclineClaimLiability'
	End as ReviewType

	into ccst_liabilityreview

	from DM_STG_EMICS.dbo.cad_audit cada
	Left join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number = cada.claim_no
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	Left join DM_STG_EMICS.dbo.CLAIM_TRIAGE ct on ct.claim_no = cada.claim_no
	left join DM_STG_EMICS.dbo.claim_activity_detail cad on cad.claim_no = cada.claim_no
	LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cad.Claims_Officer)
	LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
			FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
			ON (co.Alias = cntrl.ITEM)
	left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
	left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)
	left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
	left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
	left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
	
	
	Where cada.id in (Select ID from (Select claim_no, claim_liability_indicator, max(ID) as ID from DM_STG_EMICS.dbo.cad_audit
			Group by claim_no, claim_liability_indicator) a)
	and (cada.claim_liability_indicator in (7,9,11) or cd.date_deceased is not null)
	and cd.is_null =0
	and cada.claim_liability_indicator <>1

END