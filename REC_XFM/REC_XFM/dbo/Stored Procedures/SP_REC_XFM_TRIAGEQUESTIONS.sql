﻿-- =============================================
-- Author:		Saranya
-- Create date: 14/05/2019
-- Description:	TRIAGE initial build v35
--				09/07/2019 added isnull = 0
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_TRIAGEQUESTIONS]
	
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_triagequestion_icare','Triage - v39 09/07/2019'

;WITH triage_question as(
SELECT
	ref.Ref_No AS [LUWID]
	,'EMICS:' + ref.Ref_No + '_' + ref.Category + ':USER_RESPONSE' AS [PublicID]
	,q1.AbleProvideSuitableWork
	,q1.AbleProvideSuitableWorkText
	,q2.AbleToUseTransportation
	,q2.AbleToUseTransportationText
	,q3.AdditionalHealthConditions
	,q3.AdditionalHealthConditionsText
	,q4.AdmittedToHospital
	,q4.AdmittedToHospitalText
	,q5.ClinicalPresentationConsistent
	,q6.ConcernsWithEmpOrEnv
	,q7.FactorsAffectingRecovery
	,q7.FactorsAffectingRecoveryText
	,q8.FeelsInControlOfRecovery
	,q8.FeelsInControlOfRecoveryText
	,q9.InjuredMotivatedRTW
	,q9.InjuredMotivatedRTWText
	,LTRIM(RTRIM(q10.InjuryConcerns)) as InjuryConcerns
	,LTRIM(RTRIM(q10.InjuryConcernsText)) as InjuryConcernsText
	,q11.IsInjuredCurrentlyWorking
	,q12.PersonReturnEstimation
	,q13.ThereIsSupport
	,q13.ThereIsSupportText	
FROM 
(SELECT DISTINCT
	Ref_No
	,q.Category
FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur
INNER JOIN [DM_STG_EMICS].dbo.[Question] q
ON ur.Question_ID = q.ID
WHERE Question_ID NOT IN (24,25,26,27,38,39,40)
and ur.id in (select max(u.id) from dm_stg_emics.dbo.USER_RESPONSE u group by ref_no,question_id)
) ref
INNER JOIN [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct
ON ref.Ref_No = ct.Claim_No
INNER JOIN [DM_STG_EMICS].dbo.claim_detail ic ON LTRIM(RTRIM(ref.Ref_No)) = ic.Claim_Number
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 'Yes'
			WHEN Option_Answer = 'N' THEN 'No'
			WHEN Option_Answer = 'U' THEN 'Unknown'
			ELSE NULL
		END AS [AbleProvideSuitableWork]
		,Answer AS [AbleProvideSuitableWorkText] 
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q1
ON ref.Ref_No = q1.Ref_No AND ref.Category = q1.Category AND q1.Question_ID IN (6,35)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
		ELSE NULL
		END AS [AbleToUseTransportation]
		,Answer AS [AbleToUseTransportationText] 
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q2
ON ref.Ref_No = q2.Ref_No AND ref.Category = q2.Category AND q2.Question_ID IN (15,41)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [AdditionalHealthConditions]
		,Answer AS [AdditionalHealthConditionsText]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q3
ON ref.Ref_No = q3.Ref_No AND ref.Category = q3.Category AND q3.Question_ID = 17
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 'Yes'
			WHEN Option_Answer = 'N' THEN 'No'
			WHEN Option_Answer = 'U' THEN 'Unknown'
			ELSE NULL
		END AS [AdmittedToHospital]
		,Answer AS [AdmittedToHospitalText]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q4
ON ref.Ref_No = q4.Ref_No AND ref.Category = q4.Category AND q4.Question_ID IN (3,11,32)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [ClinicalPresentationConsistent] 
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q5
ON ref.Ref_No = q5.Ref_No AND ref.Category = q5.Category AND q5.Question_ID = 20
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [ConcernsWithEmpOrEnv]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q6
ON ref.Ref_No = q6.Ref_No AND ref.Category = q6.Category AND q6.Question_ID = 21
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 'Yes'
			WHEN Option_Answer = 'N' THEN 'No'
			WHEN Option_Answer = 'U' THEN 'Unknown'
			ELSE NULL
		END AS [FactorsAffectingRecovery]
		,Answer AS [FactorsAffectingRecoveryText]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q7
ON ref.Ref_No = q7.Ref_No AND ref.Category = q7.Category AND q7.Question_ID IN (7,29,36)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [FeelsInControlOfRecovery]
		,Answer AS [FeelsInControlOfRecoveryText]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q8
ON ref.Ref_No = q8.Ref_No AND ref.Category = q8.Category AND q8.Question_ID = 16
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 'Yes'
			WHEN Option_Answer = 'N' THEN 'No'
			WHEN Option_Answer = 'U' THEN 'Unknown'
			ELSE NULL
		END AS [InjuredMotivatedRTW]
		,Answer AS [InjuredMotivatedRTWText] 
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q9
ON ref.Ref_No = q9.Ref_No AND ref.Category = q9.Category AND q9.Question_ID IN (8,13,37)
LEFT OUTER JOIN (SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [InjuryConcerns]
		,Answer AS [InjuryConcernsText]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q10
ON ref.Ref_No = q10.Ref_No AND ref.Category = q10.Category AND q10.Question_ID IN (4,33,28)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [IsInjuredCurrentlyWorking]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q11
ON ref.Ref_No = q11.Ref_No AND ref.Category = q11.Category AND q11.Question_ID IN (1,9,30)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'W0T2' THEN '0_2_weeks'
			WHEN Option_Answer = 'W2T4' THEN '2_4_weeks'
			WHEN Option_Answer = 'W4T6' THEN '4_6_weeks'
			WHEN Option_Answer = 'W4P' THEN '4_plus_weeks'
			WHEN Option_Answer = 'W6P' THEN '6_plus_weeks'
			WHEN Option_Answer = 'UNC' THEN 'uncertain'
			ELSE NULL
		END AS [PersonReturnEstimation]
	FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q12
ON ref.Ref_No = q12.Ref_No AND ref.Category = q12.Category AND q12.Question_ID IN (5,12,18,34)
LEFT OUTER JOIN 
(SELECT ur.ID
		,Ref_No
		,Question_ID
		,q.Category
		,CASE 
			WHEN Option_Answer = 'Y' THEN 1
			WHEN Option_Answer = 'N' THEN 0
			ELSE NULL
		END AS [ThereIsSupport]
		,Answer AS [ThereIsSupportText]
		FROM [DM_STG_EMICS].dbo.[USER_RESPONSE] ur INNER JOIN [DM_STG_EMICS].dbo.[Question] q  ON ur.Question_ID = q.ID INNER JOIN [DM_STG_EMICS].dbo.[CLAIM_DETAIL] cd ON ur.Ref_No = cd.Claim_Number) q13
ON ref.Ref_No = q13.Ref_No AND ref.Category = q13.Category AND q13.Question_ID = 14

Where ic.is_null = 0
)	

select * into ccst_triagequestion_icare from triage_question

END