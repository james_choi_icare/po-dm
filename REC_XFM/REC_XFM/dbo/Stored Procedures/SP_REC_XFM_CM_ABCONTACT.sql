﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 18/07/2019
-- Description:	Contacts - Main Contact V2.19
-- 1. This SP is only for Main Contacts -E,R Unmatched. To load CONTACT data for Contact Manager.
--
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CM_ABCONTACT]

AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'abst_abcontact', 'MC_v2.19';

IF OBJECT_ID('temptable_max_claimnumber') is not null
drop table dbo.temptable_max_claimnumber;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_verifiedPC_temp1') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_verifiedPC_temp1;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_verifiedPC_temp2') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_verifiedPC_temp2;

IF OBJECT_ID('ContactREF_temptable_maincontact_CM_ER_unmatched') is not null
drop table dbo.ContactREF_temptable_maincontact_CM_ER_unmatched;

IF OBJECT_ID('abst_abcontact') is not null
drop table dbo.abst_abcontact;





--drop table temptable_max_claimnumber
--Getting the max(claimnumber) from the CC base data for a givne LinkID as we cannot insert duplicate records for
--the same LinkID into CM
select * into temptable_max_claimnumber
from
(
select LinkID, max(LUWID) as max_LUWID from 
[dbo].[ContactREF_temptable_MC_ER_VerifiedPC_unmatched]
--where LinkID='1514801MC'
group by LinkID
)X


--drop table temptable_CM_MC_ER_unmatched_verifiedPC_temp1
--From CC base data table for a given LinkID and LUWID (ClaimNumber) combo, we match for the LinkID, max(LUWID) combo from
--the temptable_max_claimnumber table.
--This will ensure we pick the max(Claimnumber)record out from CC base table and insert that into CM
select * into temptable_CM_MC_ER_unmatched_verifiedPC_temp1
from
(
select 
a.*
from 
[dbo].[ContactREF_temptable_MC_ER_VerifiedPC_unmatched] a
inner join temptable_max_claimnumber b
on a.LUWID=b.max_LUWID and a.LinkID=b.LinkID
)X


--drop table temptable_CM_MC_ER_unmatched_verifiedPC_temp2
--Loading the final temp table to include data for columns that are only present in CM and not in CC
select * into temptable_CM_MC_ER_unmatched_verifiedPC_temp2
from
(
select 
ABN_icare
ABNIsActive_icare				,
ABNValidated_icare				,
ABSLanguageCode_icare			,
ABSLanguageDescription_icare	,
ACN_icare						,
AdjudicativeDomain				,
AdjudicatorLicense				,
NULL as AggregateRemittance_icare		,
NULL as ApprovedPaymentMethod_icare		,
NULL as ArchiveSchemaInfo				,
AttorneyLicense					,
AttorneySpecialty				,
AttorneySpecialty_icare			,
AutoRepairLicense				,
AutoTowingLicense				,
BlockedVendor_icare				,
CellPhone						,
CellPhoneCountry				,
CellPhoneExtension				,
CommencementDate_icare			,
CommunicationPreferences_icare	,
'approved' as CreateStatus					,
CRMVersion_icare				,
DateOfBirth						,
DoctorSpecialty					,
DoctorSpecialty_icare			,
DoNotDestroy					,
EmailAddress1					,
EmailAddress2					,
EmployeeNumber					,
FaxPhone						,
FaxPhoneCountry					,
FaxPhoneExtension				,
FirstName						,
FirstNameKanji					,
FormerName						,
Gender							,
HomePhone						,
HomePhoneCountry				,
HomePhoneExtension				,
InterpreterRequired_icare		,
NULL as Keyword							,
NULL as KeywordKanji					,
LastName						,
LastNameKanji					,
LawFirmSpecialty				,
LegalSpecialty_icare			,
LicenseNumber					,
LicenseState					,
LinkID							,
LUWID							,
MaritalStatus					,
MedicalLicense					,
MedicalOrgSpecialty				,
MedicalSpecialty_icare			,
MedicareIRN_icare				,
MedicareNumber_icare			,
MedicareValidUntil_icare		,
MiddleName						,
NULL as MinimumCriteriaVerified			,
Name							,
NameKanji						,
Notes							,
NumDependents					,
NumDependentsU18				,
NumDependentsU25				,
Occupation						,
OktaID_icare					,
OrganisationType_icare			,
Particle						,
Preferred						,
PreferredCurrency				,
PreferredPaymentMethod_icare	,
Prefix							,
PrimaryAddressID				,
PrimaryPhone					,
PublicID						,
RegisteredForGST_icare			,
Retired							,
--RowNumber						,
Score							,
SingleRemittancePerDay_icare	,
SmsNotification_icare			,
SourceSystem_icare				,
Subtype							,
Suffix							,
TaxFilingStatus					,
TaxID							,
TaxStatus						,
TradingName_icare				,
TrustABN_icare					,
TrustABNIsActive_icare			,
TrustABNValidated_icare			,
TrusteeName_icare				,
TrustName_icare					,
TrustRegisteredForGST_icare		,
TrusteeType_icare				,
NULL as UpdateScore						,
ValidationLevel					,
NULL as VendorAvailability				,
VendorNumber					,
NULL as VendorServicesLoadStatus		,
VendorType						,
NULL as VendorUnavailableMessage		,
VenueType						,
W9Received						,
W9ReceivedDate					,
W9ValidFrom						,
W9ValidTo						,
WithholdingRate					,
WorkPhone						,
WorkPhoneCountry				,
WorkPhoneExtension
from 
temptable_CM_MC_ER_unmatched_verifiedPC_temp1 a
)X


--drop table [ContactREF_temptable_maincontact_CM_ER_unmatched]
--Storing the data into a REF table before it can be loaded into the final destnation abst_abcontact
select * into [ContactREF_temptable_maincontact_CM_ER_unmatched]
from
(
select
*
from
temptable_CM_MC_ER_unmatched_verifiedPC_temp2
)X

--drop table abst_abcontact
--Loading the data into final destnation abst_abcontact
select * into abst_abcontact
from
(
select
*
from
[ContactREF_temptable_maincontact_CM_ER_unmatched]
)X


IF OBJECT_ID('temptable_max_claimnumber') is not null
drop table dbo.temptable_max_claimnumber;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_verifiedPC_temp1') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_verifiedPC_temp1;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_verifiedPC_temp2') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_verifiedPC_temp2;

END