﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_ADDITIONALPERIODS]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 7/05/2019
-- Domain: Work Capacity v7.3 ccst_additionalperiods_icare 
-- Comments: v7.3 Unsure about DaysPerWeek source value, mapping references SC.Estimated_work_days and round up to 1 decimal but no field matches this name exactly
--			17/05 checked against v8.0 no change
--			20/06 checked against v9.2 no change
--			09/07 updated SC
-- =============================================
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_additionalperiods_icare','Work Capacity - v12.1 09/07/2019'

	Select 
		New_EstimatedWorkDays as DaysPerWeek,
		'EMICS:'+Rtrim(ltrim(CD.Claim_Number))+'_'+convert(varchar,MC.ID)+':EMPLOYMENTCAPACITY' as EmploymentCapacityID,
		MC.Date_To as EndDate,
		Case when MC.Type = '5' then '05'
			when MC.Type = '6' then '06'
			when MC.Type = 'M' then '05'
			when MC.Type = 'P' then '00'
			when MC.Type = 'S' then '05'
			when MC.Type = 'I' then '01'
			when MC.Type = 'T' then '06'
			End as Fitness,
		Cast(New_EstimatedWorkHoursPerDay as decimal(18,2)) as HoursPerDay,--ROUND(New_EstimatedWorkHoursPerDay as Decimal,2) as HoursPerDay,
		MC.Claim_no as LUWID,
		'EMICS:'+rtrim(ltrim(MC.Claim_No))+'_'+convert(varchar, MC.ID)+':ADDITIONALPERIODS' as PublicID,
		MC.Date_From as StartDate

into ccst_additionalperiods_icare
	
From DM_STG_EMICS.dbo.Medical_Cert mc
Left outer join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on mc.Claim_no = cd.Claim_Number
--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number

Left outer join (
	Select cd.claim_number, mc.ID, cd.Work_Days, (len(cd.work_Days) - len(REPLACE(Cd.work_days, '1',''))) as current_workdays
		, mc.hours_per_week as estimatedhoursperweek, mc.hours_per_week / replace((len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) as Current_estimatedWorkHoursPerDay
		, 
		
		Case when mc.Hours_per_week between 35 and 40  and (len(cd.work_days) - len(replace(cd.work_days,'1',''))) < 5 then 5
			When mc.hours_per_week / REPLACE((LEN(cd.work_days) - LEN(REPLACE(cd.work_days,'1',''))),0,1) > 12 then 
				Case when ceiling(mc.hours_per_Week/12) > 7 then 7 else ceiling(mc.hours_per_week/12) end
			Else replace((len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) end as New_estimatedWorkDays


		, Case when mc.hours_per_week between 35 and 40 and (Len(cd.work_days) - len(Replace(cd.work_days,'1',''))) < 5 then mc.hours_per_Week/5 
			When mc.hours_per_week / REPLACE((LEN(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) > 12 then 
			Case when ceiling(mc.hours_per_week / 12) > 7 then mc.hours_per_Week/ 7 else mc.hours_per_week/ceiling(mc.hours_per_week/12) end
			Else mc.Hours_per_week / replace((Len(cd.work_days) - len(Replace(cd.work_days,'1',''))),0,1) end as New_EstimatedWorkHoursPerDay

		From DM_STG_EMICS.dbo.MEDICAL_CERT mc 
		Left outer join DM_STG_EMICS.dbo.Claim_Detail cd on mc.claim_no = cd.claim_number
		Where mc.Hours_per_Week > 0
	) as NewEstimatedTime on mc.claim_no = newestimatedtime.claim_number and mc.ID = newestimatedTime.ID
	Where cd.is_null = 0
	order by cd.claim_number, cd.id

END