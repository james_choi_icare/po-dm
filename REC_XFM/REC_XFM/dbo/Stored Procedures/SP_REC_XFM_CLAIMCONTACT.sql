﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 13/06/2019
-- Description:	Contacts - Named Insured V003.8  Main Contact v2.17
-- 1. This is the master Stored Proc that will call the different contacts type CLAIMCONTACT stored proc such as
--    SP_REC_XFM_CLAIMCONTACT_EMPLOYER_STAGING
-- 2. <24-Jun-2019><Satish>: Added call to [dbo].[SP_REC_XFM_CLAIMCONTACT_MAINCONTACT_STAGING]
-- 3. <09Aug2019><Satish>: Modified the SP to account for ER_VerifiedPC_unmatched and Rest_of_all_VerifiedCDR MainContacts too. Earlier
--    it was looking only at Matched Main Contacts. The base changes have all been done under the child main contact SP.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMCONTACT]
	
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimcontact', 'EMP_v3.8 MC_v2.17'
EXEC SP_REC_XFM_CLAIMCONTACT_EMPLOYER_STAGING
EXEC SP_REC_XFM_CLAIMCONTACT_MAINCONTACT_STAGING
--EXEC SP_REC_XFM_CONTACT_INJURED_WORKER
;

IF OBJECT_ID('ccst_claimcontact') is not null
drop table dbo.ccst_claimcontact;



/****** Object:  Table [dbo].[ccst_claimcontact]    Script Date: 5/30/2019 11:41:40 AM ******/

CREATE TABLE [dbo].[ccst_claimcontact](
	[BenefitEndDate] [varchar](30) NULL,
	[BenefitEndReason] [varchar](30) NULL,
	[BenefitEndReasonType] [varchar](30) NULL,
	[ClaimID] [varchar](25) NULL,
	[ClaimantFlag] [int] NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ccst_claimcontact] ADD [ContactID] [varchar](33) NOT NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ccst_claimcontact] ADD [ContactProhibited] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [ContactValidFrom] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [ContactValidTo] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [DependentType] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [Details_icare] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [EssentialServiceType] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [LUWID] [varchar](19) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [PolicyID] [varchar](32) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [ProviderType] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [PublicID] [varchar](33) NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [Retired] [int] NOT NULL
ALTER TABLE [dbo].[ccst_claimcontact] ADD [Service] [varchar](30) NULL

--Inserting claimcontact records for Employer Verified PC
INSERT INTO [dbo].ccst_claimcontact
(
	[BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
)
SELECT 
    [BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
  FROM [dbo].[ClaimContactREF_temptable_employer_verifiedPC]
  ;

--Inserting claimcontact records for Employer Verified CDR
INSERT INTO [dbo].ccst_claimcontact
(
	[BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
)
SELECT 
    [BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
  FROM [dbo].[ClaimContactREF_temptable_employer_verifiedCDR]
  ;

--Inserting claimcontact records for MainContact
INSERT INTO [dbo].ccst_claimcontact
(
	[BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
)
SELECT 
    [BenefitEndDate],
	[BenefitEndReason],
	[BenefitEndReasonType],
	[ClaimID],
	[ClaimantFlag],
	[ContactID],
	[ContactProhibited],
	[ContactValidFrom],
	[ContactValidTo],
	[DependentType],
	[Details_icare],
	[EssentialServiceType],
	[LUWID],
	[PolicyID],
	[ProviderType],
	[PublicID],
	[Retired],
	[Service] 
  FROM [dbo].[ClaimContactREF_temptable_maincontact_matched_and_unmatched]
  ;



END