﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_EMPLOYMENTDATA]
AS
BEGIN
-- ===============================
-- Author: Ann
-- Domain: Lodgement and linking tables
-- Version: v33 mapping spec ccst_employmentdata
--			14/05 checked against v37 no change
--			11/07 checked against v43 no change
--			24/07 v46 ag updated xform for hiredate, lastworkeddate
--			24/07 v47 ag updated numdaysworked, workfriday_icare
-- =====================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_employmentdata','Lodgement - v46 24/07/2019'

-- drop table ccst_employmentdata
Select 
	CASE WHEN CD.SHIFT_ALLOWANCES > 0 OR CD.OT_Allowances > 0 THEN 1 ELSE 0 --No
		END AS AllowancesOvertEarnings_icare,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,
	-- JCFIX ----------------
	replace('[' + Case when SUBSTRING(cd.Work_days,1,1) = 1 then ISNULL('"Mon"','') else '' END +
		Case when  SUBSTRING(cd.Work_days,2,1) = 1 then ISNULL(',"Tue"','') else '' END +
		Case when  SUBSTRING(cd.Work_days,3,1) = 1 then ISNULL(',"Wed"','') else '' END +
		Case when  SUBSTRING(cd.Work_days,4,1) = 1 then ISNULL(',"Thu"','') else '' END +
		Case when  SUBSTRING(cd.Work_days,5,1) = 1 then ISNULL(',"Fri"','') else '' END + 
		Case when  SUBSTRING(cd.Work_days,6,1) = 1 then ISNULL(',"Sat"','') else '' END +
		Case when  SUBSTRING(cd.Work_days,7,1) = 1 then ISNULL(',"Sun"','') else '' 
	End +']','[,','[') as DaysWorkedWeek,
	-- JCFIX ----------------
	cd.terminated_date as EmploymentEndDate,
	CASE WHEN CD.Permanent_Ind = 0 OR CD.Permanent_Ind is null THEN NULL
		WHEN CD.Permanent_Ind = 1 THEN 1    --Permanent
		WHEN CD.Permanent_Ind = 2 THEN 2    --Non-permanent
		WHEN CD.Permanent_Ind = 3 THEN 3    --s457 Visa
		END AS EmploymentStatus,
	Case when cd.Date_Employed is null then convert(datetime, DATEADD(YEAR, -1, convert(date,cd.Date_of_Injury)),103)
		ELSE convert(datetime, convert(date,CD.DATE_EMPLOYED),103)
		end as HireDate,
	'Date_Employed: ' + ISNULL(convert(varchar, cd.Date_Employed),'NULL') as HireDate_SRC_VALUE,
	Case when cd.ANZSIC is not null then Industrycode.ID 
		Else NULL
		End as IndustryCode_icareID,
	convert(datetime2,convert(varchar, MinDate.CEASED_WORK_DATE_ID) ,112) as LastWorkedDate,
	CASE cad.Work_Status_Code
        WHEN 1 THEN '01'
        WHEN 2 THEN '02'
        WHEN 3 THEN '03'
        WHEN 4 THEN '04'
        WHEN 6 THEN '06'
        WHEN 8 THEN '08'
        WHEN 9 THEN '09'
        WHEN 10 THEN '10'
        WHEN 13 THEN '13'    
        ELSE NULL		END AS LatestWorkStatusDenorm_icare,
	cd.claim_number as LUWID,
	CASE WHEN CD.Work_hours between 35 and 40 and (len(cd.work_days) - len(replace(cd.work_days,'1','')))<5 then convert(decimal(5,2),5)		When cd.Work_hours / replace((LEN(cd.work_days) - LEN(REPLACE(cd.work_days,'1',''))),0,1)>12			Then case when CEILING(cd.work_hours/12) > 7 then convert(decimal(5,2),7) else convert(decimal(5,2),ceiling(cd.work_hours/12)) end		ELSE convert(decimal(5,2), REPLACE((LEN(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1))		End as NumDaysWorked, --updated v47
	'CD.Work_Days: ' + convert(Varchar, CD.Work_Days) as NumDaysWorked_SRC_VALUE,
	CASE WHEN cd.work_hours between 35 and 40 and (len(cd.work_days) - len(replace(cd.work_days,'1','')))<5 then convert(decimal(5,2),cd.work_hours /5)
		WHEN cd.work_hours/replace((LEN(cd.work_days) - LEN(REPLACE(cd.work_days,'1',''))),0,1) > 12
			THEN CASE WHEN CEILING(cd.work_hours/12) > 7 then convert(decimal(5,2),cd.work_hours/7) else convert(decimal(5,2),cd.work_hours /ceiling(cd.work_hours/12)) end
		ELSE convert(decimal(5,2),cd.work_hours/replace((LEN(cd.work_days) -len(replace(cd.work_days,'1',''))),0,1))
		End as NumHoursWorked,
	'Work_days: '+ convert(varchar, cd.Work_days) + ' work hours: ' + convert(varchar, cd.Work_Hours) as NumHoursWorked_SRC_VALUE,
	asco.Description as Occupation,
	CASE WHEN CD.OT_Allowances = 0 THEN NULL
		ELSE CD.OT_Allowances
		END AS OvertimeRate,	'EMICS:' + RTRIM(CD.Claim_Number) + ':EMPLOYMENT_DATA' as PublicID,	CASE WHEN CD.Employment_Terminated_Reason = 0 THEN 'Terminated'
		WHEN CD.Employment_Terminated_Reason = 1 THEN 'Retired'                   --EMICS: Age Retirement
		WHEN CD.Employment_Terminated_Reason = 2 THEN 'Retired'                   --EMICS: Medical Retirement
		WHEN CD.Employment_Terminated_Reason = 3 THEN 'Resigned'                --EMICS: Resigned
		WHEN CD.Employment_Terminated_Reason = 4 THEN 'Terminated'            --EMICS: Dismissed
		WHEN CD.Employment_Terminated_Reason = 5 THEN 'Other'                       --EMICS: Other
		ELSE 'Other'
		END AS TerminationReason_icare,	ISNULL(cd.training_status_code,0) as TrainingStatus_icare,	CASE WHEN SUBSTRING(CD.Work_Days,5,1) = 1 THEN 1 ELSE 0 --No --updated v47
		END AS WorkFriday_icare,	CASE WHEN SUBSTRING(CD.Work_Days,1,1) = 1 THEN 1 ELSE 0 --No
		END AS WorkMonday_icare,	CASE WHEN SUBSTRING(CD.Work_Days,6,1) = 1 THEN 1 ELSE 0 --No
		END AS WorkSaturday_icare,	CASE WHEN SUBSTRING(cd.Work_Days,7,1) = 1 THEN 1 ELSE 0 --No
		END AS WorkSunday_icare,	CASE WHEN SUBSTRING(cd.Work_Days,4,1) = 1 THEN 1 ELSE 0 --No
		END AS WorkThursday_icare,	CASE WHEN SUBSTRING(cd.Work_Days,2,1) = 1 THEN 1 ELSE 0 --No
		END AS WorkTuesday_icare,	CASE WHEN SUBSTRING(cd.Work_Days,3,1) = 1 THEN 1 ELSE 0 --No		END AS WorkWednesday_icare


	Into ccst_employmentdata

	

	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd --Claim Details Main Table
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad ON cd.Claim_Number = cad.Claim_no
	Left join (Select LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3) AS Claim_number,
				clm.Ceased_Work_Date_ID
				FROM DM_STG_CDR..DM_STG_CDR_D_CLAIM_MASTER clmmstr
				INNER JOIN DM_STG_CDR..DM_STG_CDR_D_COMPANY comp on clmmstr.MANAGING_COMPANY_ID = comp.COMPANY_ID
				INNER JOIN DM_STG_CDR..DM_STG_CDR_E_ROLLING_CLAIM clm on  clm.CLAIM_WCA_ID = clmmstr.CLAIM_WCA_ID
				WHERE clm.WCA_STATUS_ID = 181644
				and comp.SOURCE_INSURER_ID = 702
				and ISNULL(clm.CEASED_WORK_DATE_ID,0) <> 0 ) MinDate on Mindate.Claim_number = cd.Claim_Number


	Left join (Select ID, convert(varchar,Version) as Version from DM_STG_CC.dbo.ccx_industrycode_icare) Industrycode on convert(varchar, Industrycode.ID) = convert(varchar, cd.ANZSIC) and convert(varchar, Industrycode.version) = convert(varchar, cd.ANZSIC_Version)
	Left join (SELECT Description, ID FROM DM_STG_EMICS.dbo.ASCO) asco on asco.ID = cd.Asco_ID
	--Left outer join (Select ID, IndustryCode, Version, * From DM_XFM.dbo.DM_LKP_GW_CCX_industrycode_icare) cc on convert(varchar, cc.IndustryCode) = cd.ANZSIC and cc.version = cd.ANZSIC_Version
	--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.Claim_Number
	WHERE cd.is_Null = 0 --Exclude NULL Claims (Out Of Scope)
	

END