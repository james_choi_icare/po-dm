﻿
CREATE proc [dbo].[SP_REC_XFM_COVERAGE]
as
BEGIN
-- =============================================
-- Author: Ann
-- Modified date:
-- Domain: COVER v.12.35 ccst_coverage
-- Comments: 13/05 checked ver 12.36 no change
--           15/07 SQL Performance optimization by GXu
-- =============================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_coverage','Cover - v12.36 13/05/2019'

	--Verified PC 3 sets - 1. Workers Comp
	select 
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		'EMICS:' + ltrim(Rtrim(cd.claim_number)) + 'Workers Comp' + ':COVERAGE' PublicID,
		'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
		-- JCFIX ----------------
		cast('WCEmpLiabCov' as varchar(50)) as [Type]
		--'Workers' + CHAR(39) + ' Comp Employer' + CHAR(39) + 's Liability' as [Type]
	into ccst_coverage
	from CoverREF_temptable_consolidated temp_cons_pp
	left outer join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_cons_pp.LUWID
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	left outer join dm_stg_pc.dbo.pc_policyperiod pc_pp	on temp_cons_pp.PC_PP_ID=pc_pp.ID

	--Verified PC 3 sets - 2. Statutory Workers Comp
	Insert into ccst_coverage
	select 
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		'EMICS:' + ltrim(Rtrim(cd.claim_number)) + 'Statutory Workers Comp' + ':COVERAGE' PublicID,
		'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
		-- JCFIX ----------------
		'WCWorkersCompCov' as [Type]
		--'Statutory Workers' + CHAR(39) + ' Comp' as [Type]
	from CoverREF_temptable_consolidated temp_cons_pp
	left outer join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_cons_pp.LUWID
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	left outer join dm_stg_pc.dbo.pc_policyperiod pc_pp	on temp_cons_pp.PC_PP_ID=pc_pp.ID

	
	--Verified PC 3 sets - 3. Medical and other exp
	Insert into ccst_coverage
	select 
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		'EMICS:' + ltrim(Rtrim(cd.claim_number)) + 'Medical and other exp' + ':COVERAGE' PublicID,
		'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
		-- JCFIX ----------------
		'WCWorkersCompMED_icare' as [Type]
		--'Medical & Other Expenses' as [Type]
	from CoverREF_temptable_consolidated temp_cons_pp
	left outer join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_cons_pp.LUWID
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	left outer join dm_stg_pc.dbo.pc_policyperiod pc_pp	on temp_cons_pp.PC_PP_ID=pc_pp.ID



	--Verified CDR/Invalid 3 sets - 1. Workers Comp
	Insert into ccst_coverage
	select
	ltrim(rtrim(CD.Claim_number)) as LUWID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) +'Workers Comp' +':COVERAGE' PublicID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
	-- JCFIX ----------------
		'WCEmpLiabCov' as [Type]
		--'10029' as Type
	from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number))
	LEFT OUTER JOIN CoverREF_temptable_consolidated  CTC ON ltrim(rtrim(CD.Claim_Number))  = CTC.LUWID -- GXu 15/07
	where CD.is_Null=0
	    AND  CTC.LUWID IS NULL 
	--where CD.is_Null=0
	--	and ltrim(rtrim(CD.Claim_Number)) not in  --This to get claims that are NOT verified PC
	--	(select LUWID from CoverREF_temptable_consolidated) --This is list of all Verified PC Claims


	--Verified CDR/Invalid 3 sets - 2. Statutory Workers Comp
	Insert into ccst_coverage
	select
	ltrim(rtrim(CD.Claim_number)) as LUWID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) + 'Statutory Workers Comp' + ':COVERAGE' PublicID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
	-- JCFIX ----------------
		'WCWorkersCompCov' as [Type]
		--'10109' as Type
	from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number))
	LEFT OUTER JOIN CoverREF_temptable_consolidated  CTC ON ltrim(rtrim(CD.Claim_Number))  = CTC.LUWID -- GXu 15/07
	where CD.is_Null=0
	    AND  CTC.LUWID IS NULL  
	--where CD.is_Null=0
	--	and ltrim(rtrim(CD.Claim_Number)) not in  --This to get claims that are NOT verified PC
	--	(select LUWID from CoverREF_temptable_consolidated) --This is list of all Verified PC Claims

	--Verified CDR/Invalid 3 sets - 3. Medical and other exp
	Insert into ccst_coverage
	select
	ltrim(rtrim(CD.Claim_number)) as LUWID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) + 'Medical and other exp' + ':COVERAGE' PublicID,
	'EMICS:' + ltrim(rtrim(CD.Claim_number)) +':POLICY' as PolicyID,
	-- JCFIX ----------------
		'WCWorkersCompMED_icare' as [Type]
		--'10135' as Type
	from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number))
	LEFT OUTER JOIN CoverREF_temptable_consolidated  CTC ON ltrim(rtrim(CD.Claim_Number))  = CTC.LUWID -- GXu 15/07
	where CD.is_Null=0
	    AND  CTC.LUWID IS NULL 
	--where CD.is_Null=0
	--	and ltrim(rtrim(CD.Claim_Number)) not in  --This to get claims that are NOT verified PC
	--	(select LUWID from CoverREF_temptable_consolidated) --This is list of all Verified PC Claims

END