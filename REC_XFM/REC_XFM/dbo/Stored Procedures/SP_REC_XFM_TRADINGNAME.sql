﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_TRADINGNAME]

AS
BEGIN
	--COVER v12.32 ccst_tradingname_icare
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_tradingname_icare','Cover - v12.32 20/04/2019'

	Select 
	CASE WHEN PCX_PT.TradingName is not null then PCX_PT.TradingName
		WHEN PCX_PT.TradingName is null and PT.TRADING_NAME is not null Then PT.Trading_name
		ELSE 'REMOVE FROM SELECTION CRTIERA' --NOTE .. how to exclude this record?
		End as Name,
	PCX_PT.TradingName as Name_SRC_VALUE,
	PT.Trading_Name as AltName_SRC_VALUE,
	PCX_PT.Period as NamePeriod_SRC_VALUE,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':POLICY' As Policy,
	'EMICS:' + RTRIM(cd.Claim_Number) + '_' + convert(varchar,PCX_PT.ID) + ':POLICY' + ':TRADINGNAMEICARE' as PublicID,
	RTRIM(cd.Claim_Number)  as LUWID

	INTO ccst_tradingname_icare

	From CoverREF_temptable_consolidated temp_cons
	inner join DM_STG_EMICS.dbo.claim_detail cd on ltrim(Rtrim(CD.claim_number)) = temp_cons.LUWID
	left outer join DM_STG_Emics.dbo.policy_term_detail PT on PT.POLICY_NO = cd.policy_no
	left outer join DM_STG_PC.dbo.pc_policyperiod PC_PP on PC_PP.ID = temp_cons.pc_pp_id
	left outer join dm_stg_PC.dbo.pcx_policytradingname_icare PCX_PT on PCX_PT.period = PC_PP.ID
	--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

	where PCX_PT.ID is not null
	and (PCX_PT.TradingName is not null or ltrim(Rtrim(PT.trading_name)) <> '')
	and cd.is_null = 0

END