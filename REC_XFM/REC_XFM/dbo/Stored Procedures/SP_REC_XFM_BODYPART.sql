﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_BODYPART]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 15/05
-- Description:	Work Capacity CCST_bodypartdetails
-- Version:	15/05 v7.3 initial build
--			16/05 v8 WPIClaimedPart_icareID - updated transformation rule and added Selection criteria
--			20/06 v9.2 checked spec - no changes
-- =============================================
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_bodypartdetails','Work Capacity - v9.2'

SELECT

	'Please refer to Claim File Notes for more information' AS CompensabilityComments
	,Case when cad.Claim_Liability_Indicator = 1 then 01
		when cad.Claim_Liability_Indicator = 2 then 02
		when cad.Claim_Liability_Indicator = 5 then 05
		when cad.Claim_Liability_Indicator = 6 then 06
		when cad.Claim_Liability_Indicator = 7 then 07
		when cad.Claim_Liability_Indicator = 8 then 08
		when cad.Claim_Liability_Indicator = 9 then 09
		when cad.Claim_Liability_Indicator = 10 then 10
		when cad.Claim_Liability_Indicator = 11 then 11
		when cad.Claim_Liability_Indicator = 12 then 12
		End AS CompensabilityDecision
	,cad.Date_of_Liability AS CompensabilityDecisionDate
	,bpt.GW_DETAIL_TYPECODE as DetailedBodyPart --(Use cd.Location_of_Injury for lookup)
	,NULL AS DetailedBodyPartDesc

	,cad.WPI_Final AS ImpairmentPercentage
	,'EMICS:' + RTRIM(cd.Claim_Number) + ':INCIDENT' AS IncidentID
	,cd.Claim_Number AS LUWID
	,NULL AS Ordering
	,bpt.TGT_Name as PrimaryBodyPart
	,'EMICS:' + RTRIM(cd.Claim_Number) + ':BODYPART' AS PublicID
	,CASE 
		WHEN cad.Claim_Liability_Indicator IN ('2','8','11') THEN 1
		ELSE 0
	 END AS Settled_icare
	,NULL AS SideOfBody
	,Case when s66.claim_no is not null then 'EMICS:' + RTRIM(cd.Claim_Number) + ':WPIASSESSRECORD' 
	Else Null End AS WPIClaimedPart_icareID

into ccst_bodypartdetails

FROM DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd ON cad.Claim_No = cd.Claim_Number
--	INNER JOIN [DM_XFM].dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
	Left join [DM_XFM].dbo.DM_LKP_SRC_TGT_BODYPARTTYPE bpt on bpt.EMICS_CODE = cd.Location_of_Injury
	Left join DM_STG_EMICS.dbo.Section66 s66 on cad.claim_no = s66.claim_no --added 16/05
WHERE cd.is_Null = 0
	-- AND WPI <> -1 removed 15/05
END