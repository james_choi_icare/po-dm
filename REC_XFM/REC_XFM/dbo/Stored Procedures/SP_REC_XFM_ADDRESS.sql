﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 06/06/2019
-- Description:	Contacts - Named Insured V003 Main Contacts v2.17
-- 1. This is the master Stored Proc that will call the different contacts type ADDRESS stored proc such as
--    SP_REC_XFM_ADDRESS_EMPLOYER_STAGING
-- 20Jun2019 <Satish>: Added call to SP_REC_XFM_ADDRESS_MAINCONTACT_STAGING as well
-- Added code to create table ccst_address
-- 17Jul2019 <Satish>: Added code to create ER Unmatched (Verified PC) addresses
-- 17Jul2019 <Satish>: Added code to create rest of all (verified CDR) Unmatched addresses
-- 26Aug2019 <Satish>: 
--    (a) Modified code for Employer Verified CDR only for AddressLine1 - picking only 60 chars - there are only 90 claims with addresses more than 60 chars and we
--        can manually eliminate these claims as they will be different from XFM. Otherwise need to implement a very complex address clean logic which I am avoiding
--        for now.
-- 27Aug2019 <Satish>: 
--    (a) Modified code for Main Concact - ER unmatched and Rest of all Verified CDR sections for AddressLine1 - picking only 60 chars - there are only quite a few 
--        claims with addresses more than 60 chars and hopefully we can manually eliminate these claims as they will be different from XFM. Trying to avoid implementing
--        a very complex address clean logic.
-- 17Sep2019 <Satish>: As part of DMIG-8106 
--    (a) Modified code for Main Concact - ER unmatched and Rest of all Verified CDR sections for AddressLine1 - applying REPLACE functions to remove Carriage Return 
--        (char(13) and Line Feed (Char(10). Example Main Contact:EMICS:1591791:INSURED_MC:BUSINESSADDRESS
-- 
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_ADDRESS]
	
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_address', 'EMP_v3.8 MC_v2.17'
EXEC SP_REC_XFM_ADDRESS_EMPLOYER_STAGING
EXEC SP_REC_XFM_ADDRESS_MAINCONTACT_STAGING
--EXEC [SP_REC_XFM_ADDRESS_POLICYLOCATION_STAGING] --no longer used as it is now part of Policy Location SP
--EXEC SP_REC_XFM_CONTACT_INJURED_WORKER
;

IF OBJECT_ID('ccst_address') is not null
drop table dbo.ccst_address;

--Creating the QA ccst_address table first
CREATE TABLE [dbo].[ccst_address](
	[PublicID] [varchar](50) NULL,
	[AddressBookUID] [varchar](64) NULL,
	[AddressLine1] [varchar](60) NULL,
	[AddressLine1Kanji] [varchar](60) NULL,
	[AddressLine2] [varchar](60) NULL,
	[AddressLine2Kanji] [varchar](60) NULL,
	[AddressLine3] [varchar](60) NULL,
	[AddressType] [varchar](50) NULL,
	[BatchGeocode] [bit] NULL,
	[CEDEX] [bit] NULL,
	[CEDEXBureau] [varchar](2) NULL,
	[City] [varchar](40) NULL,
	[CityKanji] [varchar](60) NULL,
	[Country] [varchar](50) NULL,
	[County] [varchar](500) NULL,
	[DPID_icare] [int] NULL,
	[Description] [varchar](255) NULL,
	[ExternalLinkID] [int] NULL,
	[IsAddressFromPC_icare] [int] NOT NULL,
	[IsValidated_icare] [bit] NULL,
	[ObfuscatedInternal] [int] NOT NULL,
	[PostalCode] [varchar](20) NULL,
	[Retired] [int] NOT NULL,
	--[StandardizedAddressID_icare] [int] NULL,
	[StandardizedAddressID_icare] [varchar](255) NULL,
	[State] [varchar](50) NULL,
	[SubType] [varchar](50) NULL,
	[ValidUntil] [datetime2](7) NULL,
	[LUWID] [varchar](19) NULL
) ON [PRIMARY]


--Inserting Employer (verified PC) addresses
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
--REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedCDR]
--FROM REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedPC]
[dbo].[AddressREF_temptable_employer_verifiedPC]
;


--Inserting Employer (verified CDR) addresses
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	substring([AddressLine1],1,60) as [AddressLine1], 
	--26/08/2019: As part of MDIG-8003, modified to pick only 60 chars asthere are some postal addressess > 60 chars
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
--REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedCDR]
--FROM REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedPC]
[dbo].[AddressREF_temptable_employer_verifiedCDR]
;

--Inserting MC (E,R, W,O,P) (matched) addresses
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
--REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedCDR]
--FROM REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedPC]
[dbo].[AddressREF_temptable_maincontact_all_matched]
;

--Inserting MC (E,R) (UNmatched) (Verified PC) addresses
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
	[PublicID] ,
	[AddressBookUID],
	--substring ([AddressLine1],1,60) as [AddressLine1], 
	substring(replace(replace(addressline1,char(13),''),char(10),''),1,60) as [AddressLine1],
	--28Aug2019 The address clean logic is quite complex to implement in QA. So just picking first 60 chars for now. Hopefully we can eyeball the differences and confirm they are ok.
	--17Sep2019 The addressline1 sometimes has Carriage Return and so to remove that appying the REPLACE function
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
--REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedCDR]
--FROM REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedPC]
dbo.AddressREF_temptable_Address_maincontact_ER_verifiedPC_unmatched
;


--Inserting Rest of all (UNmatched) (Verified CDR) addresses
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	--substring ([AddressLine1],1,60) as [AddressLine1], 
	substring(replace(replace(addressline1,char(13),''),char(10),''),1,60) [AddressLine1],
	--28Aug2019 The address clean logic is quite complex to implement in QA. So just picking first 60 chars for now. Hopefully we can eyeball the differences and confirm they are ok.
	--17Sep2019 The addressline1 sometimes has Carriage Return and so to remove that appying the REPLACE function
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
--REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedCDR]
--FROM REC_XFM.[dbo].[ContactREF_temptable_employer_verifiedPC]
[dbo].[AddressREF_temptable_Address_maincontact_rest_of_all_verifiedCDR_unmatched]
;


--Inserting Policy Location addresses - these addresses are reference in ccst_policylocation
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
[dbo].[AddressREF_temptable_policylocation_all]
;

END