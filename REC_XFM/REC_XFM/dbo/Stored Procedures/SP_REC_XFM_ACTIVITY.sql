﻿-- =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description: Activity Workflow ccst_activity
-- Version: Initial table build v8 , users and groups v1.0
--			Added notask in sc v8.1 04/07/2019
--			19/07/2019 fixed typo on Implorance field v8.2
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_ACTIVITY]
AS
BEGIN
--drop table ccst_activity
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_activity','Activity Workflow - v8.2 19/07/2019'

Select
	'task' as ActivityClass,
	 RTRIM(tm.claimNumber) as LUWID,
	ap.PublicID as ActivityPatternID,--(select PublicID from cc_activityPattern where retired = 0 and code = 'generaluse') 
	
	coalesce (cc_user.PublicID, cc_user_sm.PublicID) as AssignedByUserID,
	Case when cc_user2.PublicID is not null OR cc_assignqueue.PublicID is not null then cc_group2.PublicID
		Else cc_group.PublicID
		End as AssignedGroupID,
	ISNULL(cc_assignqueue.PublicID,NULL) as AssignedQueueID, --v1.2
	coalesce (cc_user2.PublicID , cc_user.PublicID, cc_user_sm.PublicID) as AssignedUserID,
	
	tm.CreateDate as AssignmentDate,
	'assigned' as AssignmentStatus,
	'EMICS:' + RTRIM(tm.claimNumber) as ClaimID,
	LEFT(CASE WHEN tm.ApplicationName = 1 and len(LTRIM(RTRIM(dbo.StripHTML(tm.BodyText)))) < 5 
		Then 'Document Type: ' + tm.DocumentType + CHAR(10) + 'Document No: '+ tm.EntityNumber + CHAR(10) + 'Action Name: ' + tm.ActionName
		When tm.ApplicationName <> 1 and len(LTRIM(RTRIM(dbo.StripHTML(tm.BodyText)))) < 5 then tm.Subject
		Else dbo.StripHTML(LTRIM(RTRIM(tm.BodyText)))
		End,1333) As Description,
	'false' as ExternalOwned,
	Case when TM.Importance = 1 then 'normal'
		When tm.Importance = 2 then 'high'
		When tm.Importance = 3 then 'urgent'
		Else 'low'
		End as Priority,
	'EMICS:' + convert(varchar, RTRIM(tm.ClaimNumber)) + '_' + convert(varchar(50), TaskID) + ':TASK' as PublicID,
	'open' as Status,
	'Activity' as Subtype,
	'general' as Type,
	'notOnCalendar' as Importance,
	'payment' as ValidationLevel

Into ccst_activity

From DM_STG_TASKMGR.dbo.TM_Task_Incomplete tm --37718
Inner join DM_STG_EMICS.dbo.Claim_Detail cd on tm.ClaimNumber = cd.claim_number
Left outer join [DM_XFM].[dbo].[DM_LKP_GW_CC_ACTIVITYPATTERN] ap on retired = 0 and code = 'generaluse'


LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cad.Claims_Officer)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co2  ON (co2.alias = tm.OwnerAlias)

LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)

LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
		ON (tm.OwnerAlias = cntrl2.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID) -- Task OwnerAlias PublicID

left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups on (lkp_eml_team_gwcc_groups.EML_TEAM = co.grp)
left join DM_STG_CC.[dbo].[cc_group] cc_group on ( cc_group.Name = lkp_eml_team_gwcc_groups.gwcc_group )

left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups2 on (lkp_eml_team_gwcc_groups2.EML_TEAM = co2.grp)
left join DM_STG_CC.[dbo].[cc_group] cc_group2 on ( cc_group2.Name = lkp_eml_team_gwcc_groups2.gwcc_group )


left join DM_XFM.[dbo].[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on (tm.OwnerAlias = lkp_eml_gwcc_queues.EMLQueueUser)
left join DM_STG_CC.[dbo].[cc_assignqueue] cc_assignqueue on (lkp_eml_gwcc_queues.GWCC_Queue = cc_assignqueue.Name) and cc_assignqueue.retired = 0

Where cd.is_null = 0 and  tm.OwnerAlias<>'NoTask'




END