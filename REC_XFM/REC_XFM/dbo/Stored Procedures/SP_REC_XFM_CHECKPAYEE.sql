﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CHECKPAYEE] AS

BEGIN
	/*========================================
		Author: Ann
		Created Date:
		Domain: Payments and recoveries - ccst_checkpayee v40.3
		Comments: 13/05 checked against v45.6 AG no change
					Checked against v46.3.9 11/07/2019
				  04/09 updated checkid and publicid xform, added cpc SC
	========================================*/	

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_checkpayee','Payments and Recoveries - v47.2 04/09/2019'
	--drop table ccst_checkpayee

	SELECT distinct
		CASE WHEN CHEQUE_NO IS  NULL THEN 'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+ISNULL(convert(varchar,cpc.ID),'00') + ':CHECK'
			ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+ISNULL(convert(varchar,cpc.ID),'00') + ':CHECK'
		END as CheckID,
		CASE WHEN CPR.PAYEE_TYPE =1 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'
			WHEN CPR.PAYEE_TYPE in (4,3) THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,CPR.Payee_Code)+':VENDOR'
			WHEN PAYEE_TYPE=2 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INSURED'
			WHEN PAYEE_TYPE=23 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,DP.ID) + ':DEPENDENT'
			ELSE NULL 
			End as ClaimContactID,
		CPR.Claim_number as LUWID,
		CASE WHEN CPR.PAYEE_TYPE =1 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'
			WHEN CPR.PAYEE_TYPE in (4,3) THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,CPR.Payee_Code)+':VENDOR'
			WHEN PAYEE_TYPE=2 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INSURED'
			WHEN PAYEE_TYPE=23 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,DP.ID) + ':DEPENDENT'
			ELSE NULL
			End as PayeeDenormID,
		CASE WHEN PAYEE_TYPE='1' THEN 'claimant'
			WHEN PAYEE_TYPE in ('3','4') THEN 'vendor'
			WHEN PAYEE_TYPE='2'THEN 'insured'
			else 'other'
			end AS Payeetype,
		'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+CONVERT(varchar,CPR.Payee_Type)+'_'+ISNULL(convert(varchar,cpc.ID),'00') + ':CHECKPAYEE' as PublicID

into ccst_checkpayee

FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
INNER JOIN (select * from DM_STG_EMICS.dbo.Payment_Recovery) PR on CPR.Claim_number=PR.Claim_No AND CPR.Payment_no=PR.Payment_no
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON CD.Claim_Number=CPR.Claim_number
LEFT OUTER JOIN DM_STG_EMICS.dbo.DEPENDENTS DP ON CPR.Payee_Code=DP.Creditor_no
LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_STG_CC..CCX_paycodepaymenttype_icare A,
 (select * from DM_STG_CC..CCX_paycode_icare where Retired=0) B, DM_STG_CC..cctl_paymenttype_icare C
WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC
ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)

WHERE CD.is_Null=0 AND PR.Estimate_type<70
and  ISNULL(CPR.Cheque_Status,'00') not in (3,6) and Reversed<>1
and (pr.garnishee_no is null or (pr.garnishee_no is not null and pr.Trans_Amount>0));


END