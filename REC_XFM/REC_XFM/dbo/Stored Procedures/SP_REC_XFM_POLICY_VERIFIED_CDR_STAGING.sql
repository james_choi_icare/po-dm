﻿-- =============================================
-- Author:		Saranya Jayaseelan
-- Create date: 5/4/2019
-- Mapping Version	 : 12.32
-- Questions to Satish 1)  Check with Satish if this is to check verified PC records
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_POLICY_VERIFIED_CDR_STAGING]
	
AS
BEGIN

WITH temptable_CDR as (

		select Z.*
		from
		(
		select row_number () over (partition by luwid,rownum order by RENEWAL_NO desc) latest, Y.* --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with different period start dates
		from
		(
		select
		X.*
		from
		(
		select 
		distinct
		Case when CD.is_gst_registered=1 then 100 Else 0 end as ITCPercentagePEL,
		'WorkersComp' as PolicyType,
		'Standard' as PolicyType_icare,			
		NULL as PC_PP_ID,	
		PD.PERIOD_START as PD_EffectiveDate,
		PD.PERIOD_EXPIRY as PD_ExpirationDate,	
		concat ('EMICS_', ltrim(rtrim(CD.Policy_No))) as AccountNumber,	
		PD.RENEWAL_NO,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		--CD.itc_pcent,
		LTRIM(RTRIM(CD.Policy_No)) as PolicyNumberCD
		--PT.icare_policy_no as PT_icare_PolicyNumber,
		--PT.ID as PT_ID,
		--PT.CANCELLED_DATETIME
		,PD.rownum
		--,PD.POLICY_YEAR
		
		--,NULL as EmployerCategory_icare
		--,NULL as LabourHire_icare
		--, NULL as ProducerCode
		--,NULL as ProducerName_icare
		,NULL as Status
		from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
		left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
			on PT.POLICY_NO = CD.Policy_No
		left outer join  
		(select row_number () over (partition by PD.POLICY_NO,PD.PERIOD_START order by PD.RENEWAL_NO desc) rownum, PD.POLICY_NO, PD.PERIOD_START,
		PD.PERIOD_EXPIRY,PD.RENEWAL_NO, PD.POLICY_YEAR from DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD) PD --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with same period start dates
			on PD.POLICY_NO = PT.POLICY_NO
				--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
		-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
			  and CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
		left outer join DM_STG_CDR.[dbo].[DM_STG_CDR_D_POLICY_MASTER] CDR_PM -- CDR Policy Master table
		on ltrim(rtrim(CDR_PM.POLICY_SOURCE_NUMBER))=ltrim(rtrim(CD.Policy_No))
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		where CD.is_Null=0
		and ltrim(rtrim(CD.Claim_number)) NOT in (select LUWID from ccst_interm_policy_PC) 
		and CDR_PM.POLICY_SOURCE_NUMBER is not null
		) X
		where (X.rownum=1 or X.rownum is null)
		)Y
		)Z
		where Z.latest=1
)
,
 temptable_ccst_policy_VerifiedCDR as  
(
-- 1st part of the SQL - This is to fetch all Verified CDR record that have EMICS PREMIUM_DETAIL matching records i.e. we can get cover start and end dates from EMICS
		select 
		--distinct
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,	
		tmp.PD_EffectiveDate as Cover_EffectiveDate,
		tmp.PD_ExpirationDate as Cover_ExpirationDate,	
		tmp.RENEWAL_NO,					
		tmp.LUWID,
		tmp.Date_of_Injury	
		,tmp.LUWID as Claim_Number		
		,tmp.AccountNumber
		--tmp.itc_pcent,
		,LTRIM(RTRIM(tmp.PolicyNumberCD))	as PolicyNumberCD	
		,tmp.rownum		
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		--tmp.PT_icare_PolicyNumber,
		--tmp.PT_ID,
		--tmp.CANCELLED_DATETIME,
		'EMICS' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		--,tmp.POLICY_YEAR
		,NULL as Status
		from coverref_temptable_CDR tmp   --2254 2463
		where PD_EffectiveDate is NOT NULL  --picking ony those verified CDR claims for which we DO have Cover dates in EMICS
	union
-- 2nd part of the SQL - This is to fetch all Verified CDR record that DO NOT have EMICS PREMIUM_DETAIL matching records i.e. we CANNOT get cover start and end dates from EMICS
-- so we go and get the cover dates from matching records in CDR instead
		select 
		--distinct
		
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,		
		CONVERT(DATETIME2, CONVERT(VARCHAR(10),rp.COVER_COMMENCE_DATE_ID,120)) AS Cover_EffectiveDate,
		CONVERT(DATETIME2, CONVERT(VARCHAR(10),ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID),120)) AS Cover_ExpirationDate,
		rp.POLICY_ID as RENEWAL_NO,
		tmp.LUWID,		
		tmp.Date_of_Injury,
		tmp.LUWID as Claim_Number,
		--tmp.Currency,
		--tmp.is_gst_registered,
		--tmp.Retired,
		tmp.AccountNumber,
		--tmp.PD_EffectiveDate,
		--tmp.PD_ExpirationDate,
		--tmp.itc_pcent,
		LTRIM(RTRIM(tmp.PolicyNumberCD)) as PolicyNumberCD			
		,tmp.rownum
		 
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		--tmp.PT_icare_PolicyNumber,
		--tmp.PT_ID,
		--tmp.CANCELLED_DATETIME,
		'CDR' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		--tmp.POLICY_YEAR		
		,NULL as Status
		from Coverref_temptable_CDR tmp   --2254 2463
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
	union
-- 3rd part of the SQL - This is to fetch all Verified CDR record that DO NOT have either EMICS PREMIUM_DETAIL matching records nor CDR DM_STG_CDR_E_ROLLING_POLICY matching records
-- so we CANNOT get cover dates from either EMICS or CDR. Hence we are defaulting the cover dates to (Date_Of_Injury-1) and (Date_Of_Injury+1).
		select 
		--distinct
		
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,
		CAST(DATEADD(day,-1,tmp.Date_of_Injury) as DATE) AS Cover_EffectiveDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
		CAST(DATEADD(day,1,tmp.Date_of_Injury) as DATE) AS Cover_ExpirationDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
--DATEAD
		convert(varchar(10),tmp.Date_of_Injury,112) as RENEWAL_NO,
		tmp.LUWID,
				tmp.Date_of_Injury,
		tmp.LUWID as Claim_Number,
		--tmp.Currency,
		--tmp.is_gst_registered,
		--tmp.Retired,
		tmp.AccountNumber,
		--tmp.PD_EffectiveDate,
		--tmp.PD_ExpirationDate,

		--DATEADD(day,-1,tmp.Date_of_Injury)  AS Cover_EffectiveDate,
		--DATEADD(day,1,tmp.Date_of_Injury)  AS Cover_ExpirationDate,
		--tmp.LUWID as RENEWAL_NO,
		--tmp.itc_pcent,
		LTRIM(RTRIM(tmp.PolicyNumberCD)) as PolicyNumberCD	,		
		tmp.rownum	
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		--tmp.PT_icare_PolicyNumber,
		--tmp.PT_ID,
		--tmp.CANCELLED_DATETIME,
		'NOT_IN_EMICS_OR_CDR' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		--tmp.POLICY_YEAR		
		,NULL as Status
		from coverref_temptable_CDR tmp   
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		LEFT OUTER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID		
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		and rp.POLICY_ID is NULL -- no match found for cover dates in CDR too so we then need to go for default cover dates !
)
/*
(
-- 1st part of the SQL - This is to fetch all Verified CDR record that have EMICS PREMIUM_DETAIL matching records i.e. we can get cover start and end dates from EMICS
select 
--distinct
tmp.LUWID,
tmp.LUWID as Claim_Number,
--tmp.Currency,
tmp.ITCPercentagePEL,
--tmp.is_gst_registered,
tmp.PolicyType,
tmp.PolicyType_icare,
--tmp.Retired,
tmp.AccountNumber,
tmp.PD_EffectiveDate as Cover_EffectiveDate,
tmp.PD_ExpirationDate as Cover_ExpirationDate,
tmp.RENEWAL_NO,
tmp.Date_of_Injury,
--tmp.itc_pcent,
tmp.PolicyNumberCD,
--tmp.PT_icare_PolicyNumber,
--tmp.PT_ID,
--tmp.CANCELLED_DATETIME,
'EMICS' as Cover_period_dates_source_flag,
tmp.PublicID
--,tmp.rownum
--,tmp.POLICY_YEAR
from temptable_CDR tmp   --2254 2463
	where PD_EffectiveDate is NOT NULL  --picking ony those verified CDR claims for which we DO have Cover dates in EMICS
union

-- 2nd part of the SQL - This is to fetch all Verified CDR record that DO NOT have EMICS PREMIUM_DETAIL matching records i.e. we CANNOT get cover start and end dates from EMICS
-- so we go and get the cover dates from matching records in CDR instead
select 
--distinct
tmp.LUWID,
tmp.LUWID as Claim_Number,
--tmp.Currency,
tmp.ITCPercentagePEL,
--tmp.is_gst_registered,
tmp.PolicyType,
tmp.PolicyType_icare,
--tmp.Retired,
tmp.AccountNumber,
--tmp.PD_EffectiveDate,
--tmp.PD_ExpirationDate,
CONVERT(DATETIME2, CONVERT(VARCHAR(10),rp.COVER_COMMENCE_DATE_ID,120)) AS Cover_EffectiveDate,
CONVERT(DATETIME2, CONVERT(VARCHAR(10),ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID),120)) AS Cover_ExpirationDate,
rp.POLICY_ID as RENEWAL_NO,
tmp.Date_of_Injury,
--tmp.itc_pcent,
tmp.PolicyNumberCD,
--tmp.PT_icare_PolicyNumber,
--tmp.PT_ID,
--tmp.CANCELLED_DATETIME,
'CDR' as Cover_period_dates_source_flag,
tmp.PublicID
--tmp.rownum,
--tmp.POLICY_YEAR
from temptable_CDR tmp   --2254 2463
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
	union
-- 3rd part of the SQL - This is to fetch all Verified CDR record that DO NOT have either EMICS PREMIUM_DETAIL matching records nor CDR DM_STG_CDR_E_ROLLING_POLICY matching records
-- so we CANNOT get cover dates from either EMICS or CDR. Hence we are defaulting the cover dates to (Date_Of_Injury-1) and (Date_Of_Injury+1).
select 
--distinct
tmp.LUWID,
tmp.LUWID as Claim_Number,
--tmp.Currency,
tmp.ITCPercentagePEL,
--tmp.is_gst_registered,
tmp.PolicyType,
tmp.PolicyType_icare,
--tmp.Retired,
tmp.AccountNumber,
--tmp.PD_EffectiveDate,
--tmp.PD_ExpirationDate,
CAST(DATEADD(day,-1,tmp.Date_of_Injury) as DATE) AS Cover_EffectiveDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
CAST(DATEADD(day,1,tmp.Date_of_Injury) as DATE) AS Cover_ExpirationDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
--DATEADD(day,-1,tmp.Date_of_Injury)  AS Cover_EffectiveDate,
--DATEADD(day,1,tmp.Date_of_Injury)  AS Cover_ExpirationDate,
--tmp.LUWID as RENEWAL_NO,
convert(varchar(10),tmp.Date_of_Injury,112) as RENEWAL_NO,
tmp.Date_of_Injury,
--tmp.itc_pcent,
tmp.PolicyNumberCD,
--tmp.PT_icare_PolicyNumber,
--tmp.PT_ID,
--tmp.CANCELLED_DATETIME,
'NOT_IN_EMICS_OR_CDR' as Cover_period_dates_source_flag,
tmp.publicID
--tmp.rownum,
--tmp.POLICY_YEAR
from temptable_CDR tmp   
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		LEFT OUTER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		and rp.POLICY_ID is NULL -- no match found for cover dates in CDR too so we then need to go for default cover dates !
)

*/
 select * into [ccst_interm_policy_CDR] 
from temptable_ccst_policy_VerifiedCDR


END