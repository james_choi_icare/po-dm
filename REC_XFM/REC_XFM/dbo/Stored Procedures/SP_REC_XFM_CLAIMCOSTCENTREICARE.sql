﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMCOSTCENTREICARE]
AS
BEGIN
-- =============================================
-- Author: Ann
-- Modified date:
-- Domain: COVER domain ccst_claimcostcentreicare
-- Comments: v12.35 
--			13/05 checked v12.36 no change
-- =============================================
	
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimcostcentreicare', 'Cover - v12.36 13/05/2019'


	--Verified PC and matched WIC
	Select 
	ltrim(rtrim(CD.claim_number)) as LUWID,
	concat('EMICS:',LTRIM(rTRIM(CD.Claim_number)),'_',PC_CC.ID,':POLICY',':CLAIMCOSTCENTREICARE') as PublicID,
	Concat('EMICS:',LTRIM(RTRIM(CD.claim_number)),'_',PC_CC.ID,':POLICY',':COSTCENTREICARE') as ForeignentityID,
	concat('EMICS:',+Ltrim(Rtrim(CD.claim_number))) as OwnerID,
	'VerifiedPC and matchedWIC' as PublicIDType_SRC_VALUE

	Into ccst_claimcostcentreicare

	from CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
	inner join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_WIC_matched.Claim_Number
	left outer join dm_stg_pc.dbo.pcx_costcenter_icare PC_CC on PC_CC.ID=temp_WIC_matched.CostCenter_icare
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	WHERE CD.is_null = 0



	Insert into ccst_claimcostcentreicare
	--Verified CDR/Invalid/Unmatched WIC ccst_claimcostcentreicare 
	select
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		concat('EMICS:', LTRIM(RTRIM(CD.Claim_Number)) ,':POLICY',':CLAIMCOSTCENTREICARE') as PublicID,
		concat('EMICS:', ltrim(rtrim(CD.Claim_Number)), ':POLICY',':COSTCENTREICARE') as ForeignentityID,
		concat('EMICS:', ltrim(rtrim(CD.Claim_Number)) ) as OwnerID,
		'VerifiedCDR' as PublicIDType_SRC_VALUE

	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
--	Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS IC on IC.Claim_number = CD.claim_number
	where ltrim(rtrim(CD.Claim_Number)) in
	(
	select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_unmatched_WIC
	union 
	select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_verified_CDR
	)
	
END