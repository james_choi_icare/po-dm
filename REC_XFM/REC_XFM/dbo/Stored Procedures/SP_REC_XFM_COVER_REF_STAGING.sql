﻿
-- =============================================
-- Author:		Ann Go
-- 
-- 1. <22Jul2019><Satish>: Based on James diff report, modified  CoverREF_temptable_CDR for ITCPercentagePEL to be based
--    off is_registered_for_gst from EMICS CLIAM_DETAL table for Verified CDR claims.
-- 2. <13Aug2019><Satish>: As part of DMIG-7812, had to -
--    (a) Add new tables pc_policyline and pctl_policyline to the selections SQLs for CoverREF_temptable_pc_pp, CoverREF_temptable_pc_pp_1,
--        CoverREF_temptable_pc_pp_2 and CoverREF_temptable_pc_pp_3. These were missing from the QA code whereas were always present in the 
--        mapping spec. Also ensured the "PolicyType" is not hard coded to "WorkersComp" in these Selection SQLs and instead derived from 
--        pc_policyline.NAME.
--    <15Aug2019><Satish>: As part of Cohort2 QA build, had to -
--    (a) Add [dbo].[ADM_Claim_Cohort] as inner join to some of the selection SQLs below to ensure, we pick only Cohort2 claims for processing.
--        If we don't add this table as a filter, the publicid for tables such as ccst_policyperiod gets affected as the max(claim_number) then 
--        is different in REC_XFM compared to what ETL populate in DM_XFM.     
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_COVER_REF_STAGING]
AS
BEGIN
	--Verified CDR/Invalid/Unmatched WIC ccst_claimcostcentreicare 
---Verified PC
--Getting policy matches using Policy_term_detail.icare_policy_no against pc_policyperiod.policy_number
--Getting policy matches using Policy_term_detail.icare_policy_no against pc_policyperiod.policy_number

IF OBJECT_ID('CoverREF_temptable_pc_pp') is not null
drop table CoverREF_temptable_pc_pp
select
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
'POLICY_TERM_DETAIL policy_number_icare with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE
into CoverREF_temptable_pc_pp
from
(
select
PT.ACN_ARBN as ACN_icare,
0 as AssignedRisk,
'aud' as Currency,
CD.itc_pcent as ITCPercentagePEL,
--'WorkersComp' as PolicyType,--13Aug2019 commented this out as it shuld not be hard code as per Verified PC logic in spec
CASE 
WHEN PCTL_PL.NAME='icare Workers'' Comp' then 'WorkersComp'
ELSE 'bad data'
END
as PolicyType,--13Aug2019 Added this in as part of DMIG-7812
'Standard' as PolicyType_icare,
0 as Retired,
PC_PP.ID AS PC_PP_Id,
PC_PP.CancellationDate,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PD.PERIOD_START as PD_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
PD.PERIOD_EXPIRY as PD_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
PT.icare_policy_no as PT_icare_PolicyNumber,
PT.ID as PT_ID,
PT.POLICY_STATUS as PT_Policy_Status,
PC_PP.TermNumber ,
EmployerCategory_icare
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2 
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
on PT.POLICY_NO = CD.Policy_No
left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
    on PD.POLICY_NO = PT.POLICY_NO
        --and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
    and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
left outer join 
(
select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate, EmployerCategory_icare
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 --bound
and MostRecentModel=1
and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP
on (PT.icare_policy_no=PC_PP.PolicyNumber )
--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
--on PT.icare_policy_no=PC_PP.PolicyNumber
--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
left outer join [DM_STG_PC].dbo.pc_policyline PC_PL on PC_PL.BranchID=PC_PP.ID and PC_PL.ExpirationDate is null
left outer join [DM_STG_PC].dbo.pctl_policyline PCTL_PL on PCTL_PL.ID=PC_PL.Subtype
--13Aug2019 As part of DMIG-7812, added this selection sql
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
and (PC_PP.ID is not null)
) X



IF OBJECT_ID('CoverREF_temptable_pc_pp_1') is not null
drop table CoverREF_temptable_pc_pp_1
select
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
'CD Policy_no with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE
into CoverREF_temptable_pc_pp_1
from
(
select
PT.ACN_ARBN as ACN_icare,
0 as AssignedRisk,
'aud' as Currency,
CD.itc_pcent as ITCPercentagePEL,
--'WorkersComp' as PolicyType,--13Aug2019 commented this out as it shuld not be hard code as per Verified PC logic in spec
CASE 
WHEN PCTL_PL.NAME='icare Workers'' Comp' then 'WorkersComp'
ELSE 'bad data'
END
as PolicyType,--13Aug2019 Added this in as part of DMIG-7812
'Standard' as PolicyType_icare,
0 as Retired,
PC_PP.ID AS PC_PP_Id,
PC_PP.CancellationDate,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PD.PERIOD_START as PD_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
PD.PERIOD_EXPIRY as PD_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
PT.icare_policy_no as PT_icare_PolicyNumber,
PT.ID as PT_ID,
PT.POLICY_STATUS as PT_Policy_Status,
PC_PP.TermNumber ,
EmployerCategory_icare
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
on PT.POLICY_NO = CD.Policy_No
left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
    on PD.POLICY_NO = PT.POLICY_NO
        --and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
    and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
left outer join 
(
select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate, EmployerCategory_icare
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 --bound
and MostRecentModel=1
and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP
--on (PT.icare_policy_no=PC_PP.PolicyNumber )
on (ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber )
--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
--on PT.icare_policy_no=PC_PP.PolicyNumber
--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
left outer join [DM_STG_PC].dbo.pc_policyline PC_PL on PC_PL.BranchID=PC_PP.ID and PC_PL.ExpirationDate is null
left outer join [DM_STG_PC].dbo.pctl_policyline PCTL_PL on PCTL_PL.ID=PC_PL.Subtype
--13Aug2019 As part of DMIG-7812, added this selection sql
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
and (PC_PP.ID is not null)
) X

--Getting policy matches using Claim_detail.Policy_No and matching against pc_policyperiod.legacypolicynumber_icare
IF OBJECT_ID('CoverREF_temptable_pc_pp_2') is not null
drop table CoverREF_temptable_pc_pp_2
select
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
POLICY_STATUS,
TermNumber,
EmployerCategory_icare,
'CD Policy_no with PC_PP LEGACY PolicyNumber match' as POLICY_MATCH_TYPE
into CoverREF_temptable_pc_pp_2
from
(
select
PT.ACN_ARBN as ACN_icare,
0 as AssignedRisk,
'aud' as Currency,
CD.itc_pcent as ITCPercentagePEL,
--'WorkersComp' as PolicyType,--13Aug2019 commented this out as it shuld not be hard code as per Verified PC logic in spec
CASE 
WHEN PCTL_PL.NAME='icare Workers'' Comp' then 'WorkersComp'
ELSE 'bad data'
END
as PolicyType,--13Aug2019 Added this in as part of DMIG-7812
'Standard' as PolicyType_icare,
0 as Retired,
PC_PP.ID AS PC_PP_Id,
PC_PP.CancellationDate,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PD.PERIOD_START as PD_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
PD.PERIOD_EXPIRY as PD_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
PT.icare_policy_no as PT_icare_PolicyNumber,
PT.ID as PT_ID,
PT.POLICY_STATUS,
PC_PP.TermNumber ,
EmployerCategory_icare
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
on PT.POLICY_NO = CD.Policy_No
left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
    on PD.POLICY_NO = PT.POLICY_NO
        --and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
    and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
left outer join 
(
select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate, EmployerCategory_icare
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 --bound
and MostRecentModel=1
and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP
--on (PT.icare_policy_no=PC_PP.PolicyNumber )
on (ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
--on PT.icare_policy_no=PC_PP.PolicyNumber
--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
left outer join [DM_STG_PC].dbo.pc_policyline PC_PL on PC_PL.BranchID=PC_PP.ID and PC_PL.ExpirationDate is null
left outer join [DM_STG_PC].dbo.pctl_policyline PCTL_PL on PCTL_PL.ID=PC_PL.Subtype
--13Aug2019 As part of DMIG-7812, added this selection sql
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
and (PC_PP.ID is not null)
) X


--28/01 DMIG-731 Checking to see if we can strip 701 from CD.PolicyNo and try and match it against PC PolicyNumber
IF OBJECT_ID('CoverREF_temptable_pc_pp_3') is not null
drop table CoverREF_temptable_pc_pp_3
select
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
'(701) CD Policy_no with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE
into CoverREF_temptable_pc_pp_3
from
(
select
PT.ACN_ARBN as ACN_icare,
0 as AssignedRisk,
'aud' as Currency,
CD.itc_pcent as ITCPercentagePEL,
--'WorkersComp' as PolicyType,--13Aug2019 commented this out as it shuld not be hard code as per Verified PC logic in spec
CASE 
WHEN PCTL_PL.NAME='icare Workers'' Comp' then 'WorkersComp'
ELSE 'bad data'
END
as PolicyType,--13Aug2019 Added this in as part of DMIG-7812
'Standard' as PolicyType_icare,
0 as Retired,
PC_PP.ID AS PC_PP_Id,
PC_PP.CancellationDate,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PD.PERIOD_START as PD_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
PD.PERIOD_EXPIRY as PD_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
PT.icare_policy_no as PT_icare_PolicyNumber,
PT.ID as PT_ID,
PT.POLICY_STATUS as PT_Policy_Status,
PC_PP.TermNumber,
EmployerCategory_icare 
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
on PT.POLICY_NO = CD.Policy_No
left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
    on PD.POLICY_NO = PT.POLICY_NO
        --and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
    and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
left outer join 
(
select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate, EmployerCategory_icare
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 --bound
and MostRecentModel=1
and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP
--on (PT.icare_policy_no=PC_PP.PolicyNumber
on substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3)=PC_PP.PolicyNumber
--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
--on PT.icare_policy_no=PC_PP.PolicyNumber
--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
left outer join [DM_STG_PC].dbo.pc_policyline PC_PL on PC_PL.BranchID=PC_PP.ID and PC_PL.ExpirationDate is null
left outer join [DM_STG_PC].dbo.pctl_policyline PCTL_PL on PCTL_PL.ID=PC_PL.Subtype
--13Aug2019 As part of DMIG-7812, added this selection sql
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701')
and (PC_PP.ID is not null)
) X

--select distinct PolicyType from CoverREF_temptable_pc_pp_3

--IF OBJECT_ID('tempdb..#temptable_consolidated') is not null
IF OBJECT_ID('CoverREF_temptable_all') is not null
--drop table #temptable_consolidated
drop table CoverREF_temptable_all
select
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
POLICY_MATCH_TYPE
--into #temptable_consolidated
into CoverREF_temptable_all
from
(
	select
	row_number() over(partition by X.LUWID,X.PC_PP_ID  order by X.PD_EffectiveDate desc) as RowNum,
	X.*
	from
	(
	select * from CoverREF_temptable_pc_pp
	union
	(
		select a.* from CoverREF_temptable_pc_pp_1 a
		left outer join CoverREF_temptable_pc_pp b
		on a.LUWID=b.LUWID 
		left outer join CoverREF_temptable_pc_pp_3 c  --28/01 added this join
		on a.LUWID=c.LUWID 
		--left outer join #temptable_pc_pp_4 d  --28/01 added this join
		--on a.LUWID=d.LUWID 
		where b.LUWID is null 
		and c.LUWID is null --28/01 added this
		--and d.LUWID is null --28/01 added this
	)
	union
	(
	select a1.* from CoverREF_temptable_pc_pp_2 a1
	left outer  join CoverREF_temptable_pc_pp b1
	on a1.LUWID=b1.LUWID 
	left outer  join CoverREF_temptable_pc_pp_1 c1
	on a1.LUWID =c1.LUWID
	left outer  join CoverREF_temptable_pc_pp_3 d1 --28/01 added this new union
	on a1.LUWID =d1.LUWID
	--left outer  join #temptable_pc_pp_4 e1 --28/01 added this new union
	--on a1.LUWID =e1.LUWID
	where b1.PC_PP_Id is  null
	and c1.PC_PP_Id is  null
	and d1.PC_PP_Id is  null
	--and e1.PC_PP_Id is  null
	)
	union --28/01 added this new union (27 missing matches getting picked up now)
	(
	select a1.* from CoverREF_temptable_pc_pp_3 a1
	left outer  join CoverREF_temptable_pc_pp b1
	on a1.LUWID=b1.LUWID 
	left outer  join CoverREF_temptable_pc_pp_1 c1
	on a1.LUWID =c1.LUWID
	left outer  join CoverREF_temptable_pc_pp_2 d1
	on a1.LUWID =d1.LUWID
	--left outer  join #temptable_pc_pp_4 e1
	--on a1.LUWID =e1.LUWID
	where b1.PC_PP_Id is  null
	and c1.PC_PP_Id is  null
	and d1.PC_PP_Id is  null
	--and e1.PC_PP_Id is  null
	)
	)X
)Y
where Y.RowNum=1


--As part of DMIG-4959 (DMIG-5640), have added the temp table #temptable_consolidated below - eliminate duplicate PC_PP_IDs for the same LUWID
IF OBJECT_ID('CoverREF_temptable_consolidated') is not null
drop table CoverREF_temptable_consolidated
select
row_number() over(partition by LUWID order by PC_PP_Id desc  ) as RANKRW,
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
POLICY_MATCH_TYPE
into CoverREF_temptable_consolidated
from
(
select 
row_number() over(partition by LUWID order by PC_PP_Id desc  ) as RANKRW,
ACN_icare,
AssignedRisk,
Currency,
ITCPercentagePEL,
PolicyType,
PolicyType_icare,
Retired,
PC_PP_Id,
CancellationDate,
AccountNumber,
PC_PP_EffectiveDate,
PD_EffectiveDate,
PC_PP_ExpirationDate,
PD_ExpirationDate,
LUWID,
Date_of_Injury,
PC_PP_PolicyNumber,
PD_Legacy_PolicyNumber,
CD_Legacy_PolicyNumberCD,
PT_icare_PolicyNumber,
PT_ID,
PT_Policy_Status,
TermNumber,
EmployerCategory_icare,
POLICY_MATCH_TYPE
from CoverREF_temptable_all
) X
where X.RANKRW=1




------ THIRD TO LAST /*Getting all matched WIC related matched claims into one bucket*/
/*Getting all matched WIC related matched claims into one bucket*/
/*Getting all matched WIC related matched claims into one bucket*/
IF OBJECT_ID('CoverREF_tmp_EMICS_WIC_Matched_PC') is not null
DROP TABLE CoverREF_tmp_EMICS_WIC_Matched_PC
SELECT * into CoverREF_tmp_EMICS_WIC_Matched_PC
from
(
SELECT distinct
ltrim(rtrim(CD.Claim_Number)) as Claim_Number
,CD.Tariff_No as EMICS_WICCode
,pp.PolicyNumber
,pp.LegacyPolicyNumber_icare
,wagewic.Code AS WICRateNumber
,pp.PeriodStart
,pp.PeriodEnd
,pp.ID as PC_PolicyPeriodID
,wage.CostCenter_icare
,(SELECT PolicyLocation FROM DM_STG_PC.dbo.pcx_costcenter_icare pcx_cc WHERE pcx_cc.ID = wage.CostCenter_icare and pcx_cc.ExpirationDate is null) AS PolicyLocation
FROM --#tmpEMICSPolicyNo tmp_DQ_EML
CoverREF_temptable_consolidated temp_cons  --Has all Verified PC claims
left outer join DM_STG_EMICS.dbo.Claim_Detail CD on ltrim(rtrim(CD.Claim_Number)) = temp_cons.LUWID
left outer join DM_STG_PC.dbo.pc_policyperiod pp on pp.LegacyPolicyNumber_icare =ltrim(rtrim(temp_cons.CD_Legacy_PolicyNumberCD))
INNER JOIN DM_STG_PC.dbo.pcx_directwage_icare wage ON pp.ID = wage.BranchID AND wage.ExpirationDate IS NULL
LEFT OUTER JOIN DM_STG_PC.dbo.pcx_directwagewic_icare wagewic ON wage.DirectWageID = wagewic.ID AND wagewic.ExpirationDate IS NULL
WHERE 
pp.Status=9 --bound --Added this condition as this is what was recommended by Guidewire and we have used this in our DM ETL code as well
and pp.MostRecentModel = 1 --Get the most recent (current ) list of WIC Codes.
and pp.retired=0
--and pp.CancellationDate is null --commented this out as the correct conditon is the one below this line
and cast(pp.periodstart as date)<>cast(isnull(pp.cancellationdate,'') as date) --Added this condition as this is what was recommended by Guidewire and we have used this in our DM ETL code as well
AND wagewic.Code =CASE WHEN len(ltrim(rtrim(CD.Tariff_No)))=5 THEN concat('0',convert(varchar,ltrim(rtrim(CD.Tariff_No))))
ELSE ltrim(rtrim(CD.Tariff_No)) 
END --matching on EMICS WIC and PC WIC to see if there is a match
and temp_cons.Date_of_injury >= PP.periodStart  and temp_cons.Date_of_injury < PP.periodEnd
) X



/*If you observe the contents of #tmp_EMICS_WIC_Matched_PC above, you will find for some of the claims, we have */
/*more than 1 costcentre record associated with a policy period id, in this case ,we  are taking the max costcentre record - Based on similar situtation issue under JIRA1998 where we agreed to pick max costcentre*/
IF OBJECT_ID('CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record') is not null
DROP TABLE CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record
SELECT * into CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record
from
(
select * from
(
select row_number () over (partition by matched.Claim_Number order by matched.costcenter_icare desc) Latest,
matched.*
from CoverREF_tmp_EMICS_WIC_Matched_PC matched
) sorted
where sorted.Latest=1
)X

---SECOND TO LAST Verified PC and Unmatched WIC / Verified CDR/Invalid

--select * from CoverREF_tmp_ccst_policylocation_unmatched_WIC
--union 
--select * from CoverREF_tmp_ccst_policylocation_verified_CDR
--Please refer to the Selection SQLs below on how these 2 temp tables were created

--revised ccst_policylocation (Verified PC and Unmatched WIC only)
--This will get poliyclocation for all claims that are Verified PC but have NOT got EMICS WIC matching with a PC WIC
IF OBJECT_ID('CoverREF_tmp_ccst_policylocation_unmatched_WIC') is not null
DROP TABLE CoverREF_tmp_ccst_policylocation_unmatched_WIC
SELECT * into CoverREF_tmp_ccst_policylocation_unmatched_WIC
from
(
select
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY'+':POLICYLOCATION' as PublicID,
0 as Retired,
1 as PrimaryLocation,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY' as PolicyID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICYLOCATIONADDRESS' as AddressID,
ltrim(rtrim(CD.Claim_Number)) as LUWID
from CoverREF_temptable_consolidated temp_cons
inner join DM_STG_EMICS.dbo.CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_cons.LUWID
where
ltrim(rtrim(CD.Claim_number)) not in
(
Select Claim_Number from CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched  
) -- to get all Verified PC claims for which we we got a WIC match
) X

--revised ccst_policylocation (Verified CDR/Invalid only)
--This will get poliyclocation for all claims that are NOT Verified PC i.e are either Verified CDR/Invalid
IF OBJECT_ID('CoverREF_tmp_ccst_policylocation_verified_CDR') is not null
DROP TABLE CoverREF_tmp_ccst_policylocation_verified_CDR
SELECT * into CoverREF_tmp_ccst_policylocation_verified_CDR
from
(
select
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY'+':POLICYLOCATION' as PublicID,
0 as Retired,
1 as PrimaryLocation,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY' as PolicyID,
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICYLOCATIONADDRESS' as AddressID,
ltrim(rtrim(CD.Claim_Number)) as LUWID
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
where CD.is_Null=0
and ltrim(rtrim(CD.Claim_Number)) not in
(
Select LUWID as Claim_Number from CoverREF_temptable_consolidated temp_cons
) -- to get all claims for which we did not get a cover in PC i.e. Verified CDR/Invalid
) X



IF OBJECT_ID('COVERREF_tmp_ccst_policyperiod_ACCOUNT_verifiedPC_before_Prod_check') is not null
DROP TABLE COVERREF_tmp_ccst_policyperiod_ACCOUNT_verifiedPC_before_Prod_check
SELECT * into COVERREF_tmp_ccst_policyperiod_ACCOUNT_verifiedPC_before_Prod_check
from
(
select 
X.LUWID,
X.AccountNumber,
Concat('EMICS:',X.LUWID,':ACCOUNT',':POLICYPERIOD') as PublicID
FROM
(
select max(b.LUWID) as LUWID,AccountNumber
from
(
select
PC_PP.Id as PC_PP_Id,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
PC_PP.TermNumber 
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join COVERREF_temptable_consolidated temp_cons_pp on temp_cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join 
(
select ID,policyID,PolicyNumber,PeriodStart, periodend, TermNumber, CancellationDate
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 and MostRecentModel=1 and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP
on temp_cons_pp.PC_PP_Id=PC_PP.ID
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
) b
inner join
(
select a.PC_PP_Id, max(a.LUWID) as LUWID
from
(
select
PC_PP.Id as PC_PP_Id,
PC_A.AccountNumber,
PC_PP.PeriodStart as PC_PP_EffectiveDate,
PC_PP.PeriodEnd as PC_PP_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
PC_PP.TermNumber 
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.Cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join COVERREF_temptable_consolidated temp_cons_pp on temp_cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join 
(
select ID,policyID,PolicyNumber,PeriodStart, periodend, TermNumber, CancellationDate
from dm_stg_pc.dbo.pc_policyperiod
Where status=9 and MostRecentModel=1 and Retired=0
and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
) PC_PP on temp_cons_pp.PC_PP_Id=PC_PP.ID
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.[Claim_Number]=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
) a
group by a.PC_PP_Id
) c
on c.PC_PP_Id=b.PC_PP_Id and c.LUWID=b.LUWID
--where b.AccountNumber='1000177'
group by b.AccountNumber
) X
)Y


---#temptable_CDR
IF OBJECT_ID('CoverREF_temptable_CDR') is not null
drop table CoverREF_temptable_CDR
select
*
into CoverREF_temptable_CDR
from
(
select Z.*
from
(
select row_number () over (partition by luwid,rownum order by RENEWAL_NO desc) latest, Y.* --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with different period start dates
from
(
select
X.*
from
(
select 
distinct
'aud' as Currency,
--CD.itc_pcent as ITCPercentagePEL,
CASE WHEN  CD.is_gst_registered=1 THEN 100
ELSE 0
END as ITCPercentagePEL,  --22Jul2019 - modified this to match what spec says (v12.41)
CD.is_gst_registered,
'WorkersComp' as PolicyType,
'Standard' as PolicyType_icare,
0 as Retired,
concat ('EMICS_', ltrim(rtrim(CD.Policy_No))) as AccountNumber,
PD.PERIOD_START as PD_EffectiveDate,
PD.PERIOD_EXPIRY as PD_ExpirationDate,
PD.RENEWAL_NO,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
CD.itc_pcent,
LTRIM(RTRIM(CD.Policy_No)) as PolicyNumberCD,
PT.icare_policy_no as PT_icare_PolicyNumber,
PT.ID as PT_ID,
PT.CANCELLED_DATETIME
,PD.rownum
,PD.POLICY_YEAR
, CD.Tariff_No -- JC: added this as it was referencing it from VERIFIED_CDR_STAGING
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
inner join [dbo].[ADM_Claim_Cohort] cohort
on cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and cohort.cohort=2
--15/08/2019 - added this new table to ensure we filter our only Cohort 2 claims else public id for PolicyPeriod gets affected - does not match ETL
left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT   on PT.POLICY_NO = CD.Policy_No
left outer join  
(
select row_number () over (partition by PD.POLICY_NO,PD.PERIOD_START order by PD.RENEWAL_NO desc) rownum, PD.POLICY_NO, PD.PERIOD_START,
PD.PERIOD_EXPIRY,PD.RENEWAL_NO, PD.POLICY_YEAR from DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
) PD --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with same period start dates
on PD.POLICY_NO = PT.POLICY_NO
        --and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
      and CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
left outer join DM_STG_CDR.[dbo].[DM_STG_CDR_D_POLICY_MASTER] CDR_PM -- CDR Policy Master table
on ltrim(rtrim(CDR_PM.POLICY_SOURCE_NUMBER))=ltrim(rtrim(CD.Policy_No))
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
where CD.is_Null=0
and ltrim(rtrim(CD.Claim_number)) NOT in (select LUWID from CoverREF_temptable_consolidated)
and CDR_PM.POLICY_SOURCE_NUMBER is not null
--and ltrim(rtrim(CD.Claim_number))=1686757
) X
where (X.rownum=1 or X.rownum is null)
)Y
)Z
where Z.latest=1
)X



---Verified CDR/Invalid
IF OBJECT_ID('CoverREF_temptable_ccst_policy_VerifiedCDR') is not null
drop table CoverREF_temptable_ccst_policy_VerifiedCDR
select * into CoverREF_temptable_ccst_policy_VerifiedCDR
from
(
-- 1st part of the SQL - This is to fetch all Verified CDR record that have EMICS PREMIUM_DETAIL matching records i.e. we can get cover start and end dates from EMICS
	select 
	--distinct
	tmp.LUWID,
	tmp.LUWID as Claim_Number,
	tmp.Currency,
	tmp.ITCPercentagePEL,
	tmp.is_gst_registered,
	tmp.PolicyType,
	tmp.PolicyType_icare,
	tmp.Retired,
	tmp.AccountNumber,
	tmp.PD_EffectiveDate as Cover_EffectiveDate,
	tmp.PD_ExpirationDate as Cover_ExpirationDate,
	tmp.RENEWAL_NO,
	tmp.Date_of_Injury,
	tmp.itc_pcent,
	tmp.PolicyNumberCD,
	tmp.PT_icare_PolicyNumber,
	tmp.PT_ID,
	tmp.CANCELLED_DATETIME,
	'EMICS' as Cover_period_dates_source_flag
	--,tmp.rownum
	--,tmp.POLICY_YEAR
	from CoverREF_temptable_CDR tmp   --2254 2463
	where PD_EffectiveDate is NOT NULL  --picking ony those verified CDR claims for which we DO have Cover dates in EMICS
union
-- 2nd part of the SQL - This is to fetch all Verified CDR record that DO NOT have EMICS PREMIUM_DETAIL matching records i.e. we CANNOT get cover start and end dates from EMICS
-- so we go and get the cover dates from matching records in CDR instead
select 
	--distinct
	tmp.LUWID,
	tmp.LUWID as Claim_Number,
	tmp.Currency,
	tmp.ITCPercentagePEL,
	tmp.is_gst_registered,
	tmp.PolicyType,
	tmp.PolicyType_icare,
	tmp.Retired,
	tmp.AccountNumber,
	--tmp.PD_EffectiveDate,
	--tmp.PD_ExpirationDate,
	CONVERT(DATETIME2, CONVERT(VARCHAR(10),rp.COVER_COMMENCE_DATE_ID,120)) AS Cover_EffectiveDate,
	CONVERT(DATETIME2, CONVERT(VARCHAR(10),ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID),120)) AS Cover_ExpirationDate,
	rp.POLICY_ID as RENEWAL_NO,
	tmp.Date_of_Injury,
	tmp.itc_pcent,
	tmp.PolicyNumberCD,
	tmp.PT_icare_PolicyNumber,
	tmp.PT_ID,
	tmp.CANCELLED_DATETIME,
	'CDR' as Cover_period_dates_source_flag
	--tmp.rownum,
	--tmp.POLICY_YEAR
	from CoverREF_temptable_CDR tmp   --2254 2463
	inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
	INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
	INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
	WHERE cd.Is_Null = 0
	and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
	AND rp.WCA_STATUS_ID = 181644
	AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
	AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
union
-- 3rd part of the SQL - This is to fetch all Verified CDR record that DO NOT have either EMICS PREMIUM_DETAIL matching records nor CDR DM_STG_CDR_E_ROLLING_POLICY matching records
-- so we CANNOT get cover dates from either EMICS or CDR. Hence we are defaulting the cover dates to (Date_Of_Injury-1) and (Date_Of_Injury+1).
select 
	--distinct
	tmp.LUWID,
	tmp.LUWID as Claim_Number,
	tmp.Currency,
	tmp.ITCPercentagePEL,
	tmp.is_gst_registered,
	tmp.PolicyType,
	tmp.PolicyType_icare,
	tmp.Retired,
	tmp.AccountNumber,
	--tmp.PD_EffectiveDate,
	--tmp.PD_ExpirationDate,
	CAST(DATEADD(day,-1,tmp.Date_of_Injury) as DATE) AS Cover_EffectiveDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
	CAST(DATEADD(day,1,tmp.Date_of_Injury) as DATE) AS Cover_ExpirationDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
	--DATEADD(day,-1,tmp.Date_of_Injury)  AS Cover_EffectiveDate,
	--DATEADD(day,1,tmp.Date_of_Injury)  AS Cover_ExpirationDate,
	--tmp.LUWID as RENEWAL_NO,
	convert(varchar(10),tmp.Date_of_Injury,112) as RENEWAL_NO,
	tmp.Date_of_Injury,
	tmp.itc_pcent,
	tmp.PolicyNumberCD,
	tmp.PT_icare_PolicyNumber,
	tmp.PT_ID,
	CASE WHEN (tmp.CANCELLED_DATETIME is not null) and (tmp.CANCELLED_DATETIME < CAST(DATEADD(day, -1, tmp.date_of_injury) as DATE))
	THEN CAST(DATEADD(day,1,tmp.date_of_injury) as DATE)
	ELSE tmp.CANCELLED_DATETIME
	end as cancelled_DATETIME,
	'NOT_IN_EMICS_OR_CDR' as Cover_period_dates_source_flag
	--tmp.rownum,
	--tmp.POLICY_YEAR
from CoverREF_temptable_CDR tmp   
inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
LEFT OUTER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
AND rp.WCA_STATUS_ID = 181644
AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
WHERE cd.Is_Null = 0
and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
and rp.POLICY_ID is NULL -- no match found for cover dates in CDR too so we then need to go for default cover dates !
)X

-------------- Verified CDR Policy level PolicyPeriod -------


IF OBJECT_ID('CoverREF_temptable_ccst_policyperiod_VerifiedCDR') is not null
drop table CoverREF_temptable_ccst_policyperiod_VerifiedCDR

select * into CoverREF_temptable_ccst_policyperiod_VerifiedCDR
from
(
select
--distinct
'EMICS:' + ltrim(rtrim(tmp.PolicyNumberCD)) + '_' + convert(varchar,tmp.RENEWAL_NO) + ':POLICY' + ':POLICYPERIOD' as PublicID,
tmp.AccountNumber,
tmp.Cover_EffectiveDate,
tmp.Cover_ExpirationDate,
tmp.LUWID,
tmp.Date_of_Injury,
tmp.PolicyNumberCD,
tmp.PT_icare_PolicyNumber,
tmp.RENEWAL_NO
from 
CoverREF_temptable_ccst_policy_VerifiedCDR tmp
inner join
(
select PolicyNumberCD, Cover_EffectiveDate,Cover_ExpirationDate, max(LUWID) as max_LUWID 
from CoverREF_temptable_ccst_policy_VerifiedCDR tmp
group by PolicyNumberCD, Cover_EffectiveDate,Cover_ExpirationDate
) max_claim  --Getting the max(claim_number) record out as the Policy Period grain is cover start and end dates and policy number
on max_claim.max_LUWID=tmp.LUWID   
)X

--------------Verified CDR Account level PolicyPeriod 
--Verified CDR/Invalid
IF OBJECT_ID('CoverREF_temptable_ccst_policyperiod_ACCOUNT_VerifiedCDR') is not null
drop table CoverREF_temptable_ccst_policyperiod_ACCOUNT_VerifiedCDR
select * into CoverREF_temptable_ccst_policyperiod_ACCOUNT_VerifiedCDR
from
(
select
distinct
--'EMICS:' + tmp.AccountNumber + ':ACCOUNT' + ':POLICYPERIOD' as PublicID,
'EMICS:' + SUBSTRING(tmp.AccountNumber,7, len(tmp.AccountNumber)) + ':ACCOUNT' + ':POLICYPERIOD' as PublicID, --17/05/2019 Corrected the format
tmp.AccountNumber,
tmp.LUWID
from 
CoverREF_temptable_ccst_policy_VerifiedCDR tmp
inner join
(
select PolicyNumberCD,
--Cover_EffectiveDate,Cover_ExpirationDate, 
max(LUWID) as max_LUWID 
from CoverREF_temptable_ccst_policy_VerifiedCDR tmp

group by PolicyNumberCD
--, Cover_EffectiveDate,Cover_ExpirationDate
) max_claim
on max_claim.max_LUWID=tmp.LUWID
)X


END