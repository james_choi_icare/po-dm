﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 01/07/2019
-- Description:	Contacts - Named Insured V003.10
-- 1. This is the child CM Stored Proc for EMPLOYER contact that will called by the master stored proc SP_REC_XFM_CONTACT
-- Please note that CM EMPLOYER a.k.a NAMED INSURED CONTACTS are updated/inserted into CM using API by ETL
-- The following data gets updated/inserted -
-- (a) Contact - SourceSystem_icare, SingleRemittance_icare, AggregateRemittance_icare, CRM_Version_icare UPDATED
-- (b) ContactTag - Type: ClaimParty is INSERTED
-- (c) eftdata - bank details where available INSERTED
-- Dependency: QA EFT DATA SP should have already run before this SP
-- 2. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CM_CONTACT_EMPLOYER_STAGING]
	
AS
BEGIN

IF OBJECT_ID('temptable_max_claim_number') is not null
drop table dbo.temptable_max_claim_number;

IF OBJECT_ID('temptable_CM_employer_verifiedPC_temp1') is not null
drop table dbo.temptable_CM_employer_verifiedPC_temp1;

IF OBJECT_ID('temptable_CM_employer_verifiedPC_temp2') is not null
drop table dbo.temptable_CM_employer_verifiedPC_temp2;

IF OBJECT_ID('ContactREF_temptable_employer_CM_all') is not null
drop table dbo.ContactREF_temptable_employer_CM_all;

IF OBJECT_ID('ContactREF_temptable_employer_CM_contacttag') is not null
drop table dbo.ContactREF_temptable_employer_CM_contacttag;

IF OBJECT_ID('ContactREF_temptable_employer_CM_eftdata') is not null
drop table dbo.ContactREF_temptable_employer_CM_eftdata;






--IF OBJECT_ID('ContactREF_temptable_employer_verifiedCDR') is not null
--drop table REC_XFM.dbo.ContactREF_temptable_employer_verifiedCDR;

--IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC') is not null
--drop table temptable_EMPLOYER_VerifiedPC;

--IF OBJECT_ID('temptable_EMPLOYER_VerifiedCDR') is not null
--drop table temptable_EMPLOYER_VerifiedCDR;

SELECT * INTO temptable_max_claim_number
FROM
(
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT max([LUWID]) as LUWID
      ,[AddressBookUID]
  FROM [dbo].[ContactREF_temptable_employer_verifiedPC]
    where [AddressBookUID] is not null
  group by [AddressBookUID]
)X

SELECT * INTO temptable_CM_employer_verifiedPC_temp1
--ContactREF_CM_temptable_employer_verifiedPC
FROM
(
select 
--LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],''))) as CRM_Claim_Number,
CC_emp_contact.LUWID as LUWID,
'EMICS:' + max_claim.LUWID  + ':INSURED' as PublicID,
crm_a.GUIDEWIRE_PUBLIC_ID__C,
'SC5 - Insured Employer' as SC_Type,
CC_emp_contact.AddressBookUID as CM_LinkID,
CC_emp_contact.AddressBookUID as LinkID,
CASE
WHEN LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],''))) is null then 'N'
ELSE 'Y'
END
as ClaimMissingCRM,
CASE 
WHEN crm_a.ID is null then 'Y'
ELSE 'N'
END
as ContactMissingCRM,
CASE
WHEN CC_emp_contact.AddressBookUID is null then 'Y'
ELSE 'N'
END
as ContactMissingCM,
CASE 
WHEN crm_a.ID is not null then cast(VERSION_NUMBER__C as float)+1
ELSE 1 
END
as CRMVersion_icare,
ab_con.CRMVersion_icare as CM_CRMVersion_icare,
CRM_a.VERSION_NUMBER__C,
CASE 
WHEN ab_con.CRMVersion_icare<>CRM_a.VERSION_NUMBER__C THEN 'Y'
ELSE 'N'
END
as CRM_CM_Version_Diff,
CRM_a.LAST_UPDATED_SOURCE__C as CRM_SourceSystem_icare, --need to check with Louw
ab_con.SourceSystem_icare as CM_SourceSystem_icare,
CRM_a.DEFER_PAYMENT__C,
0 as SingleRemittancePerDay_icare,
CASE
WHEN CASE 
     WHEN CRM_a.DEFER_PAYMENT__C='N' THEN 0
	 ELSE 1 END <> 0 THEN 'Y'
ELSE 'N'
END
as  DEFER_PAYMENT__C_Diff,
CASE
WHEN CASE 
     WHEN ab_con.SingleRemittancePerDay_icare is NULL THEN 0
	 ELSE 1 END <> 0 THEN 'Y'
ELSE 'N'
END
as  SingleRemittancePerDay_icare_Diff,
CASE
when eft.LUWID is not null then 'Electronic Funds' 
ELSE 'cheque'
END
as PreferredPaymentMethod_icare,
CRM_a.PREFERRED_METHOD_OF_PAYMENT__C,
ab_con.PreferredPaymentMethod_icare as CM_PreferredPaymentMethod_icare,
CASE
WHEN  isnull(CRM_a.PREFERRED_METHOD_OF_PAYMENT__C,0)<>CASE
													  when eft.LUWID is not null then 'Electronic Funds' 
													  ELSE 'cheque'
													  END 
THEN 'Y'
ELSE 'N'
END
as  PREFERRED_METHOD_OF_PAYMENT__C_Diff,
CASE
WHEN  isnull(ab_con.PreferredPaymentMethod_icare,0)<>CASE
													  when eft.LUWID is not null then 'Electronic Funds' 
													  ELSE 'cheque'
													  END 
THEN 'Y'
ELSE 'N'
END
as  PreferredPaymentMethod_icare_Diff,
1 as ApprovedPaymentMethod_icare,
CRM_a.APPROVED_METHOD_OF_PAYMENT__C,
ab_con.ApprovedPaymentMethod_icare as CM_ApprovedPaymentMethod_icare,
CASE
WHEN  CASE WHEN CRM_a.APPROVED_METHOD_OF_PAYMENT__C='false' THEN 0 ELSE 1 END<>1
THEN 'Y'
ELSE 'N'
END
as  APPROVED_METHOD_OF_PAYMENT__C_Diff,
CASE
WHEN  CASE WHEN ab_con.ApprovedPaymentMethod_icare=0  THEN 0
      WHEN ab_con.ApprovedPaymentMethod_icare is null then 1 
	  ELSE 1 END <>1
THEN 'Y'
ELSE 'N'
END
as  ApprovedPaymentMethod_icare_Diff,
0 as AggregateRemittance_icare,
CRM_a.AGGREGATE_REMITTANCE__C,
ab_con.AggregateRemittance_icare as CM_AggregateRemittance_icare,
CASE
WHEN  CASE WHEN CRM_a.AGGREGATE_REMITTANCE__C='false' THEN 0 ELSE 1 END<>0
THEN 'Y'
ELSE 'N'
END
as  AGGREGATE_REMITTANCE__C_Diff,
CASE
WHEN  CASE WHEN ab_con.AggregateRemittance_icare=0  THEN 0
      WHEN ab_con.AggregateRemittance_icare is null then 0 
	  ELSE 1 END <>0
THEN 'Y'
ELSE 'N'
END
as  AggregateRemittance_icare_Diff,
--'GWCM' as SourceSystem_icare,
CRM_a.LAST_UPDATED_SOURCE__C as SourceSystem_icare,
CRM_a.LAST_UPDATED_SOURCE__C,
--ab_con.SourceSystem_icare as CM_SourceSystem_icare,
CASE
WHEN  CRM_a.LAST_UPDATED_SOURCE__C<>CRM_a.LAST_UPDATED_SOURCE__C--'GWCM'
THEN 'Y'
ELSE 'N'
END
as  LAST_UPDATED_SOURCE__C_Diff,
CASE
WHEN  ab_con.SourceSystem_icare<>CRM_a.LAST_UPDATED_SOURCE__C   --'GWCM'
THEN 'Y'
ELSE 'N'
END
as  SourceSystem_icare_Diff,
ab_con.CreateTime as CM_CreateTime,
ab_con.UpdateTime as CM_UpdateTime,
CASE
WHEN  ab_con.CreateTime<>ab_con.UpdateTime  THEN 'Y'
ELSE 'N'
END
as  CM_UpdateTime_Post_BGL,
CRM_a.CREATEDDATE as CRM_CREATEDDATE,
CRM_a.LASTMODIFIEDDATE as CRM_LASTMODIFIEDDATE,
CASE
WHEN  CRM_a.CREATEDDATE<>CRM_a.LASTMODIFIEDDATE
THEN 'Y'
ELSE 'N'
END
as  CRM_LASTMODIFIEDDATE_Post_BGL,
'EMICS:' + max_claim.LUWID  + ':INSURED:CLAIMPARTY' as External_UniqueID,
'EMICS:' + max_claim.LUWID  + ':INSURED:CLAIMPARTY' as External_PublicID
--'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':BUSINESSADDRESS' as PublicID,
from [dbo].[ContactREF_temptable_employer_verifiedPC] CC_emp_contact
inner join temptable_max_claim_number max_claim
on max_claim.LUWID=CC_emp_contact.LUWID
left outer join [dbo].[ccst_eftdata] eft
on eft.AddressBookUID=CC_emp_contact.AddressBookUID and eft.LUWID=max_claim.LUWID
--left outer join [DM_XFM_LD].[dbo].[INTM_CM_EFTDATA_EMPLOYER] eft
--on eft.LUWID=concat('EMICS:',max_claim.LUWID,':INSURED')
LEFT OUTER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_CLAIM] ccl
ON LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))=max_claim.LUWID
--LEFT OUTER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_POLICY] p
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
--LEFT OUTER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_ACCOUNT] crm_a
--ON p.[ACCOUNT__C] = crm_a.[ID]
LEFT OUTER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_ACCOUNT] crm_a
ON crm_a.[ID]=CC_emp_contact.AddressBookUID
--left outer join  DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] ab_con on ab_con.LinkID=CC_emp_contact.AddressBookUID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] ab_con on ab_con.LinkID= CC_emp_contact.AddressBookUID --'0032O000005Bm5AQAS'

)X


--temptable_CM_employer_verifiedPC_temp2 is the equivalent of DM_POST_BGL_HOTFIX_SC5_EMP in XFM
SELECT * INTO temptable_CM_employer_verifiedPC_temp2
--ContactREF_CM_temptable_employer_verifiedPC
FROM
(
select 
temptable_CM_employer_verifiedPC_temp1.LUWID,
temptable_CM_employer_verifiedPC_temp1.PublicID,
temptable_CM_employer_verifiedPC_temp1.GUIDEWIRE_PUBLIC_ID__C,
temptable_CM_employer_verifiedPC_temp1.SC_Type,
temptable_CM_employer_verifiedPC_temp1.CM_LinkID,
temptable_CM_employer_verifiedPC_temp1.LinkID,
temptable_CM_employer_verifiedPC_temp1.ClaimMissingCRM,
temptable_CM_employer_verifiedPC_temp1.ContactMissingCRM,
temptable_CM_employer_verifiedPC_temp1.ContactMissingCM,
temptable_CM_employer_verifiedPC_temp1.CRMVersion_icare,
temptable_CM_employer_verifiedPC_temp1.CM_CRMVersion_icare,
temptable_CM_employer_verifiedPC_temp1.VERSION_NUMBER__C,
temptable_CM_employer_verifiedPC_temp1.CRM_CM_Version_Diff,
temptable_CM_employer_verifiedPC_temp1.CRM_SourceSystem_icare,
temptable_CM_employer_verifiedPC_temp1.CM_SourceSystem_icare,
temptable_CM_employer_verifiedPC_temp1.DEFER_PAYMENT__C,
temptable_CM_employer_verifiedPC_temp1.SingleRemittancePerDay_icare,
temptable_CM_employer_verifiedPC_temp1.DEFER_PAYMENT__C_Diff,
temptable_CM_employer_verifiedPC_temp1.SingleRemittancePerDay_icare_Diff,
temptable_CM_employer_verifiedPC_temp1.PreferredPaymentMethod_icare,
temptable_CM_employer_verifiedPC_temp1.PREFERRED_METHOD_OF_PAYMENT__C,
temptable_CM_employer_verifiedPC_temp1.CM_PreferredPaymentMethod_icare,
temptable_CM_employer_verifiedPC_temp1.PREFERRED_METHOD_OF_PAYMENT__C_Diff,
temptable_CM_employer_verifiedPC_temp1.PreferredPaymentMethod_icare_Diff,
temptable_CM_employer_verifiedPC_temp1.ApprovedPaymentMethod_icare,
temptable_CM_employer_verifiedPC_temp1.APPROVED_METHOD_OF_PAYMENT__C,
temptable_CM_employer_verifiedPC_temp1.CM_ApprovedPaymentMethod_icare,
temptable_CM_employer_verifiedPC_temp1.APPROVED_METHOD_OF_PAYMENT__C_Diff,
temptable_CM_employer_verifiedPC_temp1.ApprovedPaymentMethod_icare_Diff,
temptable_CM_employer_verifiedPC_temp1.AggregateRemittance_icare,
temptable_CM_employer_verifiedPC_temp1.AGGREGATE_REMITTANCE__C,
temptable_CM_employer_verifiedPC_temp1.CM_AggregateRemittance_icare,
temptable_CM_employer_verifiedPC_temp1.AGGREGATE_REMITTANCE__C_Diff,
temptable_CM_employer_verifiedPC_temp1.AggregateRemittance_icare_Diff,
temptable_CM_employer_verifiedPC_temp1.SourceSystem_icare,
temptable_CM_employer_verifiedPC_temp1.LAST_UPDATED_SOURCE__C,
temptable_CM_employer_verifiedPC_temp1.LAST_UPDATED_SOURCE__C_Diff,
temptable_CM_employer_verifiedPC_temp1.SourceSystem_icare_Diff,
temptable_CM_employer_verifiedPC_temp1.CM_CreateTime,
temptable_CM_employer_verifiedPC_temp1.CM_UpdateTime,
temptable_CM_employer_verifiedPC_temp1.CM_UpdateTime_Post_BGL,
temptable_CM_employer_verifiedPC_temp1.CRM_CREATEDDATE,
temptable_CM_employer_verifiedPC_temp1.CRM_LASTMODIFIEDDATE,
temptable_CM_employer_verifiedPC_temp1.CRM_LASTMODIFIEDDATE_Post_BGL,
temptable_CM_employer_verifiedPC_temp1.External_UniqueID,
temptable_CM_employer_verifiedPC_temp1.External_PublicID,
CASE 
WHEN DEFER_PAYMENT__C_Diff='Y' OR PREFERRED_METHOD_OF_PAYMENT__C_Diff='Y'
OR APPROVED_METHOD_OF_PAYMENT__C_Diff='Y' or AGGREGATE_REMITTANCE__C_Diff='Y'
OR LAST_UPDATED_SOURCE__C_Diff='Y' THEN 'Y'
ELSE 'N'
END
AS Contact_Diff,
CASE 
WHEN CRM_CM_Version_Diff='Y' OR SingleRemittancePerDay_icare_Diff='Y' OR PreferredPaymentMethod_icare_Diff='Y'
OR ApprovedPaymentMethod_icare_Diff='Y' or AggregateRemittance_icare_Diff='Y'
OR SourceSystem_icare_Diff='Y' THEN 'Y'
ELSE 'N'
END
AS CM_Contact_Diff
from 
temptable_CM_employer_verifiedPC_temp1
)X

--select * from temptable_CM_employer_verifiedPC_temp2
--drop table [ContactREF_temptable_employer_CM_all]

select * into [ContactREF_temptable_employer_CM_all]
from
(
SELECT 
    distinct
	 --a.[RowNumber] [xfm_RowNumber]
	 a.[PublicID] [xfm_PublicID]
	,b.[PublicID] [lkp_PublicID]
	,b.[LinkID] [lkp_LinkID]
	,a.[CM_LinkID]
	,a.[CRMVersion_icare]
	,a.[CM_CRMVersion_icare]
	,a.[SourceSystem_icare]
	,a.[CM_SourceSystem_icare]
	,a.[PreferredPaymentMethod_icare]
	,a.[SingleRemittancePerDay_icare]
	,a.[ApprovedPaymentMethod_icare]
	,b.[Subtype]
FROM temptable_CM_employer_verifiedPC_temp2 a
--[dbo].[DM_POST_BGL_HOTFIX_SC5_EMP] a
--LEFT JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] b ON a.[LinkID] = b.[LinkID]
LEFT OUTER JOIN 
(
select b1.[PublicID], b1.[LinkID], c1.TYPECODE as Subtype
from [DM_STG_CM].[dbo].[ab_abcontact] b1
inner join [DM_STG_CM].[dbo].[abtl_abcontact] c1 
on c1.ID = b1.SubType 
)b --23Jul2019 Added this to ensure we are not referencing any DM_XFM table
on a.[LinkID] = b.[LinkID] --'0032O000005Bm5AQAS'
WHERE a.[ContactMissingCM] = 'N'
AND a.[CM_Contact_Diff] = 'Y'
)X


--drop table [ContactREF_temptable_employer_CM_contacttag]
select * into [ContactREF_temptable_employer_CM_contacttag]
from
(
SELECT  
    distinct
	--a.[RowNumber] [xfm_RowNumber]
	a.[PublicID] [xfm_PublicID]
	,b.[PublicID] [lkp_PublicID]
	,b.[LinkID] [lkp_LinkID]
	--,ct.[Type]
	,'claimparty' As Type
	--,ct.[PublicID]
	,a.External_PublicID as PublicID
FROM temptable_CM_employer_verifiedPC_temp2 a
--[dbo].[DM_POST_BGL_HOTFIX_SC5_EMP] a
--LEFT JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] b ON a.LinkID = b.LinkID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] b ON a.LinkID = b.LinkID --'0032O000005Bm5AQAS'
--LEFT JOIN [dbo].[DM_XFM_CM_ABCONTACTTAG_SC5_EMP] ct 
--ON ct.[ABContactID] = a.[PublicID]
--LEFT JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ABCONTACTTAG] ct_lkp ON ct_lkp.[ABContactID] = b.[PublicID] AND ct_lkp.[Type] = 'claimparty'
LEFT OUTER JOIN 
(
select ct_lkp1.[ABContactID],ct_lkp1.[PublicID],ABTL_CT.TYPECODE as Type
from DM_STG_CM.[dbo].[ab_abcontacttag] ct_lkp1
inner join DM_STG_CM.[dbo].[abtl_contacttagtype] ABTL_CT on ABTL_CT.ID=ct_lkp1.Type
)ct_lkp 
ON ct_lkp.[ABContactID] = b.[PublicID] AND ct_lkp.[Type] = 'claimparty'
WHERE ct_lkp.[PublicID] IS NULL
--AND ct.[ABContactID] IS NOT NULL
)X

 --select * from DM_XFM_LD.[dbo].[DM_XFM_CM_ABCONTACTTAG_SC5_EMP] 
 --select * from temptable_CM_employer_verifiedPC_temp2

--select PublicID from temptable_CM_employer_verifiedPC
--except
--select PublicID from [DM_XFM_LD].[dbo].[DM_POST_BGL_HOTFIX_SC5_EMP_XML]

--select * from [DM_XFM_LD].[dbo].[DM_POST_BGL_HOTFIX_SC5_EMP]
--where LAST_UPDATED_SOURCE__C_Diff='Y'

--drop ContactREF_temptable_employer_CM_eftdata
select * into [ContactREF_temptable_employer_CM_eftdata]
from
(
SELECT 
    distinct 
	 --a.[RowNumber] [xfm_RowNumber]
	a.[PublicID] [xfm_PublicID]
	,b.[PublicID] [lkp_PublicID]
	,b.[LinkID] [lkp_LinkID]
	,eft.[AccountName]
	,eft.[BankAccountNumber]
	,eft.[BankBranchName_icare]
	,eft.[BankName]
	,eft.[BankRoutingNumber]
	,eft.[BankType_icare]
	,eft.[IsPrimary]
	,eft.[Approved_icare]
	--,eft.[PublicID]
	,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(eft.[PublicID],'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') [PublicID]
	--Encrypting it to match what ETL is doing
	--,eft.[LinkID] [EFT_LinkID]
	,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(eft.[PublicID],'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') [EFT_LinkID]
FROM temptable_CM_employer_verifiedPC_temp2 a
--[dbo].[DM_POST_BGL_HOTFIX_SC5_EMP] a
-- LEFT JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] b ON a.[LinkID] = b.[LinkID]
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] b ON a.[LinkID] = b.[LinkID] --'0032O000005Bm5AQAS'
--INNER JOIN [dbo].[DM_XFM_CM_EFTDATA_SC5_EMP] eft
--ON eft.[ContactID] = b.[PublicID]
INNER JOIN [dbo].[ccst_eftdata] eft --QA EFT DATA SP should have already run before else this will not fetch any data
--ON eft.[ContactID] = b.[PublicID]
ON eft.[ContactID] = a.[PublicID]
--LEFT JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_EFTDATA] eft_lkp 
LEFT JOIN [DM_STG_CM].[dbo].[ab_eftdata] eft_lkp 
ON eft.[AccountName] = eft_lkp.[AccountName]
AND eft.[BankName] = eft_lkp.[BankName]
AND eft.[BankAccountNumber] = eft_lkp.[BankAccountNumber]
--AND eft.[Retired] = eft_lkp.[Retired]
AND eft_lkp.[Retired]=0
--AND b.[LinkID] = eft_lkp.[ContactLinkID]
AND b.[ID] = eft_lkp.[ContactID]
AND eft.[BankRoutingNumber] = eft_lkp.[BankRoutingNumber]
WHERE eft_lkp.[ContactID] IS NULL --ensuring the bank account details are not already existing
)X

--select * from ContactREF_temptable_employer_CM_eftdata
--select * from ContactREF_temptable_employer_CM_contacttag
 --select * from [ContactREF_temptable_employer_CM_all] a
--select lkp_linkID
--from
--(
--select b.Type, c.[BankName],a.lkp_LinkID from [ContactREF_temptable_employer_CM_all] a
--left outer join ContactREF_temptable_employer_CM_contacttag b
--on a.lkp_LinkID=b.lkp_LinkID
--left outer join ContactREF_temptable_employer_CM_eftdata c
--on a.lkp_LinkID=c.lkp_LinkID
--where a.lkp_LinkID='0012800001Ha4OlAAJ'
--)X
--group by X.lkp_linkID
--having count(1)>1

--select * from [ContactREF_temptable_employer_CM_all] a
--where lkp_LinkID='0012800001Ha4OlAAJ'

--select * from ContactREF_temptable_employer_CM_contacttag
--where lkp_LinkID='0012800001Ha4OlAAJ'

--select * from ContactREF_temptable_employer_CM_eftdata
--where lkp_LinkID='0012800001Ha4OlAAJ'

--0012800001HZj2iAAD

--select * from temptable_CM_employer_verifiedPC_temp2 a
--select LinkID,publicID,* from DM_XFM_LD.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] b 
--select * from DM_XFM_LD.[dbo].[DM_XFM_CM_EFTDATA_SC5_EMP] eft 
--select * from DM_XFM_LD.[dbo].[DM_XFM_CM_EFTDATA_SC5_EMP] eft
--REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE([PublicID],'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') [PublicID]

--select contactID, count(1) from [dbo].[ccst_eftdata] eft 
--group by contactID
--having count(1)>1
--select * from DM_XFM_LD.[dbo].[DM_LKP_GW_CM_EFTDATA] eft_lkp 
--where LUWID=1873132

--select * from [dbo].[ccst_eftdata] eft 
--where contactID='EMICS:1873132:INSURED'

--select
--*
--into REC_XFM.dbo.ContactREF_temptable_CM_employer_verifiedPC
--from
--(
--select * 
--from temptable_CM_employer_verifiedPC
--)x
--;

--select
--*
--into REC_XFM.dbo.ContactREF_temptable_employer_verifiedCDR
--from
--(
--select * 
--from temptable_EMPLOYER_VerifiedCDR
--)x
--;

IF OBJECT_ID('temptable_max_claim_number') is not null
drop table dbo.temptable_max_claim_number;

IF OBJECT_ID('temptable_CM_employer_verifiedPC_temp1') is not null
drop table dbo.temptable_CM_employer_verifiedPC_temp1;

IF OBJECT_ID('temptable_CM_employer_verifiedPC_temp2') is not null
drop table dbo.temptable_CM_employer_verifiedPC_temp2;

END