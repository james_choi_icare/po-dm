﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_SIGLITINDICATOR]
AS
BEGIN

-- =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Legal domain - ccst_siglitindicator_icare
-- Version: Initial build v30 17/06/2019
-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_siglitindicator_icare','Legal - v30 17/06/2019'

Select 
	cd.claim_number as LUWID,
	'EMICS:' + l.Claim_no + '_' + Convert(varchar, l.ID) + ':LITIGATION' as Matter_icareID,
	'EMICS:' + sl.Claim_no + '_' + convert(varchar, sl.Issue_ID) + '_' + sd.Issue_types + ':SIGNIFICANT_LEGAL_ISSUE' as PublicID,
	Case when sd.Issue_Types = 1 then 'other_icare'
		when sd.Issue_Types = 2 then 'novelorcomplexquestionsoflaw_icare'
		when sd.Issue_Types = 3 then 'constitutionalissues_icare'
		when sd.Issue_Types = 4 then 'seekstochallengethevalidityofthedeed_icare'
		when sd.Issue_Types = 5 then 'judicialreviewofanadministrativedecision_icare'
		when sd.Issue_Types = 6 then 'statintofthewclegislativescheme_icare'
		when sd.Issue_Types = 7 then 'hearingformorethan5hearingdays_icare'
		when sd.Issue_Types = 8 then 'seniorcounsel_icare'
		when sd.Issue_Types = 9 then 'Dispexistsbetweenschemeagents_icare'
		when sd.Issue_Types = 10 then 'litigationarisingfromfatalityofworker_icare'
		when sd.Issue_Types = 11 then 'other_icare'
		when sd.Issue_Types = 12 then 'mediaattention_icare'
		when sd.Issue_Types = 13 then 'bankruptcyproceedings_icare'
		when sd.Issue_Types = 14 then 'commonlawmore500k_icare'
		End as SignificantLitigation_icare

Into ccst_siglitindicator_icare

From DM_STG_EMICS.dbo.Significant_Legal_issue sl
Inner Join DM_STG_EMICS.dbo.Claim_detail cd on sl.claim_no = cd.claim_number
Inner join DM_STG_EMICS.dbo.Significant_legal_issue_Detail sd on sl.Issue_ID = sd.Issue_ID
Left outer join DM_STG_EMICS.dbo.Litigation l on sl.Claim_no = l.Claim_no
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

and l.ID <> 101 --9 records
and cd.is_null = 0
END