﻿-- =============================================
-- Author:		Saranya Jayaseelan
-- Create date: 04082019
-- Mapping Version : V12.34
--	
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_PERIODPOLICY]

AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_periodpolicy','Cover - v12.34 08/04/2019'
--drop table ccst_periodpolicy
	--Verified PC (Policy)
select distinct
    concat('EMICS:', rtrim(cast(CD.Claim_Number as varchar))) as ClaimInfoID,
    rtrim(CD.Claim_Number)  as LUWID,
    CASE WHEN cc_pp.PublicID is not null then cc_pp.PublicID  --match found in CC policy period
    ELSE concat('EMICS:', pc_pp.PolicyNumber, '_',pc_pp.TermNumber,':POLICY',':POLICYPERIOD')  -- when NO match found in CC policy period
		end as PolicyPeriodID,
     --concat('EMICS:',CD.Policy_No,'_',CLAIM_DETAIL.CLAIM_NUMBER,'_',':POLICY','_',':PERIOD_POLICY') as PublicID,
    concat('EMICS:',pc_pp.PolicyNumber,'_',rtrim(cd.CLAIM_NUMBER),':POLICY',':PERIODPOLICY') as PublicID
into ccst_periodpolicy
from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
LEFT OUTER join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No
LEFT OUTER join DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD on PD.POLICY_NO = PT.POLICY_NO
        and PD.POLICY_YEAR = CD.Policy_year
 -- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY 
      and CD.Date_of_Injury >= PD.PERIOD_START 
	  and CD.Date_of_Injury < PD.PERIOD_EXPIRY
INNER join CoverREF_temptable_consolidated temp_cons --Verified PC claims only
	on temp_cons.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join DM_STG_CC.dbo.cc_policyperiod cc_pp 
	on cc_pp.PolicyNumber = temp_cons.PC_PP_PolicyNumber 
and cc_pp.EffectiveDate = temp_cons.PC_PP_EffectiveDate --DON'T Remove timestamps when comparing
and cc_pp.ExpirationDate = temp_cons.PC_PP_ExpirationDate --DON'T Remove timestamps when comparing
and cc_pp.PolicyPeriodType=2 --Policy Level PP records only and NOT Account level
and cc_pp.Retired=0 --Ensuring we pick only Active records
inner join DM_STG_PC.dbo.pc_policyperiod pc_pp
on pc_pp.ID=temp_cons.PC_PP_ID
where CD.is_Null = 0

--80977

--Verified PC (Account)
Insert into ccst_periodpolicy
select
    'EMICS:' + ltrim(rtrim(CD.Claim_Number)) as ClaimInfoID,
    ltrim(rtrim(CD.Claim_Number))  as LUWID,
    CASE WHEN cc_pp.PublicID is not null then cc_pp.PublicID  --match found in CC policy period
    ELSE 'EMICS:' + ltrim(rtrim(pc_a.AccountNumber))  +  ':ACCOUNT' + ':POLICYPERIOD'
    END as PolicyPeriodID,  -- when NO match found in CC policy period
   concat('EMICS:',PC_A.AccountNumber,'_',RTRIM(CD.CLAIM_NUMBER),':ACCOUNT',':PERIODPOLICY')
   as PublicID

from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
INNER join CoverREF_temptable_consolidated temp_cons  --Verified PC claims only
on temp_cons.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join dm_stg_pc.dbo.pc_policyperiod PC_PP on PC_PP.ID=temp_cons.PC_PP_Id
left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
left outer join DM_STG_CC.dbo.cc_policyperiod cc_pp
--left outer join stg_cc.dbo.cc_policyperiod cc_pp 
--left outer join [TGT_GW_STG_CC].dbo.cc_policyperiod cc_pp
on cc_pp.AccountNumber = PC_A.AccountNumber 
and cc_pp.PolicyPeriodType=1 --Account Level PP records only and NOT Policy level
and cc_pp.Retired=0 --Making sure we pick only Active ACCOUNT records frm base table
where CD.is_Null=0

--80988


--Verified CDR/Invalid (policies)
Insert into ccst_periodpolicy
select
'EMICS:' + ltrim(rtrim(tmp.Claim_Number)) as ClaimInfoID,
tmp.Claim_Number as LUWID,
pol_period_CDR.PublicID as PolicyPeriodID,
--ltrim(rtrim(tmp.LUWID))  as LUWID,
--'EMICS:' + tmp.PolicyNumberCD + '_' + tmp.RENEWAL_NO + ':POLICY' + ':POLICYPERIOD'as PolicyPeriodID,
'EMICS:' + ltrim(rtrim(tmp.PolicyNumberCD)) + '_' + ltrim(rtrim(tmp.Claim_Number)) + ':POLICY' + ':PERIODPOLICY' as PublicID

from  CoverREF_temptable_ccst_policy_VerifiedCDR tmp
left outer join CoverREF_temptable_ccst_policyperiod_VerifiedCDR pol_period_CDR 
on tmp.Cover_EffectiveDate=pol_period_CDR.Cover_EffectiveDate
and tmp.Cover_ExpirationDate=pol_period_CDR.Cover_ExpirationDate
and tmp.PolicyNumberCD=pol_period_CDR.PolicyNumberCD

---Verified CDR/Invalid (accounts)
Insert into ccst_periodpolicy
select
--CDR_PM.POLICY_SOURCE_NUMBER,
'EMICS:' + ltrim(rtrim(tmp.Claim_Number)) as ClaimInfoID,
tmp.Claim_Number as LUWID,
pol_period_CDR.PublicID as PolicyPeriodID,
'EMICS:' + ltrim(rtrim(tmp.PolicyNumberCD)) + '_' + ltrim(rtrim(tmp.Claim_Number))  + ':ACCOUNT' + ':PERIODPOLICY' as PublicID
from  CoverREF_temptable_ccst_policy_VerifiedCDR tmp
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(tmp.Claim_Number)) 
left outer join CoverREF_temptable_ccst_policyperiod_ACCOUNT_VerifiedCDR pol_period_CDR 
on tmp.AccountNumber=pol_period_CDR.AccountNumber
END