﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_OCCUPATIONDETAILS]
AS
BEGIN
	-- =============================================
	-- Author: Ann
	-- Create date: 06/05/2019
	-- Domain: Triage ccst_occupationdetails_icare
	-- Comments: v35 mapping spec implemented
	--			14/05 Checked against v36.2 no change
	-- =============================================

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_occupationdetails_icare','Triage - v36.2 14/05/2019'
	-- drop table ccst_occupationdetails_icare

	SELECT 
	cd.claim_number as LUWID,
	'EMICS:'+ rtrim(ltrim(CT.Claim_No))+':CLAIM_TRIAGE' as PublicID,
	oc.UnitGroupDescription as UnitDesc,
	cd.Occupation_Code as UnitCode,
	--No source columns listed in the mapping spec for these target fields
	oc.MajorGroupCode as MajorCode,
	oc.MajorGroupDescription as MajorDesc,
	oc.MinorGroupCode as MinorCode,
	oc.MinorGroupDescription MinorDesc,
	oc.SubMajorGroupCode as SubMajorCode,
	oc.SubMajorGroupDescription as SubMajorDesc
	--ct.*,
	--oc.*
	into ccst_occupationdetails_icare


	FROM 
	[DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] CT
	INNER JOIN [DM_STG_EMICS].[dbo].[CLAIM_DETAIL] cd ON CT.Claim_No=cd.Claim_Number
	--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
	LEFT JOIN  [DM_STG_CC].[dbo].[ccx_occupationcode_icare] oc	ON cd.Occupation_Code = oc.UnitGroupCode
	where Occupation_Code is not null and oc.effectivedate > '2011'
	and cd.is_Null = 0

END