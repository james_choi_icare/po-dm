﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_POLICYPERIOD]
AS
BEGIN
	--COVER v12.35 VERIFIED POLICY RECORDS -- drop table ccst_policyperiod	
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_policyperiod','Cover - v12.39'

CREATE TABLE [dbo].[ccst_policyperiod](
	[PublicID] [varchar](100) NOT NULL,
	[AccountNumber] [varchar](100) NULL,
	[EffectiveDate] [datetime2] NULL,
	[ExpirationDate] [datetime] NULL,
	[LUWID] [varchar](100) NULL,
	[PolicyNumber] [varchar](100) NULL,
	[PolicyPeriodType] [varchar](30) NULL,
	[PolicySuffix] [varchar](30) NULL,
	[PolicyType] [varchar](30) NULL,
	[Query_SRC_VALUE] [varchar](30) NULL
) ON [PRIMARY]


--PC ACCOUNT RECORDS
Insert into ccst_policyperiod
select distinct
	'EMICS:' + convert(varchar,tmp.AccountNumber) + ':ACCOUNT' + ':POLICYPERIOD' as PublicID,
	cast(tmp.AccountNumber as varchar(100)) as AccountNumber,
	NULL as EffectiveDate,
	NULL as ExpirationDate,
	LUWID,
	NULL as PolicyNumber,
	x.Name as PolicyPeriodType,
	NULL as PolicySuffix,
	'WorkersComp' as PolicyType,
	cast('PCACCOUNTRECORDS'as varchar(100)) as Query_SRC_VALUE
from CoverREF_tmp_ccst_policyperiod_ACCOUNT_verifiedPC_before_Prod_check tmp --Run the tmp table SQL below first before
left outer join DM_STG_CC.dbo.cc_policyperiod cc_pp on cc_pp.AccountNumber = tmp.AccountNumber
	and cc_pp.PolicyPeriodType=1 --Account Level PP records only and NOT Policy level
	and cc_pp.Retired=0 --Making sure we pick only Active ACCOUNT records frm base table
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(tmp.LUWID)) 
Left outer join (Select name from  DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'account' 
where cc_pp.AccountNumber is null -- making sure we populate ACCOUNT record into ccst_policyperiod ONLY WHEN there is no corresponding ACTIVE ACCOUNT record in cc_policyperiod PROD base table


--Verified PC into ccst_policyperiod
Insert into ccst_policyperiod
select distinct
	'EMICS:' + convert(varchar,PC_PP_PolicyNumber) +  '_' + convert(varchar,TermNumber) + ':POLICY' + ':POLICYPERIOD' as PublicID,
	NULL as AccountNumber,
	-- JCFIX ----------------
	PC_PP_EffectiveDate as EffectiveDate,
	--PC_PP_ExpirationDate as EffectiveDate,
	-- JCFIX ----------------
	PC_PP_ExpirationDate as ExpirationDate,
	c.LUWID,
	PC_PP_PolicyNumber as PolicyNumber,
	x.Name as PolicyPeriodType,
	NULL as PolicySuffix,
	'WorkersComp' as PolicyType,
	cast('VERIFIEDPC'as varchar(100)) as Query_SRC_VALUE
	
from
	(select
		PC_PP.Id as PC_PP_Id,
		PC_A.AccountNumber,
		PC_PP.PeriodStart as PC_PP_EffectiveDate,
		PC_PP.PeriodEnd as PC_PP_ExpirationDate,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
		PC_PP.TermNumber,
		cc_pp.PublicID as cc_pp_PublicID
	from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
	left outer join CoverREF_temptable_consolidated temp_cons_pp on temp_cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
	left outer join (select ID,policyID,PolicyNumber,PeriodStart, periodend, TermNumber, CancellationDate from dm_stg_pc.dbo.pc_policyperiod
		Where status=9 and MostRecentModel=1 and Retired=0
		and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  ) PC_PP on temp_cons_pp.PC_PP_Id=PC_PP.ID
	left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
	left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	--left outer join stg_cc.dbo.cc_policyperiod cc_pp 
	--left outer join [TGT_GW_STG_CC].dbo.cc_policyperiod cc_pp 
	left outer join  DM_STG_CC.dbo.cc_policyperiod cc_pp 
	on cc_pp.PolicyNumber = PC_PP.PolicyNumber 
	and cc_pp.EffectiveDate = PC_PP.PeriodStart --DON'T remove timestamp during compare
	and cc_pp.ExpirationDate = PC_PP.PeriodEnd --DON'T remove timestamp during compare
	and cc_pp.PolicyPeriodType=2 --Policy Level PP records only and NOT Account level
	and cc_pp.Retired=0 --Ensuring we pick Active records only
	Left outer join (Select name from DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'Policy' 
	where CD.is_Null=0
	) b
inner join ( select a.PC_PP_Id, max(a.LUWID) as LUWID
	from (select
			PC_PP.Id as PC_PP_Id,
			PC_A.AccountNumber,
			PC_PP.PeriodStart as PC_PP_EffectiveDate,
			PC_PP.PeriodEnd as PC_PP_ExpirationDate,
			LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
			CD.Date_of_Injury,
			LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
			PC_PP.TermNumber 
			from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
			left outer join CoverREF_temptable_consolidated temp_cons_pp --Verified PC Claims only
			on temp_cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number)) --01/11/2018 : Added this new temp table - look at csst_policy selection to understand more about this table.
			left outer join (select ID,policyID,PolicyNumber,PeriodStart, periodend, TermNumber, CancellationDate
			from dm_stg_pc.dbo.pc_policyperiod
			Where status=9 and MostRecentModel=1 and Retired=0
			and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  ) PC_PP on temp_cons_pp.PC_PP_Id=PC_PP.ID
			left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
			left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
			Left outer join (Select name from  DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'Policy' 
			--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.claim_number)) 
			where CD.is_Null=0
		) a group by a.PC_PP_Id
	) c on c.PC_PP_Id=b.PC_PP_Id and c.LUWID=b.LUWID 
	Left outer join (Select name from  DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'Policy' 
	where b.cc_pp_PublicID is NULL  --Making sure we only create a policy period record in staging table when there is no matching base table record.


--Verified CDR/Invalid POLICY RECORDS
Insert into ccst_policyperiod
select	distinct
	'EMICS:' + convert(varchar, ltrim(rtrim(max_claim.PolicyNumberCD))) + '_' + convert(varchar,tmp.RENEWAL_NO) + ':POLICY' + ':POLICYPERIOD' as PublicID,
	NULL as AccountNumber, --	convert(varchar, tmp.AccountNumber),
	tmp.Cover_EffectiveDate as EffectiveDate,
	tmp.Cover_ExpirationDate as ExpirationDate,	
	tmp.LUWID,
	tmp.PolicyNumberCD as PolicyNumber,
	x.Name as PolicyPeriodType,
	NULL as PolicySuffix,
	PolicyType,
	'PolicyVerifiedCDR' as Query_SRC_VALUE
from 
CoverREF_temptable_ccst_policy_VerifiedCDR tmp
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(tmp.LUWID)) 		
inner join
(
	select PolicyNumberCD, Cover_EffectiveDate,Cover_ExpirationDate, max(LUWID) as max_LUWID 
	from CoverREF_temptable_ccst_policy_VerifiedCDR tmp
	group by PolicyNumberCD, Cover_EffectiveDate,Cover_ExpirationDate
) max_claim  --Getting the max(claim_number) record out as the Policy Period grain is cover start and end dates and policy number
on max_claim.max_LUWID=tmp.LUWID   
Left outer join (Select name from DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'policy' 


	
--Verified CDR/Invalid ACCOUNT RECORDS
Insert into ccst_policyperiod
select distinct
	'EMICS:' + ltrim(rtrim(cd.Policy_No)) + ':ACCOUNT' + ':POLICYPERIOD' as PublicID,
	convert(varchar, tmp.AccountNumber) as AccountNumber,
	NULL as EffectiveDate,
	NULL as ExpirationDate,
	tmp.LUWID,
	NULL as PolicyNumber,
	x.Name as PolicyPeriodType,
	NULL as PolicySuffix,
	'WorkersComp' as PolicyType,
	'AccountVERIFIEDCDR' as Query_SRC_VALUE
from CoverREF_temptable_ccst_policy_VerifiedCDR tmp
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(tmp.LUWID)) 
left join DM_STG_EMICS.dbo.claim_detail cd on cd.Claim_Number = tmp.LUWID
inner join
(
	select PolicyNumberCD,
	--Cover_EffectiveDate,Cover_ExpirationDate, 
	max(LUWID) as max_LUWID 
	from CoverREF_temptable_ccst_policy_VerifiedCDR tmp
	group by PolicyNumberCD
	--, Cover_EffectiveDate,Cover_ExpirationDate
) max_claim on max_claim.max_LUWID=tmp.LUWID
Left outer join (Select name from  DM_STG_CC.[dbo].[cctl_policyperiodtype]) x on x.Name = 'account' 

END