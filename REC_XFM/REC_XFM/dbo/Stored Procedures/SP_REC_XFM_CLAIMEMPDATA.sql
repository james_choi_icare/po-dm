﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMEMPDATA]
AS
BEGIN
	--========================================================
-- Author: ANN
-- Domain: Lodgement and linking table
-- Version: v33 ccst_claimempdata
--			checked v37 and no change required 
--			20/06/2019 checked v42 and no change required 
--========================================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimempdata','Lodgement - v42'

SELECT
		 
	'EMICS:' + RTRIM(CD.Claim_Number) + ':EMPLOYMENT_DATA' as ForeignEntityID,
	CD.Claim_number as LUWID,
	'EMICS:' + RTRIM(cd.claim_number) as OwnerID,
	'EMICS:' + RTRIM(CD.Claim_Number) + ':CLAIM_EMPLOYMENT' as PublicID

	into ccst_claimempdata


	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd --Claim Details Main Table
	--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.claim_number
	WHERE cd.is_Null = 0 --Exclude NULL Claims (Out Of Scope)


END