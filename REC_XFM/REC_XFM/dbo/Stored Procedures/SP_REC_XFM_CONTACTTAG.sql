﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 12/06/2019
-- Description:	Contacts - Named Insured V003.7  Main Contact v2.17
-- 1. This is the master Stored Proc that will call the different contacts type CONTACTTAG stored proc such as
--    SP_REC_XFM_CONTACTTAG_EMPLOYER_STAGING
-- <21-Jun-2019><Satish><MC v2.17>: Added a call to SP_REC_XFM_CONTACTTAG_MAINCONTACT_STAGING
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CONTACTTAG]
	
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_contact', 'EMP_v3.8 MC_v2.17'
EXEC SP_REC_XFM_CONTACTTAG_EMPLOYER_STAGING
EXEC SP_REC_XFM_CONTACTTAG_MAINCONTACT_STAGING
--EXEC SP_REC_XFM_CONTACT_INJURED_WORKER
;

IF OBJECT_ID('ccst_contacttag') is not null
drop table dbo.ccst_contacttag;



/****** Object:  Table [dbo].[ccst_contact]    Script Date: 5/30/2019 11:41:40 AM ******/

CREATE TABLE [dbo].[ccst_contacttag](
	[ContactID] [varchar](33) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ccst_contacttag] ADD [LUWID] [varchar](19) NULL
ALTER TABLE [dbo].[ccst_contacttag] ADD [addressbookUID] [varchar](64) NULL
ALTER TABLE [dbo].[ccst_contacttag] ADD [ExternalLinkID] [int] NULL
ALTER TABLE [dbo].[ccst_contacttag] ADD [PublicID] [varchar](44) NULL
ALTER TABLE [dbo].[ccst_contacttag] ADD [Type] [varchar](10) NOT NULL
;

INSERT INTO [dbo].ccst_contacttag
(
	   [ContactID]
      ,[LUWID]
      ,[addressbookUID]
      ,[ExternalLinkID]
      ,[PublicID]
      ,[Type]
)
SELECT [ContactID]
      ,[LUWID]
      ,[addressbookUID]
      ,[ExternalLinkID]
      ,[PublicID]
      ,[Type]
  FROM [dbo].[ContacttagREF_temptable_employer_verifiedPC]
  ;

INSERT INTO [dbo].ccst_contacttag
(
	   [ContactID]
      ,[LUWID]
      ,[addressbookUID]
      ,[ExternalLinkID]
      ,[PublicID]
      ,[Type]
)
SELECT [ContactID]
      ,[LUWID]
      ,[addressbookUID]
      ,[ExternalLinkID]
      ,[PublicID]
      ,[Type]
  FROM [dbo].[ContacttagREF_temptable_MC_matched]
  ;


END