﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_WPIDOCTORASSESSMENT]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Create date: 17/05
-- Description:	Work Capacity ccst_WPIDoctorAssessment_icare
-- Version: 17/05/2019 initial build on v8.0 mapping spec
			10/06/2019 v1.2 users and groups for Reviewer_icareID
			24/06/2019 checked against version 9.2 spec - no change
-- ============================================= */
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_wpidoctorassessment_icare','Work Capacity - v9.2 24/06/2019'

SELECT 
	'EMICS:'+RTRIM(cd.CLAIM_NUMBER)+'_'+ nom_treating_doctor+':VENDOR' as AssessingDoctor_icareID,
	mc.Date_From as AssessmentDate_icare,
	NULL as AssessmentReport_icareID,
	cd.Claim_Number as LUWID,
	'EMICS:'+RTRIM(cd.claim_number)+'_'+convert(varchar, mc.ID) +':WPIDOCTORASSESSMENT' as PublicID,
	--Reviewer_icareID
	'EMICS:' + RTRIM(cd.Claim_Number) + ':WPIASSESSRECORD' as WPIAssessRecord_icareID,
	NULL as WPIInitiatedBy_icare,
	NULL as ReportCorrect_icare,
	mc.Received_Date as ReportReceivedDate_icare,
	Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID, cc_user2.PublicID, cc_user_sm.PublicID) 
		End as Reviewer_icareID --v1.2

Into ccst_WPIDoctorAssessment_icare

FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD 
--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.CLAIM_NUMBER = CAD.CLAIM_NO
LEFT OUTER JOIN DM_STG_EMICS.dbo.MEDICAL_CERT MC ON CD.CLAIM_NUMBER = MC.CLAIM_NO

LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cd.Owner)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co2  ON (co2.alias = cad.Claims_Officer)

LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)

LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) 
		- CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
		ON (co2.Alias = cntrl2.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID)

left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)

left join DM_XFM.[dbo].[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on ( cntrl.EML_Email = lkp_eml_gwcc_queues.EMLEmailAddress 
			 and cad.Claims_Officer = lkp_eml_gwcc_queues.EMLQueueUser)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_user_queue on (lkp_eml_gwcc_queues.GWCC_Claim_User = cc_credential_user_queue.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_queue on (cc_user_queue.CredentialID = cc_credential_user_queue.ID)

WHERE CD.IS_NULL =0 AND  CAD.WPI != -1  AND MC.ID IS NOT NULL
ORDER BY CD.CLAIM_NUMBER,DATE_FROM


END