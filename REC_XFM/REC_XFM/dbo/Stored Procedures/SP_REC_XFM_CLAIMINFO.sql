﻿-- =============================================
-- Author:		Saranya
-- Modified date:
-- Domain: Lodgement and linking table ccst_claiminfo
-- Comments: v33
--			Checked v37 and no changes required
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMINFO] 
	
AS
BEGIN

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claiminfo','Lodgement - v37 21/05/2019'

--drop table ccst_claiminfo
	SELECT
		'EMICS:' + LTRIM(RTRIM(CD.Claim_Number)) as CLaimID,
		cd.Claim_Number AS ClaimNumber,
		0 as CoverageLineMatchDataInfoValid,
		'aud' as Currency,
		0 as DoNotDestroy,
		cd.Claim_Number as LUWID,
		'EMICS:' + RTRIM(CD.Claim_Number) as PublicID,
		NULL as PurgeDate,
		'EMICS:' + RTRIM(cd.claim_number) as RootPublicID
	into ccst_claiminfo 
	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd --Claim Details Main Table
--	Inner Join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
	WHERE cd.is_Null = 0 --Exclude NULL Claims (Out Of Scope)
		--AND cd.Result_of_Injury_code = 1 --Only use this flag to filter the Fatality Claims. 1 = Death (ie: Fatality)

	
END