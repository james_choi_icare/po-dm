﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CHECKGROUP]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Create date: 23/05/2019
-- Description:	Payments and recoveries ccst_checkgroup
-- Version: 23/05/2019 initial build v45.10 spec
			10/06/2019 v46.2.1 Updated transformation rules for CheckSetID, PublicID
			26/06/2019 v46.3.8 no changes - still showing duplicate publicID
			04/09/2019 v47.2 updated publicid xform, added cpc SC
-- ============================================= */
--drop table ccst_checkgroup
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_checkgroup','Payments and Recoveries - v47.2 04/09/2019'


Select distinct
	RTRIM(Pr.Claim_no) as LUWID,
	'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,pr.Payment_no)+'_'+case when ISNULL(nullif( PR.Adjust_Trans_Flag,''),'N')='Y' then '1' else '0' end+'_'+isnull(convert(varchar,cpc.ID),'00') +':CHECKSET' as CheckSetID,
		--'_' + Case when ISNULL(NULLIF(PR.Estimate_type,''),'N')='Y' Then '1' else '0' end + ':CHECKSET' as ChecksetID, 
	'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,pr.Payment_no)+'_'+case when ISNULL(nullif( PR.Adjust_Trans_Flag,''),'N')='Y'
		then '1' else '0' end +'_'+isnull(convert(varchar,cpc.ID),'00') +':CHECKGROUP' as PublicID


into ccst_checkgroup

from DM_STG_EMICS.dbo.Payment_Recovery PR
inner join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number=pr.claim_no and  cd.is_null=0
inner join (select distinct PR_Adj.Claim_No,PR_Adj.Payment_no,PR_gar.Payment_no as Garnishee_payment_no,pr_adj.Adjust_Trans_Flag from 
(select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
inner join (select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1)PR_Gar
on PR_Adj.Claim_No=PR_Gar.Claim_No
and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
and pr_adj.Garnishee_Payment_id=PR_Gar.id
) prg
on pr.Claim_No=prg.Claim_No
and pr.Payment_no=prg.Payment_no
and pr.Trans_Amount>0 and Reversed<>1 and pr.Garnishee_No is null
LEFT OUTER JOIN (
SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_STG_CC..CCX_paycodepaymenttype_icare A,
 (select * from DM_STG_CC..CCX_paycode_icare where Retired=0) B,DM_STG_CC..cctl_paymenttype_icare C
WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC
ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)
WHERE PR.ESTIMATE_TYPE<70 AND CD.is_NULL=0-- Checks are created only for payments


END