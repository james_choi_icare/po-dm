﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_WIC]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Modified date: 09/05
-- Comments: COVER v12.35 ccst_wic_icare 
--			14/05 checked against v12.36 no change
-- =============================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_wic_icare','Cover - v12.36'
	---Verified PC and matched WIC
	select 

	CASE 
		WHEN len(CD.Tariff_No)<>3 THEN ccx_wic.WICDescription
		ELSE 'Not applicable' --DMIG-4544 confirmed with Mike Dai
		END as BusinessDescription,

	CASE 
		WHEN len(CD.Tariff_No)=5 THEN concat('0',convert(varchar,CD.Tariff_No))
		WHEN len(CD.Tariff_No)=3 THEN '000000' --DMIG-4544 confirmed with Mike Dai
		ELSE CD.Tariff_No
		END as Code,
	concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PC_CC.ID,':POLICY',':COSTCENTREICARE') as CostCentreID,
	CASE 
		WHEN len(CD.Tariff_No)<>3 THEN ccx_wic.WICDescription
		ELSE 'Not applicable' --DMIG-4544 confirmed with Mike Dai
		END as Description,


	LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
	concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),':POLICY',':WICICARE') as PublicID
	into ccst_wic_icare

	from CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
	inner join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_WIC_matched.Claim_Number
	left outer join dm_stg_pc.dbo.pc_policylocation PL on PL.ID=temp_WIC_matched.PolicyLocation
	left outer join dm_stg_pc.dbo.pcx_costcenter_icare PC_CC on PC_CC.ID=temp_WIC_matched.CostCenter_icare
	left outer join DM_STG_CC.dbo.ccx_wiccode_icare ccx_wic on 
		CASE 
			WHEN len(CD.Tariff_No)=5 THEN concat('0',convert(varchar,CD.Tariff_No))
			ELSE CD.Tariff_No
			END =ccx_wic.WICCode
	

	--Verified CDR/Invalid/Unmatched WIC
	insert into ccst_wic_icare
	select
	'Dont map' as BusinessDescription,
	CASE 
		WHEN len(CD.Tariff_No)=5 THEN concat('0',convert(varchar,CD.Tariff_No))
		WHEN len(CD.Tariff_No)=3 THEN '000000' --DMIG-4544 confirmed with Mike Dai
		ELSE CD.Tariff_No
		END as Code,
	concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':POLICY',':COSTCENTREICARE') as CostCentreID,
	'Dont map' as Description,

	LTRIM(RTRIM(CD.Claim_Number)) as LUWID,

	concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':POLICY',':WICICARE') as PublicID

	from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
	left outer join DM_STG_CC.dbo.ccx_wiccode_icare  ccx_wic on 
		CASE
		WHEN len(CD.Tariff_No)=5 THEN concat('0',convert(varchar,CD.Tariff_No))
		ELSE CD.Tariff_No
		END =ccx_wic.WICCode		
		where 
		CD.is_null=0
		and ltrim(rtrim(CD.Claim_Number)) in
		(
			select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_unmatched_WIC
			union
			select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_verified_CDR
		)
	

END