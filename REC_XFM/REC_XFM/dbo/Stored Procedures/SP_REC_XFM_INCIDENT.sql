﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_INCIDENT]
AS
BEGIN
	-- ===============================
-- Author: Ann
-- Domain: Lodgement 
-- Version: v33 mapping spec ccst_incident
--			11/06/2019 v38 - Removed unused joins, added fields
--			11/07/2019 v43 checked against latest mapping spec, updated 
-- =====================================
--drop table ccst_incident

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_incident','Lodgement - v43 07/11/2019'


;WITH incident_claim as (
SELECT
	cd.Agency_of_injury as AgencyOfInjuryCode_icare,
	ad.Description as AgencyofInjuryDesc_icare,
	cd.Agency_of_accident as BreakdownAgencyCode_icare,
	ad2.Description as BreakdownAgencyDesc_icare,
	 RTRIM(cd.Claim_Number) as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,

	CASE WHEN Date_Contact_Initiated IS NOT NULL THEN convert(datetime2 ,Date_Contact_Initiated)
		 WHEN Is_Rehab = 1 THEN 
			Case when datepart(dw, cd.Date_Significant) in (1,2,3) then cast(DATEADD(DD,3,Date_Significant) as datetime2) --sun, mon, tue + 3 working days
				When datepart(dw, cd.Date_Significant) in (4,5,6) then  cast(DATEADD(DD,5,Date_Significant) as datetime2) --wed, thurs, fri then add 5 days to cover 3 working days
				When datepart(dw, cd.Date_Significant) = 7 then cast(DATEADD(DD,4,Date_Significant) as datetime2) -- sat then add 4 days to cover 3 work days 
			end
		END AS ContactCompleteDate_icare,
	59 as DetailedInjuryType,
	Case when duty_status_code = 1 then 1
		when duty_status_code = 2 then 2
		when duty_status_code = 3 then 3
		when duty_status_code = 4 then 4
		when duty_status_code = 5 then 5
		when duty_status_code = 6 then 6
		End as DutyStatus_icare,
	Case when fatality.claim_liability_indicator = 1 then 'inprogress'
		when fatality.claim_liability_indicator = 2 then 'liabilityaccepted'
		when fatality.claim_liability_indicator = 5 then 'inprogress'
		when fatality.claim_liability_indicator = 6 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 7 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 8 then 'noncompensabledeath'
		when fatality.claim_liability_indicator = 9 then 'indispute'
		when fatality.claim_liability_indicator = 10 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 11 then 'liabilityaccepted'
		End as FatalityLiabDec_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityLiabDecisionDate_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityNotificationDate_icare,
	'EMICS:' + 'INCIDENT_ADDRESS' + RTRIM(cd.claim_number) as LocationAddress,
	Case when Len(cd.Mechanism_of_injury) = 1 then '0' + CONVERT(VARCHAR,cd.Mechanism_of_Injury) else cd.Mechanism_of_injury 
		End as MechanismOfInjuryCode_icare,
	mc.Description as MechanismOfInjuryDesc_icare,
	Case when ur.Option_answer = 'Y' then 1 else 0 
		end as MultipleInjuries_icare,
	cd.Nature_of_Injury  as NatureOfInjuryCode_icare,
	ltrim(rtrim(i.Description)) as NatureOfInjuryDesc_icare,
	CASE WHEN ICD.AA_Midrange < 0
		THEN 0 
		ELSE ICD.AA_Midrange 
	 END AS ODGDuration_icare
	,CASE 
		WHEN ICD.AA_Midrange > 0
		THEN DATEADD(DD, ICD.AA_Midrange, CONVERT(Date, CD.Date_of_Injury, 103)) 
		ELSE NULL
	 END AS ODGRTWDate_icare,
 	'EMICS:' + RTRIM(CD.Claim_Number) + ':INCIDENT' as PublicID,
	CASE WHEN Is_Rehab = 1 THEN Date_Significant
         ELSE NULL
         END as SignificantInjuryDate_icare

FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad ON cd.Claim_Number = cad.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct ON cd.Claim_Number = ct.Claim_No
 INNER JOIN DM_STG_EMICS.dbo.ICD icd ON cde.ICD_Code1 = icd.ICD_10_Code

Left join (Select Result_of_Injury_code, Claim_Number, Claim_Liability_Indicator, Date_of_liability from DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
	Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cad.Claim_no = cd.Claim_Number
	where cd.is_NULL =0 and cd.REsult_of_injury_code =1) fatality on fatality.Claim_number = cd.claim_number 

 --Reference Tables
 --INCIDENT Agency codes
 LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad ON ad.Code = cd.AGENCY_OF_INJURY 
  AND ad.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad.Code)

   LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad2 ON ad2.Code = cd.Agency_of_Accident 
  AND ad2.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad2.Code)


 LEFT OUTER JOIN DM_STG_EMICS.dbo.MECHANISM_CODES mc ON mc.Code = cd. MECHANISM_OF_INJURY 
  AND mc.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.MECHANISM_CODES WHERE Code = mc.Code)

 LEFT OUTER JOIN DM_STG_EMICS.dbo.INJURY i ON i.ID = cd.Nature_ID 
  AND i.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.INJURY WHERE Code = i.Code)

LEFT OUTER JOIN DM_STG_EMICS.dbo.User_Response ur ON cd.Claim_Number = ur.Ref_No 
  AND ur.Question_ID IN(38,39,40) 
  AND ur.ID = (SELECT MAX(ID) FROM DM_STG_EMICS.dbo.User_Response WHERE Ref_No = cd.Claim_Number AND Question_ID IN(38,39,40))
 
WHERE cd.is_Null = 0 --Exclude NULL Claims (Out Of Scope)
--ORDER BY cd.Claim_Number 

), Medical_exposure as (
Select
	cd.Agency_of_injury as AgencyOfInjuryCode_icare,
	ad.Description as AgencyofInjuryDesc_icare,
	cd.Agency_of_accident as BreakdownAgencyCode_icare,
	ad2.Description as BreakdownAgencyDesc_icare,
	 RTRIM(cd.Claim_Number) as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,

	CASE WHEN Date_Contact_Initiated IS NOT NULL THEN convert(datetime2 ,Date_Contact_Initiated)
		 WHEN Is_Rehab = 1 THEN 
			Case when datepart(dw, cd.Date_Significant) in (1,2,3) then cast(DATEADD(DD,3,Date_Significant) as datetime2) --sun, mon, tue + 3 working days
				When datepart(dw, cd.Date_Significant) in (4,5,6) then  cast(DATEADD(DD,5,Date_Significant) as datetime2) --wed, thurs, fri then add 5 days to cover 3 working days
				When datepart(dw, cd.Date_Significant) = 7 then cast(DATEADD(DD,4,Date_Significant) as datetime2) -- sat then add 4 days to cover 3 work days 
			end
		END AS ContactCompleteDate_icare,
	59 as DetailedInjuryType,
	Case when duty_status_code = 1 then 1
		when duty_status_code = 2 then 2
		when duty_status_code = 3 then 3
		when duty_status_code = 4 then 4
		when duty_status_code = 5 then 5
		when duty_status_code = 6 then 6
		End as DutyStatus_icare,
	Case when fatality.claim_liability_indicator = 1 then 'inprogress'
		when fatality.claim_liability_indicator = 2 then 'liabilityaccepted'
		when fatality.claim_liability_indicator = 5 then 'inprogress'
		when fatality.claim_liability_indicator = 6 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 7 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 8 then 'noncompensabledeath'
		when fatality.claim_liability_indicator = 9 then 'indispute'
		when fatality.claim_liability_indicator = 10 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 11 then 'liabilityaccepted'
		End as FatalityLiabDec_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityLiabDecisionDate_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityNotificationDate_icare,
	'EMICS:' + 'INCIDENT_ADDRESS' + RTRIM(cd.claim_number) as LocationAddress,
	Case when Len(cd.Mechanism_of_injury) = 1 then '0' + CONVERT(VARCHAR,cd.Mechanism_of_Injury) else cd.Mechanism_of_injury 
		End as MechanismOfInjuryCode_icare,
	mc.Description as MechanismOfInjuryDesc_icare,
	Case when ur.Option_answer = 'Y' then 1 else 0 
		end as MultipleInjuries_icare,
	cd.Nature_of_Injury  as NatureOfInjuryCode_icare,
	ltrim(rtrim(i.Description)) as NatureOfInjuryDesc_icare,
	CASE WHEN ICD.AA_Midrange < 0
		THEN 0 
		ELSE ICD.AA_Midrange 
	 END AS ODGDuration_icare
	,CASE 
		WHEN ICD.AA_Midrange > 0
		THEN DATEADD(DD, ICD.AA_Midrange, CONVERT(Date, CD.Date_of_Injury, 103)) 
		ELSE NULL
	 END AS ODGRTWDate_icare,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':MEDICAL_EXPOSURE' AS PublicID,
	CASE WHEN Is_Rehab = 1 THEN Date_Significant
         ELSE NULL
         END as SignificantInjuryDate_icare
		 --,cd.is_Medical_Only
		 --,0 AS ClaimIndicator
	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd
	left outer join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD on CAD.Claim_no=CD.Claim_Number
	 INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
	 INNER JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct ON cd.Claim_Number = ct.Claim_No
	 INNER JOIN DM_STG_EMICS.dbo.ICD icd ON cde.ICD_Code1 = icd.ICD_10_Code

Left join (Select Result_of_Injury_code, Claim_Number, Claim_Liability_Indicator, Date_of_liability from DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
	Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cad.Claim_no = cd.Claim_Number
	where cd.is_NULL =0 and cd.REsult_of_injury_code =1) fatality on fatality.Claim_number = cd.claim_number 

 --Reference Tables
 --INCIDENT Agency codes
 LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad ON ad.Code = cd.AGENCY_OF_INJURY 
  AND ad.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad.Code)

   LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad2 ON ad2.Code = cd.Agency_of_Accident 
  AND ad2.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad2.Code)


 LEFT OUTER JOIN DM_STG_EMICS.dbo.MECHANISM_CODES mc ON mc.Code = cd. MECHANISM_OF_INJURY 
  AND mc.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.MECHANISM_CODES WHERE Code = mc.Code)

 LEFT OUTER JOIN DM_STG_EMICS.dbo.INJURY i ON i.ID = cd.Nature_ID 
  AND i.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.INJURY WHERE Code = i.Code)

LEFT OUTER JOIN DM_STG_EMICS.dbo.User_Response ur ON cd.Claim_Number = ur.Ref_No 
  AND ur.Question_ID IN(38,39,40) 
  AND ur.ID = (SELECT MAX(ID) FROM DM_STG_EMICS.dbo.User_Response WHERE Ref_No = cd.Claim_Number AND Question_ID IN(38,39,40))
 
	
	WHERE cd.IS_Null = 0
	 AND (  cd.is_Medical_Only = 1  OR EXISTS (
		 SELECT Claim_No 
		 FROM (select * from DM_STG_EMICS.dbo.Payment_Recovery where Reversed=0) Payment_Recovery --DMIG-5459 <Satish> Added the reversed check
		 WHERE Estimate_type IN(51,52,53,55,56,63,64) -- Medical
		 AND Claim_No = cd.Claim_Number
		 )
		  OR exists 
		(
		 SELECT Claim_no FROM [DM_STG_EMICS].[dbo].[Medical_Cert] 
		 WHERE is_deleted = 0 AND Claim_no = cd.Claim_Number
		 )
		 AND CAD.Claim_Liability_Indicator <> 12 --DMIG-5459 <Satish> Added the check on CAD table
		 ) 
	--ORDER BY 1
) , 

weekly_benefit_exposure as (
SELECT 
	cd.Agency_of_injury as AgencyOfInjuryCode_icare,
	ad.Description as AgencyofInjuryDesc_icare,
	cd.Agency_of_accident as BreakdownAgencyCode_icare,
	ad2.Description as BreakdownAgencyDesc_icare,
	 RTRIM(cd.Claim_Number) as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,

	CASE WHEN Date_Contact_Initiated IS NOT NULL THEN convert(datetime2 ,Date_Contact_Initiated)
		 WHEN Is_Rehab = 1 THEN 
			Case when datepart(dw, cd.Date_Significant) in (1,2,3) then cast(DATEADD(DD,3,Date_Significant) as datetime2) --sun, mon, tue + 3 working days
				When datepart(dw, cd.Date_Significant) in (4,5,6) then  cast(DATEADD(DD,5,Date_Significant) as datetime2) --wed, thurs, fri then add 5 days to cover 3 working days
				When datepart(dw, cd.Date_Significant) = 7 then cast(DATEADD(DD,4,Date_Significant) as datetime2) -- sat then add 4 days to cover 3 work days 
			end
		END AS ContactCompleteDate_icare,
	59 as DetailedInjuryType,
	Case when duty_status_code = 1 then 1
		when duty_status_code = 2 then 2
		when duty_status_code = 3 then 3
		when duty_status_code = 4 then 4
		when duty_status_code = 5 then 5
		when duty_status_code = 6 then 6
		End as DutyStatus_icare,
	Case when fatality.claim_liability_indicator = 1 then 'inprogress'
		when fatality.claim_liability_indicator = 2 then 'liabilityaccepted'
		when fatality.claim_liability_indicator = 5 then 'inprogress'
		when fatality.claim_liability_indicator = 6 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 7 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 8 then 'noncompensabledeath'
		when fatality.claim_liability_indicator = 9 then 'indispute'
		when fatality.claim_liability_indicator = 10 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 11 then 'liabilityaccepted'
		End as FatalityLiabDec_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityLiabDecisionDate_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityNotificationDate_icare,
	'EMICS:' + 'INCIDENT_ADDRESS' + RTRIM(cd.claim_number) as LocationAddress,
	Case when Len(cd.Mechanism_of_injury) = 1 then '0' + CONVERT(VARCHAR,cd.Mechanism_of_Injury) else cd.Mechanism_of_injury 
		End as MechanismOfInjuryCode_icare,
	mc.Description as MechanismOfInjuryDesc_icare,
	Case when ur.Option_answer = 'Y' then 1 else 0 
		end as MultipleInjuries_icare,
	cd.Nature_of_Injury  as NatureOfInjuryCode_icare,
	ltrim(rtrim(i.Description)) as NatureOfInjuryDesc_icare,
	CASE WHEN ICD.AA_Midrange < 0
		THEN 0 
		ELSE ICD.AA_Midrange 
	 END AS ODGDuration_icare
	,CASE 
		WHEN ICD.AA_Midrange > 0
		THEN DATEADD(DD, ICD.AA_Midrange, CONVERT(Date, CD.Date_of_Injury, 103)) 
		ELSE NULL
	 END AS ODGRTWDate_icare,
 	'EMICS:' + RTRIM(cd.Claim_Number) + ':WEEKLY_BENEFIT_EXPOSURE' AS PublicID,
	CASE WHEN Is_Rehab = 1 THEN Date_Significant
         ELSE NULL
         END as SignificantInjuryDate_icare

FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad ON cd.Claim_Number = cad.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct ON cd.Claim_Number = ct.Claim_No
 INNER JOIN DM_STG_EMICS.dbo.ICD icd ON cde.ICD_Code1 = icd.ICD_10_Code

Left join (Select Result_of_Injury_code, Claim_Number, Claim_Liability_Indicator, Date_of_liability from DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
	Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cad.Claim_no = cd.Claim_Number
	where cd.is_NULL =0 and cd.REsult_of_injury_code =1) fatality on fatality.Claim_number = cd.claim_number 

 --Reference Tables
 --INCIDENT Agency codes
 LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad ON ad.Code = cd.AGENCY_OF_INJURY 
  AND ad.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad.Code)

   LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad2 ON ad2.Code = cd.Agency_of_Accident 
  AND ad2.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad2.Code)


 LEFT OUTER JOIN DM_STG_EMICS.dbo.MECHANISM_CODES mc ON mc.Code = cd. MECHANISM_OF_INJURY 
  AND mc.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.MECHANISM_CODES WHERE Code = mc.Code)

 LEFT OUTER JOIN DM_STG_EMICS.dbo.INJURY i ON i.ID = cd.Nature_ID 
  AND i.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.INJURY WHERE Code = i.Code)

LEFT OUTER JOIN DM_STG_EMICS.dbo.User_Response ur ON cd.Claim_Number = ur.Ref_No 
  AND ur.Question_ID IN(38,39,40) 
  AND ur.ID = (SELECT MAX(ID) FROM DM_STG_EMICS.dbo.User_Response WHERE Ref_No = cd.Claim_Number AND Question_ID IN(38,39,40))
 
WHERE cd.IS_Null = 0
 AND (cd.is_Time_Lost = 1 
  OR cd.Date_Deceased IS NOT NULL
  OR EXISTS(SELECT Claim_No 
     FROM DM_STG_EMICS.dbo.Payment_Recovery 
     WHERE Estimate_type IN(50,54,58,59,60,65,72) -- Weekly Benefit
      AND Claim_No = cd.Claim_Number)) 
--ORDER BY 1
) , 

employer_liability_exposure as (
	SELECT 
	 cd.Agency_of_injury as AgencyOfInjuryCode_icare,
	ad.Description as AgencyofInjuryDesc_icare,
	cd.Agency_of_accident as BreakdownAgencyCode_icare,
	ad2.Description as BreakdownAgencyDesc_icare,
	 RTRIM(cd.Claim_Number) as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,

	CASE WHEN Date_Contact_Initiated IS NOT NULL THEN convert(datetime2 ,Date_Contact_Initiated)
		 WHEN Is_Rehab = 1 THEN 
			Case when datepart(dw, cd.Date_Significant) in (1,2,3) then cast(DATEADD(DD,3,Date_Significant) as datetime2) --sun, mon, tue + 3 working days
				When datepart(dw, cd.Date_Significant) in (4,5,6) then  cast(DATEADD(DD,5,Date_Significant) as datetime2) --wed, thurs, fri then add 5 days to cover 3 working days
				When datepart(dw, cd.Date_Significant) = 7 then cast(DATEADD(DD,4,Date_Significant) as datetime2) -- sat then add 4 days to cover 3 work days 
			end
		END AS ContactCompleteDate_icare,
	59 as DetailedInjuryType,
	Case when duty_status_code = 1 then 1
		when duty_status_code = 2 then 2
		when duty_status_code = 3 then 3
		when duty_status_code = 4 then 4
		when duty_status_code = 5 then 5
		when duty_status_code = 6 then 6
		End as DutyStatus_icare,
	Case when fatality.claim_liability_indicator = 1 then 'inprogress'
		when fatality.claim_liability_indicator = 2 then 'liabilityaccepted'
		when fatality.claim_liability_indicator = 5 then 'inprogress'
		when fatality.claim_liability_indicator = 6 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 7 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 8 then 'noncompensabledeath'
		when fatality.claim_liability_indicator = 9 then 'indispute'
		when fatality.claim_liability_indicator = 10 then 'liabilitydeclined'
		when fatality.claim_liability_indicator = 11 then 'liabilityaccepted'
		End as FatalityLiabDec_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityLiabDecisionDate_icare,
	CONVERT(datetime2, convert(date,fatality.date_of_liability)) as FatalityNotificationDate_icare,
	'EMICS:' + 'INCIDENT_ADDRESS' + RTRIM(cd.claim_number) as LocationAddress,
	Case when Len(cd.Mechanism_of_injury) = 1 then '0' + CONVERT(VARCHAR,cd.Mechanism_of_Injury) else cd.Mechanism_of_injury 
		End as MechanismOfInjuryCode_icare,
	mc.Description as MechanismOfInjuryDesc_icare,
	Case when ur.Option_answer = 'Y' then 1 else 0 
		end as MultipleInjuries_icare,
	cd.Nature_of_Injury  as NatureOfInjuryCode_icare,
	ltrim(rtrim(i.Description)) as NatureOfInjuryDesc_icare,
	CASE WHEN ICD.AA_Midrange < 0
		THEN 0 
		ELSE ICD.AA_Midrange 
	 END AS ODGDuration_icare
	,CASE 
		WHEN ICD.AA_Midrange > 0
		THEN DATEADD(DD, ICD.AA_Midrange, CONVERT(Date, CD.Date_of_Injury, 103)) 
		ELSE NULL
	 END AS ODGRTWDate_icare,
 	'EMICS:' + RTRIM(cd.Claim_Number) + ':EMPLOYER_LIABILITY_EXPOSURE' AS PublicID ,
	CASE WHEN Is_Rehab = 1 THEN Date_Significant
         ELSE NULL
         END as SignificantInjuryDate_icare

FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad ON cd.Claim_Number = cad.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
 INNER JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct ON cd.Claim_Number = ct.Claim_No
 INNER JOIN DM_STG_EMICS.dbo.ICD icd ON cde.ICD_Code1 = icd.ICD_10_Code

Left join (Select Result_of_Injury_code, Claim_Number, Claim_Liability_Indicator, Date_of_liability from DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
	Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cad.Claim_no = cd.Claim_Number
	where cd.is_NULL =0 and cd.REsult_of_injury_code =1) fatality on fatality.Claim_number = cd.claim_number 

 --Reference Tables
 --INCIDENT Agency codes
 LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad ON ad.Code = cd.AGENCY_OF_INJURY 
  AND ad.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad.Code)

   LEFT OUTER JOIN DM_STG_EMICS.dbo.AGENCY_DESC ad2 ON ad2.Code = cd.Agency_of_Accident 
  AND ad2.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.AGENCY_DESC WHERE Code = ad2.Code)


 LEFT OUTER JOIN DM_STG_EMICS.dbo.MECHANISM_CODES mc ON mc.Code = cd. MECHANISM_OF_INJURY 
  AND mc.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.MECHANISM_CODES WHERE Code = mc.Code)

 LEFT OUTER JOIN DM_STG_EMICS.dbo.INJURY i ON i.ID = cd.Nature_ID 
  AND i.Version = (SELECT MAX(Version) FROM DM_STG_EMICS.dbo.INJURY WHERE Code = i.Code)

LEFT OUTER JOIN DM_STG_EMICS.dbo.User_Response ur ON cd.Claim_Number = ur.Ref_No 
  AND ur.Question_ID IN(38,39,40) 
  AND ur.ID = (SELECT MAX(ID) FROM DM_STG_EMICS.dbo.User_Response WHERE Ref_No = cd.Claim_Number AND Question_ID IN(38,39,40))
	WHERE cd.IS_Null = 0
	 AND EXISTS(SELECT Claim_No 
	  FROM DM_STG_EMICS.dbo.Payment_Recovery 
	  WHERE Estimate_type IN(57) -- Employer Liability
	   AND Claim_No = cd.Claim_Number)
--	ORDER BY 1
) , 
alltypes as (
	Select * from incident_claim
		UNION
	select * from medical_exposure
		UNION
	Select * from weekly_benefit_exposure
		UNION
	Select * from employer_liability_exposure
)

Select * into ccst_incident 
from alltypes

END