﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_MEDICALCONTACTSTATUS]
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_medicalcontactstatus','Claim Management - v23 20/04/2019'

	-----Version 23 mapping for Claim Management

	SELECT 'EMICS:' + ltrim(rtrim(mc.Claim_no)) + '_' + cast(mc.ID as varchar(50)) + ':MEDICALCONTACTSTATUS' PublicID,
	RTRIM(mc.Claim_no) as LUWID,
	'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INJURED' ClaimContactID,
	'EMICS:' + ltrim(rtrim(cd.Claim_Number)) ClaimID

	INTO ccst_medicalcontactstatus

	FROM [DM_STG_EMICS].[dbo].[Medical_Cert] mc 
	INNER JOIN [DM_STG_EMICS].[dbo].[CLAIM_DETAIL] cd ON mc.Claim_No = cd.Claim_Number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 

	Where mc.is_deleted = 0 and cd.is_Null = 0
END