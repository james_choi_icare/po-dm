﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_POLICYLOCATION]
AS
BEGIN
	-- =============================================
	-- Author: Ann
	-- Modified date:
	-- Domain: COVER ccst_policylocation
	-- Comments:  v12.32
	--			14/05 checked v12.36 no change	
	-- <12-Jul-2019><Satish>: Added code to populate ccst_address with Policy location addresses attached to 
	-- the policy for each claim. ccst_poliylocation has an attribute named "addressid" and this is a foreign key 
	-- to the POLICY LOCATION addresses within ccst_address table.
	-- <22Jul2019><Satish>: Modified Verified PC Policylocation address to pick the correct format PublicID
	-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_policylocation','Cover - v12.36 14/05/2019'

	--Verified PC with matched WIC
	select 
		concat('EMICS:',Ltrim(rtrim(cd.claim_number)),'_',PL.ID,':POLICYLOCATIONADDRESS') as AddressID,
		cd.claim_number as LUWID,
		cast(a.LocationCode as varchar(50)) as LocationCode_icare,
		cast(PL.LocationNum  as varchar(50))as LocationNumber,
		concat('EMICS:',Ltrim(rtrim(cd.claim_number)),':POLICY') as PolicyID,
		cast(PL.FixedID as varchar(50)) as PolicySystemID,
		concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PL.ID,':POLICY',':POLICYLOCATION') as PublicID
	INTO ccst_policylocation
	from 
	CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
	inner join DM_STG_EMICS.dbo.CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=ltrim(rtrim(temp_WIC_matched.Claim_Number))
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	inner join dm_stg_pc.dbo.pc_policylocation PL on PL.ID=temp_WIC_matched.PolicyLocation
	left outer join DM_STG_PC.dbo.pc_address a on pl.AccountLocation = a.id  -- to get location code
	left outer join dm_stg_pc.dbo.pctl_addresstype pc_at on PL.AddressTypeInternal=pc_at.ID
	left outer join dm_stg_pc.dbo.pctl_state pc_st on PL.StateInternal=pc_st.ID
	left outer join dm_stg_pc.dbo.pctl_country pc_ct on PL.StateInternal=pc_ct.ID

	---Verified PC and Unmatched WIC / Verified CDR/Invalid
	Insert into ccst_policylocation
	Select 
		AddressID,
		LUWID,
		'DO NOT MAP' as LocationCode_icare, --"DO NOT MAP"
		'DO NOT MAP' as LocationNumber, --"DO NOT MAP"
		PolicyID,
		'DO NOT MAP' as PolicySystemID, --"DO NOT MAP"
		PublicID
	From (select * from CoverREF_tmp_ccst_policylocation_unmatched_WIC
	union 
	select * from CoverREF_tmp_ccst_policylocation_verified_CDR
	) PL_CDR
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(PL_CDR.LUWID)) 

-- <12-Jul-2019><Satish>:
-- Creating this table here as we need to load Policy Location Addresses too.
-- The SP_REC_XFM_ADDRESS runs after SP_REC_XFM_CONTACT that means when SP_REC_XFM_CONTACT runs 
-- there will NOT DATA yet in REXC_XFM_ADDRESS. And we need policy location addresses to be there upfront 
-- as we check for it in SP_REC_XFM_CONTACT (unmatched main contact). So thats is why we need 
-- to load Policy location addresses here.


IF OBJECT_ID('temptable_Address_POLICYLOCATION_verified_CDR') is not null
drop table temptable_Address_POLICYLOCATION_verified_CDR;

IF OBJECT_ID('temptable_Address_POLICYLOCATION_verified_PC') is not null
drop table temptable_Address_POLICYLOCATION_verified_PC;

IF OBJECT_ID('AddressREF_temptable_policylocation_all') is not null
drop table AddressREF_temptable_policylocation_all;

IF OBJECT_ID('ccst_address') is not null
drop table dbo.ccst_address;

--Getting policy location addresses for Verified PC claims
SELECT * INTO temptable_Address_POLICYLOCATION_verified_PC
FROM
(
select
--distinct
--'EMICS:' + LUWID + ':POLICYLOCATIONADDRESS' as PublicID
PublicID  --22Jul2019  - ensured we are picking the right PublicID now
,NULL as AddressBookUID
,convert (varchar(60),AddressLine1Internal) as AddressLine1
--,CM_A.AddressLine1Kanji
,NULL as AddressLine1Kanji
,AddressLine2Internal as AddressLine2
--,CM_A.AddressLine2Kanji
,NULL as AddressLine2Kanji
,AddressLine3Internal as AddressLine3
--,CM_A.AddressType
--,CM_AddressType.TYPECODE as  AddressType-- eg postal
,'business' as  AddressType-- eg postal
,0 as BatchGeocode
--,CM_A.CEDEX
,NULL as CEDEX
--,CM_A.CEDEXBureau
,NULL as CEDEXBureau
,CityInternal as City
--,CM_A.CityKanji
,NULL as CityKanji
--,CM_A.Country
--,ABTL_CNY.TYPECODE as Country--Eg AU
,CountryTYPECODE as Country
--,CM_A.County
,NULL as County
,NULL as DPID_icare
,NULL as Description
,NULL as ExternalLinkID
--,CM_A.IsAddressFromPC_icare as IsAddressFromPC_icare  --20/06/2019: Need to raised a defect against mapper as this attribute is not there in CM_A
,1 as IsAddressFromPC_icare
,NULL as IsValidated_icare
,0 as ObfuscatedInternal
,PostalCodeInternal as PostalCode
--,CM_A.PublicID as CM_A_PublicID
,0 as  Retired
--,NULL as StandardizedAddressID_icare
,NULL as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW  --20/06/2019 : Need to change this to below in other SPs
,StateTYPECODE as State--eg AU_NSW
--,CM_A.Subtype
,'Address' as Subtype -- eg Address
,NULL as ValidUntil
--,ltrim(rtrim(CD.Claim_Number)) as LUWID
,LUWID
From
(
select 
--concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PL.BranchID,'_',PL.LocationNum,':POLICY',':POLICYLOCATION') as PublicID,
concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PL.ID,':POLICY',':POLICYLOCATION') as PublicID,
temp_WIC_matched.PC_PolicyPeriodID as PC_PP_Id,
temp_WIC_matched.PeriodStart as PC_PP_EffectiveDate,
temp_WIC_matched.PeriodEnd as PC_PP_ExpirationDate,
LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
CD.Date_of_Injury,
LTRIM(RTRIM(temp_WIC_matched.PolicyNumber)) as PC_PP_PolicyNumber,
LTRIM(RTRIM(temp_WIC_matched.LegacyPolicyNumber_icare)) as CD_Legacy_PolicyNumberCD,
PL.ID as PolicyLocation_ID,
PL.BranchID,
PL.LocationNum,
PL.AddressLine1Internal,
PL.AddressLine2Internal,
PL.AddressLine3Internal,
PL.CityInternal,
PL.StateInternal,
pc_st.DESCRIPTION as StateDescription,
pc_st.TYPECODE as StateTYPECODE,
PL.CountryInternal,
pc_ct.DESCRIPTION as CountryDescription,
pc_ct.TYPECODE as CountryTYPECODE,
PL.PostalCodeInternal,
PL.AddressTypeInternal,
PL.FixedID,
pc_at.DESCRIPTION as AddressTypeDescription,
pc_at.TYPECODE as AddressTypeTYPECODE,
a.LocationCode
from 
[dbo].[CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record] temp_WIC_matched
--#tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
inner join DM_STG_EMICS.dbo.CLAIM_DETAIL CD 
on ltrim(rtrim(CD.Claim_Number))=ltrim(rtrim(temp_WIC_matched.Claim_Number))
inner join dm_stg_pc.dbo.pc_policylocation PL
on PL.ID=temp_WIC_matched.PolicyLocation
left outer join DM_STG_PC.dbo.pc_address a on pl.AccountLocation = a.id  -- to get location code
left outer join dm_stg_pc.dbo.pctl_addresstype pc_at on PL.AddressTypeInternal=pc_at.ID
left outer join dm_stg_pc.dbo.pctl_state pc_st on PL.StateInternal=pc_st.ID
left outer join dm_stg_pc.dbo.pctl_country pc_ct on PL.CountryInternal=pc_ct.ID
) X
)Y

--Getting policy location addresses for Verified CDR claims
SELECT * INTO temptable_Address_POLICYLOCATION_verified_CDR
FROM
(
select
--distinct
--'EMICS:' + LUWID + ':POLICYLOCATIONADDRESS' as PublicID
PublicID
,NULL as AddressBookUID
,convert (varchar(60),AddressLine1) as AddressLine1
--,CM_A.AddressLine1Kanji
,NULL as AddressLine1Kanji
,NULL as AddressLine2
--,CM_A.AddressLine2Kanji
,NULL as AddressLine2Kanji
,NULL as AddressLine3
--,CM_A.AddressType
--,CM_AddressType.TYPECODE as  AddressType-- eg postal
,'business' as  AddressType-- eg postal
,0 as BatchGeocode
--,CM_A.CEDEX
,NULL as CEDEX
--,CM_A.CEDEXBureau
,NULL as CEDEXBureau
,City
--,CM_A.CityKanji
,NULL as CityKanji
--,CM_A.Country
--,ABTL_CNY.TYPECODE as Country--Eg AU
,CountryTYPECODE as Country
--,CM_A.County
,NULL as County
,NULL as DPID_icare
,NULL as Description
,NULL as ExternalLinkID
--,CM_A.IsAddressFromPC_icare as IsAddressFromPC_icare  --20/06/2019: Need to raised a defect against mapper as this attribute is not there in CM_A
,0 as IsAddressFromPC_icare
,NULL as IsValidated_icare
,0 as ObfuscatedInternal
,PostalCode
--,CM_A.PublicID as CM_A_PublicID
,0 as  Retired
--,NULL as StandardizedAddressID_icare
,NULL as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW  --20/06/2019 : Need to change this to below in other SPs
,StateTYPECODE as State--eg AU_NSW
--,CM_A.Subtype
,'Address' as Subtype -- eg Address
,NULL as ValidUntil
--,ltrim(rtrim(CD.Claim_Number)) as LUWID
,LUWID
From
(
select
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICYLOCATIONADDRESS' as PublicID,
0 as Retired,
0 as BatchGeoCode,
0 as ObfuscatedInternal,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
ltrim(rtrim(CD.Claim_Number)) as Claim_Number,
pc_st.TYPECODE as StateTYPECODE,
pc_ct.TYPECODE as CountryTYPECODE,
CASE WHEN ltrim(rtrim(PTD.ADDRESS_LOCALITY)) <> ''  THEN   ltrim(rtrim(PTD.ADDRESS_LOCALITY))
                                                    ELSE 'Sydney'
--default if source is NULL 
                    END as City,
CASE WHEN ltrim(rtrim(PTD.ADDRESS_POST_CODE)) <> '' THEN   ltrim(rtrim(PTD.ADDRESS_POST_CODE))
                                                    ELSE 2000
    --default if source is NULL 
                    END as PostalCode,
'Address' as SubType,
CASE WHEN ltrim(rtrim(PTD.ADDRESS_STREET)) <> ''  THEN   ltrim(rtrim(PTD.ADDRESS_STREET))
                                                    ELSE 'VERIFIED CDR DUMMY LINE1'
--default if source is NULL 
                    END as AddressLine1
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
left outer join DM_STG_EMICS.dbo.POLICY_TERM_DETAIL PTD on PTD.POLICY_NO=CD.Policy_No
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=ltrim(rtrim(CD.Claim_Number))
left outer join dm_stg_pc.dbo.pctl_state pc_st
on pc_st.TYPECODE=CASE PTD.ADDRESS_STATE WHEN 'ACT' THEN 'AU_ACT'
                                         WHEN 'QLD' THEN 'AU_QLD'
WHEN 'SA'  THEN 'AU_SA'
WHEN 'VIC' THEN 'AU_VIC'
WHEN 'WA'  THEN 'AU_WA'
WHEN 'NSW' THEN 'AU_NSW'
ELSE 'AU_NSW'
--default if source is NULL 
END
left outer join dm_stg_pc.dbo.pctl_country pc_ct on pc_ct.DESCRIPTION='Australia'
where CD.is_Null=0
--and ltrim(rtrim(CD.Claim_Number)) not in
and ltrim(rtrim(CD.Claim_Number)) in
--(Select LUWID from #temptable_consolidated) --getting only verified CDR/invalid claims
(
select LUWID from [dbo].[CoverREF_tmp_ccst_policylocation_unmatched_WIC]
union 
select LUWID from [dbo].[CoverREF_tmp_ccst_policylocation_verified_CDR]
)
) A
) X


CREATE TABLE [dbo].[AddressREF_temptable_policylocation_all](
	[PublicID] [varchar](47) NULL,
	[AddressBookUID] [int] NULL,
	[AddressLine1] [varchar](60) NULL,
	[AddressLine1Kanji] [int] NULL,
	[AddressLine2] [varchar](60) NULL,
	[AddressLine2Kanji] [int] NULL,
	[AddressLine3] [varchar](60) NULL,
	[AddressType] [varchar](8) NOT NULL,
	[BatchGeocode] [int] NOT NULL,
	[CEDEX] [int] NULL,
	[CEDEXBureau] [int] NULL,
	[City] [varchar](60) NULL,
	[CityKanji] [int] NULL,
	[Country] [varchar](50) NULL,
	[County] [int] NULL,
	[DPID_icare] [int] NULL,
	[Description] [int] NULL,
	[ExternalLinkID] [int] NULL,
	[IsAddressFromPC_icare] [int] NOT NULL,
	[IsValidated_icare] [int] NULL,
	[ObfuscatedInternal] [int] NOT NULL,
	[PostalCode] [varchar](60) NULL,
	[Retired] [int] NOT NULL,
	[StandardizedAddressID_icare] [int] NULL,
	[State] [varchar](50) NULL,
	[Subtype] [varchar](7) NOT NULL,
	[ValidUntil] [int] NULL,
	[LUWID] [varchar](19) NULL
) ON [PRIMARY]

insert into [dbo].[AddressREF_temptable_policylocation_all]
(
[PublicID] ,
	[AddressBookUID] ,
	[AddressLine1],
	[AddressLine1Kanji] ,
	[AddressLine2] ,
	[AddressLine2Kanji] ,
	[AddressLine3] ,
	[AddressType] ,
	[BatchGeocode] ,
	[CEDEX],
	[CEDEXBureau] ,
	[City] ,
	[CityKanji] ,
	[Country] ,
	[County] ,
	[DPID_icare],
	[Description] ,
	[ExternalLinkID] ,
	[IsAddressFromPC_icare],
	[IsValidated_icare] ,
	[ObfuscatedInternal] ,
	[PostalCode] ,
	[Retired]  ,
	[StandardizedAddressID_icare] ,
	[State] ,
	[Subtype] ,
	[ValidUntil],
	[LUWID] 
)
select
	[PublicID] ,
	[AddressBookUID] ,
	[AddressLine1],
	[AddressLine1Kanji] ,
	[AddressLine2] ,
	[AddressLine2Kanji] ,
	[AddressLine3] ,
	[AddressType] ,
	[BatchGeocode] ,
	[CEDEX],
	[CEDEXBureau] ,
	[City] ,
	[CityKanji] ,
	[Country] ,
	[County] ,
	[DPID_icare],
	[Description] ,
	[ExternalLinkID] ,
	[IsAddressFromPC_icare],
	[IsValidated_icare] ,
	[ObfuscatedInternal] ,
	[PostalCode] ,
	[Retired]  ,
	[StandardizedAddressID_icare] ,
	[State] ,
	[Subtype] ,
	[ValidUntil],
	[LUWID] 
	from 
dbo.temptable_Address_POLICYLOCATION_verified_PC;


insert into [dbo].[AddressREF_temptable_policylocation_all]
(
    [PublicID] ,
	[AddressBookUID] ,
	[AddressLine1],
	[AddressLine1Kanji] ,
	[AddressLine2] ,
	[AddressLine2Kanji] ,
	[AddressLine3] ,
	[AddressType] ,
	[BatchGeocode] ,
	[CEDEX],
	[CEDEXBureau] ,
	[City] ,
	[CityKanji] ,
	[Country] ,
	[County] ,
	[DPID_icare],
	[Description] ,
	[ExternalLinkID] ,
	[IsAddressFromPC_icare],
	[IsValidated_icare] ,
	[ObfuscatedInternal] ,
	[PostalCode] ,
	[Retired]  ,
	[StandardizedAddressID_icare] ,
	[State] ,
	[Subtype] ,
	[ValidUntil],
	[LUWID] 
) 

select
[PublicID] ,
	[AddressBookUID] ,
	[AddressLine1],
	[AddressLine1Kanji] ,
	[AddressLine2] ,
	[AddressLine2Kanji] ,
	[AddressLine3] ,
	[AddressType] ,
	[BatchGeocode] ,
	[CEDEX],
	[CEDEXBureau] ,
	[City] ,
	[CityKanji] ,
	[Country] ,
	[County] ,
	[DPID_icare],
	[Description] ,
	[ExternalLinkID] ,
	[IsAddressFromPC_icare],
	[IsValidated_icare] ,
	[ObfuscatedInternal] ,
	[PostalCode] ,
	[Retired]  ,
	[StandardizedAddressID_icare] ,
	[State] ,
	[Subtype] ,
	[ValidUntil],
	[LUWID] 
	from 
	dbo.temptable_Address_POLICYLOCATION_verified_CDR
;

--select * from temptable_Address_POLICYLOCATION_verified_PC
--where AddressLine1 like '%117%OLD%PITTWATER%RD%'

--select * 
--from temptable_Address_POLICYLOCATION_verified_CDR
--where AddressLine1 like '%117%OLD%PITTWATER%RD%'


--Creating the QA ccst_address table first

 
CREATE TABLE [dbo].[ccst_address](
	[PublicID] [varchar](50) NULL,
	[AddressBookUID] [varchar](64) NULL,
	[AddressLine1] [varchar](60) NULL,
	[AddressLine1Kanji] [varchar](60) NULL,
	[AddressLine2] [varchar](60) NULL,
	[AddressLine2Kanji] [varchar](60) NULL,
	[AddressLine3] [varchar](60) NULL,
	[AddressType] [varchar](50) NULL,
	[BatchGeocode] [bit] NULL,
	[CEDEX] [bit] NULL,
	[CEDEXBureau] [varchar](2) NULL,
	[City] [varchar](40) NULL,
	[CityKanji] [varchar](60) NULL,
	[Country] [varchar](50) NULL,
	[County] [varchar](500) NULL,
	[DPID_icare] [int] NULL,
	[Description] [varchar](255) NULL,
	[ExternalLinkID] [int] NULL,
	[IsAddressFromPC_icare] [int] NOT NULL,
	[IsValidated_icare] [bit] NULL,
	[ObfuscatedInternal] [int] NOT NULL,
	[PostalCode] [varchar](20) NULL,
	[Retired] [int] NOT NULL,
	--[StandardizedAddressID_icare] [int] NULL,
	[StandardizedAddressID_icare] [varchar](255) NULL,
	[State] [varchar](50) NULL,
	[SubType] [varchar](50) NULL,
	[ValidUntil] [datetime2](7) NULL,
	[LUWID] [varchar](19) NULL
) ON [PRIMARY]


--Inserting Policy Location addresses - these addresses are reference in ccst_policylocation
INSERT INTO [dbo].ccst_address
(
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	[ValidUntil],
	[LUWID]
)

SELECT
[PublicID] ,
	[AddressBookUID],
	[AddressLine1],
	[AddressLine1Kanji],
	[AddressLine2],
	[AddressLine2Kanji],
	[AddressLine3],
	[AddressType],
	[BatchGeocode],
	[CEDEX],
	[CEDEXBureau],
	[City],
	[CityKanji],
	[Country],
	[County],
	[DPID_icare],
	[Description],
	[ExternalLinkID],
	[IsAddressFromPC_icare],
	[IsValidated_icare],
	[ObfuscatedInternal],
	[PostalCode],
	[Retired],
	[StandardizedAddressID_icare],
	[State],
	[SubType],
	--[ValidUntil],
	Try_Convert(date,cast([ValidUntil] as varchar(8))) AS [ValidUntil],
	[LUWID]
FROM 
[dbo].[AddressREF_temptable_policylocation_all]
;

IF OBJECT_ID('temptable_Address_POLICYLOCATION_verified_CDR') is not null
drop table temptable_Address_POLICYLOCATION_verified_CDR;

IF OBJECT_ID('temptable_Address_POLICYLOCATION_verified_PC') is not null
drop table temptable_Address_POLICYLOCATION_verified_PC;

END