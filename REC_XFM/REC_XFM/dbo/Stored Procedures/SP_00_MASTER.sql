﻿--20190515: [JA]: Added email alerts to Dev group and James

CREATE PROC [dbo].[SP_00_MASTER] AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @runID int, @defaultCohort int;

	SET @defaultCohort = 2

	exec SP_00a_MASTER_BUILD_XFM_TABLES @runID output

	exec SP_00b_MASTER_RECONCILE @runID,'CM_UPDATE',@defaultCohort
	exec SP_00b_MASTER_RECONCILE @runID,'CM',@defaultCohort
	exec SP_00b_MASTER_RECONCILE @runID,'CC',@defaultCohort

	
END