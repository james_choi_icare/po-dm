﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_INJURYDIAGNOSIS]
AS
BEGIN
/*-- =============================================
-- Author:		Ann
-- Create date: 14/05
-- Description:	Work capacity domain - ccst_injurydiagnosis
-- Version: 14/05 v7.3 initial build 
--			17/05 checked v8.0 no change, 
--					updated CC_ICDCODE join as part of DMIG-5637
-- =============================================*/

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_injurydiagnosis','Work Capacity - v8 17/05/2019'


SELECT
	NULL as Comments,
	Claim_number as LUWID,
	Case when(icd_code is not null and icd_code <> '') then 1 else 0 end as Compensable,
	'EMICS:'+rtrim(ltrim(cd.CLAIM_NUMBER))+'_'+nom_treating_doctor+':VENDOR' as ContactID,
	NULL as DateEnded,
	NULL as DateStarted,
	publicID as ICDCode,
	'EMICS:' + rtrim(ltrim(Claim_number)) + ':INCIDENT' as InjuryIncidentID,
	Case when (Potential_IS_PRIMARY = 1 and Row_Number()
		over (Partition by CD.Claim_Number order by Potential_IS_PRIMARY DESC,cd.ICDtype1 DESC,cd.ICDtype2 DESC,cd.ICDtype3 DESC) = 1) then 1 else 0 
		end as IsPrimary,
	'EMICS:'+rtrim(ltrim(Claim_number))+'_'+ICD_Code+':INJURYDIAGNOSIS' as PublicID,
	1 as Payable_icare

into ccst_InjuryDiagnosis

From
(Select 
	CD.Claim_number,
	CD.NOM_TREATING_DOCTOR
	, InjuryDiagnosis.*
	, ICD1.ICDtype1
	, ICD2.ICDtype2
	, ICD3.ICDtype3 
	, Row_Number() over (Partition by CD.Claim_Number order by Potential_IS_PRIMARY DESC,ICD1.ICDtype1 DESC,ICD2.ICDtype2 DESC,ICD3.ICDtype3 DESC) AS InternalOrder
	, CC_ICDCODE.PublicID

From DM_STG_EMICS.dbo.Claim_detail CD 
--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
join
(
	select a.*, b.Potential_IS_PRIMARY  from(
	   select entries.*,icd.AA_MIDRANGE--, row_number() over (  order by ( select null )) as rownum
	   FROM
	   (
			SELECT CLAIM_NO,ICD_CODE
			FROM(
					SELECT CLAIM_NO,ICD_CODE1,ICD_CODE2,ICD_CODE3
					FROM DM_STG_EMICS.DBO.CLAIM_DETAIL_EXTRA
			) AS CP
			UNPIVOT
			(ICD_CODE FOR ICD_CODES IN (ICD_CODE1,ICD_CODE2,ICD_CODE3)) AS UP
	   ) AS ENTRIES
	   LEFT OUTER JOIN DM_STG_EMICS.DBO.ICD ON ENTRIES.ICD_CODE = ICD.ICD_10_CODE
	) AS A
	LEFT OUTER JOIN
    (

      SELECT CLAIM_NO,MAX(AA_MIDRANGE) AS MAX_AA_MIDRANGE,Potential_IS_PRIMARY = 1
      FROM (
            SELECT ENTRIES.*, ICD.AA_MIDRANGE
            FROM
            (
                  SELECT Claim_no, ICD_CODE
                  FROM(
                              SELECT CLAIM_NO,ICD_CODE1,ICD_CODE2,ICD_CODE3
                              FROM DM_STG_EMICS.DBO.CLAIM_DETAIL_EXTRA ) AS CP
                              UNPIVOT
                              (ICD_CODE FOR ICD_CODES IN (ICD_CODE1,ICD_CODE2,ICD_CODE3 )) AS UP
                  ) AS ENTRIES
                  LEFT OUTER JOIN DM_STG_EMICS.DBO.ICD
                  ON ENTRIES.ICD_CODE = ICD.ICD_10_CODE
             ) MAX_ENTRIES
      GROUP BY CLAIM_NO
      ) AS B
		ON A.CLAIM_NO = B.CLAIM_NO AND A.AA_MIDRANGE = B.MAX_AA_MIDRANGE
) as InjuryDiagnosis
On CD.CLAIM_NUMBER = InjuryDiagnosis.claim_no

-----------------------------------
left outer join (
	select Claim_no, ICD_Code1, 'ICDCODE1' AS ICDtype1
	from DM_STG_EMICS.DBO.Claim_Detail_Extra cde1
	) AS ICD1 ON InjuryDiagnosis.Claim_no = icd1.Claim_no and InjuryDiagnosis.ICD_CODE = icd1.ICD_Code1
left outer join (
	select Claim_no, ICD_Code2, 'ICDCODE2' AS ICDtype2
	from DM_STG_EMICS.DBO.Claim_Detail_Extra cde2
	) AS ICD2 ON InjuryDiagnosis.Claim_no = icd2.Claim_no and InjuryDiagnosis.ICD_CODE = icd2.ICD_Code2
left outer join ( 
	select Claim_no, ICD_Code3, 'ICDCODE3' AS ICDtype3
	from DM_STG_EMICS.DBO.Claim_Detail_Extra cde3
	) AS ICD3 ON InjuryDiagnosis.Claim_no = icd3.Claim_no and InjuryDiagnosis.ICD_CODE = icd3.ICD_Code3
left outer join DM_STG_CC.dbo.cc_icdcode CC_ICDCODE on ICD_CODE = CC_ICDCODE.Code --updated DMIG-5637
--TGT_GW_STG_CC.[dbo].[cc_icdcode] CC_ICDCODE on InjuryDiagnosis.ICD_CODE = CC_ICDCODE.Code 
-----------------------------------
where cd.is_null = 0 
--order by cd.Claim_Number,InjuryDiagnosis.Potential_IS_PRIMARY desc, ICD1.ICDtype1 DESC,ICD2.ICDtype2 DESC,ICD3.ICDtype3 DESC
) CD

END