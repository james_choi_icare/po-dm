﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIM]
AS
BEGIN
/*	-- ===============================
	-- Author: Ann
	-- Domain: Lodgement and linking table
	-- Description: v33 mapping spec ccst_claim
	-- Version: 10/06/2019 v1.2 Users and groups spec - AssignedByUserID, AssignedGroupID, AssignedQueueID, AssignedUserID

	-- Comments: 2/05 - A few fields do not work due to look up to user list, taking the max value of createdate but joining back to cd
	--					CoverageInQuestion field looks up DM_XFM.dbo.INTM_EXTRACT_POLICY_CLAIM_BUCKET and uses cd but no join inside the query
	--					EmployerSize_icare field doesnt have cd inside select statement. Not sure what the lookup is doing either
	--					LocationCodeID field - what is CASE WHEN 'VerifiedPC and WIC matched PC' look up
	--					WICDescription_icare field - doesnt have cd join inside select statement
	--			13/05; on hold until mapping stabalizes
				10/06/2019 updated to latest mapping spec v33
				17/07/2019 fixed SC for employersize_icare v44.1
				22/07/2019 changed sc for employersize_icare v45
				09/09/2019 changed FinalisationDocsTrigger_icare to populate for open claims and claim segments E and G

	-- ===================================== */
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claim','Lodgement - v47 26/07/2019'

	--drop table ccst_claim
;WITH incidentreport as (
	SELECT cd.Claim_Number ,
		CASE WHEN timelost.claimnumber > 0 THEN 1	
				When (Select SUM(Trans_Amount) as TransSum from DM_STG_EMICS.dbo.PAYMENT_RECOVERY pr where pr.claim_no = cd.claim_number and estimate_type = '50' and reversed <> 1) is not null then 1
				ELSE 0
				END AS TimeLostReport	
			,CASE
				WHEN medicalreport1.claim_number > 0
				THEN 1
				WHEN medicalreport2.claim_number > 0
				THEN 1
				WHEN CAD.WPI > 0
				THEN 1
	WHEN CD.Result_of_Injury_Code in (1,2,3) 
				THEN 1
				WHEN CAD.Claim_Liability_Indicator IN (2,5,7,8,10,11)
				THEN 1
				WHEN CAD.Claim_Liability_Indicator = 9 AND CAD.Reasonable_Excuse_Code <> 8
				THEN 1
				ELSE 0
		END AS MedicalReport
		

	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.Claim_Number = CAD.Claim_no


	left join (SELECT LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3) as claimnumber
				FROM DM_STG_CDR..DM_STG_CDR_D_CLAIM_MASTER clmmstr
					INNER JOIN DM_STG_CDR..DM_STG_CDR_D_COMPANY comp ON clmmstr.MANAGING_COMPANY_ID = comp.COMPANY_ID
					INNER JOIN DM_STG_CDR..DM_STG_CDR_E_ROLLING_CLAIM clm ON clm.CLAIM_WCA_ID = clmmstr.CLAIM_WCA_ID
					--LEFT JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd on cd.claim_number = LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3)
				WHERE clm.WCA_STATUS_ID = 181644
					AND comp.SOURCE_INSURER_ID = 702
					AND ISNULL(clm.CEASED_WORK_DATE_ID,0) <> 0
					AND LEN(clmmstr.SOURCE_CLAIM_NUMBER) > 3) timelost on timelost.claimnumber=cd.claim_number


	Left join (SELECT DISTINCT CD1.Claim_Number, cd1.is_null
				FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD1
				INNER JOIN (SELECT Claim_no FROM DM_STG_EMICS.dbo.PAYMENT_RECOVERY 
					WHERE Reversed = 0 GROUP BY claim_no HAVING SUM(trans_amount) > 0) PR1 ON PR1.Claim_No = CD1.Claim_Number) medicalreport1 on CD.Claim_Number = medicalreport1.Claim_Number
						AND medicalreport1.is_Null = 0
	Left join (SELECT DISTINCT CD2.Claim_Number
						FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD2
						INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD2 ON CD2.Claim_Number = CAD2.Claim_no
						LEFT OUTER JOIN (SELECT * FROM DM_STG_EMICS.dbo.PAYMENT_RECOVERY WHERE Reversed = 0) PR2 ON PR2.Claim_No = CD2.Claim_Number
						LEFT OUTER JOIN (SELECT DISTINCT Claim_No FROM DM_STG_EMICS.dbo.MEDICAL_CERT WHERE is_Deleted = 0) MC2 ON CAD2.Claim_no = MC2.Claim_No
					WHERE CD2.is_Null = 0 
						AND CAD2.Claim_Liability_Indicator <> 12
						AND (PR2.Estimate_Type IN (51,52,53,55,56,63,64) 
							OR CD2.is_Medical_Only = 1
							OR MC2.Claim_No IS NOT NULL)) medicalreport2 on medicalreport2.claim_number = cad.claim_no

)
Select distinct
	Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID, cc_user_sm.PublicID) 
		End as AssignedByUserID, --v1.2
	cc_group.PublicID as AssignedGroupID, --v1.2
	NULL as AssignedQueueID, --v1.2
	Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID, cc_user_sm.PublicID) 
		End as AssignedUserID, --v1.2
	'assigned' as AssignmentStatus,
	'sydney' as BranchInsurer_icare,
	CASE WHEN CD.Date_Notice_Given < CD.Date_of_Injury
		THEN CONVERT(DATETIME2,CONVERT(DATETIME, CONVERT(VARCHAR(11), CD.Date_Notice_Given, 111) + ' ' +   CONVERT(VARCHAR(8),CONVERT(TIME, DATEADD(second,1, CD.Date_of_Injury))), 111))
		ELSE CONVERT(DATETIME2,CD.Date_Notice_Given)
		END AS ClaimantRprtdDate,
	701 as ClaimsAgent_icare,
	CASE WHEN ct.Segment_Code = 'C' THEN 'care_icare'
         WHEN ct.Segment_Code = 'E' THEN 'ni_empower_icare'
         WHEN ct.Segment_Code = 'G' THEN 'ni_guide_icare'
         WHEN ct.Segment_Code = 'P' THEN 'ni_support_icare'
         WHEN ct.Segment_Code = 'S' THEN 'ni_specialised_icare'
         WHEN ct.Segment_Code = 'U' THEN 'unassigned_icare'
         ELSE 'unassigned_icare'
		 END AS ClaimTier,
		 RTRIM(cd.Claim_Number) as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':WORKCOMP' AS ClaimWorkCompID,
	CONVERT(DATETIME2, cad.Date_Claim_Closed) as CloseDate,
	cml.CommonLawDate as CommonLawDateFlagged_icare,
	CASE WHEN cde.Is_Concurrent_Employment =  0 THEN 'No'    
		WHEN cde.Is_Concurrent_Employment =  1 THEN 'Yes'    -- 'Yes'
		END as ConcurrentEmp,
	CASE WHEN (coverage1.LUWID is not null OR coverage2.LUWID is not null) AND (coverage1.[PC_PP_EffectiveDate] IS NOT NULL OR coverage2.[PD_EffectiveDate] IS NOT NULL) THEN 0 --No: Coverage is not in question
		ELSE 1 END AS CoverageInQuestion,
	CONVERT(DATETIME2, cad.Date_Claim_Lodged) as DateCompDcsnMade,
	CONVERT(DATETIME2, cad.Date_Claim_Lodged) as DateMade_icare,
	CONVERT(DATETIME2, cad.date_claim_received) as DateRptdToAgent,
	CASE WHEN cd.Date_Notice_Given < cd.Date_of_Injury
		THEN CONVERT(DATETIME2,CONVERT(DATETIME, CONVERT(VARCHAR(11), cd.Date_Notice_Given, 111) + ' ' +   CONVERT(VARCHAR(8),CONVERT(TIME, DATEADD(second,1, cd.Date_of_Injury))), 111))
		ELSE CONVERT(DATETIME2,cd.Date_Notice_Given)
		END AS DateRptdToEmployer,
	CASE WHEN cd.Date_Notice_Given < cd.Date_of_Injury
		THEN CONVERT(DATETIME2,CONVERT(DATETIME, CONVERT(VARCHAR(11), cd.Date_Notice_Given, 111) + ' ' +   CONVERT(VARCHAR(8),CONVERT(TIME, DATEADD(second,1, cd.Date_of_Injury))), 111))
		ELSE CONVERT(DATETIME2,cd.Date_Notice_Given)
		END AS DateRptdToInsured,
	CONVERT(DATETIME2, cd.Date_Deceased) as DeathDate,
	CASE WHEN EmployerSize.EmployerCategory_icare is NULL THEN 'any' --Default to Any		WHEN EmployerSize.EmployerCategory_icare = 10001 THEN 'corp'		WHEN EmployerSize.EmployerCategory_icare = 10002 THEN 'sme'		WHEN EmployerSize.EmployerCategory_icare = 10003 THEN 'any'		WHEN EmployerSize.EmployerCategory_icare = 10004 THEN 'any'		WHEN EmployerSize.EmployerCategory_icare = 10005 THEN 'sme'		WHEN EmployerSize.EmployerCategory_icare = 10006 THEN 'corp'		WHEN EmployerSize.EmployerCategory_icare = 10007 THEN 'sme'		WHEN EmployerSize.EmployerCategory_icare = 10008 THEN 'corp'		WHEN EmployerSize.EmployerCategory_icare = 10009 THEN 'corp'		WHEN EmployerSize.EmployerCategory_icare = 10010 THEN 'sme'		ELSE 'any'		END AS EmployerSize_icare, 
	'EmployerSize.EmployerCategory_icare: ' + convert(varchar, EmployerSize.EmployerCategory_icare) as EmployerSize_icare_SRC_VALUE,
	case when ct.Segment_Code in ('E','G') and cad.Claim_Closed_Flag = 'N' then convert(datetime2,DATEADD(Week,6,CONVERT(Date,GetDate(),103))) 
		else NULL 
		end as  FinalisationDocsTrigger_icare, --v38 -- 90/09/2019
	'neverflagged' as Flagged,
	Case when cd.Source = 'C' then 'mail'
		When cd.Source = 'E' then 'email_icare'
		When cd.Source = 'F' then 'fax'
		When cd.Source = 'P' then 'phone'
		When cd.Source = 'S' then 'mail'
		When cd.Source = 'W' then 'portal_icare'
		End as HowReported,
	CASE WHEN incidentreport.TimeLostReport = 0 AND incidentreport.MedicalReport = 0
		THEN 1
		ELSE 0
		END AS IncidentReport,
	--'TimeLostReport: ' + convert(varchar, incident_report_flag.TimeLostReport) + ' MedicalReport: ' + convert(varchar, incident_report_flag.MedicalReport) as IncidentReport_SRC_VALUE,
	CASE cd.Accident_Location_Code
         WHEN 1 THEN 1          --Normal Workplace -- Yes
         ELSE 0                             -- No
		END AS InjuredOnPremises,
	'false' as IsEditableByPortal_icare,
	CASE WHEN cad.Claim_Liability_Indicator = 1 THEN '01'		WHEN cad.Claim_Liability_Indicator = 2 THEN '02'		WHEN cad.Claim_Liability_Indicator = 5 THEN '05'		WHEN cad.Claim_Liability_Indicator = 6 THEN '06'		WHEN cad.Claim_Liability_Indicator = 7 THEN '07'		WHEN cad.Claim_Liability_Indicator = 8 THEN '08'		WHEN cad.Claim_Liability_Indicator = 9 THEN '09'		WHEN cad.Claim_Liability_Indicator = 10 THEN '10'		WHEN cad.Claim_Liability_Indicator = 11 THEN '11'		WHEN cad.Claim_Liability_Indicator = 12 THEN '12'        ELSE NULL
		END AS LiabilityStatusDenorm_icare,
	LS.LitigationStatus,
	CASE WHEN temp_WIC_matched.Claim_Number is not null
		THEN 'EMICS:' + RTRIM(cd.Claim_Number )+ '_' + convert(varchar, temp_WIC_matched.PolicyLocation) + ':POLICY' + ':POLICYLOCATION'
		ELSE 'EMICS:' + RTRIM(cd.Claim_Number) + ':POLICY'+':POLICYLOCATION' END AS LocationCodeID,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':INCIDENTADDRESS' as LossLocationID,
	'WC' as LossType,
	CASE WHEN ISNULL(Is_Restricted,0) = 0 THEN 'unsecuredclaim'
		ELSE 'sensitiveclaim'
		END AS PermissionRequired,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':POLICY' as PolicyID,
	'EMICS:' + RTRIM(cd.Claim_Number) as PublicID,
	Case when cad.Reopen_Reason = 0 then NULL
		when cad.Reopen_Reason = 1 then 1
		when cad.Reopen_Reason = 2 then 2
		when cad.Reopen_Reason = 3 then 3
		when cad.Reopen_Reason = 4 then 4
		when cad.Reopen_Reason = 5 then 5
		when cad.Reopen_Reason = 6 then 6
		End as ReopenedReason,
	Case when cd.Notified_By = 'U' then 'other'
		when cd.Notified_By = 'R' then '05'
		when cd.Notified_By = 'P' then '03'
		when cd.Notified_By = 'O' then '04'
		when cd.Notified_By = 'W' then '01'
		when cd.Notified_By = 'M' then 'medicalprovider'
		when cd.Notified_By = 'E' then '02'
		end as ReportedByType,
	CASE WHEN cad.Date_Claim_Entered < cd.Date_of_Injury
		THEN CONVERT(DATETIME, CONVERT(VARCHAR(11), cad.Date_Claim_Entered, 111) + ' ' +   CONVERT(VARCHAR(8),CONVERT(TIME, DATEADD(second,1, cd.Date_of_Injury))), 111)
        ELSE cad.Date_Claim_Entered
		END AS ReportedDate,
	CASE WHEN ct.Segment_Code = 'C' THEN 'care_icare'
         WHEN ct.Segment_Code = 'E' THEN 'ni_empower_icare'
         WHEN ct.Segment_Code = 'G' THEN 'ni_guide_icare'
         WHEN ct.Segment_Code = 'P' THEN 'ni_support_icare'
         WHEN ct.Segment_Code = 'S' THEN 'ni_specialised_icare'
         WHEN ct.Segment_Code = 'U' THEN 'unassigned_icare'
         ELSE 'unassigned_icare'
		 END AS Segment,
	ISNULL(cd.shared_claim_code,0) as SharedClaim_icare,
	CASE cad.Claim_Closed_Flag
		WHEN 'Y'  THEN 'closed'    --Closed		WHEN 'N' THEN 'open'   -- Open		END as State,
	CASE WHEN cde.PayCutOffDate IS NOT NULL THEN 1 --'Yes'		ELSE 0 --'No'
		END AS TriggerAssessReminder,
	CASE WHEN cde.PayCutOffDate IS NOT NULL 	    THEN DATEADD(Day, -5, cde.PayCutOffDate)		ELSE NULL		END AS TriggerAssessReminderDate,	
	CASE WHEN LEN(cd.Tariff_No) = 5 Then Trigger1.DivisionComb1
		Else Trigger2.DivisionComb2
		End as WICDescription_icare

INTO ccst_claim


	FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd --Claim Details Main Table
	 INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad ON cd.Claim_Number = cad.Claim_no
	 INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
	 Inner JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct ON cd.Claim_Number = ct.Claim_No
	 INNER JOIN DM_STG_EMICS.dbo.ICD icd ON cde.ICD_Code1 = icd.ICD_10_Code
	 LEFT JOIN  ( Select l.Create_date, clm.Claim_number, l.claim_no		FROM DM_STG_EMICS.dbo.CLAIM_DETAIL clm
        INNER JOIN DM_STG_EMICS.dbo.Litigation L ON clm.Claim_Number = L.Claim_No WHERE L.Type = 1     --1 = CommonLawDM_STG_EMICS.dbo.Litigation l ON cd.Claim_Number = l.Claim_No
		) lit on lit.claim_no = cd.claim_number
left join incidentreport on incidentreport.claim_number = cd.claim_number
	
	Cross join (Select max(status_date) as LitigationStatus from DM_STG_EMICS.dbo.Litigation lit
		Inner join DM_STG_EMICS.dbo.Ref_Look_Up rlu on lit.status = rlu.code
		Inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on lit.Claim_No = cd.Claim_Number
		Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cd.Claim_Number = cad.Claim_no
		Where rlu.Name = 'litigationstatus' and cd.is_null =0 and lit.Is_Deleted =0) LS
	Cross join (Select max(cast(convert(varchar, l.create_date,112) as datetime2)) as CommonLawDate from DM_STG_EMICS.dbo.CLAIM_DETAIL clm 
		INNER JOIN DM_STG_EMICS.dbo.Litigation L on clm.claim_number = l.claim_no where l.Type = 1) cml 
	Left join dbo.CoverREF_temptable_consolidated Coverage1 on coverage1.LUWID = CD.Claim_Number
	Left join dbo.CoverREf_temptable_CDR Coverage2 on coverage2.LUWID = CD.Claim_Number


		Left join [CoverREF_temptable_consolidated] EmployerSize on EmployerSize.LUWID = RTRIM(cd.claim_number) 


	Left join CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched on ltrim(rtrim(CD.Claim_Number))=temp_WIC_matched.Claim_Number
	Left join (SELECT DivisionCode + ': ' + DivisionDesc as DivisionComb1, WICCode FROM DM_STG_CC.dbo.ccx_wiccode_icare) Trigger1 on cd.Tariff_no = Trigger1.WICCode				and Trigger1.WICCode = '0' + CONVERT(varchar,cd.Tariff_No)	Left join (SELECT DivisionCode + ': ' + DivisionDesc as DivisionComb2, WICCode FROM DM_STG_CC.dbo.ccx_WICCode_icare) Trigger2 on cd.Tariff_no = Trigger2.WICCode				and Trigger2.WICCode = CONVERT(varchar,cd.Tariff_No)		

	LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cad.Claims_Officer)
	LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
			FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
			ON (co.Alias = cntrl.ITEM)


	left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
	left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)
	left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
	left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
	left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
	left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups on (lkp_eml_team_gwcc_groups.EML_TEAM = co.grp)
	left join DM_STG_CC.[dbo].[cc_group] cc_group on ( cc_group.Name = lkp_eml_team_gwcc_groups.gwcc_group )
	left join DM_XFM.[dbo].[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on ( cntrl.EML_Email = lkp_eml_gwcc_queues.EMLEmailAddress 
				 and cad.Claims_Officer = lkp_eml_gwcc_queues.EMLQueueUser)
	left join DM_STG_CC.[dbo].[cc_credential] cc_credential_user_queue on (lkp_eml_gwcc_queues.GWCC_Claim_User = cc_credential_user_queue.UserName)
	left join DM_STG_CC.[dbo].[cc_user] cc_user_queue on (cc_user_queue.CredentialID = cc_credential_user_queue.ID)
 
	WHERE cd.is_Null = 0  --Exclude NULL Claims (Out Of Scope)


END