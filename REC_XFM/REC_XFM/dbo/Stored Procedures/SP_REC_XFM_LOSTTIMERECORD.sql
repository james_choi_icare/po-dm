﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_LOSTTIMERECORD]

AS
BEGIN
	/*========================================
		Author: Ann
		Domain: ESTIMATES AND RESERVES v26 ccst_losttimerecord_icare
		Comments:  v26
				14/05 checked against 27.2 no change
	========================================*/
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_losttimerecord_icare','Estimates and Reserves - v26 14/05/2019'

	
	Select 
	'EMICS:'+RTRIM(LTRIM(cd.Claim_Number))+':TIMELOST' PublicID,
	RTRIM(cd.Claim_Number) as LUWID,
	CASE
		WHEN clm.resumed_work_actual_date_id <> 0
		THEN CONVERT (DATETIME, CONVERT(VARCHAR(8),clm.resumed_work_actual_date_id))
		ELSE NULL
		END AS ActualResumedWorkDate,
	CASE
		WHEN clm.ceased_work_date_id <> 0 
		THEN CONVERT(DATETIME,CONVERT(VARCHAR(8),clm.ceased_work_date_id))
		ELSE NULL
		END AS CeasedWorkDate,
	CASE
	WHEN clm.est_deemed_resume_date_id <> 0
	THEN CONVERT(DATETIME,CONVERT(VARCHAR(8),clm.est_deemed_resume_date_id))
	ELSE NULL
	END AS EstResumeWorkDate,
	'EMICS:'+RTRIM(LTRIM(cd.Claim_Number))+':WORKCOMP' ClaimWorkCompID,
	'0' Retired
	
	INTO  ccst_losttimerecord_icare

    FROM [DM_STG_EMICS].dbo.CLAIM_DETAIL cd
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	INNER JOIN [DM_STG_CDR].[dbo].[DM_STG_CDR_D_CLAIM_MASTER] clmmstr on cd.Claim_Number= LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3)
	INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_CLAIM clm ON clm.CLAIM_WCA_ID = clmmstr.CLAIM_WCA_ID

	WHERE cd.Is_Null = 0
	AND clm.WCA_STATUS_ID = 181644
	AND LEN(clmmstr.SOURCE_CLAIM_NUMBER) > 3
	AND ISNULL(clm.ceased_work_date_id, 0) <> 0
END