﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_NONPECUNIARYBENEFIT]
	
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Modified date: 09/05
-- Comments: BENEFITS v36 ccst_nonpecuniarybenefit_icare
--				v45 updated on 09/05 ag
-- =============================================

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_nonpecuniarybenefit_icare','Benefits - v45 09/05/2019'
	--drop table ccst_nonpecuniarybenefit_icare
	Select distinct
	cd.claim_number as LUWID,
	'EMICS:'+ ltrim(rtrim(cd.Claim_Number)) + ':NONPECUNIARYBENEFIT' PublicID,
	'EMICS:'+ ltrim(rtrim(cd.Claim_Number)) + ':INDEMNITY' ExposureID,
	NULL as EndDate,
	NULL as NonPecuniaryBenefitsType,
	cd.Non_Pecuniary as Value
		
	INTO ccst_nonpecuniarybenefit_icare

	FROM [DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
	lEFT JOIN [DM_STG_EMICS].[dbo].CLAIM_ACTIVITY_DETAIL cad on cad.Claim_no=cd.Claim_Number
	Left join [DM_STG_EMICS].[dbo].Payment_recovery pr on pr.claim_no = cd.claim_number
	
	Where EXISTS (
		Select 1 from dbo.ccst_POLICY cp WHERE LTRIM(RTRIM(cd.Claim_Number)) = LTRIM(RTRIM(cp.LUWID)) AND cp.PolicyNumber IS NOT NULL AND cp.Verified = 1)
		AND cad.Claim_liability_indicator <> 12
		AND (pr.Estimate_type in (50,72,54,58,59,60,65) OR cd.is_Time_Lost = 1 OR cd.Date_Deceased is not null)
		AND non_pecuniary > 0

END