﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 29/05/2019
-- Description:	Contacts - Named Insured V003.7
-- 1. This is the child Stored Proc for EMPLOYER contact that will called by the master stored proc SP_REC_XFM_CONTACT
-- 2. <19-Jul-2019><Satish>: As part of DMIG-6215 clarification provided, changed the logic for Verified PC aggregateremittance_icare field
-- 3. <19-Jul-2019><Satish>: Modified by removing a space before INSURED:BUSINESSADDRESS
-- 4. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 5. <27Jul2019><Satish>: 
--    (a) Modified Verified PC logic for "PrimaryPhone" as it was not picking the TYPECODE instead was picking the ID
--    (b) Modified Verified PC logic for "communicationpreferences_icare" by adding a new CM lookup table [dbo].[abtl_commmethod_icare] and using 
--        to get the TYPECODE and populating the target with that. Earlier the SP was using just the ID from ab_abcontact.
-- 6. <07Aug2019><Satish>:
--    (a) Modified Verified PC logic for "OrganisationType_icare" by adding a new CM lookup table [dbo].[abtl_accountorgtype_icare] 
--        and using to get the TYPECODE and populating the target with that. Earlier the SP was using just the OrganisationType_icare (ID) from ab_abcontact.
--    (b) Modified Verified PC logic for "TaxStatus" by adding a new CM lookup table [dbo].[abtl_taxstatus] and using to get the TYPECODE and populating 
--        the target with that. Earlier the SP was using just the TaxStatus (ID) from ab_abcontact.
--    (c) Modified Verified PC logic for "trusteetype_icare" by adding a new CM lookup table [dbo].[abtl_trusteetype_icare] and using to get the TYPECODE and 
--        populating the target with that. Earlier the SP was using just the trusteetype_icare (ID) from ab_abcontact.
-- 6. <08Aug2019><Satish>:
--    (a) Modified Verified PC logic for "SourceSystem_icare" by sourcing it from CRM instead of CM as per discussion with mapper.
-- 7. <20Aug2019><Satish>:
--    (a) Reverting the change for Verified PC logic for "SourceSystem_icare" to use CM itself.
-- 8. <30Aug2019><Satish>: As part of DMIG-8086 (DMIG-8090)
--    (a) Within Verified PC section below, logic for CRMVersion has been changed to pick the highest CRM number where possible. If CM has higher value then
--        CM CRMVersion should be used. If CRM has higher value, then CRM CRMVersion should be used.
-- 9. <06Sep2019><Satish>: As part of DMIG-7698 (DMIG-7891) and DMIG-6151
--    (a) Modified the Verified PC logic to now include matches to ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup based on 4 potential match scenarios -
--        (i)   Getting Contact matches using Policy_term_detail.icare_policy_no against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.policynumber
--        (ii)  Getting Contact matches using Claim_detail.Policy_No and matching against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.Legacypolicynumber_icare
--        (iii) Getting Contact matches using Claim_detail.Policy_No and matching against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.policynumber
--        (iv)  Getting Contact matches by checking to see if we length of CD.PolicyNo is 12 chars and it ends in 701 and if so stripping 701 from CD.PolicyNo and try 
--              and match it against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.PolicyNumber
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CONTACT_EMPLOYER_STAGING]
	
AS
BEGIN

IF OBJECT_ID('ContactREF_temptable_employer_verifiedPC') is not null
drop table dbo.ContactREF_temptable_employer_verifiedPC;

IF OBJECT_ID('ContactREF_temptable_employer_verifiedCDR') is not null
drop table dbo.ContactREF_temptable_employer_verifiedCDR;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC') is not null
drop table temptable_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedCDR') is not null
drop table temptable_EMPLOYER_VerifiedCDR;

IF OBJECT_ID('ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_1') is not null
drop table temptable_EMPLOYER_VerifiedPC_1;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_2') is not null
drop table temptable_EMPLOYER_VerifiedPC_2;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_3') is not null
drop table temptable_EMPLOYER_VerifiedPC_3;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_4') is not null
drop table temptable_EMPLOYER_VerifiedPC_4;
--select * from temptable_EMPLOYER_VerifiedPC
--where LUWID=1069665

--drop table ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
--Creating this temp table to get all the PC contacts (Employer a.k.a Named Insured and Main Contacts)
SELECT * INTO ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
FROM
(
SELECT DISTINCT
	 NULL [ID]
	,NULL [PeriodStart]
	,NULL [PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],''))) [PolicyNumber]
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],''))) [LegacyPolicyNumber_icare]
	,a.[CRMUniqueID_icare] [EmployerLinkID]
	,ctl.[TYPECODE] [Subtype]
	,NULL [FirstName]
	,NULL [LastName]
	,NULL [ContactLinkID]
	,'SC5 - Insured Employer' [SC_Type]
	,NULL [IsPPLC]
FROM [DM_STG_PC].[dbo].[pc_policyperiod] pp 
INNER JOIN [DM_STG_PC].[dbo].[pc_policy] p
ON pp.[PolicyID] = p.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_account] a
ON p.[AccountID] = a.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac
ON a.[ID] = ac.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c
ON ac.[Contact] = c.[ID]
AND a.[CRMUniqueID_icare] = c.[AddressBookUID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr
ON ac.[ID] = acr.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl
ON acr.[Subtype] = acrtl.[ID]
AND acrtl.[TYPECODE] = 'NamedInsured'
INNER JOIN [DM_STG_PC].[dbo].[pctl_contact] ctl
ON c.[Subtype] = ctl.[ID]
WHERE pp.[MostRecentModel] = 1 -- the most recent version
AND pp.[Status] = 9 -- Bound
AND CAST(pp.[PeriodStart] AS DATE) <> CAST(ISNULL(pp.[CancellationDate],'') AS DATE)
AND pp.[Retired] = 0
AND p.[Retired] = 0
AND a.[Retired] = 0
AND ac.[Retired] = 0
AND c.[Retired] = 0
AND acr.[Retired] = 0

UNION ALL

SELECT
	 pp.[ID]
	,pp.[PeriodStart]
	,pp.[PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],''))) [PolicyNumber]
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],''))) [LegacyPolicyNumber_icare]
	,a.[CRMUniqueID_icare] [EmployerLinkID]
	,ctl.[TYPECODE] [Subtype]
	,LTRIM(RTRIM(ISNULL(c1.[FirstName],''))) [FirstName]
	,LTRIM(RTRIM(ISNULL(c1.[LastName],''))) [LastName]
	,c1.[AddressBookUID] [ContactLinkID]
	,'SC15 - Insured Employer Main Contact' [SC_Type]
	,CASE WHEN acrtl1.[TYPECODE] = 'LocationContact_icare' THEN 'Y' ELSE 'N' END [IsPPLC]
FROM [DM_STG_PC].[dbo].[pc_policyperiod] pp 
INNER JOIN [DM_STG_PC].[dbo].[pc_policy] p
ON pp.[PolicyID] = p.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_account] a
ON p.[AccountID] = a.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac
ON a.[ID] = ac.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c
ON ac.[Contact] = c.[ID]
AND a.[CRMUniqueID_icare] = c.[AddressBookUID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr
ON ac.[ID] = acr.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl
ON acr.[Subtype] = acrtl.[ID]
AND acrtl.[TYPECODE] = 'NamedInsured'
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac1
ON a.[ID] = ac1.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c1
ON ac1.[Contact] = c1.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr1
ON ac1.[ID] = acr1.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl1
ON acr1.[Subtype] = acrtl1.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pctl_contact] ctl
ON c1.[Subtype] = ctl.[ID]
AND ctl.[TYPECODE] = 'Person'
WHERE pp.[MostRecentModel] = 1 -- the most recent version
AND pp.[Status] = 9 -- Bound
--AND pp.[CancellationDate] IS NULL
AND CAST(pp.[PeriodStart] AS DATE) <> CAST(ISNULL(pp.[CancellationDate],'') AS DATE)
AND pp.[Retired] = 0
AND p.[Retired] = 0
AND a.[Retired] = 0
AND ac.[Retired] = 0
AND c.[Retired] = 0
AND acr.[Retired] = 0
AND ac1.[Retired] = 0
AND c1.[Retired] = 0
AND acr1.[Retired] = 0
GROUP BY
	 pp.[ID]
	,pp.[PeriodStart]
	,pp.[PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],'')))
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],'')))
	,a.[CRMUniqueID_icare]
	,ctl.[TYPECODE]
	,LTRIM(RTRIM(ISNULL(c1.[FirstName],'')))
	,LTRIM(RTRIM(ISNULL(c1.[LastName],'')))
	,c1.[AddressBookUID]
	,CASE WHEN acrtl1.[TYPECODE] = 'LocationContact_icare' THEN 'Y' ELSE 'N' END
)X

--select * from ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
--Using PT.icare_policy_no to match with gwpc
SELECT * INTO temptable_EMPLOYER_VerifiedPC_1
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as PublicID,
--eft.is_enable_eft, --15/11/2018<Satish R>: Added as part of DMIG-2495
--Claim details
--CRM_A.id as CRM_ID,
--CD.Claim_Number,
----PC PolicyPeriod details
--PC_PP.PrimaryInsuredName,
--PC_PP.PolicyNumber,
--PC_PP.PeriodStart,
--PC_PP.PeriodEnd,
--PC_PP.ID as PC_P_ID,
-- AB contact details to populate in ccst_contact
CM_C.ABNIsActive_icare,
CM_C.ABNValidated_icare,
CM_C.ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.LinkID as AddressBookUID,
CM_C.AdjudicativeDomain,
CM_C.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CASE 
WHEN CM_C.AggregateRemittance_icare=1 then 1   --Fixing it as part of DMIG-6251
ELSE 0
END
as AggregateRemittance_icare,
CM_C.AttorneyLicense,
CM_C.AttorneySpecialty,
CM_C.AttorneySpecialty_icare,
NULL as AutoRepairLicense,
'allow' as AutoSync,
NULL as AutoTowingLicense,
CM_C.BlockedVendor_icare,
CM_C.CellPhone,
CM_C.CellPhoneCountry,
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
'FALSE' as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
CM_C.EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 16 --'AU'  
ELSE CM_C.FaxPhoneCountry  
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and companies will have this blank
CM_C.FirstNameKanji,
CM_C.FormerName,
CM_C.Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
CM_C.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
CM_C.LastNameKanji,
CM_C.LawFirmSpecialty,
CM_C.LegalSpecialty_icare,
CM_C.LicenseNumber,
CM_C.LicenseState,
CM_C.MaritalStatus,
CM_C.MedicalLicense,
CM_C.MedicalOrgSpecialty,
CM_C.MedicalSpecialty_icare,
CM_C.MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
CM_C.MedicareNumber_icare,
CM_C.MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
CM_C.NameKanji,
CM_C.Notes,
CM_C.NumDependents,
CM_C.NumDependentsU18,
CM_C.NumDependentsU25,
'FALSE' as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
CM_C.OktaID_icare,
NULL as OrganizationType,
CM_C.Particle,
NULL as PassportNumber,
CM_C.PayInfoUpdUserName_icare,
'FALSE' as PendingLinkMessage,
'PC' as PolicySystemId,
CASE WHEN CM_C.Preferred is NULL then 0
ELSE CM_C.Preferred
End
Preferred,
CM_C.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.Prefix,
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PrimaryAddressID,
'en_AU' as PrimaryLanguage,
NULL as PrimaryLocale,
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
CM_C.RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
CM_C.Score,
0 as SingleRemittance,
CM_C.SourceSystem_icare,
--crm_a.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--08/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Company' as Subtype,
CM_C.Suffix,
CM_C.TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
--CM_C.TaxStatus, --07/08/2019 commented this out as this is just the ID
abtl_tax.TYPECODE as TaxStatus,
--07/08/2019 Added this because of QA differences due to abtl_taxstatus ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
Null as TempCRMUniqueID_icare,
CM_C.TradingName_icare,
CM_C.TrustName_icare,
CM_C.TrusteeName_icare,
'loadsave' as ValidationLevel,
CM_C.VendorNumber,
CM_C.VendorType,
CM_C.VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
CM_C.WithholdingRate,
CM_C.WorkPhone,
CM_C.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
CM_C.ACN_icare,
CM_C.CommencementDate_icare,
--CM_C.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
abtl_organizationtype.TYPECODE as OrganisationType_icare, --07/08/2019 --Added this to resolve a QA difference in 105 (Cohort 1)
NULL as OtherLanguage_icare,
CM_C.TrustABN_icare,
NULL as TrustABNIsActive_icare,
CM_C.TrustABNValidated_icare,
--CM_C.TrusteeType_icare, --07/08/2019 commented this out as this is just the ID
abtl_trustee.TYPECODE as TrusteeType_icare,
--07/08/2019 Added this because of QA differences due to [dbo].[abtl_trusteetype_icare] ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.TrusteeType_icare as the code was earlier simply mapping this directly from CM_C which is just the ID.
CM_C.TrustRegisteredForGST_icare,
NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
--CM_C.ID as CM_C_ID, --12/03/19: Added so that this can be used in CCST_CONTACTTAG selection SQL later
--CM_C.Subtype as CM_C_Subtype, --1 is company 2 Person (In this query they were all 1
--CM_C.PrimaryAddressID,
--abtl_PrimaryLanguageType.[ID],
--,abtl_abcontact.TypeCode
--,eft.is_enable_eft --15/11/2018<Satish R>: Added as part of DMIG-2495
--,cpr.preferredPaymentDerivedByCPR
,--CASE 
--WHEN eft.is_enable_eft=1 and eftdata.bankname is not null then 'eft'  --making sure we check for the presence of a valid bank account before setting to eft
--WHEN cpr.preferredPaymentDerivedByCPR='eft' and eftdata.bankname is not null THEN 'eft' --making sure we check for the presence of a valid bank account before setting to eft
--ELSE 'check'
--END as PreferredPaymentMethod_icare_based_on_bankaccount,
1 as ApprovedPaymentMethod
--0 as AggregaeRemittance--,

--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup gwpc_con_lkp on gwpc_con_lkp.PolicyNumber=PT.icare_policy_no and gwpc_con_lkp.SC_Type='SC5 - Insured Employer'
left outer join --15/11/2018<Satish R>: Added for DMIG-2495
(
select distinct eft.is_enable_eft,cd.Policy_No from DM_STG_EMICS.dbo.CLAIM_DETAIL cd
left outer join  
(SELECT * FROM DM_STG_EMICS.dbo.EFT WHERE Object_Type='E') EFT
ON cd.Policy_No=EFT.Object AND RTRIM(LTRIM(cd.Policy_No)) IS NOT NULL
where eft.Object is  not null
) eft
on eft.Policy_No=CD.Policy_No
left outer join
(
select cpr1.*,A.preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.CLAIM_PAYMENT_RUN cpr1 
left outer join 
(
select Payee_Code, case WHEN max(Cheque_Status)  = '9' then 'eft' else 'check' end as preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.claim_payment_run 
where cheque_status <> '3'
group by Payee_Code
) as A
on cpr1.Payee_Code = A.Payee_Code
inner join 
(
select max(payment_no) as PaymentNo, Payee_type
from DM_STG_EMICS.DBO.CLAIM_PAYMENT_RUN
GROUP BY CLAIM_NUMBER,Payee_type   
)  Payee_Payment_Number 
on cpr1.Payment_no = Payee_Payment_Number.PaymentNo
where cpr1.Cheque_Status <> '3' 
and cpr1.Payee_type = 1 -- Inj Worker
) cpr
on cpr.Claim_Number=cd.Claim_Number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.EFT where Object_Type='E') eft1
on eft1.Object=cd.Claim_Number
LEFT OUTER JOIN DM_STG_EMICS.dbo.TFN 
on CD.Claim_Number=TFN.Claim_no
--10-Jan-2019: Adding CRM tables below as part of DMIG-2777 fix
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_CLAIM] ccl --Remember to switch _LD back to without _LD
--ON LTRIM(RTRIM(ISNULL(cd.[Claim_Number],''))) = LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_POLICY] p --Remember to switch _LD back to without _LD
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON gwpc_con_lkp.EmployerLinkID = crm_a.[ID]  
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
---select * from [DM_STG_CM].[dbo].[ab_abcontact]
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--Ash 3/5/2019
--Added extra RecordType
--INNER JOIN [DM_STG_CRM_LD].[preload].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --Remember to switch _LD back to without _LD
INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--select count(1) from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C 
--where ID=255715
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS'
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= gwpc_con_lkp.EmployerLinkID
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
--select PrimaryAddressID,* from [DM_STG_CM].[dbo].[ab_abcontact] CM_C where LinkID='0032O000005Bm5AQAS'
--select AddressType,* from DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A where PublicID='EMICS:10112:VENDOR:BUSINESSADDRESS'
--select AddressType,* from [DM_STG_CM].[dbo].[ab_address] where ID='2882'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN DM_STG_CM.[dbo].[abtl_accountorgtype_icare] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.organizationtype_icare as the ref table prior to this that was being used i.e. abtl_organizationtype is incorrect.
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_taxstatus] abtl_tax on abtl_tax.ID = CM_C.TaxStatus
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_trusteetype_icare] abtl_trustee on abtl_trustee.ID = CM_C.TrusteeType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
-- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and PC_P.MostRecentModel=1 --the most recent version (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_P.status=9 --policy is bound (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_PCR.subtype=19  --19 making sure it is primarynamedinsured role contact only
--and cons_pp.LUWID is not null --15/11/2018 <Satish R>: Making sure its verified PC only
--and eft.Object is  not null  --making sure we pick the ones where we have eft data
--and CD.date_of_injury between PC_P.PeriodStart and PC_P.PeriodEnd (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_ADD.AddressLine1 is null
--and PC_PCR.ExpirationDate is null --15/11/2018 <Satish R>: Added this to eliminate duplicate Address records.
--and cons_pp.POLICY_FLAG=0 --verified PC only
--and ltrim(rtrim(CD.Claim_Number))=1025345
--61528
)X


--Using CD.Policy_No to match with gwpc_contacts LegacyPolicyNumber_icare
SELECT * INTO temptable_EMPLOYER_VerifiedPC_2
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as PublicID,
--eft.is_enable_eft, --15/11/2018<Satish R>: Added as part of DMIG-2495
--Claim details
--CRM_A.id as CRM_ID,
--CD.Claim_Number,
----PC PolicyPeriod details
--PC_PP.PrimaryInsuredName,
--PC_PP.PolicyNumber,
--PC_PP.PeriodStart,
--PC_PP.PeriodEnd,
--PC_PP.ID as PC_P_ID,
-- AB contact details to populate in ccst_contact
CM_C.ABNIsActive_icare,
CM_C.ABNValidated_icare,
CM_C.ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.LinkID as AddressBookUID,
CM_C.AdjudicativeDomain,
CM_C.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CASE 
WHEN CM_C.AggregateRemittance_icare=1 then 1   --Fixing it as part of DMIG-6251
ELSE 0
END
as AggregateRemittance_icare,
CM_C.AttorneyLicense,
CM_C.AttorneySpecialty,
CM_C.AttorneySpecialty_icare,
NULL as AutoRepairLicense,
'allow' as AutoSync,
NULL as AutoTowingLicense,
CM_C.BlockedVendor_icare,
CM_C.CellPhone,
CM_C.CellPhoneCountry,
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
'FALSE' as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
CM_C.EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 16 --'AU'  
ELSE CM_C.FaxPhoneCountry  
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and companies will have this blank
CM_C.FirstNameKanji,
CM_C.FormerName,
CM_C.Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
CM_C.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
CM_C.LastNameKanji,
CM_C.LawFirmSpecialty,
CM_C.LegalSpecialty_icare,
CM_C.LicenseNumber,
CM_C.LicenseState,
CM_C.MaritalStatus,
CM_C.MedicalLicense,
CM_C.MedicalOrgSpecialty,
CM_C.MedicalSpecialty_icare,
CM_C.MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
CM_C.MedicareNumber_icare,
CM_C.MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
CM_C.NameKanji,
CM_C.Notes,
CM_C.NumDependents,
CM_C.NumDependentsU18,
CM_C.NumDependentsU25,
'FALSE' as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
CM_C.OktaID_icare,
NULL as OrganizationType,
CM_C.Particle,
NULL as PassportNumber,
CM_C.PayInfoUpdUserName_icare,
'FALSE' as PendingLinkMessage,
'PC' as PolicySystemId,
CASE WHEN CM_C.Preferred is NULL then 0
ELSE CM_C.Preferred
End
Preferred,
CM_C.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.Prefix,
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PrimaryAddressID,
'en_AU' as PrimaryLanguage,
NULL as PrimaryLocale,
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
CM_C.RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
CM_C.Score,
0 as SingleRemittance,
CM_C.SourceSystem_icare,
--crm_a.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--08/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Company' as Subtype,
CM_C.Suffix,
CM_C.TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
--CM_C.TaxStatus, --07/08/2019 commented this out as this is just the ID
abtl_tax.TYPECODE as TaxStatus,
--07/08/2019 Added this because of QA differences due to abtl_taxstatus ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
Null as TempCRMUniqueID_icare,
CM_C.TradingName_icare,
CM_C.TrustName_icare,
CM_C.TrusteeName_icare,
'loadsave' as ValidationLevel,
CM_C.VendorNumber,
CM_C.VendorType,
CM_C.VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
CM_C.WithholdingRate,
CM_C.WorkPhone,
CM_C.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
CM_C.ACN_icare,
CM_C.CommencementDate_icare,
--CM_C.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
abtl_organizationtype.TYPECODE as OrganisationType_icare, --07/08/2019 --Added this to resolve a QA difference in 105 (Cohort 1)
NULL as OtherLanguage_icare,
CM_C.TrustABN_icare,
NULL as TrustABNIsActive_icare,
CM_C.TrustABNValidated_icare,
--CM_C.TrusteeType_icare, --07/08/2019 commented this out as this is just the ID
abtl_trustee.TYPECODE as TrusteeType_icare,
--07/08/2019 Added this because of QA differences due to [dbo].[abtl_trusteetype_icare] ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.TrusteeType_icare as the code was earlier simply mapping this directly from CM_C which is just the ID.
CM_C.TrustRegisteredForGST_icare,
NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
--CM_C.ID as CM_C_ID, --12/03/19: Added so that this can be used in CCST_CONTACTTAG selection SQL later
--CM_C.Subtype as CM_C_Subtype, --1 is company 2 Person (In this query they were all 1
--CM_C.PrimaryAddressID,
--abtl_PrimaryLanguageType.[ID],
--,abtl_abcontact.TypeCode
--,eft.is_enable_eft --15/11/2018<Satish R>: Added as part of DMIG-2495
--,cpr.preferredPaymentDerivedByCPR
,--CASE 
--WHEN eft.is_enable_eft=1 and eftdata.bankname is not null then 'eft'  --making sure we check for the presence of a valid bank account before setting to eft
--WHEN cpr.preferredPaymentDerivedByCPR='eft' and eftdata.bankname is not null THEN 'eft' --making sure we check for the presence of a valid bank account before setting to eft
--ELSE 'check'
--END as PreferredPaymentMethod_icare_based_on_bankaccount,
1 as ApprovedPaymentMethod
--0 as AggregaeRemittance--,

--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup gwpc_con_lkp on gwpc_con_lkp.LegacyPolicyNumber_icare=ltrim(rtrim(CD.Policy_No)) and gwpc_con_lkp.SC_Type='SC5 - Insured Employer'
left outer join temptable_EMPLOYER_VerifiedPC_1 tmp1 on tmp1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--02/09/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
left outer join --15/11/2018<Satish R>: Added for DMIG-2495
(
select distinct eft.is_enable_eft,cd.Policy_No from DM_STG_EMICS.dbo.CLAIM_DETAIL cd
left outer join  
(SELECT * FROM DM_STG_EMICS.dbo.EFT WHERE Object_Type='E') EFT
ON cd.Policy_No=EFT.Object AND RTRIM(LTRIM(cd.Policy_No)) IS NOT NULL
where eft.Object is  not null
) eft
on eft.Policy_No=CD.Policy_No
left outer join
(
select cpr1.*,A.preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.CLAIM_PAYMENT_RUN cpr1 
left outer join 
(
select Payee_Code, case WHEN max(Cheque_Status)  = '9' then 'eft' else 'check' end as preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.claim_payment_run 
where cheque_status <> '3'
group by Payee_Code
) as A
on cpr1.Payee_Code = A.Payee_Code
inner join 
(
select max(payment_no) as PaymentNo, Payee_type
from DM_STG_EMICS.DBO.CLAIM_PAYMENT_RUN
GROUP BY CLAIM_NUMBER,Payee_type   
)  Payee_Payment_Number 
on cpr1.Payment_no = Payee_Payment_Number.PaymentNo
where cpr1.Cheque_Status <> '3' 
and cpr1.Payee_type = 1 -- Inj Worker
) cpr
on cpr.Claim_Number=cd.Claim_Number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.EFT where Object_Type='E') eft1
on eft1.Object=cd.Claim_Number
LEFT OUTER JOIN DM_STG_EMICS.dbo.TFN 
on CD.Claim_Number=TFN.Claim_no
--10-Jan-2019: Adding CRM tables below as part of DMIG-2777 fix
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_CLAIM] ccl --Remember to switch _LD back to without _LD
--ON LTRIM(RTRIM(ISNULL(cd.[Claim_Number],''))) = LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_POLICY] p --Remember to switch _LD back to without _LD
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON gwpc_con_lkp.EmployerLinkID = crm_a.[ID]  
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
---select * from [DM_STG_CM].[dbo].[ab_abcontact]
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--Ash 3/5/2019
--Added extra RecordType
--INNER JOIN [DM_STG_CRM_LD].[preload].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --Remember to switch _LD back to without _LD
INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--select count(1) from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C 
--where ID=255715
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS'
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= gwpc_con_lkp.EmployerLinkID
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
--select PrimaryAddressID,* from [DM_STG_CM].[dbo].[ab_abcontact] CM_C where LinkID='0032O000005Bm5AQAS'
--select AddressType,* from DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A where PublicID='EMICS:10112:VENDOR:BUSINESSADDRESS'
--select AddressType,* from [DM_STG_CM].[dbo].[ab_address] where ID='2882'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN DM_STG_CM.[dbo].[abtl_accountorgtype_icare] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.organizationtype_icare as the ref table prior to this that was being used i.e. abtl_organizationtype is incorrect.
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_taxstatus] abtl_tax on abtl_tax.ID = CM_C.TaxStatus
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_trusteetype_icare] abtl_trustee on abtl_trustee.ID = CM_C.TrusteeType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
-- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
and tmp1.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
--and PC_P.MostRecentModel=1 --the most recent version (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_P.status=9 --policy is bound (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_PCR.subtype=19  --19 making sure it is primarynamedinsured role contact only
--and cons_pp.LUWID is not null --15/11/2018 <Satish R>: Making sure its verified PC only
--and eft.Object is  not null  --making sure we pick the ones where we have eft data
--and CD.date_of_injury between PC_P.PeriodStart and PC_P.PeriodEnd (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_ADD.AddressLine1 is null
--and PC_PCR.ExpirationDate is null --15/11/2018 <Satish R>: Added this to eliminate duplicate Address records.
--and cons_pp.POLICY_FLAG=0 --verified PC only
--and ltrim(rtrim(CD.Claim_Number))=1025345
--61528
)X


--Using CD.Policy_No to match with gwpc_contacts PolicyNumber
SELECT * INTO temptable_EMPLOYER_VerifiedPC_3
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as PublicID,
--eft.is_enable_eft, --15/11/2018<Satish R>: Added as part of DMIG-2495
--Claim details
--CRM_A.id as CRM_ID,
--CD.Claim_Number,
----PC PolicyPeriod details
--PC_PP.PrimaryInsuredName,
--PC_PP.PolicyNumber,
--PC_PP.PeriodStart,
--PC_PP.PeriodEnd,
--PC_PP.ID as PC_P_ID,
-- AB contact details to populate in ccst_contact
CM_C.ABNIsActive_icare,
CM_C.ABNValidated_icare,
CM_C.ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.LinkID as AddressBookUID,
CM_C.AdjudicativeDomain,
CM_C.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CASE 
WHEN CM_C.AggregateRemittance_icare=1 then 1   --Fixing it as part of DMIG-6251
ELSE 0
END
as AggregateRemittance_icare,
CM_C.AttorneyLicense,
CM_C.AttorneySpecialty,
CM_C.AttorneySpecialty_icare,
NULL as AutoRepairLicense,
'allow' as AutoSync,
NULL as AutoTowingLicense,
CM_C.BlockedVendor_icare,
CM_C.CellPhone,
CM_C.CellPhoneCountry,
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
'FALSE' as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
CM_C.EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 16 --'AU'  
ELSE CM_C.FaxPhoneCountry  
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and companies will have this blank
CM_C.FirstNameKanji,
CM_C.FormerName,
CM_C.Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
CM_C.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
CM_C.LastNameKanji,
CM_C.LawFirmSpecialty,
CM_C.LegalSpecialty_icare,
CM_C.LicenseNumber,
CM_C.LicenseState,
CM_C.MaritalStatus,
CM_C.MedicalLicense,
CM_C.MedicalOrgSpecialty,
CM_C.MedicalSpecialty_icare,
CM_C.MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
CM_C.MedicareNumber_icare,
CM_C.MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
CM_C.NameKanji,
CM_C.Notes,
CM_C.NumDependents,
CM_C.NumDependentsU18,
CM_C.NumDependentsU25,
'FALSE' as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
CM_C.OktaID_icare,
NULL as OrganizationType,
CM_C.Particle,
NULL as PassportNumber,
CM_C.PayInfoUpdUserName_icare,
'FALSE' as PendingLinkMessage,
'PC' as PolicySystemId,
CASE WHEN CM_C.Preferred is NULL then 0
ELSE CM_C.Preferred
End
Preferred,
CM_C.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.Prefix,
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PrimaryAddressID,
'en_AU' as PrimaryLanguage,
NULL as PrimaryLocale,
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
CM_C.RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
CM_C.Score,
0 as SingleRemittance,
CM_C.SourceSystem_icare,
--crm_a.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--08/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Company' as Subtype,
CM_C.Suffix,
CM_C.TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
--CM_C.TaxStatus, --07/08/2019 commented this out as this is just the ID
abtl_tax.TYPECODE as TaxStatus,
--07/08/2019 Added this because of QA differences due to abtl_taxstatus ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
Null as TempCRMUniqueID_icare,
CM_C.TradingName_icare,
CM_C.TrustName_icare,
CM_C.TrusteeName_icare,
'loadsave' as ValidationLevel,
CM_C.VendorNumber,
CM_C.VendorType,
CM_C.VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
CM_C.WithholdingRate,
CM_C.WorkPhone,
CM_C.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
CM_C.ACN_icare,
CM_C.CommencementDate_icare,
--CM_C.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
abtl_organizationtype.TYPECODE as OrganisationType_icare, --07/08/2019 --Added this to resolve a QA difference in 105 (Cohort 1)
NULL as OtherLanguage_icare,
CM_C.TrustABN_icare,
NULL as TrustABNIsActive_icare,
CM_C.TrustABNValidated_icare,
--CM_C.TrusteeType_icare, --07/08/2019 commented this out as this is just the ID
abtl_trustee.TYPECODE as TrusteeType_icare,
--07/08/2019 Added this because of QA differences due to [dbo].[abtl_trusteetype_icare] ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.TrusteeType_icare as the code was earlier simply mapping this directly from CM_C which is just the ID.
CM_C.TrustRegisteredForGST_icare,
NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
--CM_C.ID as CM_C_ID, --12/03/19: Added so that this can be used in CCST_CONTACTTAG selection SQL later
--CM_C.Subtype as CM_C_Subtype, --1 is company 2 Person (In this query they were all 1
--CM_C.PrimaryAddressID,
--abtl_PrimaryLanguageType.[ID],
--,abtl_abcontact.TypeCode
--,eft.is_enable_eft --15/11/2018<Satish R>: Added as part of DMIG-2495
--,cpr.preferredPaymentDerivedByCPR
,--CASE 
--WHEN eft.is_enable_eft=1 and eftdata.bankname is not null then 'eft'  --making sure we check for the presence of a valid bank account before setting to eft
--WHEN cpr.preferredPaymentDerivedByCPR='eft' and eftdata.bankname is not null THEN 'eft' --making sure we check for the presence of a valid bank account before setting to eft
--ELSE 'check'
--END as PreferredPaymentMethod_icare_based_on_bankaccount,
1 as ApprovedPaymentMethod
--0 as AggregaeRemittance--,

--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup gwpc_con_lkp on gwpc_con_lkp.PolicyNumber=ltrim(rtrim(CD.Policy_No)) and gwpc_con_lkp.SC_Type='SC5 - Insured Employer'
left outer join temptable_EMPLOYER_VerifiedPC_1 tmp1 on tmp1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_EMPLOYER_VerifiedPC_2 tmp2 on tmp2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--02/09/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
left outer join --15/11/2018<Satish R>: Added for DMIG-2495
(
select distinct eft.is_enable_eft,cd.Policy_No from DM_STG_EMICS.dbo.CLAIM_DETAIL cd
left outer join  
(SELECT * FROM DM_STG_EMICS.dbo.EFT WHERE Object_Type='E') EFT
ON cd.Policy_No=EFT.Object AND RTRIM(LTRIM(cd.Policy_No)) IS NOT NULL
where eft.Object is  not null
) eft
on eft.Policy_No=CD.Policy_No
left outer join
(
select cpr1.*,A.preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.CLAIM_PAYMENT_RUN cpr1 
left outer join 
(
select Payee_Code, case WHEN max(Cheque_Status)  = '9' then 'eft' else 'check' end as preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.claim_payment_run 
where cheque_status <> '3'
group by Payee_Code
) as A
on cpr1.Payee_Code = A.Payee_Code
inner join 
(
select max(payment_no) as PaymentNo, Payee_type
from DM_STG_EMICS.DBO.CLAIM_PAYMENT_RUN
GROUP BY CLAIM_NUMBER,Payee_type   
)  Payee_Payment_Number 
on cpr1.Payment_no = Payee_Payment_Number.PaymentNo
where cpr1.Cheque_Status <> '3' 
and cpr1.Payee_type = 1 -- Inj Worker
) cpr
on cpr.Claim_Number=cd.Claim_Number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.EFT where Object_Type='E') eft1
on eft1.Object=cd.Claim_Number
LEFT OUTER JOIN DM_STG_EMICS.dbo.TFN 
on CD.Claim_Number=TFN.Claim_no
--10-Jan-2019: Adding CRM tables below as part of DMIG-2777 fix
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_CLAIM] ccl --Remember to switch _LD back to without _LD
--ON LTRIM(RTRIM(ISNULL(cd.[Claim_Number],''))) = LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_POLICY] p --Remember to switch _LD back to without _LD
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON gwpc_con_lkp.EmployerLinkID = crm_a.[ID]  
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
---select * from [DM_STG_CM].[dbo].[ab_abcontact]
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--Ash 3/5/2019
--Added extra RecordType
--INNER JOIN [DM_STG_CRM_LD].[preload].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --Remember to switch _LD back to without _LD
INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--select count(1) from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C 
--where ID=255715
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS'
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= gwpc_con_lkp.EmployerLinkID
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
--select PrimaryAddressID,* from [DM_STG_CM].[dbo].[ab_abcontact] CM_C where LinkID='0032O000005Bm5AQAS'
--select AddressType,* from DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A where PublicID='EMICS:10112:VENDOR:BUSINESSADDRESS'
--select AddressType,* from [DM_STG_CM].[dbo].[ab_address] where ID='2882'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN DM_STG_CM.[dbo].[abtl_accountorgtype_icare] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.organizationtype_icare as the ref table prior to this that was being used i.e. abtl_organizationtype is incorrect.
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_taxstatus] abtl_tax on abtl_tax.ID = CM_C.TaxStatus
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_trusteetype_icare] abtl_trustee on abtl_trustee.ID = CM_C.TrusteeType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
-- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
and tmp1.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp2.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
--and PC_P.MostRecentModel=1 --the most recent version (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_P.status=9 --policy is bound (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_PCR.subtype=19  --19 making sure it is primarynamedinsured role contact only
--and cons_pp.LUWID is not null --15/11/2018 <Satish R>: Making sure its verified PC only
--and eft.Object is  not null  --making sure we pick the ones where we have eft data
--and CD.date_of_injury between PC_P.PeriodStart and PC_P.PeriodEnd (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_ADD.AddressLine1 is null
--and PC_PCR.ExpirationDate is null --15/11/2018 <Satish R>: Added this to eliminate duplicate Address records.
--and cons_pp.POLICY_FLAG=0 --verified PC only
--and ltrim(rtrim(CD.Claim_Number))=1025345
--61528
)X

--Using CD.Policy_No where PolicyNo ends in '%701'and stripping this away and trying to then match with gwpc_contacts PolicyNumber
SELECT * INTO temptable_EMPLOYER_VerifiedPC_4
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as PublicID,
--eft.is_enable_eft, --15/11/2018<Satish R>: Added as part of DMIG-2495
--Claim details
--CRM_A.id as CRM_ID,
--CD.Claim_Number,
----PC PolicyPeriod details
--PC_PP.PrimaryInsuredName,
--PC_PP.PolicyNumber,
--PC_PP.PeriodStart,
--PC_PP.PeriodEnd,
--PC_PP.ID as PC_P_ID,
-- AB contact details to populate in ccst_contact
CM_C.ABNIsActive_icare,
CM_C.ABNValidated_icare,
CM_C.ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.LinkID as AddressBookUID,
CM_C.AdjudicativeDomain,
CM_C.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CASE 
WHEN CM_C.AggregateRemittance_icare=1 then 1   --Fixing it as part of DMIG-6251
ELSE 0
END
as AggregateRemittance_icare,
CM_C.AttorneyLicense,
CM_C.AttorneySpecialty,
CM_C.AttorneySpecialty_icare,
NULL as AutoRepairLicense,
'allow' as AutoSync,
NULL as AutoTowingLicense,
CM_C.BlockedVendor_icare,
CM_C.CellPhone,
CM_C.CellPhoneCountry,
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
'FALSE' as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
CM_C.EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 16 --'AU'  
ELSE CM_C.FaxPhoneCountry  
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and companies will have this blank
CM_C.FirstNameKanji,
CM_C.FormerName,
CM_C.Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
CM_C.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
CM_C.LastNameKanji,
CM_C.LawFirmSpecialty,
CM_C.LegalSpecialty_icare,
CM_C.LicenseNumber,
CM_C.LicenseState,
CM_C.MaritalStatus,
CM_C.MedicalLicense,
CM_C.MedicalOrgSpecialty,
CM_C.MedicalSpecialty_icare,
CM_C.MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
CM_C.MedicareNumber_icare,
CM_C.MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
CM_C.NameKanji,
CM_C.Notes,
CM_C.NumDependents,
CM_C.NumDependentsU18,
CM_C.NumDependentsU25,
'FALSE' as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
CM_C.OktaID_icare,
NULL as OrganizationType,
CM_C.Particle,
NULL as PassportNumber,
CM_C.PayInfoUpdUserName_icare,
'FALSE' as PendingLinkMessage,
'PC' as PolicySystemId,
CASE WHEN CM_C.Preferred is NULL then 0
ELSE CM_C.Preferred
End
Preferred,
CM_C.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.Prefix,
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PrimaryAddressID,
'en_AU' as PrimaryLanguage,
NULL as PrimaryLocale,
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
CM_C.RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
CM_C.Score,
0 as SingleRemittance,
CM_C.SourceSystem_icare,
--crm_a.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--08/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Company' as Subtype,
CM_C.Suffix,
CM_C.TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
--CM_C.TaxStatus, --07/08/2019 commented this out as this is just the ID
abtl_tax.TYPECODE as TaxStatus,
--07/08/2019 Added this because of QA differences due to abtl_taxstatus ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
Null as TempCRMUniqueID_icare,
CM_C.TradingName_icare,
CM_C.TrustName_icare,
CM_C.TrusteeName_icare,
'loadsave' as ValidationLevel,
CM_C.VendorNumber,
CM_C.VendorType,
CM_C.VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
CM_C.WithholdingRate,
CM_C.WorkPhone,
CM_C.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
CM_C.ACN_icare,
CM_C.CommencementDate_icare,
--CM_C.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
abtl_organizationtype.TYPECODE as OrganisationType_icare, --07/08/2019 --Added this to resolve a QA difference in 105 (Cohort 1)
NULL as OtherLanguage_icare,
CM_C.TrustABN_icare,
NULL as TrustABNIsActive_icare,
CM_C.TrustABNValidated_icare,
--CM_C.TrusteeType_icare, --07/08/2019 commented this out as this is just the ID
abtl_trustee.TYPECODE as TrusteeType_icare,
--07/08/2019 Added this because of QA differences due to [dbo].[abtl_trusteetype_icare] ref table missing from SQL join below. 
--Have now added this ref table to map ccst_contact.TrusteeType_icare as the code was earlier simply mapping this directly from CM_C which is just the ID.
CM_C.TrustRegisteredForGST_icare,
NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_a.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_a.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
--CM_C.ID as CM_C_ID, --12/03/19: Added so that this can be used in CCST_CONTACTTAG selection SQL later
--CM_C.Subtype as CM_C_Subtype, --1 is company 2 Person (In this query they were all 1
--CM_C.PrimaryAddressID,
--abtl_PrimaryLanguageType.[ID],
--,abtl_abcontact.TypeCode
--,eft.is_enable_eft --15/11/2018<Satish R>: Added as part of DMIG-2495
--,cpr.preferredPaymentDerivedByCPR
,--CASE 
--WHEN eft.is_enable_eft=1 and eftdata.bankname is not null then 'eft'  --making sure we check for the presence of a valid bank account before setting to eft
--WHEN cpr.preferredPaymentDerivedByCPR='eft' and eftdata.bankname is not null THEN 'eft' --making sure we check for the presence of a valid bank account before setting to eft
--ELSE 'check'
--END as PreferredPaymentMethod_icare_based_on_bankaccount,
1 as ApprovedPaymentMethod
--0 as AggregaeRemittance--,

--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup gwpc_con_lkp 
on gwpc_con_lkp.PolicyNumber=substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3) 
and gwpc_con_lkp.SC_Type='SC5 - Insured Employer'
left outer join temptable_EMPLOYER_VerifiedPC_1 tmp1 on tmp1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_EMPLOYER_VerifiedPC_2 tmp2 on tmp2.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_EMPLOYER_VerifiedPC_3 tmp3 on tmp3.LUWID=ltrim(rtrim(CD.Claim_Number))  
--02/09/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
left outer join --15/11/2018<Satish R>: Added for DMIG-2495
(
select distinct eft.is_enable_eft,cd.Policy_No from DM_STG_EMICS.dbo.CLAIM_DETAIL cd
left outer join  
(SELECT * FROM DM_STG_EMICS.dbo.EFT WHERE Object_Type='E') EFT
ON cd.Policy_No=EFT.Object AND RTRIM(LTRIM(cd.Policy_No)) IS NOT NULL
where eft.Object is  not null
) eft
on eft.Policy_No=CD.Policy_No
left outer join
(
select cpr1.*,A.preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.CLAIM_PAYMENT_RUN cpr1 
left outer join 
(
select Payee_Code, case WHEN max(Cheque_Status)  = '9' then 'eft' else 'check' end as preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.claim_payment_run 
where cheque_status <> '3'
group by Payee_Code
) as A
on cpr1.Payee_Code = A.Payee_Code
inner join 
(
select max(payment_no) as PaymentNo, Payee_type
from DM_STG_EMICS.DBO.CLAIM_PAYMENT_RUN
GROUP BY CLAIM_NUMBER,Payee_type   
)  Payee_Payment_Number 
on cpr1.Payment_no = Payee_Payment_Number.PaymentNo
where cpr1.Cheque_Status <> '3' 
and cpr1.Payee_type = 1 -- Inj Worker
) cpr
on cpr.Claim_Number=cd.Claim_Number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.EFT where Object_Type='E') eft1
on eft1.Object=cd.Claim_Number
LEFT OUTER JOIN DM_STG_EMICS.dbo.TFN 
on CD.Claim_Number=TFN.Claim_no
--10-Jan-2019: Adding CRM tables below as part of DMIG-2777 fix
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_CLAIM] ccl --Remember to switch _LD back to without _LD
--ON LTRIM(RTRIM(ISNULL(cd.[Claim_Number],''))) = LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))
--LEFT OUTER JOIN [DM_STG_CRM_LD].dbo.[DM_STG_CRM_POLICY] p --Remember to switch _LD back to without _LD
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON gwpc_con_lkp.EmployerLinkID = crm_a.[ID]  
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
---select * from [DM_STG_CM].[dbo].[ab_abcontact]
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--Ash 3/5/2019
--Added extra RecordType
--INNER JOIN [DM_STG_CRM_LD].[preload].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --Remember to switch _LD back to without _LD
INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--select count(1) from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C 
--where ID=255715
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= pc_a.[CRMUniqueID_icare] --'0032O000005Bm5AQAS'
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= gwpc_con_lkp.EmployerLinkID
--16/08/2019 -- As part of Technical Debt fix for Cohort 2 (CommunicationPreferences_icare DMIG-7585), ensuring QA looks up gwpc_contact_lookup and not use CoverRefStaging anymore.
--select PrimaryAddressID,* from [DM_STG_CM].[dbo].[ab_abcontact] CM_C where LinkID='0032O000005Bm5AQAS'
--select AddressType,* from DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A where PublicID='EMICS:10112:VENDOR:BUSINESSADDRESS'
--select AddressType,* from [DM_STG_CM].[dbo].[ab_address] where ID='2882'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN DM_STG_CM.[dbo].[abtl_accountorgtype_icare] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.organizationtype_icare as the ref table prior to this that was being used i.e. abtl_organizationtype is incorrect.
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_taxstatus] abtl_tax on abtl_tax.ID = CM_C.TaxStatus
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_trusteetype_icare] abtl_trustee on abtl_trustee.ID = CM_C.TrusteeType_icare
--07/08/2019 QA differences because of this ref table missing. Have now added this ref table to map ccst_contact.taxstatus as the code was earlier simply mapping this directly from CM_C which is just the ID.
-- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
and tmp1.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp2.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp3.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701') --02/09/2019 Ensuring we only pick those which end in '%701'
--and PC_P.MostRecentModel=1 --the most recent version (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_P.status=9 --policy is bound (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_PCR.subtype=19  --19 making sure it is primarynamedinsured role contact only
--and cons_pp.LUWID is not null --15/11/2018 <Satish R>: Making sure its verified PC only
--and eft.Object is  not null  --making sure we pick the ones where we have eft data
--and CD.date_of_injury between PC_P.PeriodStart and PC_P.PeriodEnd (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_ADD.AddressLine1 is null
--and PC_PCR.ExpirationDate is null --15/11/2018 <Satish R>: Added this to eliminate duplicate Address records.
--and cons_pp.POLICY_FLAG=0 --verified PC only
--and ltrim(rtrim(CD.Claim_Number))=1025345
--61528
)X


SELECT * INTO temptable_EMPLOYER_VerifiedCDR
FROM
(
select
--'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':BUSINESSADDRESS' as PublicID,
concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED') as PublicID,
Null as ABNIsActive_icare,
Null as ABNValidated_icare,
--PTD.ABN as ABN_icare, --10/06/2019 Need to convert this into ## ### ### ### format as shown below
concat(substring(CONVERT(varchar(100), CAST(PTD.ABN AS decimal(38))),1,2),' ',substring(CONVERT(varchar(100), CAST(PTD.ABN AS decimal(38))),3,3),' ',substring(CONVERT(varchar(100), CAST(PTD.ABN AS decimal(38))),6,3),' ',substring(CONVERT(varchar(100), CAST(PTD.ABN AS decimal(38))),9,3)) as ABN_icare,
Null as ABSLanguageCode_icare,
Null as ABSLanguageDescription_icare,
Null as AddressBookUID,
Null as AdjudicativeDomain,
Null as AdjudicatorLicense,
0 as AggregateRemittance_icare,
Null as AttorneyLicense,
Null as AttorneySpecialty,
Null as AttorneySpecialty_icare,
NULL as AutoRepairLicense,
'allow' as AutoSync,
NULL as AutoTowingLicense,
Null as BlockedVendor_icare,
Null as CellPhone,
Null as CellPhoneCountry,
Null as CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
Null as CommunicationPreferences_icare,
Null as DateOfBirth,
Null as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as EducationLevel,
NULL as EligibleOffset_icare,
Null as EmailAddress1,
Null as EmailAddress2,
Null as EmployeeNumber,
NULL as EmployeeSecurityID,
Null as FaxPhone,
Null as FaxPhoneCountry,
Null as FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
Null as FirstName, --Individuals will have this populated and companies will have this blank
Null as FirstNameKanji,
Null as FormerName,
Null as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
Null as HomePhone,
Null as HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
Null as HomePhoneExtension,
Null as InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
Null as LastName,
Null as LastNameKanji,
Null as LawFirmSpecialty,
Null as LegalSpecialty_icare,
Null as LicenseNumber,
Null as LicenseState,
Null as MaritalStatus,
Null as MedicalLicense,
Null as MedicalOrgSpecialty,
Null as MedicalSpecialty_icare,
Null as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
Null as MedicareNumber_icare,
Null as MedicareValidUntil_icare,
Null as MiddleName,
--Null as Name,  --Companies will have this populated but individuals will have this blank
CASE WHEN PTD.LEGAL_NAME  is not null then PTD.LEGAL_NAME 
ELSE concat('Company', ltrim(rtrim(CD.Claim_Number)))
END 
AS Name,
Null as NameKanji,
Null as Notes,
Null as NumDependents,
Null as NumDependentsU18,
Null as NumDependentsU25,
'FALSE' as ObfuscatedInternal,
Null as Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
Null as OktaID_icare,
NULL as OrganizationType,
Null as Particle,
NULL as PassportNumber,
Null as PayInfoUpdUserName_icare,
'FALSE' as PendingLinkMessage,
Null as  PolicySystemId,
Null as Preferred,
Null as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
Null as Prefix,
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PrimaryAddressID,
Null as PrimaryLanguage,
NULL as PrimaryLocale,
Null as PrimaryPhone,
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
Null as RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
Null as Score,
0 as SingleRemittance,
'GWCC' as SourceSystem_icare,
Null as SpecialtyType,
'Company' as Subtype,
Null as Suffix,
Null as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
Null as TaxStatus,
Null as TempCRMUniqueID_icare,
Null as TradingName_icare,
Null as TrustName_icare,
Null as TrusteeName_icare,
'loadsave' as ValidationLevel,
Null as VendorNumber,
Null as VendorType,
Null as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
Null as WithholdingRate,
Null as WorkPhone,
Null as WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
Null as WorkPhoneExtension,
--PTD.ACN_ARBN as ACN_icare, --10/06/2019 Need to convert this into ## ### ### ### format as shown below
concat(substring(REPLACE(STR(PTD.ACN_ARBN,9),' ','0'),1,3),' ',substring(REPLACE(STR(PTD.ACN_ARBN,9),' ','0'),4,3),' ',substring(REPLACE(STR(PTD.ACN_ARBN,9),' ','0'),7,3)) as ACN_icare,
Null as CommencementDate_icare,
Null as OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
Null as TrustABN_icare,
NULL as TrustABNIsActive_icare,
Null as TrustABNValidated_icare,
Null as TrusteeType_icare,
Null as TrustRegisteredForGST_icare,
NULL as DoctorSpecialty_icare,
Null as CRMVersion_icare,
ltrim(rtrim(CD.Claim_Number)) as LUWID,
1 as ApprovedPaymentMethod
--0 as AggregaeRemittance
--0 as Retired,
--0 as BatchGeoCode,

--pc_st.TYPECODE as StateTYPECODE,
--pc_ct.TYPECODE as CountryTYPECODE,
--CASE WHEN ltrim(rtrim(PTD.ADDRESS_LOCALITY)) <> ''  THEN   ltrim(rtrim(PTD.ADDRESS_LOCALITY))
--                                                    ELSE 'Sydney'
----default if source is NULL  
--                    END as City,   
--CASE WHEN ltrim(rtrim(PTD.ADDRESS_POST_CODE)) <> '' THEN   ltrim(rtrim(PTD.ADDRESS_POST_CODE))
--                                                    ELSE 2000
--    --default if source is NULL  
--                    END as PostalCode,
--'Address' as SubType,
--CASE WHEN ltrim(rtrim(PTD.ADDRESS_STREET)) <> ''  THEN   ltrim(rtrim(PTD.ADDRESS_STREET))
--                                                    ELSE 'VERIFIED CDR DUMMY LINE1'
----default if source is NULL  
--                    END as AddressLine1
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
left outer join DM_STG_EMICS.dbo.POLICY_TERM_DETAIL PTD on PTD.POLICY_NO=CD.Policy_No
left outer join temptable_EMPLOYER_VerifiedPC_1 tmp1 on tmp1.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_EMPLOYER_VerifiedPC_2 tmp2 on tmp2.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_EMPLOYER_VerifiedPC_3 tmp3 on tmp3.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_EMPLOYER_VerifiedPC_4 tmp4 on tmp4.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--left outer join dm_stg_pc.dbo.pctl_state pc_st 
--on pc_st.TYPECODE=CASE PTD.ADDRESS_STATE WHEN 'ACT' THEN 'AU_ACT'
--                                         WHEN 'QLD' THEN 'AU_QLD'
--WHEN 'SA'  THEN 'AU_SA'
--WHEN 'VIC' THEN 'AU_VIC'
--WHEN 'WA'  THEN 'AU_WA'
--WHEN 'NSW' THEN 'AU_NSW'
--ELSE 'AU_NSW'
----default if source is NULL  
--END
--left outer join dm_stg_pc.dbo.pctl_country pc_ct on pc_ct.DESCRIPTION='Australia'
where CD.is_Null=0
and tmp1.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp2.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp3.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
and tmp4.LUWID is null --02/09/2019 Ensuring there is no exist match in the previous search result temp table
--and ltrim(rtrim(CD.Claim_Number)) NOT IN -- to get only verified CDR/Invalid claims
--  (select LUWID from [dbo].[CoverREF_temptable_consolidated] cons_pp) 
)Y
;

--06/09/2019 (DMIG-7891) Ensuring that all the 4 temp tables for EMPLOYER_VerifiedPC are collated into ONE single temp table
select * into temptable_EMPLOYER_VerifiedPC
from
(
select * from temptable_EMPLOYER_VerifiedPC_1
union
select * from temptable_EMPLOYER_VerifiedPC_2
union
select * from temptable_EMPLOYER_VerifiedPC_3
union
select * from temptable_EMPLOYER_VerifiedPC_4
)X


select
*
into dbo.ContactREF_temptable_employer_verifiedPC
from
(
select * 
from temptable_EMPLOYER_VerifiedPC
)x
;

select
*
into dbo.ContactREF_temptable_employer_verifiedCDR
from
(
select * 
from temptable_EMPLOYER_VerifiedCDR
)x
;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC') is not null
drop table temptable_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedCDR') is not null
drop table temptable_EMPLOYER_VerifiedCDR;

IF OBJECT_ID('ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_1') is not null
drop table temptable_EMPLOYER_VerifiedPC_1;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_2') is not null
drop table temptable_EMPLOYER_VerifiedPC_2;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_3') is not null
drop table temptable_EMPLOYER_VerifiedPC_3;

IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC_4') is not null
drop table temptable_EMPLOYER_VerifiedPC_4;

END