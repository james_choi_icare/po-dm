﻿CREATE PROC [dbo].[SP_02_PROCESS_RECON_XFM_COMPARE_TABLE] 
	@runID int, @targetSystem varchar(10), @targetTable varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (select * from sys.indexes where name = 'CIX_LUWID' )
		DROP INDEX [CIX_LUWID] ON [dbo].[ADM_RECON_RESULT]

	IF EXISTS (select * from sys.indexes where name = 'CIX_Tgt_Table' )
		DROP INDEX [CIX_Tgt_Table] ON [dbo].[ADM_RECON_RESULT]

	IF EXISTS (select * from sys.indexes where name = 'CIX_XFM_Key' )
		DROP INDEX [CIX_XFM_Key] ON [dbo].[ADM_RECON_RESULT]

	IF EXISTS (select * from sys.indexes where name = 'CIX_Expected_Key' )
		DROP INDEX [CIX_Expected_Key] ON [dbo].[ADM_RECON_RESULT]

	IF EXISTS (select * from sys.indexes where name = 'CIX_RunID' )
		DROP INDEX [CIX_RunID] ON [dbo].[ADM_RECON_RESULT]


	DECLARE @columns varchar(max) = '', @xfm_table varchar(100), @xfm_recon_table varchar(100), @query nvarchar(max), @vi int, @vi2 int,  @logFileName varchar(100);
	
	DECLARE @startTime datetime, @endTime datetime;

	DECLARE tables_cursor CURSOR FOR
	SELECT replace(xfm_table,'DM_XFM_POST_','DM_POST_'), xfm_recon_table
		FROM [dbo].[ADM_XFM_TABLE_MAPPING] a
			INNER JOIN sys.tables b ON a.xfm_recon_table = b.name
		WHERE Process = 1 AND a.Target_System = @targetSystem AND a.xfm_table = @targetTable
		ORDER BY xfm_table;

	OPEN tables_cursor

	FETCH NEXT FROM tables_cursor
	INTO @xfm_table,@xfm_recon_table

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @query = 'SELECT @vi = count(*)
						FROM (SELECT PublicID FROM ' + @xfm_recon_table + ' GROUP BY PublicID HAVING COUNT(*) > 1) a'
		EXEC sp_executesql @query, N'@vi int OUTPUT', @vi OUTPUT

		SET @query = 'SELECT @vi2 = count(*)
						FROM (SELECT PublicID FROM [DM_XFM].dbo.' + @xfm_table + ' GROUP BY PublicID HAVING COUNT(*) > 1) a'
		EXEC sp_executesql @query, N'@vi2 int OUTPUT', @vi2 OUTPUT

		IF (@vi = 0 AND @vi2 = 0) OR @xfm_table = 'DM_XFM_TRANSACTIONSET'
		BEGIN

			SELECT @startTime = CURRENT_TIMESTAMP
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			-- CREATE INDEXES FROM XFM and REC_XFM
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------			
			SET @query = 'IF EXISTS (select * from [DM_XFM].sys.indexes where name = ''ClusteredIndex-rec-' + @xfm_table + ''' )
							DROP INDEX [ClusteredIndex-rec-' + @xfm_table + '] ON [DM_XFM].[dbo].[' + @xfm_table + '] WITH ( ONLINE = OFF )
		
						CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-rec-' + @xfm_table + '] ON [DM_XFM].dbo.' + @xfm_table + '
							(
								[PublicID] ASC
							)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)'
			
			IF @xfm_table = 'DM_XFM_TRANSACTIONSET'
			BEGIN
				SET @query = replace(@query, ' UNIQUE ',' ')
			END

			exec sp_executesql @query

			SET @query = 'IF EXISTS (select * from sys.indexes where name = ''ClusteredIndex-rec-' + @xfm_recon_table + ''' )
							DROP INDEX [ClusteredIndex-rec-' + @xfm_recon_table + '] ON [dbo].[' + @xfm_recon_table + '] WITH ( ONLINE = OFF )

						CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-rec-' + @xfm_recon_table + '] ON dbo.' + @xfm_recon_table + '
							(
								[PublicID] ASC
							)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)'
			
			IF @xfm_table = 'DM_XFM_TRANSACTIONSET'
			BEGIN
				SET @query = replace(@query, ' UNIQUE ',' ')
			END

			exec sp_executesql @query	
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			EXEC [dbo].[SP_02a_PROCESS_RECON_XFM_COMPARE_COLUMNS] @runID, @xfm_table, @xfm_recon_table
			EXEC [dbo].[SP_02b_PROCESS_RECON_XFM_MISSING_ROWS] @runID, @xfm_table, @xfm_recon_table
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			-- DROP INDEXES FROM XFM and REC_XFM
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			SET @query = 'IF EXISTS (select * from [DM_XFM].sys.indexes where name = ''ClusteredIndex-rec-' + @xfm_table + ''' )
							DROP INDEX [ClusteredIndex-rec-' + @xfm_table + '] ON [DM_XFM].[dbo].[' + @xfm_table + '] WITH ( ONLINE = OFF )';
			exec sp_executesql @query
			SET @query = 'IF EXISTS (select * from sys.indexes where name = ''ClusteredIndex-rec-' + @xfm_recon_table + ''' )
							DROP INDEX [ClusteredIndex-rec-' + @xfm_recon_table + '] ON dbo.[' + @xfm_recon_table + '] WITH ( ONLINE = OFF )';
			exec sp_executesql @query
			------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			SELECT @endTime = CURRENT_TIMESTAMP

			INSERT INTO [dbo].[ADM_RECON_RUN_AUDIT_DETAIL]
			SELECT @runID, 'Completed comparing ' + @xfm_table + ' and RECXFM.' + @xfm_recon_table, @startTime, @endTime
					
			SET @query = 'select top 1 @logFileName = name from sys.database_files where name like ''%log%'' order by name';
			exec sp_executesql @query, N'@logFileName varchar(100) OUTPUT', @logFileName = @logFileName OUTPUT

			SET @query = 'dbcc shrinkfile(' + @logFileName + ',1)'
			exec sp_executesql @query	
		END
		ELSE
		BEGIN
			INSERT INTO [dbo].[ADM_RECON_RUN_ERROR] SELECT @runID, 'Duplicates found : ' + CASE WHEN @vi > 0 THEN @xfm_recon_table + ' ' WHEN @vi2 > 0 THEN @xfm_table END
		END		

		

		FETCH NEXT FROM tables_cursor
		INTO @xfm_table,@xfm_recon_table
	END
	CLOSE tables_cursor
	DEALLOCATE tables_cursor

	CREATE NONCLUSTERED INDEX [CIX_LUWID] ON [dbo].[ADM_RECON_RESULT]
	(
		[LUWID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


	CREATE NONCLUSTERED INDEX [CIX_Tgt_Table] ON [dbo].[ADM_RECON_RESULT]
	(
		[Tgt_Table] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


	CREATE NONCLUSTERED INDEX [CIX_XFM_Key] ON [dbo].[ADM_RECON_RESULT]
	(
		[XFM_Key] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


	CREATE NONCLUSTERED INDEX [CIX_Expected_Key] ON [dbo].[ADM_RECON_RESULT]
	(
		[Expected_Key] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [CIX_RunID] ON [dbo].[ADM_RECON_RESULT]
	(
		[RunID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END