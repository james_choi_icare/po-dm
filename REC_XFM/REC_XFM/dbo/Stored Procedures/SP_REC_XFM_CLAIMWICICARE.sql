﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMWICICARE]
AS
BEGIN
-- =============================================
-- Author: Ann
-- Modified date:
-- Domain: COVER ccst_claimwicicare
-- Comments: v12.32
--			13/05 checked v12.36 no change
-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimwicicare','Cover - v12.36 13/05/2019'

	--POLICY RECORDS
	select
	cast(concat('EMICS:',LTRIM(rTRIM(CD.claim_number)),':POLICY',':CLAIMWICICARE') as varchar(150)) PublicID,
	cast(concat('EMICS:',LTRIM(RTRIM(cd.claim_number)),':POLICY',':WICICARE') as varchar(150)) ForeignEntityId,
	cast(concat('EMICS:',CD.CLAIM_NUMBER) as varchar(150)) OwnerID,
	RTRIM(CD.claim_number) as LUWID

	into ccst_claimwicicare
	from CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
	inner join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_WIC_matched.Claim_Number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	left outer join DM_STG_CC.dbo.ccx_wiccode_icare  ccx_wic on 
		CASE
		WHEN len(CD.Tariff_No)=5 THEN concat('0',convert(varchar,CD.Tariff_No))
		ELSE CD.Tariff_No
		END =ccx_wic.WICCode

	--Drop table ccst_claimwicicare
	--Select * from ccst_claimwicicare
	--Verified CDR/Invalid/Unmatched WIC ccst_claimwicicare 
	insert into ccst_claimwicicare
	select
	concat('EMICS:', LTRIM(RTRIM(CD.Claim_Number)) ,':POLICY',':CLAIMWICICARE') as PublicID,
	concat('EMICS:', ltrim(rtrim(CD.Claim_Number)) , ':POLICY' , ':WICICARE') as ForeignentityID,
	'EMICS:'+ ltrim(rtrim(CD.Claim_Number)) as OwnerID,
	ltrim(rtrim(CD.Claim_Number)) as LUWID

	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	where ltrim(rtrim(CD.Claim_Number)) in
	(
		select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_unmatched_WIC
		union 
		select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_verified_CDR
	)
END