﻿/* =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description:	Claim Management Domain - ccst_servicerequestctm_icare
-- Version: 12/06/2019 v24 initial build
-- =============================================*/
CREATE PROCEDURE [dbo].[SP_REC_XFM_SERVICEREQUESTCTM]
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_servicerequestctm_icare','Claim Management - v24 12/06/2019'

Select distinct
	r.Service_Provision_Type as Category,
	'EMICS:' + r.claim_no as ClaimID,
	convert(date, r.date_referred, 103) as DateFrom,
	convert(date, r.service_end_date, 103) as DateTo,
	r.Host_Employer_ABN as HostABN,
	r.Claim_no as LUWID,
	convert(date, r.cancelled_date, 103) as NullDate,
	'EMICS:' + rtrim(r.claim_no) + '_' + convert(varchar, r.ID) + ':SERVICEREQUEST' as PublicID,
	r.Service_sub_type as ServiceType,
	sira.PublicID as SiraAccredRehabProvider

into ccst_servicerequestctm_icare

From DM_STG_EMICS.dbo.REHABILITATION_DETAIL r --12507
Inner join DM_STG_EMICS.dbo.claim_detail cd on r.claim_no = cd.claim_number and cd.is_null = 0
Left outer join DM_STG_EMICS.dbo.CREDITORS c on r.Provider_code = c.creditor_no and c.Is_Acredited_provider = 1
Left outer join DM_STG_EMICS.dbo.providers p on p.wc_reg_no = c.WC_Provider_Code and p.Type = 'R'
Left outer join (Select PublicID, ProviderNumber from DM_STG_CC.dbo.CCX_siraaccredservprov_icare Where ProviderType = '10034') sira on p.WC_Reg_no = sira.ProviderNumber  
--Inner Join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.claim_number
Where r.cancelled_date is null 
and r.Sequence_no is not null
END