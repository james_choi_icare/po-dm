﻿CREATE PROC [dbo].[SP_03_PROCESS_RECON_XFM_SUMMARY_TABLE] 
		@runID int, @targetSystem varchar(10), @summaryTable varchar(100), @resultTable varchar(100), @targetTable varchar(100), @cohort int = null
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @columns varchar(max) = '', @xfm_table varchar(100), @xfm_recon_table varchar(100), @error_count varchar(100),  @expected_key_count varchar(100), @query nvarchar(max), 
			@dmXfmRowCount int, @recXfmRowCount int, @missingRowCount int, @additionalRowCount int, @passRate int, @XFM_Filtering varchar(1000);
	DECLARE @startTime datetime = CURRENT_TIMESTAMP;

	SET @query = 'DECLARE tables_cursor CURSOR FOR
					SELECT replace(a.xfm_table,''DM_XFM_POST_'',''DM_POST_''), a.xfm_recon_table, count(distinct XFM_Key) Total_Error_Count, count(distinct Expected_Key) Expected_Key_Count, max(a.XFM_Filtering) XFM_Filtering
					FROM  [dbo].[ADM_XFM_TABLE_MAPPING] a
						LEFT JOIN ' + @resultTable + ' b ON replace(a.xfm_table,''DM_XFM_POST_'',''DM_POST_'') = b.Tgt_Table AND b.RunID = ' + cast(@runID as varchar) + '
					WHERE a.Process = 1 AND a.Target_System = ''' + @targetSystem + ''' AND replace(a.xfm_table,''DM_XFM_POST_'',''DM_POST_'') = ''' + @targetTable + '''
					GROUP BY a.xfm_table, a.xfm_recon_table';

	exec sp_executesql @query

	OPEN tables_cursor

	FETCH NEXT FROM tables_cursor
	INTO @xfm_table,@xfm_recon_table,@error_count,@expected_key_count, @XFM_Filtering

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY

			SET @query = 'DELETE FROM ' + @summaryTable + ' WHERE XFM_TABLE = ''' + @xfm_table + '''';
			exec sp_executesql @query

			-- COUNT DM_XFM rows
			SET @query = 'SELECT @dmXfmRowCount = count(*) FROM [DM_XFM].dbo.' + @xfm_table;
			IF @cohort IS NOT NULL
			BEGIN
				SET @query = @query + ' a INNER JOIN [dbo].[ADM_Claim_Cohort] b ON a.LUWID = b.ClaimNumber WHERE b.Cohort = ' + cast(@cohort as varchar)
				IF @XFM_Filtering IS NOT NULL
				BEGIN
					SET @query = @query + ' AND ('+ replace(@XFM_Filtering,'XFM_Key','PublicID') + ')'
				END
				IF @targetSystem = 'CM_UPDATE'
				BEGIN
					SET @query = replace(@query,'a.LUWID = ','replace(replace(a.LUWID,''EMICS:'',''''),'':INSURED_MC'','''') = ')
				END
				
			END 
			ELSE IF @XFM_Filtering IS NOT NULL
			BEGIN
				SET @query = @query + ' WHERE ('+ replace(@XFM_Filtering,'XFM_Key','PublicID') + ')'
			END
			
			EXEC sp_executesql @query, N'@dmXfmRowCount int OUTPUT', @dmXfmRowCount OUTPUT
			

			-- COUNT REC_XFM row
			SET @query = 'SELECT @recXfmRowCount = count(*) FROM ' + @xfm_recon_table;
			IF @cohort IS NOT NULL
			BEGIN
				SET @query = @query + ' a INNER JOIN [dbo].[ADM_Claim_Cohort] b ON a.LUWID = b.ClaimNumber WHERE b.Cohort = ' + cast(@cohort as varchar)
			END 
			IF @targetSystem = 'CM_UPDATE'
				BEGIN
					SET @query = replace(@query,'a.LUWID = ','replace(replace(a.LUWID,''EMICS:'',''''),'':INSURED_MC'','''') = ')
				END
			EXEC sp_executesql @query, N'@recXfmRowCount int OUTPUT', @recXfmRowCount OUTPUT

			-- COUNT DM_XFM missing records
			SET @query = 'SELECT @missingRowCount = COUNT(*)
								FROM [' + @resultTable + ']
								WHERE Tgt_Column = ''PublicID'' AND Tgt_Table = ''' + @xfm_table + ''' AND XFM_Value is null';			
			EXEC sp_executesql @query, N'@missingRowCount int OUTPUT', @missingRowCount OUTPUT

			-- COUNT DM_XFM additional records
			SET @query = 'SELECT @additionalRowCount = COUNT(*)
								FROM [' + @resultTable + ']
								WHERE Tgt_Column = ''PublicID'' AND Tgt_Table = ''' + @xfm_table + ''' AND Expected_Value is null';
			IF @XFM_Filtering IS NOT NULL
			BEGIN
				SET @query = @query + ' AND ('+ @XFM_Filtering + ')'
			END
			EXEC sp_executesql @query, N'@additionalRowCount int OUTPUT', @additionalRowCount OUTPUT

			print @xfm_table + '  @dmXfmRowCount:' + cast(@dmXfmRowCount as varchar) + '  @recXfmRowCount:' + cast(@recXfmRowCount as varchar)
					 + '  @additionalRowCount:' + cast(@additionalRowCount as varchar) + '  @missingRowCount:' + cast(@missingRowCount as varchar)

			IF @error_count = 0 AND @recXfmRowCount = 0
			BEGIN
				SET @passRate = 0;
			END
			ELSE IF @error_count = 0 AND @dmXfmRowCount = @recXfmRowCount
			BEGIN
				SET @passRate = 100;
			END
			ELSE IF @error_count > 0 AND CASE WHEN @cohort IS NOT NULL THEN @recXfmRowCount ELSE @dmXfmRowCount END > @error_count AND (@additionalRowCount = 0 OR @missingRowCount = 0)
			BEGIN
				SET @passRate = (CASE WHEN @cohort IS NOT NULL THEN @recXfmRowCount ELSE @dmXfmRowCount END -  @error_count) * 100 / CASE WHEN @cohort IS NOT NULL THEN @recXfmRowCount ELSE @dmXfmRowCount END;
			END
			ELSE IF @additionalRowCount > 0 OR @missingRowCount > 0 
			BEGIN
				SET @passRate = (CASE WHEN @additionalRowCount + @missingRowCount > @recXfmRowCount THEN 0 ELSE @recXfmRowCount - @additionalRowCount - @missingRowCount END * 100) / @recXfmRowCount;
			END
			ELSE 
			BEGIN
				SET @passRate = 0;
			END

			print @xfm_table + ' Error Count:' + cast(@error_count as varchar) + '  @dmXfmRowCount:' + cast(@dmXfmRowCount as varchar) + '  @recXfmRowCount:' + cast(@recXfmRowCount as varchar)
					 + '  @additionalRowCount:' + cast(@additionalRowCount as varchar) + '  @missingRowCount:' + cast(@missingRowCount as varchar)

			SET @query = 'INSERT INTO ' + @summaryTable + '
							SELECT ''' + @xfm_table + ''' XFM_TABLE,							
							' + cast(@passRate as varchar) + ' PASS_RATE,
							' + cast(@missingRowCount as varchar) + ' MISSING_RECORDS_IN_DM_XFM,
							' + cast(@additionalRowCount as varchar) + ' ADDITIONAL_RECORDS_IN_DM_XFM,
							' + cast(@runID as varchar) + ' RunID,
							(SELECT MAPPING_VERSION
								FROM [dbo].[ADM_XFM_TABLE_MAPPING] a
								WHERE xfm_table = ''' + @xfm_table + ''') MAPPING_VERSION';
			exec sp_executesql @query

			
			SET @query = 'UPDATE ' + @summaryTable + ' SET RUN_ID = ' + cast(@runID as varchar) + ' WHERE XFM_TABLE = ''' + @xfm_table + ''';'
			exec sp_executesql @query
		END TRY
		BEGIN CATCH
			IF ERROR_NUMBER() is not null
			BEGIN
				INSERT INTO [dbo].[ADM_RECON_RUN_ERROR]
				SELECT @runID, @query + ' ' + ERROR_MESSAGE()
			END
		END CATCH
		--print @query
		
		FETCH NEXT FROM tables_cursor
		INTO @xfm_table,@xfm_recon_table,@error_count,@expected_key_count, @XFM_Filtering
	END
	CLOSE tables_cursor
	DEALLOCATE tables_cursor

	INSERT INTO [dbo].[ADM_RECON_RUN_AUDIT_DETAIL]
	SELECT @runID, 'Reconciliation Summary Generated for RunID in ' + @summaryTable + ':' + cast(@runID as varchar(50)), @startTime, CURRENT_TIMESTAMP
END