﻿-- =============================================
-- Author:		Ann
-- Create date: 14/05
-- Description:	Work capacity domain ccst_WorkStatus
-- Version: 14/05 v7.3 initial build
--			16/05 v8.0 updated WageLoss_icare transformation rules
--			20/06 v9.2 added fields
--			09/07 v12.1 updated publicID xform
--			18/07 v14 updated wageloss_icare xform
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_WORKSTATUS]
AS
BEGIN
	--drop table ccst_WorkStatus
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_workstatus','Work Capacity - v14 18/07/2019' 

	IF OBJECT_ID('tempdb..#CAD') is not null
		drop table #CAD

	IF OBJECT_ID('tempdb..#New_DayHour_Worked') is not null
		drop table #New_DayHour_Worked

	Select CLaim_no, work_status_code, work_status_date, lead(work_status_date) over (Partition by claim_no order by claim_no, work_status_Date) as Next_Work_Status_Date
	into #CAD
	From (
		Select DISTINCT claim_no, work_status_code, convert(Date,Work_Status_date,112) as work_status_date, rank() over (partition by claim_no, convert(Date, work_status_date,112) order by 
			convert(date,work_status_date,112),transaction_date desc,id desc) as rnk
			FROM DM_STG_EMICS.dbo.CAD_AUDIT
			Where work_status_code is not null and work_status_date is not null
		) a where rnk = 1

	Select cd.claim_number, cd.work_days, (len(cd.work_days) - len(replace(cd.work_days,'1',''))) as current_workdays, cd.work_hours as Current_hoursworkedperweek, 
		cd.work_hours/replace((len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) as current_hoursworkedperday,
		case when cd.work_hours between 35 and 40 and (len(cd.work_days) - len(replace(cd.work_days,'1',''))) < 5 then 5 
			When cd.work_hours / replace((Len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) > 12 then	
				Case when ceiling(cd.work_hours/12)> 7 then 7 else ceiling(cd.work_hours/12) end
			Else replace((len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1) 
			End as New_DaysPerWeek_worked,
		Case when cd.work_hours between 35 and 40 and (len(cd.work_days) - len(Replace(cd.work_days,'1',''))) < 5 then cd.work_hours/5
			When cd.work_hours /replace((Len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1)> 12
				Then case when ceiling(cd.work_hours/12) > 7 then cd.work_hours/7 else cd.work_hours/ceiling(cd.work_hours/12) end
			Else cd.work_hours / replace((Len(cd.work_days) - len(replace(cd.work_days,'1',''))),0,1)
			End as New_hoursPerDay_worked 
		
		INTO #New_DayHour_Worked
	
		From DM_STG_EMICS.dbo.CLAIM_DETAIL cd Where cd.work_hours>0

	SELECT 

		RTRIM(LTRIM(cd.Injury_comment)) as Comments,
		'EMICS:'+ltrim(rtrim(cd.Claim_Number))+':EMPLOYMENT_DATA' EmploymentDataID,
		cd.Claim_Number as LUWID,

		case when New_DayHour_Worked.NEW_DaysPerWeek_Worked IS NOT NULL then New_DayHour_Worked.NEW_DaysPerWeek_Worked 
			ELSE CASE WHEN CD.Work_Hours = 0 then 0 else null end end as NumDaysWorked,--New_WORK_DAYS,


		case when New_DayHour_Worked.NEW_HoursPerDay_Worked IS NOT NULL 
			then convert(decimal (5,2) ,New_DayHour_Worked.NEW_HoursPerDay_Worked) ELSE CASE WHEN convert(decimal (5,2) , CD.Work_Hours) = 0 then 0 else null end end as NumHoursWorked,--New_HoursPerDay,
		Null as PaidFullForLastWorked,
		'EMICS:'+rtrim(ltrim(cd.Claim_number))+'_' + convert(Varchar, rank() over (Partition by cad.claim_no order by cad.work_status_Date, cad.next_Work_status_date)) + ':WORKSTATUS' PublicID,
		Case when cast(cad.Work_Status_code as varchar) = '0' then '01'
			when cast(cad.Work_Status_code as varchar) = '1' then '01'
			when cast(cad.Work_Status_code as varchar) = '10' then '10'
			when cast(cad.Work_Status_code as varchar) = '11' then '09'
			when cast(cad.Work_Status_code as varchar) = '12' then '09'
			when cast(cad.Work_Status_code as varchar) = '13' then '13'
			when cast(cad.Work_Status_code as varchar) = '14' then '09'
			when cast(cad.Work_Status_code as varchar) = '15' then '13'
			when cast(cad.Work_Status_code as varchar) = '2' then '02'
			when cast(cad.Work_Status_code as varchar) = '2b' then '02'
			when cast(cad.Work_Status_code as varchar) = '3' then '03'
			when cast(cad.Work_Status_code as varchar) = '4 'then '04'
			when cast(cad.Work_Status_code as varchar) = '4b' then '04'
			when cast(cad.Work_Status_code as varchar) = '5' then '08'
			when cast(cad.Work_Status_code as varchar) = '6' then '06'
			when cast(cad.Work_Status_code as varchar) = '7' then '09'
			when cast(cad.Work_Status_code as varchar) = '8' then '08'
			when cast(cad.Work_Status_code as varchar) = '9' then '09'
			End as Status,
		cast(cad.Work_status_date as datetime2) as StatusDate,
		Case when cad.work_status_date = cad.Next_Work_Status_Date then convert(datetime2, cad.work_status_date) else convert(datetime2, DATEADD(dd,-1,cad.Next_Work_Status_Date)) End as StatusEndDate,
		cd.PIAWE as WageAmountPostInjury,
		CASE WHEN cad.Work_Status_code IN ('2','4','6','8') THEN '1' ELSE '0'
			END AS WageLoss_icare 

	Into ccst_workstatus

	From #CAD CAD
	Left outer join #New_DayHour_Worked New_DayHour_Worked on cad.claim_no = new_dayHour_worked.claim_number
	inner join DM_STG_EMICS.dbo.claim_Detail cd on cd.Claim_Number = cad.Claim_no
		--inner join [DM_XFM_LD].dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

	Where cd.is_Null = 0

	drop table #CAD
	drop table #New_DayHour_Worked
END