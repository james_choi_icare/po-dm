﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 24/06/2019
-- Description:	Contacts - Main Contact V002.17
-- 1. This is the child Stored Proc for MAINCONTACT claimcontact that will called by the master stored proc SP_REC_XFM_CLAIMCONTACT
-- 2. <09Aug2019><Satish>: Modified the SP to account for ER_VerifiedPC_unmatched and Rest_of_all_VerifiedCDR MainContacts too. Earlier
--    it was looking only at Matched Main Contacts. Renamed the temp and final Ref tables too.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMCONTACT_MAINCONTACT_STAGING]
	
AS
BEGIN

IF OBJECT_ID('ClaimContactREF_temptable_maincontact_matched_and_unmatched') is not null
drop table dbo.ClaimContactREF_temptable_maincontact_matched_and_unmatched;

IF OBJECT_ID('temptable_ClaimContact_MAINCONTACT_matched_and_unmatched') is not null
drop table temptable_ClaimContact_MAINCONTACT_matched_and_unmatched;

SELECT * INTO temptable_ClaimContact_MAINCONTACT_matched_and_unmatched
FROM
(
select 
--distinct
Null as BenefitEndDate,
Null as BenefitEndReason,
Null as BenefitEndReasonType,
'EMICS:' + QA_contact_ref_staging.LUWID as ClaimID,
0 as ClaimantFlag,
concat('EMICS:',QA_contact_ref_staging.LUWID,':INSURED_MC') as ContactID,
Null as ContactProhibited,
Null as ContactValidFrom,
Null as ContactValidTo,
Null as DependentType,
Null as Details_icare,
Null as EssentialServiceType,
QA_contact_ref_staging.LUWID as LUWID,
Null as PolicyID,
Null as ProviderType,
concat('EMICS:',QA_contact_ref_staging.LUWID,':INSURED_MC') as PublicID,
0 as Retired,
Null as Service
FROM
(
select LUWID
--,AddressBookUID
from
(
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_ER_VerifiedPC_unmatched]
union
select LUWID
--,AddressBookUID 
from [dbo].[ContactREF_temptable_MC_rest_of_all_VerifiedCDR]
)X
) QA_contact_ref_staging
)X

select
*
into dbo.ClaimContactREF_temptable_maincontact_matched_and_unmatched
from
(
select * 
from temptable_ClaimContact_MAINCONTACT_matched_and_unmatched
)x
;


IF OBJECT_ID('temptable_ClaimContact_MAINCONTACT_matched_and_unmatched') is not null
drop table temptable_ClaimContact_MAINCONTACT_matched_and_unmatched;

END