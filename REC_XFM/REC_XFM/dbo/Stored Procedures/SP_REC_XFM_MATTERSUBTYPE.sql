﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_MATTERSUBTYPE]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 16/05
-- Description:	Legal Domain - ccst_mattersubtype_icare
-- Version: 16/05 initial build v29 mapping

-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_mattersubtype_icare','Legal - v29 16/05/2019'

--drop table ccst_mattersubtype_icare
Select 
	cd.claim_number as LUWID,
	'EMICS:' + l.claim_no + '_' + convert(varchar, l.ID) + ':Litigation' as Matter_icareID,
	Case when Common_Law_Action_Date is not null then --refer code value mapping
		Case when l.Common_Law_Action_Type = 1 then '01'
			when l.Common_Law_Action_Type = 2 then '02'
			when l.Common_Law_Action_Type = 3 then '03'
			when l.Common_Law_Action_Type = 4 then '04'
			when l.Common_Law_Action_Type = 5 then '05'
			when l.Common_Law_Action_Type = 6 then 'other_icare'
			when l.Common_Law_Action_Type = 7 then '07'
			when l.Common_Law_Action_Type = 8 then '08'
			when l.Common_Law_Action_Type = 9 then '09'
			when l.Common_Law_Action_Type = 10 then '10'
			when l.Common_Law_Action_Type = 11 then '11'
		End
	Else
		Case when Type_Of_Legal_Action = 'WCP' or Type_Of_Legal_Action = 'PRP' then 'lumpsumcompensationwpi_icare'
			When Type_Of_Legal_Action = 'WCM' Then 'medical_icare'
			When Type_Of_Legal_Action != 'WCP' or
				Type_Of_Legal_Action != 'PRP' or
				Type_Of_Legal_Action != 'WCM' then 'other_icare'
		End		
	End as MatterSubType_icare,
	'EMICS:' + rtrim(ltrim(l.claim_no)) + '_' + convert(varchar, l.iD) + ':Litigation' PublicID

into ccst_mattersubtype_icare

From DM_STG_EMICS.dbo.Litigation l
Inner join DM_STG_EMICS.dbo.claim_detail cd on cd.Claim_Number = l.Claim_No
--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number

Where l.ID <> 101
and cd.is_null = 0
END