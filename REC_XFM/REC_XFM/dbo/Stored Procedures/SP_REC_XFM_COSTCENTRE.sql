﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_COSTCENTRE]
AS
BEGIN

--COVER v12.32 ccst_costcentre_icare


EXEC SP_05_ADM_MAPPING_VERSION 'ccst_costcentre_icare','Cover - v12.32 20/04/2019'

	--Verified PC and WIC Matched
	select distinct
	LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
	cast(concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PC_CC.ID,':POLICY',':COSTCENTREICARE') as varchar(150)) as PublicID, --Using the revised public id format for ccst_policylocation
	cast(concat('EMICS:',LTRIM(RTRIM(CD.Claim_Number)),'_',PL.ID,':POLICY',':POLICYLOCATION') as varchar(150)) as PolicyLocationID, --Changing the PublicID format and keeping it more simple now
	'Verified PC and WIC' as PublicIDType_SRC_VALUE
	into ccst_costcentre_icare

	from  CoverREF_tmp_EMICS_WIC_Matched_PC_Max_Record temp_WIC_matched
	inner join DM_STG_EMICS.[dbo].CLAIM_DETAIL CD on ltrim(rtrim(CD.Claim_Number))=temp_WIC_matched.Claim_Number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	left outer join dm_stg_pc.dbo.pc_policylocation PL	on PL.ID=temp_WIC_matched.PolicyLocation
	left outer join dm_stg_pc.dbo.pcx_costcenter_icare PC_CC on PC_CC.ID=temp_WIC_matched.CostCenter_icare

	---Verified CDR/Invalid
	Insert into ccst_costcentre_icare
	select
	ltrim(rtrim(CD.Claim_Number)) as LUWID,
	'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY'+':COSTCENTREICARE' as PublicID,
	'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':POLICY'+':POLICYLOCATION' as PolicyLocation_PublicID,
	'Verified CDR' as PublicIDType_SRC_VALUE

	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	where CD.is_Null=0
	and ltrim(rtrim(CD.Claim_Number)) in
	(
	select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_unmatched_WIC
	union 
	select LUWID as Claim_Number from CoverREF_tmp_ccst_policylocation_verified_CDR
	)



END