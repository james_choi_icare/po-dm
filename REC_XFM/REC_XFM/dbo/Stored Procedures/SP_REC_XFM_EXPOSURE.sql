﻿
CREATE PROC [dbo].[SP_REC_XFM_EXPOSURE] AS
BEGIN
-- ===============================
-- Author: Ann
-- Domain: ESTIMATES AND RESERVES ccst_exposure
-- Version: v25
--			v26 updated 26/04
--			v27 updated 3/05
--			14/05 updated v27.2 updated transformation rule for excessrecoveryamount for medical, indemnity and employer
--			07/06/2019 v1.2 Users and groups spec - updated assignedbyuserID, AssignedGroupID, AssignedQueueID, AssigendUserID
-- =====================================

--drop table ccst_exposure
 EXEC SP_05_ADM_MAPPING_VERSION 'ccst_exposure','Estimates and Reserves - v27.2'


;With UsersAndGroups as (
	Select Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID, cc_user_sm.PublicID)  end as AssignedbyUserID,
		--coalesce (cc_group.PublicID, 
		--		(select TOP 1 cc_group.PublicID from DM_STG_CC.[dbo].[cc_group] cc_group, DM_STG_CC.[dbo].[cc_groupuser] cc_groupuser
		--		where cc_group.ID = cc_groupuser.GroupID AND cc_groupuser.UserID = cc_user.ID)) as AssignedGroupID,
		cc_group.PublicID as AssignedGroupID,
		NULL as AssignedQueueID,
		Case when co.alias in ('ADMINQ','CCI','DISPUTEQ','IMSQUEUE','LITIGATNQ','s.centre','S1002','SCCORP1TQ',
		'SCCORP2TQ','SCCORP3TQ','SCCORP4TQ','SCCORP5TQ','SCSME1TQ','SCSME2TQ','SCSME3TQ','SCSME4TQ','SCSME5TQ',
		'SCSME6TQ','TECHSPECQ','tobenulled','WPIQ') then cc_user_queue.PublicID
		 Else coalesce (cc_user.PublicID, cc_user_sm.PublicID) end as AssignedUserID,
		cd.claim_number
	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.Claim_Number=CAD.Claim_no
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
	Left outer join DM_STG_EMICS.dbo.Claims_Officers co on cad.claims_officer = co.Alias
	 left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on cd.Claim_Number = ct.Claim_No

	 	Left outer join DM_STG_EMICS.dbo.Claims_Officers co2 on cd.Owner = co2.Alias --Claim owner group
	left join DM_XFM.dbo.[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups2	on ( lkp_eml_team_gwcc_groups2.EML_TEAM = co2.grp)
	left join DM_STG_CC.dbo.cc_group cc_group2 on ( cc_group2.Name = lkp_eml_team_gwcc_groups2.gwcc_group )


		 -- Bring in EML User Emails from EMICs Control table
		LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* FROM	[DM_STG_EMICS].[dbo].[CONTROL] cntrl
					where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  
			) cntrl  ON (co.Alias = cntrl.ITEM)
	left join DM_STG_CC.dbo.cc_credential cc_credential	on (cntrl.EML_Email = cc_credential.UserName)
	left join DM_STG_CC.dbo.cc_user cc_user	on (cc_user.CredentialID = cc_credential.ID)
		left join DM_XFM.dbo.[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm	on ct.Segment_Code = lkp_eml_sm.segment_code
		left join  DM_STG_CC.dbo.cc_credential cc_credential_sm	on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
		left join DM_STG_CC.dbo.cc_user cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
		left join DM_XFM.dbo.[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups	on ( lkp_eml_team_gwcc_groups.EML_TEAM = co.grp)
		left join DM_STG_CC.dbo.cc_group cc_group on ( cc_group.Name = lkp_eml_team_gwcc_groups.gwcc_group )
		left join DM_XFM.dbo.[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on (cntrl.EML_Email = lkp_eml_gwcc_queues.EMLEmailAddress 
					 and cad.Claims_Officer = lkp_eml_gwcc_queues.EMLQueueUser)
		left join DM_STG_CC.dbo.cc_credential cc_credential_user_queue on (lkp_eml_gwcc_queues.GWCC_Claim_User = cc_credential_user_queue.UserName)
		left join DM_STG_CC.dbo.cc_user cc_user_queue on (cc_user_queue.CredentialID = cc_credential_user_queue.ID)
		where 
		CD.is_Null=0 AND CAD.Claim_Liability_Indicator <>12
),
Medical_Exposure as (
		select distinct 
		uag.AssignedbyUserID,
		uag.AssignedGroupID,
		NULL as AssignedQueueID,
		uag.AssignedUserID,
		 NULL AS AverageWeeklywages,
		'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'  AS ClaimantDenormid, 
		'EMICS:'+LTRIM(RTRIM(CD.Claim_Number)) as ClaimID,
		CAD.Date_Claim_Closed AS CloseDate,
		'EMICS:' + LTRIM(RTRIM(cd.Claim_Number)) + 'Medical and other exp' + ':COVERAGE' AS CoverageID,
		NULL as ExistingWeeklyBenefits_icare,
		NULL as FatalityInterest_icare,  --CAD.Worker_Comp_Commission AS FatalityInterest_icare,
		'EMICS:' + LTRIM(RTRIM(cd.Claim_Number)) + ':MEDICAL_EXPOSURE' as IncidentID,
		LTRIM(RTRIM(CD.Claim_Number)) AS luwid,
		'None' AS ISOStatus, --0
		'EMICS:'  + RTRIM(cd.Claim_Number) + ':EMPLOYMENT_DATA' as NewEmpDataId,
		'WCWorkersCompMED_icare' as PrimaryCoverage,
		 'EMICS:'+ LTRIM(RTRIM(cd.Claim_Number))+':MEDONLYEXP' as PublicId,
		 NULL as ReOpenDate, --CAD.Date_Claim_reopened as ReOpenDate,
		 NULL as ReopenedReason, --CAD.Reopen_Reason as ReopenedReason,
		 CASE WHEN CAD.Date_Claim_Closed IS NOT NULL AND CAD.Date_Claim_Closed>ISNULL(Date_Claim_reopened,'1900-01-17 12:15:00')
			 THEN 'closed' 
			 else 'open' 
			 END
			 State,
		'CAD.Date_Claim_Closed = ' + ISNULL(convert(varchar, cad.date_Claim_Closed),'') + ' cad.Date_claim_reopened = ' + ISNULL(convert(varchar, Date_Claim_Reopened),'') as State_SRC_VALUE, 
		-- NULL AS StateLineId,
		CASE WHEN UR_Disb.Option_Answer='Y' THEN 1
			ELSE 0
			END WCPreexDisblty,
		UR_Disb.Answer as  WCPreexDisbltyInfo,
		'WCWorkersCompMED'as CoverageSubtype,
		CASE WHEN UR_diag.Option_Answer='Y' THEN 1
            WHEN UR_diag.Option_Answer='N' THEN 0
            ELSE NULL
			END DiagnosticCnsistnt,
		'medical' as ExposureTier,
		'WCInjuryDamage' as Exposuretype,
		'AU_NSW' as JurisdictionState,
		'wc_med_only' as Segment,
		'0'	AS ExcessRecoveryAmount

	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.Claim_Number=CAD.Claim_no
	LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.PAYMENT_RECOVERY where reversed=0) PR--19/09/2018--Changed it to LEFT OUTER Join because all claims cannot be paid, but we would still have to create an exposure for those claims
																		--25/01/2019--Changed it to exluse reversed transaction
		ON PR.Claim_No=CD.Claim_Number
	LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.USER_RESPONSE where Question_ID='28') UR_Disb ON UR_Disb.Ref_No=CD.Claim_Number
	LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.USER_RESPONSE where Question_ID='20') UR_Diag	ON UR_Diag.Ref_No=CD.Claim_Number
	LEFT OUTER JOIN (SELECT DISTINCT Claim_no FROM DM_STG_EMICS.dbo.Medical_Cert WHERE is_deleted = 0) MC ON CAD.Claim_no = MC.Claim_no--25/1/2019: added it to include medical certificates
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
	Left outer join DM_STG_EMICS.dbo.Claims_Officers co on cad.claims_officer = co.Alias
	Left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on cd.Claim_Number = ct.Claim_No
	Left outer join UsersAndGroups uag on uag.claim_number = cd.claim_number 
	where
	CD.is_Null=0 AND CAD.Claim_Liability_Indicator <>12
		AND (PR.Estimate_type  IN (51,52,53,55,56,63,64) or CD.is_Medical_Only=1 or mc.Claim_no is not null)
) 	
, Indemnity_Exposure as (
	select distinct 
		uag.AssignedbyUserID,
		uag.AssignedGroupID,
		NULL as AssignedQueueID,
		uag.AssignedUserID,
		cd.Weekly_Wage AS AverageWeeklywages,
		'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'  AS ClaimantDenormid, 
		'EMICS:'+ LTRIM(RTRIM(CD.Claim_Number)) as Claimid,
		CAD.Date_Claim_Closed AS CloseDate,
		'EMICS:' + RTRIM(CD.Claim_Number) + 'Statutory Workers Comp' + ':COVERAGE' AS CoverageID,
		NULL as ExistingWeeklyBenefits_icare,
		NULL as FatalityInterest_icare, --CAD.Worker_Comp_Commission AS FatalityInterest_icare,
		'EMICS:' + RTRIM(cd.Claim_Number) + ':WEEKLY_BENEFIT_EXPOSURE' as IncidentID,
		RTRIM(CD.Claim_Number) AS luwid,
		'None' AS ISOStatus, --0
		'EMICS:'  + RTRIM(cd.Claim_Number) + ':EMPLOYMENT_DATA' as NewEmpDataId,
		'WCWorkersCompCov' as PrimaryCoverage,
		'EMICS:'+RTRIM(CD.Claim_Number)+':INDEMNITY' as PublicId,
		 NULL as ReOpenDate,
		 NULL as ReopenReason,
		 CASE WHEN CAD.Date_Claim_Closed IS NOT NULL AND CAD.Date_Claim_Closed>ISNULL(Date_Claim_reopened,'1900-01-17 12:15:00') THEN 'closed' 
			else 'open' 
			END AS State,
		'CAD.Date_Claim_Closed = ' + ISNULL(convert(varchar, cad.date_Claim_Closed),'') + ' cad.Date_claim_reopened = ' + ISNULL(convert(varchar, Date_Claim_Reopened),'') as State_SRC_VALUE, 
		 --NULL AS StateLineId,
		 CASE WHEN UR_Disb.Option_Answer='Y' THEN 1 ELSE 0
			END AS WCPreexDisblty,
		 UR_Disb.Answer as  WCPreexDisbltyInfo,
		 'WCWorkersCompWAGES'as CoverageSubtype,
		 NULL as DiagnosticCnsistnt,
		 'indemnity' as ExposureTier,
		 'LostWages' as Exposuretype,
		 'AU_NSW' as JurisdictionState,
		 'wc_lost_time' as Segment,		
		Case when Paid_Excess_Amount is not null then Paid_Excess_Amount
			Else '0' end AS ExcessRecoveryAmount
	from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.Claim_Number=CAD.Claim_no
	LEFT OUTER JOIN(select * from DM_STG_EMICS.dbo.PAYMENT_RECOVERY where reversed=0) PR--19/09/2018--Changed it to LEFT OUTER Join because all claims cannot be paid, but we would still have to create an exposure for those claims
		ON PR.Claim_No=CD.Claim_Number
	LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.USER_RESPONSE where Question_ID='28') UR_Disb ON UR_Disb.Ref_No=CD.Claim_Number

	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
	Left outer join DM_STG_EMICS.dbo.Claims_Officers co on cad.claims_officer = co.Alias
	left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on cd.Claim_Number = ct.Claim_No
	Left outer join UsersAndGroups uag on uag.claim_number = cd.claim_number 
	where 
	CD.is_Null=0 AND CAD.Claim_Liability_Indicator <>12
		AND (PR.Estimate_type  IN (50,72,54,58,59,60,65) or CD.is_Time_Lost=1 or 
	cd.Date_Deceased is not null )
		
)
,Employer_Exposure as (
	select distinct
		uag.AssignedbyUserID,
		uag.AssignedGroupID,
		NULL as AssignedQueueID,
		uag.AssignedUserID,
		NULL as AverageWeeklyWages,--cd.Weekly_Wage AS AverageWeeklywages,
		'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'  AS ClaimantDenormid, 
		'EMICS:'+LTRIM(RTRIM(CD.Claim_Number)) as Claimid,
		CAD.Date_Claim_Closed AS CloseDate,
		'EMICS:' + RTRIM(CD.Claim_Number) + 'Workers Comp' + ':COVERAGE' AS CoverageID,
		NULL as ExistingWeeklyBenefits_icare,
		NULL as FatalityInterest_icare,--CAD.Worker_Comp_Commission AS FatalityInterest_icare,
		'EMICS:' + ltrim(RTRIM(CD.Claim_Number)) + ':EMPLOYER_LIABILITY_EXPOSURE' as IncidentID,
		RTRIM(CD.Claim_Number) AS luwid,
		'None' AS ISOStatus, --0
		'EMICS:'  + ltrim(RTRIM(Cd.Claim_Number)) + ':EMPLOYMENT_DATA' as NewEmpDataId,
		'WCEmpLiabCov' as PrimaryCoverage,
		 'EMICS:'+ltrim(RTRIM(CD.Claim_Number))+ ':EMPLIAB' as PublicId,
		 NULL as ReOpenDate,
		 NULL as ReopenReason,
		 CASE WHEN CAD.Date_Claim_Closed IS NOT NULL AND CAD.Date_Claim_Closed>ISNULL(Date_Claim_reopened,'1900-01-17 12:15:00')
			 THEN 'closed' 
			 else 'open' 
			 END AS State,
		'CAD.Date_Claim_Closed = ' + ISNULL(convert(varchar, cad.date_Claim_Closed),'') + ' cad.Date_claim_reopened = ' + ISNULL(convert(varchar, Date_Claim_Reopened),'') as State_SRC_VALUE, 
		 --NULL AS StateLineId,
		 NULL WCPreexDisblty,
		 NULL as  WCPreexDisbltyInfo,
		 'WCEmpLiabCov'as CoverageSubtype,
		 NULL as DiagnosticCnsistnt,
		 'el' as ExposureTier,
		 'EmployerLiability' as Exposuretype,
		 'AU_NSW' as Jurisdictiondate,
		 'wc_liability' as Segment,		
		 '0' AS ExcessRecoveryAmount --Paid_Excess_Amount
		from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CD.Claim_Number=CAD.Claim_no
		LEFT OUTER JOIN  (select * from DM_STG_EMICS.dbo.PAYMENT_RECOVERY where reversed=0) PR ON PR.Claim_No=CD.Claim_Number
		LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde ON cd.Claim_Number = cde.Claim_no
		Left outer join DM_STG_EMICS.dbo.Claims_Officers co on cad.claims_officer = co.Alias
		left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on cd.Claim_Number = ct.Claim_No

		Left outer join UsersAndGroups uag on uag.claim_number = cd.claim_number 

		where	CD.is_Null=0 AND CAD.Claim_Liability_Indicator <>12
		 AND PR.Estimate_type  IN (57)
) 
, target_exposure as (
	select * from Medical_Exposure
	union 
	select * from Indemnity_exposure
	union 
	select * from Employer_exposure
)
select * into  ccst_exposure from target_exposure
	
END