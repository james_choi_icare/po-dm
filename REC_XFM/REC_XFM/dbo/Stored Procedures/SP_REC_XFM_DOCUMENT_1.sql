﻿/* =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Document domain - ccst_document
-- Version: Initial build v13 17/06/2019	
-- =============================================*/
CREATE PROCEDURE [dbo].[SP_REC_XFM_DOCUMENT]
AS
BEGIN

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_document','Documents - v13 17/06/2019'


Select 

	'Migrated' as Author,
	'EMICS:' + dm.Claim_Number as ClaimID,
	Case when Subcategory = 'Outbound' then 'automatic' 
		Else NULL 
		end as CreationType_icare,
	Case when ob.Description is not null then ob.Description else NULL end as Description,
	'1' as DMS,
	ob.OnbasedocID as DocUID,
	ob.channel as DocumentChannel_icare,
	dm.DocumentID as DocumentIdentifier,
	ob.CLMSubCategory as DocumentSubSection_icare,
	dm.DocumentID + '.pdf' as FileName_icare,
	Case when ob.CLMCategory = 'Outbound' then '0' else '1' End as Inbound,
	'en_AU' as Language,
	'WorkersCompLine' as LineOfBusiness_icare,
	dm.Claim_number as LUWID,
	'application/pdf' as MimeType,
	ob.CLMSubCategory + '-' + dm.Claim_Number as Name,
	0 as Obsolete,
	'employer' as PortalUserType_icare,
	'OnBase:' + dm.Claim_Number + '_' + ob.OnbaseDocID + ':Document' as PublicID,
	0 as Redacted_icare,
	'702' as SchemeAgent_icare,
	ob.CLMCategory as Section,
	'unrestricted' as SecurityType,
	0 as SharedWithEmployer_icare,
	0 as SHaredWithWorker_icare,
	'Final' as Status,
	ob.CLMSubType as Type,
	Case when ob.CLMCategory = 'Outbound' then 'Outbound'
		else 'Inbound' End as Direction_icare

into ccst_document

From DM_EDOCS.dbo.DOC_METADATA dm
INNER JOIN dm_edocs.dbo.DOC_ONBASE_LOAD ob on ob.DocumentID = dm.DocumentID --83133
--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = dm.claim_number


END