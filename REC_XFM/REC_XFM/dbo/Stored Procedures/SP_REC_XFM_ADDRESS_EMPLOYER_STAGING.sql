﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 05/06/2019
-- Description:	Address - Named Insured V003
-- 1. This is the child Stored Proc for EMPLOYER address that will called by the master stored proc SP_REC_XFM_ADDRESS
-- 2. <10-Jul-2019><Satish>: Added INSCOPE table into the joins
-- 3. <17-Jul-2019><Satish>: Commented out INSCOPE table
-- Dependency:QA code for ccst_eftdata should have run first before this SP runs
-- 4. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 5. <24Jul2019><Satish>: Fixed QA differences for Named Insured "State" attribute. It was sourcing from PC instead of CM
-- 6. <31Jul2019><Satish>: Applied a fix to set addresstpye to 'postal_icare' for all Verified CDR Named Insured based on DMIG-7570 - as suggested by mapper
-- 7. <26Aug2019><Satish>: As part of DMIG-8003
--    (a) Changed Verified CDR section below to use POSTAL attributes instead of BUSINESS attributes. This is as per mapper confirmation. Dev have already
--        made this change. QA needs to align with Dev and mapping spec.
--    (b) Also ensured that if PTD.POSTAL_ADDRESS has the company name embedded in it i.e. PTD.LEGAL_NAME, then that name will be removed from the address
--        that goes into the QA table.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_ADDRESS_EMPLOYER_STAGING]
	
AS
BEGIN

IF OBJECT_ID('AddressREF_temptable_employer_verifiedPC') is not null
drop table dbo.AddressREF_temptable_employer_verifiedPC;

IF OBJECT_ID('AddressREF_temptable_employer_verifiedCDR') is not null
drop table dbo.AddressREF_temptable_employer_verifiedCDR;

IF OBJECT_ID('temptable_Address_EMPLOYER_VerifiedPC_stg') is not null
drop table temptable_Address_EMPLOYER_VerifiedPC_stg;

IF OBJECT_ID('temptable_Address_EMPLOYER_VerifiedPC') is not null
drop table temptable_Address_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_Address_EMPLOYER_VerifiedCDR') is not null
drop table temptable_Address_EMPLOYER_VerifiedCDR;

SELECT * INTO temptable_Address_EMPLOYER_VerifiedPC_stg
FROM
(
--79262
select
distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
--eft.is_enable_eft, --15/11/2018<Satish R>: Added as part of DMIG-2495
--Claim details
--CRM_A.id as CRM_ID,
--CD.Claim_Number,
--PC PolicyPeriod details
-- AB contact details to populate in ccst_contact
--CM_C.ID as CM_C_ID --12/03/19: Added so that this can be used in CCST_CONTACTTAG selection SQL later
--PC Address fields to Populate ccst_Address (WILL JUST BE THE PRIMARY ADDRESS)
'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ':INSURED:BUSINESSADDRESS' as PublicID
,CM_A.LinkID as AddressBookUID
,CM_A.AddressLine1
,CM_A.AddressLine1Kanji
,CM_A.AddressLine2
,CM_A.AddressLine2Kanji
,CM_A.AddressLine3
--,CM_A.AddressType
,CM_AddressType.TYPECODE as  AddressType-- eg postal
,CM_A.BatchGeocode
,CM_A.CEDEX
,CM_A.CEDEXBureau
,CM_A.City
,CM_A.CityKanji
--,CM_A.Country
,ABTL_CNY.TYPECODE as Country--Eg AU
,CM_A.County
,NULL as DPID_icare
,CM_A.Description as Description
,NULL as ExternalLinkID
,1 as IsAddressFromPC_icare
,CM_A.IsValidated_icare
,0 as ObfuscatedInternal
,CM_A.PostalCode
--,CM_A.PublicID as CM_A_PublicID
,0 as Retired
,NULL as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW
,ABTL_State.TYPECODE as State  --24Jul2019 - corrected this as part of priortiy issues identified by James on 23Jul
--,CM_A.Subtype
--,PC_Subtype.TYPECODE as SubType -- eg Address
,ABTL_Address.TYPECODE as SubType --24Jul2019 - corrected this as part of priortiy issues identified by James on 23Jul
,CM_A.ValidUntil
,ltrim(rtrim(CD.Claim_Number)) as LUWID
--,abtl_abcontact.TypeCode
--,eft.is_enable_eft --15/11/2018<Satish R>: Added as part of DMIG-2495
--,cpr.preferredPaymentDerivedByCPR
--,CASE 
--WHEN eft.is_enable_eft=1 and eftdata.bankname is not null then 'eft'  --making sure we check for the presence of a valid bank account before setting to eft
--WHEN cpr.preferredPaymentDerivedByCPR='eft' and eftdata.bankname is not null THEN 'eft' --making sure we check for the presence of a valid bank account before setting to eft
--ELSE 'check'
--END as PreferredPaymentMethod_icare_based_on_bankaccount,
--1 as ApprovedPaymentMethod,
--0 as AggregaeRemittance,
--0 as SingleRemittance,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
--LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--  LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_P on PC_P.PolicyNumber = PT.icare_policy_no
--inner join #temptable_consolidated cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--inner join  DM_STG.[dbo].INTM_EXTRACT_POLICY_CLAIM_BUCKET cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
  LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
   left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
   left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID --08/02/2019: Added to get ACN and ABN
   LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policycontactrole] PC_PCR on PC_PCR.BranchID = PC_P.Id  --Note BranchID is the ID
    --LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_policycontactrole] PCTL_PCR on PC_PCR.Subtype = PCTL_PCR.ID
    LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_contact] PC_C on PC_C.ID = PC_PCR.ContactDenorm   --PC_A.Contact
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_address] PC_ADD on PC_ADD.ID = PC_C.PrimaryAddressID --NB the address being returned is just for the primary address
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_country] PCTL_CNY on PCTL_CNY.Id = PC_C.Country
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_country] PCTL_CNY_home on PCTL_CNY_home.Id = PC_C.HomePhoneCountry
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_country] PCTL_CNY_work on PCTL_CNY_work.Id = PC_C.WorkPhoneCountry
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_country] PCTL_CNY_fax on PCTL_CNY_fax.Id = PC_C.FaxPhoneCountry
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_primaryphonetype] PrimaryPhoneType on PrimaryPhoneType.Id = PC_C.PrimaryPhone
     LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_languagetype] PrimaryLanguageType on PrimaryLanguageType.Id = PC_C.PrimaryLanguage
LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_addresstype] AddressType ON PC_ADD.AddressType = AddressType.ID
LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_state] PC_State ON PC_ADD.State = PC_State.ID
LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_address] PC_Subtype ON PC_ADD.Subtype = PC_Subtype.ID
LEFT OUTER JOIN [DM_STG_PC].[dbo].[pctl_country] PC_Country ON PC_ADD.Country = PC_Country.ID
left outer join --15/11/2018<Satish R>: Added for DMIG-2495
(
select distinct eft.is_enable_eft,cd.Policy_No from DM_STG_EMICS.dbo.CLAIM_DETAIL cd
left outer join  
(SELECT * FROM DM_STG_EMICS.dbo.EFT WHERE Object_Type='E') EFT
ON cd.Policy_No=EFT.Object AND RTRIM(LTRIM(cd.Policy_No)) IS NOT NULL
where eft.Object is  not null
) eft
on eft.Policy_No=CD.Policy_No
left outer join
(
select cpr1.*,A.preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.CLAIM_PAYMENT_RUN cpr1 
left outer join 
(
select Payee_Code, case WHEN max(Cheque_Status)  = '9' then 'eft' else 'check' end as preferredPaymentDerivedByCPR
from dm_stg_EMICS.dbo.claim_payment_run 
where cheque_status <> '3'
group by Payee_Code
) as A
on cpr1.Payee_Code = A.Payee_Code
inner join 
(
select max(payment_no) as PaymentNo, Payee_type
from DM_STG_EMICS.DBO.CLAIM_PAYMENT_RUN
GROUP BY CLAIM_NUMBER,Payee_type   
)  Payee_Payment_Number 
on cpr1.Payment_no = Payee_Payment_Number.PaymentNo
where cpr1.Cheque_Status <> '3' 
and cpr1.Payee_type = 1 -- Inj Worker
) cpr
on cpr.Claim_Number=cd.Claim_Number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.EFT where Object_Type='E') eft1
on eft1.Object=cd.Claim_Number
LEFT OUTER JOIN DM_STG_EMICS.dbo.TFN 
on CD.Claim_Number=TFN.Claim_no
----10-Jan-2019: Adding CRM tables below as part of DMIG-2777 fix
--LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CLAIM] ccl --Remember to switch _LD back to without _LD
--ON LTRIM(RTRIM(ISNULL(cd.[Claim_Number],''))) = LTRIM(RTRIM(ISNULL(ccl.[CLAIM_NUMBER_FOR_INTEGRATION__C],'')))
--LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_POLICY] p --Remember to switch _LD back to without _LD
--ON ccl.[POLICY_NUMBER__C] = p.[ID]
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_ACCOUNT] crm_a --Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
---select * from [DM_STG_CM].[dbo].[ab_abcontact]
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--Ash 3/5/2019
--Added extra RecordType
--INNER JOIN [DM_STG_CRM].[preload].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --Remember to switch _LD back to without _LD
INNER JOIN [DM_STG_CRM].[dbo].[DM_STG_CRM_RECORDTYPE] rt ON crm_a.[RECORDTYPEID] = rt.[ID] --10/06/2019 - DM_STG_CRM_LD is not accessible anymore
   AND rt.[NAME] = 'Customer Account' AND rt.[SOBJECTTYPE] = 'Account'
--select count(1) from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C
--select * from DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C 
--where ID=255715
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = crm_a.[ID] --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
-- LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 -- LEFT OUTER JOIN select * from [DM_STG_CM].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
left outer join 
(
--select * from DM_XFM.[dbo].[DM_XFM_EFTDATA] where ContactID like '%:INSURED'
select * from dbo.ccst_eftdata where ContactID like '%:INSURED'
) eftdata 
on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and PC_P.MostRecentModel=1 --the most recent version (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_P.status=9 --policy is bound (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_PCR.subtype=19  --19 making sure it is primarynamedinsured role contact only
--and cons_pp.LUWID is not null --15/11/2018 <Satish R>: Making sure its verified PC only
--and eft.Object is  not null  --making sure we pick the ones where we have eft data
--and CD.date_of_injury between PC_P.PeriodStart and PC_P.PeriodEnd (31/10: this is taken care in #temptable_consolidated, so commented now)
--and PC_ADD.AddressLine1 is null
--and PC_PCR.ExpirationDate is null --15/11/2018 <Satish R>: Added this to eliminate duplicate Address records.
--and cons_pp.POLICY_FLAG=0 --verified PC only
--and ltrim(rtrim(CD.Claim_Number))=1025345
--61528
)X

SELECT * INTO temptable_Address_EMPLOYER_VerifiedPC
FROM temptable_Address_EMPLOYER_VerifiedPC_stg

SELECT * INTO temptable_Address_EMPLOYER_VerifiedCDR
FROM
(
select
'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':BUSINESSADDRESS' as PublicID,
NULL as AddressBookUID,
CASE WHEN ltrim(rtrim(PTD.POSTAL_LOCALITY)) <> ''  THEN   ltrim(rtrim(PTD.POSTAL_LOCALITY))
                                                    ELSE 'Sydney'
--default if source is NULL  
                    END as City,   
CASE WHEN ltrim(rtrim(PTD.POSTAL_POST_CODE)) <> '' THEN   ltrim(rtrim(PTD.POSTAL_POST_CODE))
                                                    ELSE 2000
    --default if source is NULL  
                    END as PostalCode,
'Address' as SubType,
--ptd.[LEGAL_NAME],
--ptd.[POSTAL_STREET],
CASE 
WHEN ltrim(rtrim(PTD.POSTAL_STREET)) <> ''  
THEN 
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( case WHEN ISNULL(ptd.[POSTAL_STREET],'') <> '' THEN replace(ptd.POSTAL_STREET,ptd.LEGAL_NAME,'')/*ver6*/  end,'  ','<>'),'><',' '),'  ',''),'< >',' '),'<>',' ') 
--THEN   ltrim(rtrim(PTD.POSTAL_STREET))
ELSE 'VERIFIED CDR DUMMY LINE1'
--default if source is NULL  
END as AddressLine1,
NULL as AddressLine1Kanji
,NULL as AddressLine2
,NULL as AddressLine2Kanji
,NULL as AddressLine3
--,CM_A.AddressType
--,NULL as  AddressType-- eg postal
,'postal_icare' as AddressType --31Jul2019 - As part of mapper feeback on DMIG-7570, making this change on QA end too
,0 as BatchGeocode
,NULL as CEDEX
,NULL as CEDEXBureau
--,CM_A.City
,NULL as CityKanji
--,CM_A.Country
--,pc_ct.TYPECODE as Country,--Eg AU
,pc_ct.TYPECODE as Country
,NULL as County
,NULL as DPID_icare
,NULL as Description
,NULL as ExternalLinkID
,0 as IsAddressFromPC_icare
,NULL AS IsValidated_icare
,0 as ObfuscatedInternal
--,CM_A.PostalCode
--,CM_A.PublicID as CM_A_PublicID
,0 as Retired
,NULL as StandardizedAddressID_icare
--,CM_A.State
,pc_st.TYPECODE as State--eg AU_NSW
--,CM_A.Subtype
--,'Address' as SubType -- eg Address
,NULL as ValidUntil
,ltrim(rtrim(CD.Claim_Number)) as LUWID
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
left outer join DM_STG_EMICS.dbo.POLICY_TERM_DETAIL PTD on PTD.POLICY_NO=CD.Policy_No
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
left outer join dm_stg_pc.dbo.pctl_state pc_st 
on pc_st.TYPECODE=CASE PTD.POSTAL_STATE WHEN 'ACT' THEN 'AU_ACT'
                                         WHEN 'QLD' THEN 'AU_QLD'
WHEN 'SA'  THEN 'AU_SA'
WHEN 'VIC' THEN 'AU_VIC'
WHEN 'WA'  THEN 'AU_WA'
WHEN 'NSW' THEN 'AU_NSW'
ELSE 'AU_NSW'
--default if source is NULL  
END
left outer join dm_stg_pc.dbo.pctl_country pc_ct on pc_ct.DESCRIPTION='Australia'
LEFT JOIN (select LUWID from [dbo].[CoverREF_temptable_consolidated]) cons_pp ON cons_pp.LUWID = ltrim(rtrim(CD.Claim_Number))
where CD.is_Null=0 and cons_pp.LUWID is null -- to get only verified CDR/Invalid claims
--where CD.is_Null=0
--and ltrim(rtrim(CD.Claim_Number)) NOT IN -- to get only verified CDR/Invalid claims
--(select LUWID from [dbo].[CoverREF_temptable_consolidated] cons_pp) 
)Y
--,

--Use [REC_XFM]
--Go


;

select
*
into dbo.AddressREF_temptable_employer_verifiedPC
from
(
select * 
from temptable_Address_EMPLOYER_VerifiedPC
)x
;

select
*
into dbo.AddressREF_temptable_employer_verifiedCDR
from
(
select * 
from temptable_Address_EMPLOYER_VerifiedCDR
)x
;

IF OBJECT_ID('temptable_Address_EMPLOYER_VerifiedPC') is not null
drop table temptable_Address_EMPLOYER_VerifiedPC;

IF OBJECT_ID('temptable_Address_EMPLOYER_VerifiedCDR') is not null
drop table temptable_Address_EMPLOYER_VerifiedCDR;

END