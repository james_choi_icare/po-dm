﻿-- =============================================
-- Author:		Ann
-- Create date: 26/07/2019
-- Description:	This logic was taken from the Contact team in order for QA to independently build the vendorselection table instead of relying on xfm's table.
-- =============================================
CREATE PROCEDURE [SP_REC_XFM_EFTDATA_VENDORSELECTION_STAGING]
AS
BEGIN
	
IF OBJECT_ID('INTM_EXTRACT_VENDORSELECTION') IS NOT NULL
DROP TABLE INTM_EXTRACT_VENDORSELECTION;


IF OBJECT_ID('VendorSC_VendorSelection1') IS NOT NULL
DROP TABLE VendorSC_VendorSelection1;

 

IF OBJECT_ID('VendorSC_VendorSelection2') IS NOT NULL

DROP TABLE VendorSC_VendorSelection2;

 

IF OBJECT_ID('VendorSC_VendorSelection3') IS NOT NULL

DROP TABLE VendorSC_VendorSelection3;

 

IF OBJECT_ID('VendorSC_mcpr') IS NOT NULL

DROP TABLE VendorSC_mcpr;

 

IF OBJECT_ID('VendorSC_pr') IS NOT NULL

DROP TABLE VendorSC_pr;

 

IF OBJECT_ID('VendorSC_c') IS NOT NULL

DROP TABLE VendorSC_c;

 

IF OBJECT_ID('VendorSC_mpr') IS NOT NULL

DROP TABLE VendorSC_mpr;

 

IF OBJECT_ID('VendorSC_c2') IS NOT NULL

DROP TABLE VendorSC_c2;

 

--***********************  ********************************************************

 

SELECT * INTO VendorSC_mcpr

FROM (

SELECT

[Claim_number]

,[Payee_Code]

,MAX([Payment_no]) [Payment_no]

FROM [DM_STG_EMICS].[dbo].[CLAIM_PAYMENT_RUN]

WHERE [Payee_type] IN (3,4) -- Remove payee_type 5 - Louw Smit 20181121

GROUP BY 

[Claim_number]

,[Payee_Code]

) mcpr;

 

----------------

 

SELECT * INTO VendorSC_pr

FROM (

SELECT

[Service_Provider_Id]

,[Payment_no]

,[Claim_No]

FROM [DM_STG_EMICS].[dbo].[Payment_Recovery]

WHERE [Service_Provider_Id] IS NOT NULL

GROUP BY

[Service_Provider_Id]

,[Payment_no]

,[Claim_No]

) pr;

 

----------------

 

SELECT * INTO VendorSC_c

FROM (

SELECT

[WC_Provider_Code] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND LTRIM(RTRIM(ISNULL([WC_Provider_Code],''))) <> ''

UNION

SELECT

[HC_Number] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND LTRIM(RTRIM(ISNULL([HC_Number],''))) <> ''

UNION

SELECT

[Creditor_no] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND LTRIM(RTRIM(ISNULL([Creditor_no],''))) <> ''

) c;

 

----------------

 

SELECT * INTO VendorSC_mpr

FROM (

SELECT 

[Payment_no]

FROM [DM_STG_EMICS].[dbo].[Payment_Recovery]

WHERE [Service_Provider_Id] IS NOT NULL

GROUP BY [Payment_no]

HAVING COUNT(DISTINCT [Service_Provider_Id]) > 1

) mpr;

 

----------------

 

SELECT * INTO VendorSC_c2

FROM (

SELECT

[WC_Provider_Code] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND [is_deleted] = 0

AND LTRIM(RTRIM(ISNULL([WC_Provider_Code],''))) <> ''

UNION

SELECT

[HC_Number] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND [is_deleted] = 0

AND LTRIM(RTRIM(ISNULL([HC_Number],''))) <> ''

UNION

SELECT

[Creditor_no] [Service_Provider_Id]

,[Creditor_no]

FROM [DM_STG_EMICS].[dbo].[CREDITORS]

WHERE [Inactive] = 0

AND [is_do_not_pay] = 0

AND [is_deleted] = 0

AND LTRIM(RTRIM(ISNULL([Creditor_no],''))) <> ''

) c2;

 

----------------

 

SELECT * INTO VendorSC_VendorSelection1

FROM (

-- All with payments

SELECT

cpr.[Payment_no]

,cpr.[Payee_Code] [Creditor_no]

,1 [SelectionSet]

FROM [DM_STG_EMICS].[dbo].[CLAIM_PAYMENT_RUN] cpr

INNER JOIN VendorSC_mcpr

ON cpr.[Payment_no] = VendorSC_mcpr.[Payment_no]

INNER JOIN [DM_XFM].[dbo].[DM_INSCOPE_CLAIMS] dic

ON LTRIM(RTRIM(ISNULL(cpr.[Claim_number],''))) = dic.[Claim_Number]

-- WHERE cpr.[Payee_type] IN (3,4) -- Commented out as this is now in VendorSC_mcpr - Louw Smit 20181115

 

UNION 

 

-- Payment Recovery 1

SELECT 

cpr.[Payment_no]

,z.[Creditor_no]

,2 [SelectionSet]

FROM [DM_STG_EMICS].[dbo].[CLAIM_PAYMENT_RUN] cpr

INNER JOIN VendorSC_mcpr

ON cpr.[Payment_no] = VendorSC_mcpr.[Payment_no]

INNER JOIN [DM_XFM].[dbo].[DM_INSCOPE_CLAIMS] dic

ON LTRIM(RTRIM(ISNULL(cpr.[Claim_number],''))) = dic.[Claim_Number]

INNER JOIN (

SELECT 

VendorSC_pr.[Service_Provider_Id]

,VendorSC_pr.[Payment_no]

,VendorSC_c.[Creditor_no]

FROM VendorSC_pr

INNER JOIN VendorSC_c

ON VendorSC_pr.[Service_Provider_Id] = VendorSC_c.[Service_Provider_Id]

) z

ON cpr.[Payment_no] = z.[Payment_no]

AND cpr.[Payee_Code] <> z.[Service_Provider_Id]

-- WHERE cpr.[Payee_type] IN (3,4) -- Commented out as this is now in VendorSC_mcpr - Louw Smit 20181115

GROUP BY

cpr.[Payment_no]

,z.[Creditor_no]

 

UNION

 

-- Payment Recovery 2

SELECT 

cpr.[Payment_no]

,VendorSC_c2.[Creditor_no]

,3 [SelectionSet]

FROM [DM_STG_EMICS].[dbo].[CLAIM_PAYMENT_RUN] cpr

INNER JOIN (

SELECT 

pr.[Payment_no]

,pr.[Service_Provider_Id]

FROM [DM_STG_EMICS].[dbo].[Payment_Recovery] pr

INNER JOIN VendorSC_mpr

ON pr.[Payment_no] = VendorSC_mpr.[Payment_no]

GROUP BY 

pr.[Payment_no]

,pr.[Service_Provider_Id]

) apr

ON cpr.[Payment_no] = apr.[Payment_no]

INNER JOIN VendorSC_c2

ON apr.[Service_Provider_Id] = VendorSC_c2.[Creditor_no]

INNER JOIN [DM_XFM].[dbo].[DM_INSCOPE_CLAIMS] dic

ON LTRIM(RTRIM(ISNULL(cpr.[Claim_number],''))) = dic.[Claim_Number]

WHERE cpr.[Payee_type] IN (3,4) -- Remove payee_type 5 - Louw Smit 20181121

) vs1;

 

--------------

 

SELECT * INTO VendorSC_VendorSelection2

FROM (

SELECT

vs.[Payment_no]

,mvs.[Claim_number]

,vs.[Creditor_no]

,mvs.[vs_DebtorType]

,mvs.[SelectionSet]

FROM VendorSC_VendorSelection1 vs

INNER JOIN (

SELECT 

vs.[Creditor_no]

,cpr.[Claim_number]

,vs.[Payment_no]

,vs.[SelectionSet]

,CASE WHEN cd.[Nom_Treating_Doctor] IS NOT NULL THEN '1-NTD' ELSE '5-UNK' END [vs_DebtorType]

-- ,ROW_NUMBER() OVER (PARTITION BY vs.[Creditor_no],cpr.[Claim_number] ORDER BY vs.[Payment_no] DESC,vs.[SelectionSet] ASC) [Payment_no_rank]

,ROW_NUMBER() OVER (PARTITION BY vs.[Creditor_no],cpr.[Claim_number] ORDER BY vs.[SelectionSet] ASC,vs.[Payment_no] DESC) [Payment_no_rank]

FROM VendorSC_VendorSelection1 vs

INNER JOIN [DM_STG_EMICS].[dbo].[CLAIM_PAYMENT_RUN] cpr

ON vs.[Payment_no] = cpr.[Payment_no]

LEFT OUTER JOIN [DM_STG_EMICS].[dbo].[CLAIM_DETAIL] cd

ON cpr.[Claim_number] = cd.[Claim_Number]

AND vs.[Creditor_no] = cd.[Nom_Treating_Doctor]

) mvs

ON vs.[Creditor_no] = mvs.[Creditor_no]

AND vs.[Payment_no] = mvs.[Payment_no]

AND vs.[SelectionSet] = mvs.[SelectionSet]

AND mvs.[Payment_no_rank] = 1

) vs2;

 

---------------

 

SELECT * INTO VendorSC_VendorSelection3

FROM (

SELECT 

-999 [Payment_no]

,b.[Claim_number]

,b.[Creditor_no]

,b.[vs_DebtorType]

,4 [SelectionSet]

FROM (

SELECT 

a.[Claim_number]

,a.[Creditor_no]

,a.[vs_DebtorType]

,ROW_NUMBER() OVER (PARTITION BY a.[Claim_number],a.[Creditor_no] ORDER BY a.[vs_DebtorType]) [vs_DebtorType_rank] 

FROM (

SELECT 

[Claim_Number] [Claim_number]

,LTRIM(RTRIM([Nom_Treating_Doctor])) [Creditor_no]

,'1-NTD' [vs_DebtorType]

FROM [DM_STG_EMICS].[dbo].[CLAIM_DETAIL]

WHERE LTRIM(RTRIM(ISNULL([Nom_Treating_Doctor],''))) <> ''

UNION ALL

SELECT 

[Claim_Number] [Claim_number]

,LTRIM(RTRIM([nom_Treating_Specialist])) [Creditor_no]

,'2-NTS' [vs_DebtorType]

FROM [DM_STG_EMICS].[dbo].[CLAIM_DETAIL]

WHERE LTRIM(RTRIM(ISNULL([nom_Treating_Specialist],''))) <> ''

UNION ALL

SELECT 

[Claim_Number] [Claim_number]

,LTRIM(RTRIM([Nom_Rehab_Provider])) [Creditor_no]

,'3-NRP' [vs_DebtorType]

FROM [DM_STG_EMICS].[dbo].[CLAIM_DETAIL]

WHERE LTRIM(RTRIM(ISNULL([Nom_Rehab_Provider],''))) <> ''

UNION ALL

SELECT DISTINCT

[claim_no] [Claim_number]

,LTRIM(RTRIM([creditor_no])) [Creditor_no]

,'4-A' [vs_DebtorType]

FROM [DM_STG_EMICS].[dbo].[approval]

) a

INNER JOIN [DM_XFM].[dbo].[DM_INSCOPE_CLAIMS] dic

ON LTRIM(RTRIM(ISNULL(a.[Claim_number],''))) = dic.[Claim_Number]

LEFT OUTER JOIN VendorSC_VendorSelection2 vs2

ON a.[Claim_number] = vs2.[Claim_number]

AND a.[Creditor_no] = vs2.[Creditor_no]

WHERE vs2.[Claim_number] IS NULL

) b

WHERE b.[vs_DebtorType_rank] = 1

) vs3;

 

--************************************************************

--FINAL

--************************************************************

--***********************  ********************************************************

 

 --drop table [INTM_EXTRACT_VENDORSELECTION]

Select 

*

INTO [INTM_EXTRACT_VENDORSELECTION]
From ( 


SELECT

vs2.[Payment_no]

,vs2.[Claim_number]

,vs2.[Creditor_no]

,vs2.[vs_DebtorType]

,vs2.[SelectionSet]

FROM VendorSC_VendorSelection2 vs2

UNION ALL

SELECT

vs3.[Payment_no]

,vs3.[Claim_number]

,vs3.[Creditor_no]

,vs3.[vs_DebtorType]

,vs3.[SelectionSet]

FROM VendorSC_VendorSelection3 vs3
) x;
 

IF OBJECT_ID('VendorSC_VendorSelection1') IS NOT NULL

DROP TABLE VendorSC_VendorSelection1

 

IF OBJECT_ID('VendorSC_VendorSelection2') IS NOT NULL

DROP TABLE VendorSC_VendorSelection2

 

IF OBJECT_ID('VendorSC_VendorSelection3') IS NOT NULL

DROP TABLE VendorSC_VendorSelection3

 

IF OBJECT_ID('VendorSC_mcpr') IS NOT NULL

DROP TABLE VendorSC_mcpr;

 

IF OBJECT_ID('VendorSC_pr') IS NOT NULL

DROP TABLE VendorSC_pr;

 

IF OBJECT_ID('VendorSC_c') IS NOT NULL

DROP TABLE VendorSC_c;

 

IF OBJECT_ID('VendorSC_mpr') IS NOT NULL

DROP TABLE VendorSC_mpr;

 

IF OBJECT_ID('VendorSC_c2') IS NOT NULL

DROP TABLE VendorSC_c2;

 

END