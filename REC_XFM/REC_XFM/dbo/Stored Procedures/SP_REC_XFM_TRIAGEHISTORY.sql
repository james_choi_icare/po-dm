﻿/* =============================================
-- Author:		Ann
-- Last updated: 14/05
-- Description:	triagehistory v35 ccst_triagehistory
-- Version:	14/05/2019 v36.2 - no change
			10/06/2019 v37.1 - Updated fields: Comments, CurrentSegment, DateReviewed, LocationOfInjuryToocs, ProposedSegment, PublicID, Source, UserID
							   Updated selection criteria: Triagehistory SC and added Users and groups mapping
			20/06/2019 v37.2 updated PublicID rule
			09/07/2019 v39 no change
 =============================================== */
CREATE PROCEDURE [dbo].[SP_REC_XFM_TRIAGEHISTORY]
 
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_triagehistory_icare','Triage - v39 09/07/2019'
--drop table ccst_triagehistory_icare
Select 

'EMICS:' + RTRIM(CD.CLAIM_NUMBER) as ClaimID,
cta.Segment_reason_FreeText as Comments,
Case when cta.Segment_code = 'C' then 'care_icare'
		when cta.Segment_code = 'E' then 'ni_empower_icare'
		when cta.Segment_code = 'G' then 'ni_guide_icare'
		when cta.Segment_code = 'P' then 'ni_support_icare'
		when cta.Segment_code = 'S' then 'ni_specialised_icare'
		when cta.Segment_code = 'U' then 'unassigned_icare'
		ELSE 'unassigned_icare'
		End as CurrentSegment, -- v37.1
cta.Create_date as DateReviewed,
case when cde.icd_code1 is not null then cde.icd_code1 
		when cde.icd_code2 is not null then cde.icd_code2
		when cde.icd_code3 is not null then cde.icd_code3
		Else Null
		END AS icdcode,
'icd_code1: ' + ISNULL(convert(varchar, cde.icd_code1),'NULL') + ' icd_code2: ' +  ISNULL(convert(varchar, cde.icd_code2),'NULL') + ' icd_code3: ' +  isnull(convert(varchar, cde.icd_code3),'NULL') as icdcode_SRC_VALUE,
cta.Claim_no as LUWID,
	cd.Location_of_Injury as LocationOfInjuryToocs, -- v37.1
	cd.Nature_of_injury as NatureOfInjuryToocs, -- v37.1
Case when cta.Status = 'C' then 'accept' Else NULL end as Outcome,
Case when cta.Segment_code = 'C' then 'care_icare'
	when cta.Segment_code = 'E' then 'ni_empower_icare'
	when cta.Segment_code = 'G' then 'ni_guide_icare'
	when cta.Segment_code = 'P' then 'ni_support_icare'
	when cta.Segment_code = 'S' then 'ni_specialised_icare'
	when cta.Segment_code = 'U' then 'unassigned_icare'
	End as ProposedSegment, -- v37.1
'EMICS:' + RTRIM(Cta.claim_no) + '_' + CONVERT(Varchar, cta.ID) + ':TRIAGE_HISTORY' as PublicID, --v37.2
	NULL as RiskScore,
	Case when cta.Segment_reason = 'Triage' then 'ERR01' 
		When cta.Segment_reason = '' Then NULL 
		Else NULL
		end as SegmentReason,
	case when cta.confirmation_type = 'A' then 'engine' -- v37.1
		else 'manual' 
		end as Source,
	cta.Create_date as TriageDate,
	coalesce (cc_user.PublicID, cc_user2.PublicID, cc_user_sm.PublicID)  as UserID

into ccst_triagehistory_icare

From DM_STG_EMICS.dbo.CLAIM_TRIAGE_AUDIT cta
LEFT JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd on cd.claim_number = cta.claim_no
LEFT JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL_EXTRA cde on cd.claim_number = cde.claim_no

--v1.2 users and groups
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cta.Owner)
LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)

LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co2  ON (co2.alias = cad.Claims_Officer) -- claim_triage.Owner-- ct.Owner
LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) 
		- CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
		ON (co2.Alias = cntrl2.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID)

left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
left join DM_XFM.[dbo].[DM_LKP_EML_TEAM_GWCC_GROUPS] lkp_eml_team_gwcc_groups on (lkp_eml_team_gwcc_groups.EML_TEAM = co.grp)
left join DM_STG_CC.[dbo].[cc_group] cc_group on ( cc_group.Name = lkp_eml_team_gwcc_groups.gwcc_group )
left join DM_XFM.[dbo].[DM_LKP_EML_GWCC_QUEUE] lkp_eml_gwcc_queues on ( cntrl.EML_Email = lkp_eml_gwcc_queues.EMLEmailAddress 
			 and cad.Claims_Officer = lkp_eml_gwcc_queues.EMLQueueUser)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential_user_queue on (lkp_eml_gwcc_queues.GWCC_Claim_User = cc_credential_user_queue.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user_queue on (cc_user_queue.CredentialID = cc_credential_user_queue.ID)

Where cd.is_null = 0
Order by cta.ID desc





END