﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_MEDICALACTION]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Modified date: 09/05
-- Description: Claims ccst_medicalaction
-- Version: 09/05/2019 v23 AG - added additional fields
--			11/06/2019 v24 AG updated SC, updated xform rules for Note
-- ============================================= */
--drop table ccst_medicalaction

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_medicalaction','Claim Management - v24 11/06/2019'

SELECT distinct 
	Convert(Datetime2, mc.Received_Date) as ActionDate,
	'EMICS:' + RTRIM(mc.claim_no) + ':MEDONLYEXP' as ExposureID,
	'continuing' as FutureActionReq,
	mc.claim_no as LUWID,
	'imevisit' as MedicalActionType,
	NULL as NextAppointment,
	-- JCFIX ----------------
	CASE
			WHEN LTRIM(RTRIM(REPLACE(REPLACE(MC.Treatment_Plan,' ',''),CHAR(13) + CHAR(10),''))) = '' THEN NULL
			WHEN LEN(LTRIM(RTRIM(REPLACE(REPLACE(MC.Treatment_Plan,' ',''),CHAR(13) + CHAR(10),'')))) = 1 THEN NULL
			ELSE LTRIM(RTRIM(REPLACE(REPLACE(MC.Treatment_Plan,'  ',' '),CHAR(13) + CHAR(10),' ')))
		END AS [Note],
	--REPLACE(REPLACE(mc.Treatment_plan, CHAR(13)+CHAR(10),' '),'  ',' ') as Note,
	ltrim(rtrim(mc.Doctor_name)) as ProviderName,
	--mc.Doctor_name as ProviderName,
	-- JCFIX ----------------
	'EMICS:' + ltrim(rtrim(mc.Claim_no)) + '_' + cast(mc.ID as varchar(50)) + ':MEDICALACTION' PublicID
	
INTO ccst_medicalaction


FROM DM_STG_EMICS.dbo.Medical_Cert mc
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd on mc.Claim_no = cd.Claim_Number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

INNER JOIN (SELECT DISTINCT cd2.claim_number 
		FROM DM_STG_EMICS.dbo.CLAIM_DETAIL cd2
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad2 on cd2.Claim_Number = cad2.Claim_no
		LEFT OUTER JOIN (Select * From DM_STG_EMICS.dbo.Payment_Recovery where reversed = 0 ) PR ON PR.Claim_No = cd2.Claim_Number
		LEFT OUTER JOIN (Select distinct claim_no from DM_STG_EMICS.dbo.Medical_Cert where is_Deleted = 0) MC On cad2.claim_no = mc.Claim_no
		Where cd2.is_null = 0
		and cad2.Claim_Liability_Indicator <> 12
		and (pr.estimate_type in (51,52,53,55,56,63,64) or cd2.is_Medical_only = 1 or mc.claim_no is not null))
		MEDX On cd.Claim_Number = medx.Claim_Number
		Where cd.is_null =0 


END