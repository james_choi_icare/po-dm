﻿CREATE PROC [dbo].[SP_REC_XFM_COMMONLAW] AS
BEGIN
-- =============================================
-- Author: 
-- Modified date:
-- Domain: Legal ccst_commonlaw_icare
-- Version: 13/05 checked against v29 - added more fields
--			20/06 v32 added more fields
-- =============================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_commonlaw_icare','Legal - v32'

SELECT 
		CASE WHEN c.Dispute_Type IS NULL OR c.Dispute_Type = 0 THEN 0
			ELSE 1 END ClaimDisputed_icare,
		'Dispute_Type:' + isnull(cast(c.Dispute_Type as varchar(10)),'NULL') ClaimDisputed_icare_SRC_VALUE,
		Case when Common_Law_Action_Outcome = 1 then '01'
			when Common_Law_Action_Outcome = 2 then '02'
			when Common_Law_Action_Outcome = 3 then '03'
			when Common_Law_Action_Outcome = 4 then '04'
			when Common_Law_Action_Outcome = 5 then '05'
			when Common_Law_Action_Outcome = 6 then '06'
			when Common_Law_Action_Outcome = 7 then '07'
			when Common_Law_Action_Outcome = 8 then '08'
			when Common_Law_Action_Outcome is null then null
			End as CTMResolutionType,
		cast(e.DORP as datetime2) as DateParticRece_icare,
		CASE WHEN a.Common_Law_Action_Date is not null AND a.Hearing_Type_code in ('AP','CA','DP','DR','MD') THEN 1 
			ELSE 0 END LitigationStarted,
		b.claim_number as LUWID,
		Case when Hearing_Type_code = 'MD' then Date_Hearing
			Else NULL end as MediationDate_icare,
		Case when Notice_received_date is not null then Notice_received_date
			End as NoCDateReceived_icare,
		Case when Notice_received_date is not null then 1 else 0
			End as NoticeOfClaim_icare,
		olc.OurLegalCosts_icare, 
		Case when Common_Law_Action_Outcome = 1 then '01'
			when Common_Law_Action_Outcome = 2 then '02'
			when Common_Law_Action_Outcome = 3 then '03'
			when Common_Law_Action_Outcome = 4 then '04'
			when Common_Law_Action_Outcome = 5 then '05'
			when Common_Law_Action_Outcome = 6 then '06'
			when Common_Law_Action_Outcome = 7 then '07'
			when Common_Law_Action_Outcome = 8 then '08'
			when Common_Law_Action_Outcome is null then null
			End as Outcome_icare,
		'EMICS:' + a.Claim_No + '_' + cast(a.ID as varchar(50)) + ':LITIGATION' PublicID,
		Case when Reopen_Reason = 0 then null
			when Reopen_Reason = 1 then 'Recurrence of original injury'
			when Reopen_Reason = 2 then 'Further payments/recoveries (includes further payments under provisional liability)'
			when Reopen_Reason = 3 then 'Litigation'
			when Reopen_Reason = 4 then 'Claim administration purposes'
			when Reopen_Reason = 5 then 'Provisional liability discontinued, claim subsequently lodged'
			when Reopen_Reason = 6 then 'Reasonable excuse given, claim subsequently lodged'
			End as Reasonreopened_icare,
		CASE WHEN a.Hearing_Type_code != 'MD' THEN null
			WHEN a.Hearing_Type_code = 'MD' AND a.Hearing_Outcome = 'SD' THEN 'settle_icare'
			WHEN a.Hearing_Type_code = 'MD' AND (a.Hearing_Outcome = 'AJ' OR ISNULL(a.Hearing_Outcome,'') = '') THEN 'heldopen_icare'
			ELSE 'notsettle_icare' END ResultOfMediation_icare,
		sc.SettlementCost_icare,
		'CommonLaw_icare' as Subtype,
		Case when Dispute_Type = 0 then null
			when Dispute_Type = 1 then 'liability_icare'
			when Dispute_Type = 2 then 'other_icare'
			when Dispute_Type = 3 then 'other_icare'
			when Dispute_Type = 4 then 'liability_icare'
			when Dispute_Type = 5 then 'liability_icare'
			when Dispute_Type = 6 then 'liability_icare'
			when Dispute_Type = 7 then 'other_icare'
			when Dispute_Type = 8 then 'other_icare'
			when Dispute_Type = 9 then 'other_icare'
			when Dispute_Type = 10 then 'other_icare'
			when Dispute_Type = 11 then 'other_icare'
			when Dispute_Type = 12 then 'other_icare'
			when Dispute_Type = 13 then 'other_icare'
			when Dispute_Type = 14 then 'other_icare'
			when Dispute_Type = 15 then 'wpi_icare'
			when Dispute_Type = 16 then 'other_icare'
			when Dispute_Type = 17 then 'other_icare'
			when isnull(Dispute_type,'') = '' then null
			End as TypeOfDispute_icare,
		Case when Common_Law_Action_Outcome != 1 then 'outcome_icare'
			When Common_Law_Action_Outcome is not null then 'statementofclaim_icare'
			End as ValidationLevel_icare

		INTO ccst_commonlaw_icare

	FROM [DM_STG_EMICS].dbo.Litigation a
		INNER JOIN [DM_STG_EMICS].dbo.CLAIM_DETAIL b ON a.Claim_No = b.Claim_Number
		LEFT JOIN [DM_STG_EMICS].dbo.CLAIM_ACTIVITY_DETAIL c ON a.Claim_No = c.Claim_no
		LEFT JOIN [DM_STG_EMICS].dbo.CLAIM_DETAIL_EXTRA d ON a.Claim_No = d.Claim_no
		LEFT JOIN [DM_STG_EMICS].dbo.section66 e ON a.Claim_No = e.claim_no
		Left join (Select lt.claim_no, SUM(ISNULL(Trans_amount, 0.0) - isnull(itc,0.0) - isnull(dam, 0.0)) as OurLegalCosts_icare
			From DM_STG_EMICS.dbo.Litigation lt
			Left outer join DM_STG_EMICS.dbo.Payment_recovery pr on lt.Claim_No = pr.Claim_No and pr.estimate_type = '61b'
			Group by LT.claim_No) olc on olc.Claim_No = b.Claim_Number
		Left join (Select lt.claim_no, sum(isnull(trans_amount, 0.0) - isnull(itc,0.0) - isnull(dam,0.0)) as SettlementCost_icare
			From DM_STG_EMICS.dbo.Litigation lt
			Left outer join DM_STG_EMICS.dbo.Payment_recovery pr on lt.Claim_No = pr.Claim_No and pr.estimate_type = '57'
			Group by lt.Claim_No) sc on sc.Claim_No = b.Claim_Number
	WHERE (a.Type = 1 OR a.Common_Law_Action_Date IS NOT null) AND a.ID <> 101
END