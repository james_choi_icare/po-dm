﻿-- =============================================
-- Author          :		Saranya
-- Mapping Version :          12.32
-- To complete mapping for TotalBTP
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_POLICY_VERIFIED_PC_STAGING]
	
AS
BEGIN
--------Verified PC
--Getting policy matches using Policy_term_detail.icare_policy_no against pc_policyperiod.policy_number

	;With temptable_pc_pp as (
		select
		CD.itc_pcent as ITCPercentagePEL,
		Case When pctl_polLine.name='icare Workers'' Comp' then 'WorkersComp' end as PolicyType,
		pctl_polType.TYPECODE as PolicyType_icare,	
		PC_PP.ID AS PC_PP_Id,
		PC_PP.PeriodStart as PC_PP_EffectiveDate,
		PD.PERIOD_START as PD_EffectiveDate,
		PC_PP.PeriodEnd as PC_PP_ExpirationDate,
		PD.PERIOD_EXPIRY as PD_ExpirationDate,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
		LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
		LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
		PT.icare_policy_no as PT_icare_PolicyNumber,
		PT.ID as PT_ID,
		PT.POLICY_STATUS as PT_Policy_Status,
		PC_PP.TermNumber ,
		'POLICY_TERM_DETAIL policy_number_icare with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE,
		Case when LEN(cd.Tariff_No) = 3 then cd.Tariff_No else NULL end as TariffRate_icare,
		cctl_EmpCateg.TYPECODE as EmployerCategory_icare,
		Case when pctl_labour.Name='1 - Labour Hire Firm' then 'labour_hire'
		when  pctl_labour.Name='2 - Not a Labour Hire Firm' then 'not_labour_hire'
		when  pctl_labour.Name='3 - Group training aprenticeship scheme' then 'group_training' 
		end as LabourHire_icare,
		pc_producercode.code as ProducerCode,
		pc_producercode.Description as ProducerName_icare,
		case when PD.policy_Status in (50,52) then 'inforce' END as status,
		cd.Claim_Number as ClaimNumber
		from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
		left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No
		left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD 	on PD.POLICY_NO = PT.POLICY_NO
			--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
		-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
		and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
		left outer join 
		(
		select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate,EmployerCategory_icare,
			PolicyType_icare,WCLabourHireCode_icare,ProducerCodeOfRecordID,publicid
		from dm_stg_pc.dbo.pc_policyperiod
		Where status=9 --bound
		and MostRecentModel=1
		and Retired=0
		and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
		) PC_PP
		on (PT.icare_policy_no=PC_PP.PolicyNumber )
		--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
		--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
		--on PT.icare_policy_no=PC_PP.PolicyNumber
		--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
		and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
		left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
		left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		left outer join DM_STG_PC..pctl_employercategory_icare pctl on pctl.ID = PC_PP.EmployerCategory_icare
		left outer join DM_STG_CC.dbo.cctl_employercategory_icare cctl_EmpCateg on cctl_EmpCateg.name = pctl.name
		left outer join [DM_STG_PC].dbo.pc_policyline pline on pline.BranchID = PC_PP.id
		left outer join [DM_STG_PC].dbo.pctl_policyline pctl_polLine on pctl_polLine.id = pline.Subtype
		left outer join [DM_STG_PC].dbo.pctl_policytype_icare pctl_polType on pctl_polType.id = PC_PP.PolicyType_icare
		left outer join [DM_STG_PC].dbo.pctl_wclabourhirecode_icare pctl_labour on pctl_labour.ID=PC_PP.WCLabourHireCode_icare
		left outer join [DM_STG_PC].dbo.pc_producercode on pc_producercode.ID=PC_PP.ProducerCodeOfRecordID

		where CD.is_Null=0
		and (PC_PP.ID is not null) 
		) ,

	--Getting policy matches using Claim_detail.Policy_No and matching against pc_policyperiod.policy_number
	temptable_pc_pp_1 as (

		select
		CD.itc_pcent as ITCPercentagePEL,	
		Case When pctl_polLine.name='icare Workers'' Comp' then 'WorkersComp' end as PolicyType,
		pctl_polType.TYPECODE as PolicyType_icare,	
		PC_PP.ID AS PC_PP_Id,
		PC_PP.PeriodStart as PC_PP_EffectiveDate,
		PD.PERIOD_START as PD_EffectiveDate,
		PC_PP.PeriodEnd as PC_PP_ExpirationDate,
		PD.PERIOD_EXPIRY as PD_ExpirationDate,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
		LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
		LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
		PT.icare_policy_no as PT_icare_PolicyNumber,
		PT.ID as PT_ID,
		PT.POLICY_STATUS as PT_Policy_Status,
		PC_PP.TermNumber ,
		'CD Policy_no with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE,
		Case when LEN(cd.Tariff_No) = 3 then cd.Tariff_No else NULL end as TariffRate_icare,
		cctl_EmpCateg.TYPECODE as EmployerCategory_icare,
		Case when pctl_labour.Name='1 - Labour Hire Firm' then 'labour_hire'
			when  pctl_labour.Name='2 - Not a Labour Hire Firm' then 'not_labour_hire'
			when  pctl_labour.Name='3 - Group training aprenticeship scheme' then 'group_training' 
		end as LabourHire_icare,
		pc_producercode.code as ProducerCode,
		pc_producercode.Description as ProducerName_icare,	
		case when PD.policy_Status in (50,52) then 'inforce' END as status,
		cd.Claim_Number as ClaimNumber
		from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
		left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
		on PT.POLICY_NO = CD.Policy_No
		left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
			on PD.POLICY_NO = PT.POLICY_NO
				--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
		-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
			and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
		left outer join 
		(
		select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate,EmployerCategory_icare,PolicyType_icare,WCLabourHireCode_icare,ProducerCodeOfRecordID
		from dm_stg_pc.dbo.pc_policyperiod
		Where status=9 --bound
		and MostRecentModel=1
		and Retired=0
		and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
		) PC_PP
		--on (PT.icare_policy_no=PC_PP.PolicyNumber )
		on (ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber )
		--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
		--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
		--on PT.icare_policy_no=PC_PP.PolicyNumber
		--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
		and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
		left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
		left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		left outer join DM_STG_PC..pctl_employercategory_icare pctl on pctl.ID = PC_PP.EmployerCategory_icare
		left outer join DM_STG_CC.dbo.cctl_employercategory_icare cctl_EmpCateg on cctl_EmpCateg.name = pctl.name
		left outer join [DM_STG_PC].dbo.pc_policyline pline on pline.BranchID = PC_PP.id
		left outer join [DM_STG_PC].dbo.pctl_policyline pctl_polLine on pctl_polLine.id = pline.Subtype
		left outer join [DM_STG_PC].dbo.pctl_policytype_icare pctl_polType on pctl_polType.id = PC_PP.PolicyType_icare
		left outer join [DM_STG_PC].dbo.pctl_wclabourhirecode_icare pctl_labour on pctl_labour.ID=PC_PP.WCLabourHireCode_icare
		left outer join [DM_STG_PC].dbo.pc_producercode on pc_producercode.ID=PC_PP.ProducerCodeOfRecordID

		where CD.is_Null=0
		and (PC_PP.ID is not null)
		) 

	--Getting policy matches using Claim_detail.Policy_No and matching against pc_policyperiod.legacypolicynumber_icare
	, temptable_pc_pp_2 as (
		select
		CD.itc_pcent as ITCPercentagePEL,	
		Case When pctl_polLine.name='icare Workers'' Comp' then 'WorkersComp' end as PolicyType,
		pctl_polType.TYPECODE as PolicyType_icare,	
		PC_PP.ID AS PC_PP_Id,
		PC_PP.PeriodStart as PC_PP_EffectiveDate,
		PD.PERIOD_START as PD_EffectiveDate,
		PC_PP.PeriodEnd as PC_PP_ExpirationDate,
		PD.PERIOD_EXPIRY as PD_ExpirationDate,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
		LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
		LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
		PT.icare_policy_no as PT_icare_PolicyNumber,
		PT.ID as PT_ID,
		PT.POLICY_STATUS as PT_Policy_Status,
		PC_PP.TermNumber ,
		'CD Policy_no with PC_PP LEGACY PolicyNumber match' as POLICY_MATCH_TYPE,
		Case when LEN(cd.Tariff_No) = 3 then cd.Tariff_No else NULL end as TariffRate_icare,
		cctl_EmpCateg.TYPECODE as EmployerCategory_icare,
		Case when pctl_labour.Name='1 - Labour Hire Firm' then 'labour_hire'
			when  pctl_labour.Name='2 - Not a Labour Hire Firm' then 'not_labour_hire'
			when  pctl_labour.Name='3 - Group training aprenticeship scheme' then 'group_training' 
		end as LabourHire_icare,
		pc_producercode.code as ProducerCode,
		pc_producercode.Description as ProducerName_icare,	
		case when PD.policy_Status in (50,52) then 'inforce' END as status,
		cd.Claim_Number as ClaimNumber
		from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
		left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
		on PT.POLICY_NO = CD.Policy_No
		left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
			on PD.POLICY_NO = PT.POLICY_NO
				--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
		-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
			and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
		left outer join 
		(
		select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate,EmployerCategory_icare,PolicyType_icare,WCLabourHireCode_icare,ProducerCodeOfRecordID
		from dm_stg_pc.dbo.pc_policyperiod
		Where status=9 --bound
		and MostRecentModel=1
		and Retired=0
		and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
		) PC_PP
		--on (PT.icare_policy_no=PC_PP.PolicyNumber )
		on (ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
		--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
		--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
		--on PT.icare_policy_no=PC_PP.PolicyNumber
		--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
		and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
		left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
		left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		left outer join DM_STG_PC..pctl_employercategory_icare pctl on pctl.ID = PC_PP.EmployerCategory_icare
		left outer join DM_STG_CC.dbo.cctl_employercategory_icare cctl_EmpCateg on cctl_EmpCateg.name = pctl.name
		left outer join [DM_STG_PC].dbo.pc_policyline pline on pline.BranchID = PC_PP.id
		left outer join [DM_STG_PC].dbo.pctl_policyline pctl_polLine on pctl_polLine.id = pline.Subtype
		left outer join [DM_STG_PC].dbo.pctl_policytype_icare pctl_polType on pctl_polType.id = PC_PP.PolicyType_icare
		left outer join [DM_STG_PC].dbo.pctl_wclabourhirecode_icare pctl_labour on pctl_labour.ID=PC_PP.WCLabourHireCode_icare
   		left outer join [DM_STG_PC].dbo.pc_producercode on pc_producercode.ID=PC_PP.ProducerCodeOfRecordID
		where CD.is_Null=0
		and (PC_PP.ID is not null)
		) 

	--28/01 DMIG-731 Checking to see if we can strip 701 from CD.PolicyNo and try and match it against PC PolicyNumber
	,temptable_pc_pp_3 as (
		select

		CD.itc_pcent as ITCPercentagePEL,	
		Case When pctl_polLine.name='icare Workers'' Comp' then 'WorkersComp' end as PolicyType,
		pctl_polType.TYPECODE as PolicyType_icare,
		PC_PP.ID AS PC_PP_Id,
		PC_PP.PeriodStart as PC_PP_EffectiveDate,
		PD.PERIOD_START as PD_EffectiveDate,
		PC_PP.PeriodEnd as PC_PP_ExpirationDate,
		PD.PERIOD_EXPIRY as PD_ExpirationDate,
		LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
		CD.Date_of_Injury,
		LTRIM(RTRIM(PC_PP.PolicyNumber)) as PC_PP_PolicyNumber,
		LTRIM(RTRIM(PD.Policy_No)) as PD_Legacy_PolicyNumber,
		LTRIM(RTRIM(CD.Policy_No)) as CD_Legacy_PolicyNumberCD,
		PT.icare_policy_no as PT_icare_PolicyNumber,
		PT.ID as PT_ID,
		PT.POLICY_STATUS as PT_Policy_Status,
		PC_PP.TermNumber ,
		'(701) CD Policy_no with PC_PP PolicyNumber match' as POLICY_MATCH_TYPE,	
		Case when LEN(cd.Tariff_No) = 3 then cd.Tariff_No else NULL end as TariffRate_icare,
		cctl_EmpCateg.TYPECODE as EmployerCategory_icare,
		Case when pctl_labour.Name='1 - Labour Hire Firm' then 'labour_hire'
			when  pctl_labour.Name='2 - Not a Labour Hire Firm' then 'not_labour_hire'
			when  pctl_labour.Name='3 - Group training aprenticeship scheme' then 'group_training' 
		end as LabourHire_icare,
		pc_producercode.code as ProducerCode,
		pc_producercode.Description as ProducerName_icare,	
		case when PD.policy_Status in (50,52) then 'inforce' END as status,
		cd.Claim_Number as ClaimNumber
		from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
		left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT 
		on PT.POLICY_NO = CD.Policy_No
		left outer join  DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD
			on PD.POLICY_NO = PT.POLICY_NO
				--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
		-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
			and  CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
		left outer join 
		(
		select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate,EmployerCategory_icare,PolicyType_icare,WCLabourHireCode_icare,ProducerCodeOfRecordID
		from dm_stg_pc.dbo.pc_policyperiod
		Where status=9 --bound
		and MostRecentModel=1
		and Retired=0
		and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
		) PC_PP
		--on (PT.icare_policy_no=PC_PP.PolicyNumber
		on substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3)=PC_PP.PolicyNumber
		--on (PT.icare_policy_no=PC_PP.PolicyNumber AND CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd ) 
		--or ltrim(rtrim(CD.Policy_No))=PC_PP.PolicyNumber or ltrim(rtrim(CD.Policy_No))=PC_PP.LegacyPolicyNumber_icare)
		--on PT.icare_policy_no=PC_PP.PolicyNumber
		--and CD.Date_of_Injury+'00:01:00' between PC_PP.PeriodStart and PC_PP.PeriodEnd
		and CD.Date_of_Injury >= PC_PP.PeriodStart and Date_of_Injury < PC_PP.PeriodEnd
		left outer join DM_STG_PC.dbo.pc_policy PC_P on PC_P.id=PC_PP.PolicyId
		left outer join [DM_STG_PC].dbo.pc_account PC_A on PC_A.ID=PC_P.AccountID
		inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		left outer join DM_STG_PC..pctl_employercategory_icare pctl on pctl.ID = PC_PP.EmployerCategory_icare
		left outer join DM_STG_CC.dbo.cctl_employercategory_icare cctl_EmpCateg on cctl_EmpCateg.name = pctl.name
		left outer join [DM_STG_PC].dbo.pc_policyline pline on pline.BranchID = PC_PP.id
		left outer join [DM_STG_PC].dbo.pctl_policyline pctl_polLine on pctl_polLine.id = pline.Subtype
		left outer join [DM_STG_PC].dbo.pctl_policytype_icare pctl_polType on pctl_polType.id = PC_PP.PolicyType_icare
		left outer join [DM_STG_PC].dbo.pctl_wclabourhirecode_icare pctl_labour on pctl_labour.ID=PC_PP.WCLabourHireCode_icare
		left outer join [DM_STG_PC].dbo.pc_producercode on pc_producercode.ID=PC_PP.ProducerCodeOfRecordID
		where CD.is_Null=0
		and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701')
		and (PC_PP.ID is not null)
		) ,

	--30/01 DMIG-731 Removed the selection SQL for checking to see if we can strip 702 from CD.PolicyNo and try and match it against PC PolicyNumber. 
	--702 policy no/s will never be present in EMICS, they will be present in icare (GW CC only)

	--Consolidating ALL matches together
	--07/11/2018 - REWORKED to  remove duplicates
	--28/01/2019 - DMIG-731 - Added 701 and 702 selection criteria as well
	temptable_consolidated_Verified_PC as (
		select
			ITCPercentagePEL,
			PolicyType,
			PolicyType_icare,
			PC_PP_Id,
			PC_PP_EffectiveDate ,
			PD_EffectiveDate,
			PC_PP_ExpirationDate,
			PD_ExpirationDate,		
			NULL as RENEWAL_NO,
			LUWID,
			Date_of_Injury,
			LTRIM(RTRIM(PC_PP_PolicyNumber)) as PC_PP_PolicyNumber,	
			NULL as Rownum,
			'EMICS:' + LTRIM(RTRIM(ClaimNumber)) + ':POLICY' as publicid
			,0 as ManualVerify_icare,
			--PD_Legacy_PolicyNumber,
			--CD_Legacy_PolicyNumberCD,
			--PT_icare_PolicyNumber,
			--PT_ID,
			--PT_Policy_Status,
			--TermNumber,
			--POLICY_MATCH_TYPE,
			TariffRate_icare,
			EmployerCategory_icare,
			LabourHire_icare,
			ProducerCode,
			ProducerName_icare,
			Status
		from
		(
		select row_number() over(partition by X.LUWID,X.PC_PP_ID  order by X.PD_EffectiveDate desc) as RowNum,
		X.*
			from
			(
				select * from temptable_pc_pp
				union
				(
				select a.* from temptable_pc_pp_1 a
				left outer join temptable_pc_pp b
				on a.LUWID=b.LUWID 
				left outer join temptable_pc_pp_3 c  --28/01 added this join
				on a.LUWID=c.LUWID 
				where b.LUWID is null 
				and c.LUWID is null --28/01 added this
				)
				union
				(
					select a1.* from temptable_pc_pp_2 a1
					left outer  join temptable_pc_pp b1
					on a1.LUWID=b1.LUWID 
					left outer  join temptable_pc_pp_1 c1
					on a1.LUWID =c1.LUWID
					left outer  join temptable_pc_pp_3 d1 --28/01 added this new union
					on a1.LUWID =d1.LUWID

					where b1.PC_PP_Id is  null
					and c1.PC_PP_Id is  null
					and d1.PC_PP_Id is  null
				)
				union --28/01 added this new union (27 missing matches getting picked up now)
				(
					select a1.* from temptable_pc_pp_3 a1
					left outer  join temptable_pc_pp b1
					on a1.LUWID=b1.LUWID 
					left outer  join temptable_pc_pp_1 c1
					on a1.LUWID =c1.LUWID
					left outer  join temptable_pc_pp_2 d1
					on a1.LUWID =d1.LUWID
					where b1.PC_PP_Id is  null
					and c1.PC_PP_Id is  null
					and d1.PC_PP_Id is  null
				)
			)X
	)Y
	where Y.RowNum=1)

	,BTP as (
		select * from temptable_consolidated_Verified_PC PC
		left outer join (select period.ID as Policy_Period_ID, sum(c.ActualAmountBilling) as Total_BTP_icare
		from DM_STG_PC.dbo.pc_policyperiod period left join DM_STG_PC.dbo.pcx_wccost_icare c on c.branchid = period.id
		left join DM_STG_PC.dbo.pctl_wccost_icare st on st.id = c.subtype
		left join DM_STG_PC.dbo.pc_policy policy on period.PolicyID = policy.ID
		where c.ExpirationDate is null and period.MostRecentModel=1 and period.Status=9 and period.Retired=0 and st.TYPECODE in ('WCBasicTariffCost_icare',
		'WCBasicTariffNonPerCapitaCost_icare','WCOtherBasicTariffPerCapitaCost_icare','WCTaxBasicTarrifPerCapitaCost_icare') and period.ID in (select PC_PP_ID from temptable_consolidated_Verified_PC) -- Note: I need TotalBTP only for a given Verified PC Policy Period ID as then it can be linked to the claim that belongs to that policy period id
		group by period.ID ) BTP ON BTP.Policy_Period_ID = PC.PC_PP_Id
	)



------------------------------------******************** *****************************---------------------

select 
	*
	into [ccst_interm_policy_PC] 
	from BTP 
END