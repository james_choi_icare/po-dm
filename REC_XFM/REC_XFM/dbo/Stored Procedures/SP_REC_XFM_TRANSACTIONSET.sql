﻿/* =============================================
-- Author:		Ann
-- Last updated: 04/09/2019
-- Description:	Payments and recoveries v42 
-- Version: 10/06/2019 v1.2 Users and groups RequestingUserID
			04/09/2019 v47.2 Updated checkset publicid xform
-- ============================================= */
CREATE PROCEDURE [dbo].[SP_REC_XFM_TRANSACTIONSET]
	
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_transactionset','Payments and Recoveries - v47.2 04/09/2019'

--DROP TABLE ccst_transactionset


;WITH USERCHECKSET as (
	select distinct 
		PR.Claim_no,
		PR.payment_no,	
		coalesce (cc_user.PublicID,cc_user2.publicid, cc_user_sm.PublicID) as RequestingUserID
		,  CD.is_Null, PR.Estimate_type,PR.Reversed,PR.Garnishee_No ,cpr.Cheque_status
	from (select T.*
			from (Select  pr.*, row_number() over (partition by pr.claim_no, pr.payment_no order by transaction_date desc) as rn
				From DM_STG_EMICS.dbo.PAYMENT_RECOVERY pr 
					LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON PR.Claim_No=CPR.Claim_number
						AND PR.Payment_no=CPR.Payment_no
					WHERE PR.Estimate_type<70 and PR.Reversed<>1
					and PR.Garnishee_No is null and ISNULL(cpr.Cheque_status,'00') <> 6   
			) as T Where rn = 1
		 ) PR
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.Claim_No=CD.Claim_number
		LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON PR.Claim_No=CPR.Claim_number
			AND PR.Payment_no=CPR.Payment_no
		LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE FROM DM_STG_CC.dbo.ccx_paycodepaymenttype_icare A, 
			(select * 
			from DM_STG_CC.dbo.ccx_paycode_icare where Retired=0) B, DM_STG_CC.dbo.cctl_paymenttype_icare C 
			WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)	
		LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
		LEFT JOIN  [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS]  co on co.alias = pr.owner
	
		LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
				FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
				ON (co.Alias = cntrl.ITEM)
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)

		LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co2  ON (co2.alias = cad.Claims_Officer)
			LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
				FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - 
				CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
				ON (co2.Alias = cntrl2.ITEM)
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID)

		left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
		left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)

		WHERE CD.is_Null=0 AND PR.Estimate_type<70 and PR.Reversed<>1
		and PR.Garnishee_No is null and ISNULL(cpr.Cheque_status,'00') <> 6                                                 
), USERRECOVERYSET as (
	select distinct 
		PR.Claim_no,
		PR.payment_no,	
		coalesce (cc_user.PublicID,cc_user2.publicid, cc_user_sm.PublicID) as RequestingUserID
		,  CD.is_Null, PR.Estimate_type,PR.Reversed,PR.Garnishee_No ,cpr.Cheque_status

	from (select T.*
			from (Select  pr.*, row_number() over (partition by pr.claim_no, pr.payment_no order by transaction_date desc) as rn
			From DM_STG_EMICS.dbo.PAYMENT_RECOVERY pr 
			LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON PR.Claim_No=CPR.Claim_number
				AND PR.Payment_no=CPR.Payment_no
			WHERE  (PR.Estimate_type>=70 or isnull(CPR.Cheque_Status,'00')=6 )  and PR.Reversed<>1
		) as T Where rn = 1 ) PR
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.Claim_No=CD.Claim_number
		LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON PR.Claim_No=CPR.Claim_number
			AND PR.Payment_no=CPR.Payment_no
		LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE FROM DM_STG_CC.dbo.ccx_paycodepaymenttype_icare A, 
			(select * 
			from DM_STG_CC.dbo.ccx_paycode_icare where Retired=0) B, DM_STG_CC.dbo.cctl_paymenttype_icare C 
			WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)	
		LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
		LEFT JOIN  [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS]  co on co.alias = pr.owner
	
		LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
				FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
				ON (co.Alias = cntrl.ITEM)
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID)

		LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co2  ON (co2.alias = cad.Claims_Officer)
			LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
				FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - 
				CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl2
				ON (co2.Alias = cntrl2.ITEM)
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential2 on (cntrl2.EML_Email = cc_credential2.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user2 on (cc_user2.CredentialID = cc_credential2.ID)

		left join [DM_STG_EMICS].[dbo].[CLAIM_TRIAGE] ct on (cd.Claim_Number = ct.Claim_No)
		left join DM_XFM.[dbo].[DM_LKP_EML_SEGMENTMANAGER] lkp_eml_sm on ct.Segment_Code = lkp_eml_sm.segment_code
		left join DM_STG_CC.[dbo].[cc_credential] cc_credential_sm on (lkp_eml_sm.Segment_Manager_Email = cc_credential_sm.UserName)
		left join DM_STG_CC.[dbo].[cc_user] cc_user_sm on (cc_user_sm.CredentialID = cc_credential_sm.ID)
		WHERE CD.is_Null=0 AND (PR.Estimate_type>=70 or isnull(CPR.Cheque_Status,'00')=6 )  and PR.Reversed<>1
                                  
), checkset as (

	select DISTINCT 
		CPR.Authorised_dte AS ApprovalDate,
		'approved' as ApprovalStatus,
		CASE WHEN ISNULL(nullif(PR.Adjust_Trans_Flag,''),'N')='Y' THEN 1 ELSE 0 END AS AdjustmentPayment_icare,
		'EMICS:' + rtrim(cpr.Claim_number) as ClaimID,
		NULL as DocumentLinkableID,
		pr.Claim_No as LUWID,
		case when cpc.PAYMENTTYPE_ICARE is null and pr.estimate_type=55 then 'Invoice' ELSE CPC.PAYMENTTYPE_ICARE  
			END as Paymenttype_icare,
		NULL as RecurrenceID,
		'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,pr.Payment_no)+'_'+case when ISNULL(nullif(PR.Adjust_Trans_Flag,''),'N')='Y' then '1' else '0' end
			+'_'+isnull(convert(varchar,cpc.ID),'00')+':CHECKSET' AS PublicID,
		cast('CheckSet' as varchar(20)) as Subtype,
		uag.RequestingUserID
	
	from (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* 
		from DM_STG_EMICS.dbo.Payment_Recovery) PR
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.Claim_No=CD.CLAIM_NUMBER
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON PR.Claim_No=CPR.Claim_number
		AND PR.Payment_no=CPR.Payment_no
	LEFT OUTER JOIN (SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_XFM..DM_LKP_GW_CCX_paycodepaymenttype_icare A,
		 (select * from DM_XFM..DM_LKP_GW_CCX_paycode_icare where Retired=0) B,DM_XFM..DM_LKP_GW_cctl_paymenttype_icare C
		WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)
	
	LEFT JOIN USERCHECKSET uag on uag.Payment_no = pr.Payment_no and uag.claim_no = pr.claim_no --and uag.Estimate_type = pr.Estimate_type -- and cpc.ID = uag.cpc_id and uag.Estimate_type = pr.Estimate_type
	WHERE CD.is_Null=0 AND PR.Estimate_type<70 and PR.Reversed<>1
	and PR.Garnishee_No is null
	AND ISNULL(CPR.Cheque_Status,'00')<>6

) , recoveryset as (

	select 	DISTINCT 
		CPR.Authorised_dte AS ApprovalDate,
		'approved' as ApprovalStatus,
		'0' as AdjustmentPayment_icare,
		'EMICS:' + rtrim(cpr.Claim_number) as ClaimID,
		NULL as DocumentLinkableID,
		pr.Claim_No as LUWID,
		null AS Paymenttype_icare,
		NULL as RecurrenceID,
		'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+convert(varchar,PR.Estimate_type)+'_'+'0'+':RECOVERYSET' as PublicID,
		'RecoverySet' as Subtype,
		uag.RequestingUserID

	from 
	DM_STG_EMICS.dbo.Payment_Recovery PR
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	ON PR.Claim_No=CD.CLAIM_NUMBER
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	ON PR.Claim_No=CPR.Claim_number
	AND PR.Payment_no=CPR.Payment_no
	LEFT JOIN USERRECOVERYSET uag on uag.claim_no = pr.claim_no and uag.payment_no = pr.payment_no-- and pr.Estimate_type = uag.Estimate_type
	WHERE CD.is_Null=0 AND (PR.Estimate_type>=70 or isnull(CPR.Cheque_Status,'00')=6)  and PR.Reversed<>1
), Reserveset as (
	
	select DISTINCT 
		CPR.Authorised_dte AS aPPROVALDATE,
		'approved' as ApprovalStatus,
		'0' as AdjustmentPayment_icare,
		'EMICS:' + rtrim(cpr.Claim_number) as ClaimID,
		NULL as DocumentLinkableID,
		pr.Claim_No as LUWID,
		null AS Paymenttype_icare,
		NULL as RecurrenceID,
		'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+convert(varchar,PR.Estimate_type)+'_'+'0'+':RESERVESET' AS PublicID,
		'ReserveSet' as Subtype,
		RequestingUserID

	from DM_STG_EMICS.dbo.Payment_Recovery PR
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.Claim_No=CD.CLAIM_NUMBER
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	ON PR.Claim_No=CPR.Claim_number
	AND PR.Payment_no=CPR.Payment_no
	LEFT JOIN USERCHECKSET uag on uag.claim_no = pr.Claim_No and uag.Payment_no = pr.Payment_no --and pr.Estimate_type = uag.Estimate_type
	WHERE CD.is_Null=0 AND PR.Estimate_type<70  and PR.Reversed<>1 and PR.Garnishee_No is null AND ISNULL(CPR.Cheque_Status,'00')<>6 --1337410

), RecoveryReserveset as (

select 
DISTINCT 
	CPR.Authorised_dte AS aPPROVALDATE,
	'approved' as ApprovalStatus,
	'0' as AdjustmentPayment_icare,
	'EMICS:' + rtrim(cpr.Claim_number) as ClaimID,
	NULL as DocumentLinkableID,
	pr.Claim_No as luwid,
	null AS Paymenttype_icare,
	NULL as RecurrenceID,
	'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+convert(varchar,PR.Estimate_type)+'_'+'0'+':RECOVERYRESERVESET' AS PublicID,
	'RecoveryReserveSet' as Subtype,
	RequestingUserID
from 
DM_STG_EMICS.dbo.Payment_Recovery PR
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
ON PR.Claim_No=CD.CLAIM_NUMBER
LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
ON PR.Claim_No=CPR.Claim_number
AND PR.Payment_no=CPR.Payment_no
LEFT JOIN USERRECOVERYSET uag on uag.claim_no = pr.Claim_No and uag.Payment_no = pr.Payment_no --and pr.Estimate_type = uag.Estimate_type
WHERE CD.is_Null=0 AND (PR.Estimate_type>=70 or isnull(CPR.Cheque_Status,'00')=6 )  and PR.Reversed<>1
), transactionset as 
(
	Select * From checkset 
		UNION
	select * from recoveryset
		UNION
	Select * from Reserveset
		UNION
	Select * from RecoveryReserveset
)
Select * into ccst_transactionset 
from transactionset 


END