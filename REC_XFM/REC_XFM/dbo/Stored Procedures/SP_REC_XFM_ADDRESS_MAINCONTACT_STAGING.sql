﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 20/06/2019
-- Description:	Address - Main Contact V002.17
-- 1. This is the child Stored Proc for MAIN CONTACT address that will called by the master stored proc SP_REC_XFM_ADDRESS
-- Currently this has been developed only for MATCHED main contacts
-- 2. <15-Jul-2019> <Satish>: Added code for creating addresses for E,R (verified PC) UNMATCHED main contacts addresses
-- 3. <17-Jul-2019> <Satish>: Added code for creating addresses for Rest of all (Verified CDR) UNMATCHED main contacts addresses
-- 4. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 5. <25Jul2019><Satish>: Fixed a bug for (DMIG-7520) "State" in MAINCONTACT_all_matched section.
-- 6. <25Jul2019><Satish>: Fixed a bug for address mismatch (DMIG-7471) in MAINCONTACT_unmatched section.
-- 7. <31Jul2019><Satish>:
--    (a) Applied a fix to set addresstype to 'postal_icare' for ER Unmatched based on DMIG-7469 - as suggested by mapper
--    (b) Applied a fix to set addresstype to 'postal_icare' for Rest of all unmatched Verified CDR based on DMIG-7469 - as suggested by mapper
-- 8. <05Aug2019><Satish>:
--    (a) Applied a fix to Main contact - All matched; set addresstype to TYPECODE based out of abtl_addresstype rather than hardcoding to 'business'. 
--        This is as per latest mapping changes done by mapper on Main Contact ver 2.25. QA is currently showing differences because ETL has 
--        implemented this change while QA has not yet done so.
--    (b) Applied a fix to Main contact - All matched;set address to TYPECODE based out of abtl_address rather than setting it based on
--        abst_address.SubType.
-- 9. <26Aug2019><Satish>: As part of DMIG-8037 -
--    (a) Within temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched section below, when we get State from EMICS Person_Clm table, unfortunatley the
--        format is not of required Guidewire format. For example instead of 'AU_NSW', it is just 'NSW' in EMICS Person_Clm table. I have now fixed this.
--        Dev data looks fine and its only QA that needs a fix.
-- 10. <27Aug2019><Satish>: As part of [DMIG-7471 (DMIG-8048)] -
--     (a) Fixed a bug for address mismatch in MAINCONTACT_unmatched section. We need to use max(claimnumber) when matching with PC PolicyLocation table.
-- 11. <27Aug2019><Satish>: As part of [DMIG-7402 (DMIG-8049)]
--     (a) Fixed a bug for address mismatch in MAINCONTACT_unmatched section. We need to use EMICS POLICY_TERM_DETAIL Postal Attributes instead of PolicyLocation.
--         PolicyLocation uses STREET (business) addresses whereas for Main Contacts  - both unmatched and Verified CDR, we need to use POSTAL attributes only.
--         This change overrides changes done as part of 10. above.
-- 12. <03Sep2019><Satish>: As part of DMIG-8117 -
--     (a) Within temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched and temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched sections below, when we 
--         get State from EMICS POLICY_TERM_DETAIL table, unfortunatley the format is not of required Guidewire format. For example instead of 'AU_NSW', it is just 
--         'NSW' in EMICS POLICY_TERM_DETAIL table. I have now fixed this. Dev data looks fine and its only QA that needs a fix.
-- 13. <04Sep2019><Satish>: As part of [DMIG-7471 (DMIG-8119)] -
--     (a) Within temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched and temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched sections below, when we 
--         source City,State and PostalCode from EMICS Person_Clm, we need to additionally check if pc.Address_Line1 is not null as well. Currently this is not being 
--         done and so we are seeing some differences between XFM and QA. This has been fixed now.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_ADDRESS_MAINCONTACT_STAGING]
	
AS
BEGIN

IF OBJECT_ID('AddressREF_temptable_maincontact_all_matched') is not null
drop table dbo.AddressREF_temptable_maincontact_all_matched;


IF OBJECT_ID('AddressREF_temptable_Address_maincontact_ER_verifiedPC_unmatched') is not null
drop table dbo.AddressREF_temptable_Address_maincontact_ER_verifiedPC_unmatched;

IF OBJECT_ID('AddressREF_temptable_Address_maincontact_rest_of_all_verifiedCDR_unmatched') is not null
drop table dbo.AddressREF_temptable_Address_maincontact_rest_of_all_verifiedCDR_unmatched;

IF OBJECT_ID('temptable_Address_MAINCONTACT_all_matched') is not null
drop table temptable_Address_MAINCONTACT_all_matched;

IF OBJECT_ID('temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched') is not null
drop table temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched;

IF OBJECT_ID('temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched') is not null
drop table temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched;


--Getting addresses for matched ER PPLC, matched ER non-PPLC, WOP PPLC
SELECT * INTO temptable_Address_MAINCONTACT_all_matched
FROM
(
select
distinct
--'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ': INSURED:BUSINESSADDRESS' as PublicID
'EMICS:' + QA_contact_ref_staging.LUWID + ':INSURED_MC:BUSINESSADDRESS' as PublicID
,CM_A.LinkID as AddressBookUID
,CM_A.AddressLine1
--,CM_A.AddressLine1Kanji
,NULL as AddressLine1Kanji
,CM_A.AddressLine2
--,CM_A.AddressLine2Kanji
,NULL as AddressLine2Kanji
,CM_A.AddressLine3
--,CM_A.AddressType
,CM_AddressType.TYPECODE as  AddressType-- 05Aug2019 Enabled this part as part of mapping change Main Contact v 2.25
--,'business' as  AddressType-- 05Aug2019-Commented this as part of mapping change Main Contact v 2.25
,CM_A.BatchGeocode
--,CM_A.CEDEX
,NULL as CEDEX
--,CM_A.CEDEXBureau
,NULL as CEDEXBureau
,CM_A.City
--,CM_A.CityKanji
,NULL as CityKanji
--,CM_A.Country
,ABTL_CNY.TYPECODE as Country--Eg AU
--,CM_A.County
,NULL as County
,NULL as DPID_icare
,CM_A.Description as Description
,NULL as ExternalLinkID
--,CM_A.IsAddressFromPC_icare as IsAddressFromPC_icare  --20/06/2019: Need to raised a defect against mapper as this attribute is not there in CM_A
,1 as IsAddressFromPC_icare
,CM_A.IsValidated_icare
,0 as ObfuscatedInternal
,CM_A.PostalCode
--,CM_A.PublicID as CM_A_PublicID
--,0 as Retired
,CM_A.Retired as  Retired
--,NULL as StandardizedAddressID_icare
,CM_A.StandardizedAddressID_icare as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW  --20/06/2019 : Need to change this to below in other SPs
--,CM_A.State as State--eg AU_NSW
,ABTL_State.TYPECODE as State --25Jul2019 - Fixed this as we need to use TYEPCODE - we are currnetky getting 920 differences in LD
--,CM_A.Subtype
--,CM_A.Subtype as SubType -- eg Address --05Aug2019 Commented this out as this is incorrect
,ABTL_Address.TYPECODE as SubType -- 05aug2019 Added this as QA and ETL were showing differences. ETL was correct.
,CM_A.ValidUntil
--,ltrim(rtrim(CD.Claim_Number)) as LUWID
,QA_contact_ref_staging.LUWID
FROM
(
select LUWID,AddressBookUID
from
(
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null
union
select LUWID,AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null
)X
) QA_contact_ref_staging
--14/02/2019-DMIG-3766-joining all CM tables now to source matched LinkID contacts from CM instead of PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = QA_contact_ref_staging.AddressBookUID --20/06/2019 -- Doing the join from the contact QA ref staging
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = QA_contact_ref_staging.AddressBookUID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
-- LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 -- LEFT OUTER JOIN select * from [DM_STG_CM].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from DM_XFM.[dbo].[DM_XFM_EFTDATA] where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--where CD.is_Null = 0
--where QA_contact_ref_staging.LUWID in (1789627,1790419) 
)X

--drop table temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched
--Getting addresses for unmatched ER --  (verified PC)
SELECT * INTO temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched
FROM
(
select
distinct
--'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ': INSURED:BUSINESSADDRESS' as PublicID
'EMICS:' + cont_unmatched.LUWID + ':INSURED_MC:BUSINESSADDRESS' as PublicID
,NULL as AddressBookUID
,CASE
WHEN 
LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
--LTRIM(RTRIM(pc.[Address_Line1])) is not null
then LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), '')))
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) ='' 
--then ltrim(rtrim(pl_add.[AddressLine1]))
then REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( case WHEN ISNULL(ptd.[POSTAL_STREET],'') <> '' THEN ltrim(rtrim(replace(ptd.POSTAL_STREET,ptd.LEGAL_NAME,'')))/*ver6*/  end,'  ','<>'),'><',' '),'  ',''),'< >',' '),'<>',' ')
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
ELSE NULL
END as [AddressLine1] 
--,CM_A.AddressLine1Kanji
,NULL as AddressLine1Kanji
--,replace(ltrim(replace(ptd.POSTAL_STREET,ptd.LEGAL_NAME,'')),'char(10)','')
--CASE
--WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) =''
--and ltrim(rtrim(pl_add.[AddressLine2])) is not null then ltrim(rtrim(pl_add.[AddressLine2]))
--ELSE NULL
--END as AddressLine2
,NULL as AddressLine2 --27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
--,CM_A.AddressLine2Kanji
,NULL as AddressLine2Kanji
--,CM_A.AddressLine3
,
--CASE
--WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) =''
--and ltrim(rtrim(pl_add.[AddressLine3])) is not null then ltrim(rtrim(pl_add.[AddressLine3]))
--ELSE NULL
--END as AddressLine3
NULL as AddressLine3 --27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
--,CM_A.AddressType
--,CM_AddressType.TYPECODE as  AddressType-- eg postal
--,'business' as  AddressType-- eg postal
,'postal_icare' as AddressType --31Jul2019 - As part of mapper feeback on DMIG-7469, making this change on QA end too
--,CM_A.BatchGeocode
,0 as BatchGeocode
--,CM_A.CEDEX
,NULL as CEDEX
--,CM_A.CEDEXBureau
,NULL as CEDEXBureau
--,CM_A.City
,
CASE
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
--04/09/2019 Added the extra check above as part of DMIG-7471(DMIG-8119)
and 
LTRIM(RTRIM(REPLACE(REPLACE(pc.[Suburb], CHAR(13), ' '), CHAR(10), ''))) <>''
then LTRIM(RTRIM(REPLACE(REPLACE(pc.[Suburb], CHAR(13), ' '), CHAR(10), '')))
--WHEN ltrim(rtrim(pl_add.City)) is not null then ltrim(rtrim(pl_add.City))
WHEN ltrim(rtrim(PTD.POSTAL_LOCALITY)) is not null then ltrim(rtrim(PTD.POSTAL_LOCALITY))
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
ELSE NULL
END as City
--,CM_A.CityKanji
,NULL as CityKanji
--,CM_A.Country
--,ABTL_CNY.TYPECODE as Country--Eg AU
,'AU' as Country
--,CM_A.County
,NULL as County
,NULL as DPID_icare
--,CM_A.Description as Description
,NULL as Description
,NULL as ExternalLinkID
--,CM_A.IsAddressFromPC_icare as IsAddressFromPC_icare  --20/06/2019: Need to raised a defect against mapper as this attribute is not there in CM_A
,0 as IsAddressFromPC_icare
--,CM_A.IsValidated_icare
, 1 as IsValidated_icare
,0 as ObfuscatedInternal
--,CM_A.PostalCode
,
CASE 
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
--04/09/2019 Added the extra check above as part of DMIG-7471(DMIG-8119)
and
ltrim(rtrim(CAST(pc.[Postcode] AS varchar)))  IS not null and ltrim(rtrim(CAST(pc.[Postcode] AS varchar))) <>'0'
then CAST(pc.[Postcode] AS varchar)
--WHEN ltrim(rtrim(pl_add.postalcode)) is not null then ltrim(rtrim(pl_add.postalcode))
WHEN ltrim(rtrim(PTD.POSTAL_POST_CODE)) is not null then ltrim(rtrim(PTD.POSTAL_POST_CODE))
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
END as [PostalCode]
--,CM_A.PublicID as CM_A_PublicID
,0 as Retired
--,CM_A.Retired as  Retired
,NULL as StandardizedAddressID_icare
--,CM_A.StandardizedAddressID_icare as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW  --20/06/2019 : Need to change this to below in other SPs
--,CM_A.State as State--eg AU_NSW
--26Aug2019 As part of DMIG-8037 modified the below logic to ensure it matched expected Guidewire format.
--04/09/2019 Added the extra check below when sourcing from Person_Clm to additionally check if pc.Address_Line1 is not null 
--this is as part of DMIG-7471(DMIG-8119)
,CASE 
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
and 
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='NSW' then 'AU_NSW'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
and
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='ACT' then 'AU_ACT'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
and
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='SA' then 'AU_SA'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='VIC' then 'AU_VIC'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='WA' then 'AU_WA'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
and
LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='QLD' then 'AU_QLD'
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
--LTRIM(RTRIM(pc.[State]))
--WHEN ltrim(rtrim(pl_add.State)) is not null then ltrim(rtrim(pl_add.State))
--WHEN ltrim(rtrim(PTD.POSTAL_POST_CODE)) is not null then ltrim(rtrim(PTD.POSTAL_POST_CODE))
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='NSW' then 'AU_NSW'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='ACT' then 'AU_ACT'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='SA' then 'AU_SA'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='VIC' then 'AU_VIC'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='WA' then 'AU_WA'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='QLD' then 'AU_QLD'
--03Sep2019 Added this as part of DMIG-7520 (DMIG-8117)
END as [State]  
--,CM_A.Subtype as SubType -- eg Address
,'Address' as SubType
--,CM_A.ValidUntil
,NULL as ValidUntil
--,ltrim(rtrim(CD.Claim_Number)) as LUWID
,cont_unmatched.LUWID
FROM
ContactREF_temptable_MC_ER_VerifiedPC_unmatched cont_unmatched
inner join DM_STG_EMICS.dbo.CLAIM_DETAIL CD
--on ltrim(rtrim(CD.Claim_Number))=cont_unmatched.LUWID
on ltrim(rtrim(CD.Claim_Number))=substring(cont_unmatched.LinkID,1,patindex('%M%',cont_unmatched.LinkID)-1)
--25Jul2019 As part of triaging DMIG-7471, found this bug - we need to match on max(Claimnumber) for the group of claims 
inner join [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
left outer join [dbo].[AddressREF_temptable_policylocation_all] pl_add --trying to see if we have an address from PC PolicyLocation 
--on pl_Add.LUWID=cont_unmatched.LUWID
on pl_Add.LUWID=substring(cont_unmatched.LinkID,1,patindex('%M%',cont_unmatched.LinkID)-1)
left outer join DM_STG_EMICS.dbo.POLICY_TERM_DETAIL PTD on PTD.POLICY_NO=CD.Policy_No
--27Aug2019 As part of triaging DMIG-7402 (DMIG-8049), found this bug - we need to use POLICY_TERM_DETAIL POSTAL address attributes 
-- and so cannot rely on POLICYLOCATION as it has BUSINESS address attributes.
--where cont_unmatched.LUWID=1911304
)X


--drop table temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched
--Getting addresses for rest of all unmatched --  (verified CDR)
SELECT * INTO temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched
FROM
(
select
distinct
--'EMICS:' + ltrim(rtrim(CD.Claim_Number)) + ': INSURED:BUSINESSADDRESS' as PublicID
'EMICS:' + rest_unmatched.LUWID + ':INSURED_MC:BUSINESSADDRESS' as PublicID
,NULL as AddressBookUID
,CASE
WHEN 
LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>''
--LTRIM(RTRIM(pc.[Address_Line1])) is not null
then LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), '')))
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) ='' 
--then ltrim(rtrim(pl_add.[AddressLine1]))
then REPLACE(REPLACE(REPLACE(REPLACE(REPLACE( case WHEN ISNULL(ptd.[POSTAL_STREET],'') <> '' THEN ltrim(rtrim(replace(ptd.POSTAL_STREET,ptd.LEGAL_NAME,'')))/*ver6*/  end,'  ','<>'),'><',' '),'  ',''),'< >',' '),'<>',' ')
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
ELSE NULL
END as [AddressLine1] 
--,CM_A.AddressLine1Kanji
,NULL as AddressLine1Kanji
--,CASE
--WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) =''
--and ltrim(rtrim(pl_add.[AddressLine2])) is not null then ltrim(rtrim(pl_add.[AddressLine2]))
--ELSE NULL
--END as AddressLine2
,NULL as AddressLine2 --27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
--,CM_A.AddressLine2Kanji
,NULL as AddressLine2Kanji
--,CM_A.AddressLine3
--,CASE
--WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) =''
--and ltrim(rtrim(pl_add.[AddressLine3])) is not null then ltrim(rtrim(pl_add.[AddressLine3]))
--ELSE NULL
--END as AddressLine3
,NULL as AddressLine3 --27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
--,CM_A.AddressType
--,CM_AddressType.TYPECODE as  AddressType-- eg postal
--,'business' as  AddressType-- eg postal
,'postal_icare' as AddressType --31Jul2019 - As part of mapper feeback on DMIG-7469, making this change on QA end too
--,CM_A.BatchGeocode
,0 as BatchGeocode
--,CM_A.CEDEX
,NULL as CEDEX
--,CM_A.CEDEXBureau
,NULL as CEDEXBureau
--,CM_A.City
,CASE
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
--04/09/2019 Added the extra check above when sourcing from Person_Clm to additionally check if pc.Address_Line1 is not null 
--this is as part of DMIG-7471(DMIG-8119)
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[Suburb], CHAR(13), ' '), CHAR(10), ''))) <>''
then LTRIM(RTRIM(REPLACE(REPLACE(pc.[Suburb], CHAR(13), ' '), CHAR(10), '')))
--WHEN ltrim(rtrim(pl_add.City)) is not null then ltrim(rtrim(pl_add.City))
WHEN ltrim(rtrim(PTD.POSTAL_LOCALITY)) is not null then ltrim(rtrim(PTD.POSTAL_LOCALITY))
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
ELSE NULL
END as City
--,CM_A.CityKanji
,NULL as CityKanji
--,CM_A.Country
--,ABTL_CNY.TYPECODE as Country--Eg AU
,'AU' as Country
--,CM_A.County
,NULL as County
,NULL as DPID_icare
--,CM_A.Description as Description
,NULL as Description
,NULL as ExternalLinkID
--,CM_A.IsAddressFromPC_icare as IsAddressFromPC_icare  --20/06/2019: Need to raised a defect against mapper as this attribute is not there in CM_A
,0 as IsAddressFromPC_icare
--,CM_A.IsValidated_icare
, 1 as IsValidated_icare
,0 as ObfuscatedInternal
--,CM_A.PostalCode
,CASE 
WHEN LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
--04/09/2019 Added the extra check above when sourcing from Person_Clm to additionally check if pc.Address_Line1 is not null 
--this is as part of DMIG-7471(DMIG-8119)
and ltrim(rtrim(CAST(pc.[Postcode] AS varchar)))  IS not null and ltrim(rtrim(CAST(pc.[Postcode] AS varchar))) <>'0'
then CAST(pc.[Postcode] AS varchar)
--WHEN ltrim(rtrim(pl_add.postalcode)) is not null then ltrim(rtrim(pl_add.postalcode))
WHEN ltrim(rtrim(PTD.POSTAL_POST_CODE)) is not null then ltrim(rtrim(PTD.POSTAL_POST_CODE))
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)
END as [PostalCode]
--,CM_A.PublicID as CM_A_PublicID
,0 as Retired
--,CM_A.Retired as  Retired
,NULL as StandardizedAddressID_icare
--,CM_A.StandardizedAddressID_icare as StandardizedAddressID_icare
--,CM_A.State
--,PC_State.TYPECODE as State--eg AU_NSW  --20/06/2019 : Need to change this to below in other SPs
--,CM_A.State as State--eg AU_NSW
--04/09/2019 Added the extra check below when sourcing from Person_Clm to additionally check if pc.Address_Line1 is not null 
--this is as part of DMIG-7471(DMIG-8119)
,CASE 
--when LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> '' then LTRIM(RTRIM(pc.[State]))
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='NSW' then 'AU_NSW'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='ACT' then 'AU_ACT'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='SA' then 'AU_SA'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='VIC' then 'AU_VIC'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='WA' then 'AU_WA'
when LTRIM(RTRIM(REPLACE(REPLACE(pc.[Address_Line1], CHAR(13), ' '), CHAR(10), ''))) <>'' 
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))<> ''
and LTRIM(RTRIM(REPLACE(REPLACE(pc.[State], CHAR(13), ' '), CHAR(10), '')))='QLD' then 'AU_QLD'
--27Aug2019 Added this as part of DMIG-7402 (DMIG-8049)END as [State]  
--WHEN ltrim(rtrim(pl_add.State)) is not null then ltrim(rtrim(pl_add.State))
--WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null then ltrim(rtrim(PTD.POSTAL_STATE))
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='NSW' then 'AU_NSW'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='ACT' then 'AU_ACT'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='SA' then 'AU_SA'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='VIC' then 'AU_VIC'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='WA' then 'AU_WA'
WHEN ltrim(rtrim(PTD.POSTAL_STATE)) is not null and ltrim(rtrim(PTD.POSTAL_STATE))='QLD' then 'AU_QLD'
--03Sep2019 Added this as part of DMIG-7520 (DMIG-8117)
END as [State]  
--,CM_A.Subtype as SubType -- eg Address
,'Address' as SubType
--,CM_A.ValidUntil
,NULL as ValidUntil
--,ltrim(rtrim(CD.Claim_Number)) as LUWID
,rest_unmatched.LUWID
FROM
[dbo].[ContactREF_temptable_MC_rest_of_all_VerifiedCDR] rest_unmatched
inner join DM_STG_EMICS.dbo.CLAIM_DETAIL CD
on ltrim(rtrim(CD.Claim_Number))=rest_unmatched.LUWID
inner join [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
left outer join [dbo].[AddressREF_temptable_policylocation_all] pl_add --trying to see if we have an address from PC PolicyLocation 
on pl_Add.LUWID=rest_unmatched.LUWID
left outer join DM_STG_EMICS.dbo.POLICY_TERM_DETAIL PTD on PTD.POLICY_NO=CD.Policy_No
--27Aug2019 As part of triaging DMIG-7402 (DMIG-8049), found this bug - we need to use POLICY_TERM_DETAIL POSTAL address attributes
-- and so cannot rely on POLICYLOCATION as it has BUSINESS address attributes.
)X


--select * from [dbo].[AddressREF_temptable_policylocation_all] pl_add
--where LUWID=1023290
--,

--Use [REC_XFM]
--Go
;

select
*
into dbo.AddressREF_temptable_maincontact_all_matched
from
(
select * 
from temptable_Address_MAINCONTACT_all_matched
)x
;

select
*
into dbo.AddressREF_temptable_Address_maincontact_ER_verifiedPC_unmatched
from
(
select * 
from temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched
)x
;

select
*
into dbo.AddressREF_temptable_Address_maincontact_rest_of_all_verifiedCDR_unmatched
from
(
select * 
from temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched
)x
;



IF OBJECT_ID('temptable_Address_MAINCONTACT_all_matched') is not null
drop table temptable_Address_MAINCONTACT_all_matched;


IF OBJECT_ID('temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched') is not null
drop table temptable_Address_MAINCONTACT_ER_verifiedPC_unmatched;

IF OBJECT_ID('temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched') is not null
drop table temptable_Address_MAINCONTACT_rest_of_all_verifiedCDR_unmatched;

END