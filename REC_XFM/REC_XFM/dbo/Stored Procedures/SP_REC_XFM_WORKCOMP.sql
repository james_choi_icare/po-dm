﻿-- =============================================
-- Author:		Ann
-- Last updated: 23/04/2019
-- Description:	TRIAGE ccst_workcomp v35
-- Version: v36.2 updated timelostreport xform rules
--			09/07/2019 v39 updated to latest rules and sc
--			09/09/2019 v31 updated ReasonableExcuse_icare JOINS to bring in the last excuse code on the claim, Updated medicalreport case statement to include injury code 2 and 3, updated timelossreport logic
-- =============================================

CREATE PROCEDURE [dbo].[SP_REC_XFM_WORKCOMP]
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_workcomp','Triage - v41 09/07/2019'
--drop table ccst_workcomp
SELECT DISTINCT
   ltrim(rtrim(cd.Claim_Number)) AS LUWID
   ,'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':WORKCOMP' AS PublicID
   ,0 AS Retired
		,CASE WHEN medRep1.Claim_No is not null THEN 1
				WHEN medRep2.Claim_No is not null THEN 1
				WHEN CAD.WPI > 0 THEN 1
				WHEN CD.Result_of_Injury_Code in (1,2,3) THEN 1
				WHEN CAD.Claim_Liability_Indicator IN (2,5,7,8,10,11) THEN 1
				WHEN CAD.Claim_Liability_Indicator = 9 AND CAD.Reasonable_Excuse_Code <> 8 THEN 1
				ELSE 0 END AS MedicalReport
	   ,ltrim(rtrim(cd.Injury_comment)) AS EquipmentUsed
	  -- ,(SELECT FORMAT(Reasonable_Excuse_Code, '0#') AS  Reasonable_Excuse_Code FROM DM_STG_EMICS.dbo.CAD_AUDIT WHERE Claim_Liability_Indicator = 9 AND Claim_No = cd.Claim_Number AND ID = (SELECT MAX(ID) FROM DM_STG_EMICS.dbo.CAD_AUDIT 
			--WHERE Claim_Liability_Indicator = 9 AND Claim_No = cd.Claim_Number)) AS ReasonableExcuse_icare 
		,rsn.ReasonableExcuse_icare
	   ,SUBSTRING(ltrim(rtrim(cd.Injury_Comment_How)),1,1333) AS ActivityPerformed
	   ,FORMAT(cad.Claim_Liability_Indicator,'0#') AS Compensable
	   ,ltrim(rtrim(ur2.OtherTriageQuestions_icareID)) AS OtherTriageQuestions_icareID
	   ,CASE WHEN cad.Claim_Liability_Indicator=7 THEN convert(datetime2,cad.Date_of_liability)
		ELSE NULL END AS [FullDenialEffectiveDate]
	   ,null AS [JurisdictionClaimNumber]
	   ,ltrim(rtrim(cd.Claim_Number)) AS [InsuredReportNumber]
	   ,ltrim(rtrim(ur1.EmpTriageQuestions_icareID)) AS [EmpTriageQuestions_icareID]
	   ,ct.Segment_Reason_FreeText AS [TriageQuestionsSummary_icare]
	   ,CASE WHEN cd.Date_Deceased is null THEN 0 Else 1 END AS [DeathReport]
	   ,ltrim(rtrim(ur4.InjTriageQuestions_icareID)) AS [InjTriageQuestions_icareID]
	   ,ltrim(rtrim(ur3.DocTriageQuestions_icareID)) AS [DocTriageQuestions_icareID]
	   ,FORMAT(cd.Accident_Location_Code,'0#') AS [AccidentLocationType_icare]
	   ,CASE WHEN isnull(ltrim(rtrim(cde.Auth_Contact_Relationship1)),'')='' Then cde.Auth_Contact_Relationship2
			ELSE cde.Auth_Contact_Relationship1 
			END AS [RelationshipToTheInjured_icare]
	   ,case when pr.claim_no is not null then '1' else '0' end as EmployerLiability
	 --  ,CASE WHEN (SELECT COUNT(*)
		--	FROM DM_STG_CDR..DM_STG_CDR_D_CLAIM_MASTER clmmstr
		--		INNER JOIN DM_STG_CDR..DM_STG_CDR_D_COMPANY comp ON clmmstr.MANAGING_COMPANY_ID = comp.COMPANY_ID
		--		INNER JOIN DM_STG_CDR..DM_STG_CDR_E_ROLLING_CLAIM clm ON clm.CLAIM_WCA_ID = clmmstr.CLAIM_WCA_ID
		--	WHERE clm.WCA_STATUS_ID = 181644
		--		AND comp.SOURCE_INSURER_ID = 702
		--		AND ISNULL(clm.CEASED_WORK_DATE_ID,0) <> 0
		--		AND LEN(clmmstr.SOURCE_CLAIM_NUMBER) > 3
		--		AND LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3) = LTRIM(RTRIM(cd.Claim_Number))) > 0 
		--	THEN 1	
		--	ELSE 0
		--END AS TimeLostReport	
		,CASE WHEN tlr.CLAIM_NUMBER is not null THEN 1 
			WHEN (Select SUM(trans_amount) from DM_STG_EMICS..Payment_Recovery pr where pr.claim_no = cd.claim_number and estimate_type = '50' and Reversed<>1) is not null
				then 1
			ELSE 0 END TimeLossReport
	
	INTO ccst_workcomp

	FROM DM_STG_EMICS.dbo.[CLAIM_DETAIL] cd
		--INNER JOIN DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 
		LEFT JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE CT ON cd.claim_number = ct.Claim_No
		LEFT JOIN DM_STG_EMICS.[dbo].[CLAIM_ACTIVITY_DETAIL] cad ON cad.Claim_no=ct.Claim_No
		LEFT JOIN DM_STG_EMICS.[dbo].[Claim_Detail_Extra] cde ON ct.Claim_No=cde.Claim_no
		LEFT JOIN (select distinct claim_no from DM_STG_EMICS.dbo.payment_recovery where Estimate_type='57') pr on pr.Claim_No=ct.claim_no
		LEFT JOIN (select distinct claim_no from DM_STG_EMICS.dbo.medical_cert where cancelled_date is null) mc on mc.Claim_No=cd.claim_number
		LEFT JOIN (SELECT claim_no FROM DM_STG_EMICS.[dbo].TIME_LOST_DETAIL WHERE is_deleted = 0  AND ISNULL(RTW_Goal, 0) NOT IN (2,3,4)) tld on tld.claim_no = cd.claim_number
		LEFT OUTER JOIN --//ur1
			(SELECT ur.Ref_No, 'EMICS:' + ur.Ref_No +'_'+ q.Category +':USER_RESPONSE' AS EmpTriageQuestions_icareID
			FROM DM_STG_EMICS.[dbo].[USER_RESPONSE] ur
			INNER JOIN DM_STG_EMICS.[dbo].[QUESTION] q
			ON ur.Question_ID = q.ID
			Where ur.Question_ID not in (24,25,26,27,38,39,40)
			GROUP BY ur.Ref_No, q.Category
			Having q.Category='EMP'
			) ur1 -- [EmpTriageQuestions_icareID]
			ON ur1.Ref_No = cd.Claim_Number
		LEFT OUTER JOIN --//ur2
			(SELECT ur.Ref_No, 'EMICS:' + ur.Ref_No +'_'+ q.Category +':USER_RESPONSE' AS OtherTriageQuestions_icareID
			FROM DM_STG_EMICS.[dbo].[USER_RESPONSE] ur
			INNER JOIN DM_STG_EMICS.[dbo].[QUESTION] q
			ON ur.Question_ID = q.ID
			Where ur.Question_ID not in (24,25,26,27,38,39,40)
			GROUP BY ur.Ref_No, q.Category
			Having q.Category='OTH'
			) ur2 --OtherTriageQuestions_icareID
			ON ur2.Ref_No = cd.Claim_Number
		LEFT OUTER JOIN --//ur3
			(SELECT ur.Ref_No, 'EMICS:' + ur.Ref_No +'_'+ q.Category +':USER_RESPONSE' AS DocTriageQuestions_icareID
			FROM DM_STG_EMICS.[dbo].[USER_RESPONSE] ur
			INNER JOIN DM_STG_EMICS.[dbo].[QUESTION] q
			ON ur.Question_ID = q.ID
			Where ur.Question_ID not in (24,25,26,27,38,39,40)
			GROUP BY ur.Ref_No, q.Category
			Having q.Category='DOC'
			) ur3 --[DocTriageQuestions_icareID]
			ON ur3.Ref_No = cd.Claim_Number
		LEFT OUTER JOIN --//ur4
			(SELECT ur.Ref_No, 'EMICS:' + ur.Ref_No +'_'+ q.Category +':USER_RESPONSE' AS InjTriageQuestions_icareID
			FROM DM_STG_EMICS.[dbo].[USER_RESPONSE] ur
			INNER JOIN DM_STG_EMICS.[dbo].[QUESTION] q
			ON ur.Question_ID = q.ID
			Where ur.Question_ID not in (24,25,26,27,38,39,40)
			GROUP BY ur.Ref_No, q.Category
			Having q.Category='WKR'
			) ur4 --[InjTriageQuestions_icareID]
			ON ur4.Ref_No = cd.Claim_Number
		LEFT JOIN (SELECT LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3) CLAIM_NUMBER
					FROM DM_STG_CDR..DM_STG_CDR_D_CLAIM_MASTER clmmstr
						INNER JOIN DM_STG_CDR..DM_STG_CDR_D_COMPANY comp ON clmmstr.MANAGING_COMPANY_ID = comp.COMPANY_ID
						INNER JOIN DM_STG_CDR..DM_STG_CDR_E_ROLLING_CLAIM clm ON clm.CLAIM_WCA_ID = clmmstr.CLAIM_WCA_ID
					WHERE clm.WCA_STATUS_ID = 181644
						AND comp.SOURCE_INSURER_ID = 702
						AND ISNULL(clm.CEASED_WORK_DATE_ID,0) <> 0
						AND LEN(clmmstr.SOURCE_CLAIM_NUMBER) > 3
					GROUP BY LEFT(clmmstr.SOURCE_CLAIM_NUMBER,LEN(clmmstr.SOURCE_CLAIM_NUMBER)-3)) tlr ON tlr.CLAIM_NUMBER = LTRIM(RTRIM(cd.Claim_Number))
		LEFT JOIN (SELECT Claim_No, FORMAT(Reasonable_Excuse_Code, '0#') AS  ReasonableExcuse_icare, Claim_Liability_Indicator, ID
					FROM DM_STG_EMICS.dbo.CAD_AUDIT a1
						--INNER JOIN (SELECT MAX(ID) maxID FROM DM_STG_EMICS.dbo.CAD_AUDIT WHERE Claim_Liability_Indicator = 9 GROUP BY Claim_No) a2 ON a1.ID = a2.maxID
						) rsn ON rsn.Claim_no = LTRIM(RTRIM(cd.Claim_Number)) 
						and rsn.Claim_Liability_Indicator = 9 and rsn.ID = (Select Max(ID) from DM_STG_EMICS.dbo.CAD_Audit where Claim_Liability_Indicator = 9 and claim_no = cd.Claim_Number)
		
		LEFT JOIN (SELECT Claim_no FROM DM_STG_EMICS.dbo.PAYMENT_RECOVERY WHERE Reversed = 0 GROUP BY claim_no HAVING SUM(trans_amount) > 0) medRep1 ON CD.Claim_Number = medRep1.Claim_no
		LEFT JOIN (SELECT cad2.Claim_no
					FROM DM_STG_EMICS.dbo.CLAIM_DETAIL CD2
						INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD2 ON CD2.Claim_Number = CAD2.Claim_no
						LEFT OUTER JOIN (SELECT * FROM DM_STG_EMICS.dbo.PAYMENT_RECOVERY WHERE Reversed = 0) PR2 ON PR2.Claim_No = CD2.Claim_Number
						LEFT OUTER JOIN (SELECT DISTINCT Claim_No FROM DM_STG_EMICS.dbo.MEDICAL_CERT WHERE is_Deleted = 0) MC2 ON CAD2.Claim_no = MC2.Claim_No
					WHERE CAD2.Claim_Liability_Indicator <> 12
						AND (PR2.Estimate_Type IN (51,52,53,55,56,63,64) 
							OR CD2.is_Medical_Only = 1
							OR MC2.Claim_No IS NOT NULL)
					GROUP BY cad2.Claim_no) medRep2 ON CD.Claim_Number = medRep2.Claim_no
		Where cd.is_null = 0 

END