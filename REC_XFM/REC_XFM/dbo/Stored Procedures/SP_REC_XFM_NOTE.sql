﻿/* =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description:	Claim Management Domain - ccst_note
-- Version: 12/06/2019 v24 initial build
-- ============================================= */
CREATE PROCEDURE [dbo].[SP_REC_XFM_NOTE]
AS
BEGIN
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_note','Claim Management - v24 12/06/2019'

-- drop table ccst_note

Select
	cc_user.PublicID as AuthorID,
	txt.DATA as Body,
	'OtherServices' as CastManagementServiceSubject,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':INJURED' as ClaimContactID,
	'EMICS:' + convert(varchar, ima.Claim_no) as ClaimID,
	'en_AU' as Language,
	ima.Claim_no as LUWID,
	'EMICS:' + convert(varchar,ima.ID) as PublicID,
	'public' as SecurityType,
	'EML:' + RTRIM(LTRIM(convert(varchar, UPPER(RLU.description)))) as Subject, --ima.text_type RLU.description?
	Case when ima.text_type = '1' then 'investigations_icare'
		when ima.text_type = '2' then 'medical_icare'
		when ima.text_type = '3' then 'medical_icare'
		when ima.text_type = '4' then 'medical_icare'
		when ima.text_type = '5' then 'rehab'
		when ima.text_type = 'A' then 'general'
		when ima.text_type = 'B' then 'externalcaseconference_icare'
		when ima.text_type = 'C' then 'general'
		when ima.text_type = 'E' then 'general'
		when ima.text_type = 'F' then 'general'
		when ima.text_type = 'G' then 'triage_icare'
		when ima.text_type = 'I' then 'general'
		when ima.text_type = 'K' then 'workcapacity_icare'
		when ima.text_type = 'L' then 'legal_icare'
		when ima.text_type = 'M' then 'service_icare'
		when ima.text_type = 'N' then 'medical_icare'
		when ima.text_type = 'O' then 'general'
		when ima.text_type = 'P' then 'general'
		when ima.text_type = 'Q' then 'general'
		when ima.text_type = 'R' then 'externalcaseconference_icare'
		when ima.text_type = 'S' then 'general'
		when ima.text_type = 'T' then 's39review_icare'
		when ima.text_type = 'V' then 'recoveries_icare'
		when ima.text_type = 'W' then 'wages_icare'
		when ima.text_type = 'X' then 'feedback_icare'
		when ima.text_type = 'Y' then 'legal_icare'
		when ima.text_type = 'Z' then 'general'
	End as Topic

into ccst_note

From DM_STG_EMICS.dbo.Claim_detail cd 
Inner join DM_STG_EMICS.dbo.IMA_PLAN_TEXT ima on cd.Claim_Number = ima.Claim_no
Inner join DM_STG_EMICS.dbo.CLM_MEMO_TEXT txt on ima.text_ptr = txt.TEXT_PTR
Inner join DM_STG_EMICS.dbo.Ref_Look_Up rlu on ima.text_type = rlu.Code
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cad.Claims_Officer)
LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID) and cc_credential.UserName = 'datamigrationuser'
--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
Where rlu.Name = 'claimnotetype'
and cd.is_null = 0 --3,954,142

EXEC SP_REC_XFM_NOTE_STAGING

END