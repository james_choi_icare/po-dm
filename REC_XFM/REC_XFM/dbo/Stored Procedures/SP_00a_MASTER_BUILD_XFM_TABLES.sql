﻿CREATE PROC [dbo].[SP_00a_MASTER_BUILD_XFM_TABLES] 
	@runOutputID int OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @runID int, @emailMessage varchar(500), @serverName varchar(100), @dName varchar(100), @errorMessage varchar(100) = '';

	INSERT INTO [dbo].[ADM_RECON_RUN_AUDIT] ([RunDateTime])
	SELECT CURRENT_TIMESTAMP

	SET @runID = @@IDENTITY;

	print 'Run ID: ' + cast(@runID as varchar(100))

	SET @dName = DB_NAME();
	SET @serverName = CASE WHEN SERVERPROPERTY('MachineName') = 'IPA2ACLRETLDB04' THEN '10.20.60.65'
						WHEN SERVERPROPERTY('MachineName') = 'IPA2ACLRETLDB05' THEN '10.20.60.105'
						ELSE '' END;

	SET @emailMessage = 'REX_XFM table build process has started at : ' + cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) +
							' Server: ' + @serverName + ' Database: ' + @dName

	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'james.choi@icare.nsw.gov.au' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')
	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'ann.go@tenzing.co.nz' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')
	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'satish.rajkumar@tenzing.co.nz' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')

	BEGIN TRY

		EXEC [dbo].[SP_01_PROCESS_RECON_XFM_TABLES] @runID

	END TRY
	BEGIN CATCH
	--JA
		IF XACT_STATE()!=0
		BEGIN 
		ROLLBACK TRANSACTION
		END
	--JA
		SET @errorMessage = ERROR_MESSAGE()
	END CATCH

	SET @emailMessage = CASE WHEN @errorMessage <> ''	
							THEN 'REX_XFM table build process has ended with errors at : '+cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) + ' Error Message: ' + @errorMessage
							ELSE 'REX_XFM table build process has ended at : '+cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) END 
							+ ' Server: ' + @serverName + ' Database: ' + @dName;

	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'james.choi@icare.nsw.gov.au' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')

	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'ann.go@tenzing.co.nz' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')
	Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'satish.rajkumar@tenzing.co.nz' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')

	SELECT @runOutputID = @runID
END