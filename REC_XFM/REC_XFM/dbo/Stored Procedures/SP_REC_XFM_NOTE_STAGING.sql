﻿-- =============================================
-- Author:		Ann
-- Create date: 12/08/2019
-- Description:	v6.1
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_NOTE_STAGING]
AS
BEGIN
	--not unstructured
Insert 	into ccst_note

Select
	cc_user.PublicID as AuthorID,
	dbo.fn_Claim_Note_Text(cd.claim_number, 'General') as Body,
	'OtherServices' as CastManagementServiceSubject,
	NULL as ClaimContactID,
	'EMICS:' + convert(varchar, cd.Claim_Number) as ClaimID,
	'en_AU' as Language,
	cd.claim_number as LUWID,
	'EMICS:' + rtRIM(cd.claim_number) + '_' + convert(varchar, ROW_NUMBER() OVER (ORDER BY Claim_Number ASC))  + ':DATA_MIGRATION' as PublicID,
	'public' as SecurityType,
	'Migrated Data NOte.' as Subject,
	'datamigration' as Topic

From DM_STG_EMICS.dbo.Claim_detail cd 
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIM_ACTIVITY_DETAIL] cad  ON (cd.Claim_Number = cad.Claim_no)
LEFT JOIN [DM_STG_EMICS].[dbo].[CLAIMS_OFFICERS] co  ON (co.alias = cad.Claims_Officer)
LEFT JOIN (SELECT SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) as EML_Email,* 
		FROM [DM_STG_EMICS].[dbo].[CONTROL] cntrl where TYPE = 'clmGroup' and SUBSTRING(cntrl.VALUE, CHARINDEX('*',cntrl.VALUE) + 1 , LEN(cntrl.VALUE) - CHARINDEX('*',cntrl.VALUE) + 1) is not null  ) cntrl
		ON (co.Alias = cntrl.ITEM)
left join DM_STG_CC.[dbo].[cc_credential] cc_credential on (cntrl.EML_Email = cc_credential.UserName)
left join DM_STG_CC.[dbo].[cc_user] cc_user on (cc_user.CredentialID = cc_credential.ID) and cc_credential.UserName = 'datamigrationuser'
Where cd.is_null = 0 --83,423

END