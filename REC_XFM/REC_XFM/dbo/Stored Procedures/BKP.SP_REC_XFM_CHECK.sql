﻿CREATE PROC [dbo].[BKP.SP_REC_XFM_CHECK] AS
BEGIN
	/*========================================
		Author: Ann
		Domain: Payments and recoveries - ccst_check
		Version: v45 initial build
				 02/05/2019 v45.2 - updated  to fix dulicate publicid caused by checksetID
				 10/05/2019 v45.5 - updated  status field rule to get claimstatus to derive check status
		Version: 10/06/2019 v46.2.1 - AG updated fields: CheckNumber, GroupID, PaymentMethod
	========================================*/
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_check','Payments and Recoveries - v46.2.1 10/06/2019'
	--drop table ccst_check
	
	DECLARE @paymentRecovery TABLE (
		pr_ID int,	
		estimate_type char(2),
		claim_no char(19),
		Payment_no int,
		Reversed bit, 
		Period_Start_Date smalldatetime,
		Period_End_Date smalldatetime, 
		Date_of_Service smalldatetime,
		invoice_no char(20),
			index IX1 nonclustered (claim_no,Payment_no)
	);

	insert into @paymentRecovery	
	select distinct min(id) over(partition by Claim_no,Payment_No,Invoice_no,Invoice_date,period_start_date,period_end_date,Estimate_type) as pr_id,
				estimate_type,claim_no,Payment_no, Reversed, Period_Start_Date,Period_End_Date,Date_of_Service,invoice_no 
	from dm_stg_emics.dbo.Payment_Recovery pr
	--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS IC on IC.Claim_number = pr.Claim_No
	where ESTIMATE_TYPE < 70 and Reversed <> 1;				

	DECLARE @garnishee TABLE (
		Claim_No char(19) index IX1 Nonclustered,
		Payment_no int index IX2 Nonclustered,
		Garnishee_payment_no int index IX3 Nonclustered
	);		

	insert into @garnishee	
		select distinct PR_Adj.Claim_No,
							PR_Adj.Payment_no,
							PR_gar.Payment_no as Garnishee_payment_no 
		from (select Claim_No,Garnishee_No,Period_Start_Date,Period_End_Date,Garnishee_Payment_id,Payment_no from dm_stg_emics.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
				inner join (select id,Claim_No,Garnishee_No,Period_Start_Date,Period_End_Date,Payment_no from dm_stg_emics.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1) PR_Gar
							on PR_Adj.Claim_No=PR_Gar.Claim_No
							and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
							and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
							and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
							and pr_adj.Garnishee_Payment_id=PR_Gar.id;

	DECLARE @paymentRecoveryWithGarnishee TABLE (
		claim_no char(19),
		Payment_no int,
		PRGS_Claim_No char(19),
		PRGS_Garnishee_payment_no int,
		PRGS_Payment_no int,
		PRGP_Claim_No char(19),
		PRGP_Garnishee_payment_no int,
		PRGP_Payment_no int,
			index IX1 clustered (claim_no,Payment_no)
	);

	INSERT INTO @paymentRecoveryWithGarnishee
	SELECT PR.claim_no,PR.Payment_no,
		PRGS.Claim_No PRGS_Claim_No, PRGS.Garnishee_payment_no PRGS_Garnishee_payment_no, PRGS.Payment_no PRGS_Payment_no
		,PRGP.Claim_No PRGP_Claim_No, PRGP.Garnishee_payment_no PRGP_Garnishee_payment_no, PRGP.Payment_no PRGP_Payment_no
	FROM @paymentRecovery PR
		LEFT OUTER JOIN @garnishee PRGS ON PR.claim_no = PRGS.Claim_No AND PR.Payment_no = PRGS.Garnishee_payment_no
		LEFT OUTER JOIN @garnishee PRGP ON PR.claim_no = PRGP.Claim_No AND PR.Payment_no = PRGP.Payment_no

	DECLARE @paymentDetails TABLE (
		Payee_Code char(8) index IX1 Clustered,
		creditor_email_add varchar(100),
		creditor_postal_Address varchar(255),
		DependentID int
	);		

	INSERT INTO @paymentDetails
	SELECT cpr.Payee_Code, cr.email_add, cr.Postal_Address, DP.ID DependentID
	FROM dm_stg_emics.dbo.CLAIM_PAYMENT_RUN CPR
		LEFT OUTER JOIN dm_stg_emics.dbo.CREDITORS CR ON CR.Creditor_no = cpr.Payee_Code
		--LEFT OUTER JOIN dm_stg_emics.dbo.BSB ON CPR.Payee_bsb=BSB.[NO]
		LEFT OUTER JOIN dm_stg_emics.dbo.DEPENDENTS DP ON CPR.Payee_Code = DP.Creditor_no
	GROUP BY cpr.Payee_Code, cr.email_add, cr.Postal_Address, DP.ID 

	CREATE TABLE [dbo].[ccst_check](
	[BankSwiftCode_icare] [int] NULL,
	[CheckInstructions] [varchar](15) NULL,
	[CheckNumber] [varchar](20) NULL,
	[checksetID] [varchar](98) NULL,
	[CheckType] [varchar](9) NOT NULL,
	[CheckType_SRC_VALUE] [varchar] (100) NULL,
	[ClaimContactID] [varchar](66) NULL,
	[CommMethod_icare] [varchar](5) NULL,
	[DatePresented_icare] [smalldatetime] NULL,
	[DeductionIdentifier_icare] [varchar](16) NULL,
	[DeductionType] [varchar](16) NULL,
	[DeliveryMethod] [varchar](4) NULL,
	[GroupID] [varchar](100) NULL,
	[GSTPaymentClassPEL_Ext] [varchar](5) NOT NULL,
	[InvoiceNumber] [char](20) NULL,
	[PaymentMethod] [varchar](5) NOT NULL,
	[PortionID] [varchar](93) NULL,
	[PublicID] [varchar](88) NULL,
	[PublicID_Cheque_SRC_VALUE] [varchar](20) NULL,
	[PublicID_PaymentNo_SRC_VALUE] [int] NOT NULL,
	[PublicID_PRID_SRC_VALUE] [int] NULL,
	[PublicID_ChequeNo_SRC_VALUE] [varchar](20) NULL,
	[LUWID] [char](19) NOT NULL,
	[Status] [varchar](9) NOT NULL,
	--[Status_SRC_VALUE] [varchar](20) NOT NULL,
	[WeeklyBenefitPayeeType_icare] [varchar](13) NULL
) ON [PRIMARY]

	insert into ccst_check
	Select distinct
	NULL as BankSwiftCode_icare,
	CASE WHEN is_Return2CO=0 AND is_Return2Owner=0 THEN 'default'
         WHEN is_Return2CO=1 OR is_Return2Owner=1 THEN 'return_to_icare'
		 else  'default' --v46.3.11
		END as CheckInstructions,
	Case when cpr.cheque_amount = 0 then NULL else cpr.cheque_no end as CheckNumber, --v46.2.1
	
	
	
	
	
case when PR.estimate_type<70 and prGar.PRGP_Payment_no is not null 
	then  'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,prGar.PRGP_Payment_no)+'_'+convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET'-- checkset for the primary checks of multipayee checks
when PR.Estimate_type<70 and prGar.PRGS_Payment_no  is not null 
then 'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,prGar.PRGS_Payment_no )+'_'+convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET'--checkset for the secondary checks of multipayee checksand will be same as that of primary check's checkset
when PR.estimate_type<70 and PR.Payment_no is not null  and prGar.Payment_no is  null  
and exists (select 1 from DM_STG_EMICS.dbo.Payment_recovery prc where estimate_type<70 
and pr.claim_no=prc.claim_no 
and  pr.Payment_no=prc.payment_no 
and pr.Estimate_type=prc.Estimate_type 
and Garnishee_No is null and Reversed<>1 and ISNULL(nullif(PRc.Adjust_Trans_Flag,''),'N')='N')
then  'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET' --checkset for the single payee checks where adjustments are paid under the same payment no
when PR.estimate_type<70 and PR.Payment_no is not null  and prGar.PRGP_Payment_no is  null  
and not exists (select 1 from DM_STG_EMICS.dbo.Payment_recovery prc where estimate_type<70 
and  pr.claim_no=prc.claim_no and  pr.Payment_no=prc.payment_no 
and pr.Estimate_type=prc.Estimate_type and Garnishee_No is not null 
and  Reversed<>1 and ISNULL(nullif(PRc.Adjust_Trans_Flag,''),'N')='N')
then  'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+convert(varchar,pr.Estimate_type)+'_1' +':CHECKSET' --checkset for the single payee checks where adjustments are paid under the  different payment no
ELSE NULL END as CheckSetID,
	
	
	--case when PR.estimate_type < 70 and prGar.PRGP_Payment_no is not null 
	--	then 'EMICS:' + rtrim(PR.claim_no) + '_' + convert(varchar,prGar.PRGP_Payment_no)+'_'+
	--			convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET'-- checkset for the primary checks of multipayee checks
	--	when PR.Estimate_type < 70 and prGar.PRGS_Payment_no is not null 
	--		then 'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar, prGar.PRGS_Payment_no)+'_'+
	--			convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET'--checkset for the secondary checks of multipayee checksand will be same as that of primary check's checkset
	--	when PR.estimate_type < 70 and PR.Payment_no is not null  and prGar.PRGP_Payment_no is null  
	--		and exists (select 1 from DM_STG_EMICS.dbo.Payment_recovery prc where estimate_type<70 
	--			and pr.claim_no=prc.claim_no and  pr.Payment_no=prc.payment_no and pr.Estimate_type=prc.Estimate_type and 
	--			Garnishee_No is null and Reversed<>1 and ISNULL(nullif(PRC.Adjust_Trans_Flag,''),'N')='N')
	--		then 'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+
	--			convert(varchar,pr.Estimate_type)+'_0' +':CHECKSET' --checkset for the single payee checks where adjustments are paid under the same payment no
	--	when PR.estimate_type<70 and PR.Payment_no is not null  and prGar.PRGP_Payment_no is  null  and not exists 
	--		(select 1 from DM_STG_EMICS.dbo.Payment_recovery prc where estimate_type<70 and  pr.claim_no=prc.claim_no and  
	--		pr.Payment_no=prc.payment_no and pr.Estimate_type=prc.Estimate_type and 
	--		Garnishee_No is not null and  Reversed<>1 and ISNULL(nullif(PRC.Adjust_Trans_Flag,''),'N')='N')
	--		then  'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,PR.Payment_no)+'_'+
	--			convert(varchar,pr.Estimate_type)+'_1' +':CHECKSET' --checkset for the single payee checks where adjustments are paid under the  different payment no
	--	ELSE NULL 
	--	END as checksetID,
	CASE WHEN prGar.PRGS_Claim_No IS NOT NULL THEN 'secondary'
		ELSE 'primary' 
		END as CheckType,
	prGar.PRGS_Claim_no as CheckType_SRC_VALUE,
	CASE WHEN CPR.PAYEE_TYPE =1 THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'
		WHEN CPR.PAYEE_TYPE in (4,3)
		THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,CPR.Payee_Code)+':VENDOR'
		WHEN PAYEE_TYPE=2
		THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INSURED'
		WHEN PAYEE_TYPE=23
		THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,PD.DependentID) + ':DEPENDENT'
		ELSE NULL
		End as ClaimContactID, 
	CASE WHEN cpr.Remittance_method='E' THEN 'email'
		WHEN cpr.Remittance_method='P' THEN 'post'
		WHEN cpr.Remittance_method='F'  THEN 'fax'
		--temporary work around, need to confirm with business
		WHEN cpr.Remittance_method is null THEN 
		 case when Payee_type in ('3','4') then 
			 CASE WHEN  PD.creditor_email_add<>''and PD.creditor_email_add is not null  THEN  'email'
				  WHEN  PD.creditor_postal_Address IS NOT NULL THEN 'post'
			 END
		   when Payee_type ='1' then 
			  case when CD.workplace_email is not null THEN  'email'
				when cd.Street_Address is not null or WorkPlace_Street_Address is not null then 'post'
			  END
			when Payee_type='2' then 'email'
			  END
		END as CommMethod_icare, 
	CASE WHEN isnull(Cheque_Status,'00')<>'9' and BankChq_Status=1
		THEN CPR.BankChq_Date
		ELSE NULL
		END as DatePresented_icare,
	CASE WHEN CPR.Payee_Code='33547'
          THEN 'centrelink_icare'
          WHEN CPR.Payee_Code='85581'
          THEN 'child_support'
          WHEN CPR.Payee_Code='11790'
          THEN 'medicare_icare'
		ELSE NULL
		END as DeductionIdentifier_icare,
	CASE WHEN CPR.Payee_Code='33547'
          THEN 'centrelink_icare'
          WHEN CPR.Payee_Code='85581'
          THEN 'child_support'
          WHEN CPR.Payee_Code='11790'
          THEN 'medicare_icare'
		ELSE NULL
		END as DeductionType,
	CASE WHEN Cheque_Status<>9 THEN 'send'
		WHEN Cheque_Status is NULL then 
			case when Payee_account_no is not null then NULL  
			else 'send' end --this is just added as a temporary fix to get the cheque status null records inside. According to EML, there would be no records 
		ELSE NULL
		END as DeliveryMethod,
	case when  PR.Estimate_type<70 and (prGar.PRGS_Garnishee_payment_no is not null or prGar.PRGP_Payment_no is not null)
		then 'EMICS:'+rtrim(PR.claim_no)+'_' + isnull(convert(varchar, prGar.PRGP_Payment_no),convert(varchar, prgar.PRGS_Payment_no))+'_'+ convert(varchar,PR.Estimate_type)+'_0' +':CHECKGROUP'--adjust flag is defaulty considered as N as the actuall payment from which garnishee was derived  will not be an adjustment
		else NULL end as GroupID, --updated v46.2.1
	'nogst' as GSTPaymentClassPEL_Ext, 
	CASE WHEN PR.Invoice_No='' THEN NULL
		ELSE RTRIM(LTRIM(UPPER(PR.Invoice_No))) --(make all letters uppercase) 
		END as InvoiceNumber,
	CASE WHEN Cheque_Status='9' and cheque_amount<>0 then 'eft' 
		When cheque_amount =0 then 'check'
		WHEN Cheque_Status is NULL and cheque_amount <> 0 then 
			case when Payee_account_no is not null then 'eft' 
			else 'check' end --this is just added as a temporary fix to get the cheque status null records inside. According to EML, there would be no records while go live
		ELSE 'check' END as PaymentMethod, --updated v46.2.1
	case 
		when prGar.PRGP_Payment_no is not null
		then 'EMICS:'+RTRIM(cpr.Claim_Number)+'_'+convert(varchar,cpr.Payment_No)+isnull('_'+cpr.cheque_No,'')+':PRICHECKPORTION'
		when prGar.PRGS_Garnishee_payment_no is not null
		then 'EMICS:'+RTRIM(cpr.Claim_Number)+'_'+convert(varchar,cpr.Payment_No)+isnull('_'+cpr.cheque_No,'')+':SECCHECKPORTION'
		else NULL
		end as PortionID,
	CASE WHEN CHEQUE_NO IS  NULL THEN  
		'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,PR.pr_id)
		ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+convert(varchar,PR.pr_id) END as PublicID,
	Cheque_no as PublicID_Cheque_SRC_VALUE,
	CPR.Payment_no as PublicID_PaymentNo_SRC_VALUE,
	PR.Pr_id as PublicID_PRID_SRC_VALUE,
	CPR.Cheque_no as PublicID_ChequeNo_SRC_VALUE,
	CD.claim_number as LUWID,
	CASE WHEN (Cheque_Status='9' or (cheque_status is null and Payee_account_no is not null)) and Cheque_Amount <> 0 then 'issued' 
		When cheque_status =3 and BankChq_Status is null then 'voided'
		When BankChq_Status = 2 then 'voided'
		When Cheque_status not in (3,9,6) and BankChq_Status is null then 'cleared'
		When BankChq_Status = 1 then 'cleared'
		Else 'cleared'
		End as Status,
	--'Cheque_status: ' + convert(varchar,Cheque_status) + ' Bankchq_status: ' + convert(varchar,bankchq_status) as Status_SRC_VALUE,
	CASE 
		WHEN CPR.Payee_type=1 THEN 'InjuredWorker'
		WHEN CPR.Payee_type=23 THEN 'dependant'
		WHEN CPR.Payee_type=2 THEN 'employer'
		END WeeklyBenefitPayeeType_icare
	
	FROM dm_stg_emics.dbo.CLAIM_PAYMENT_RUN CPR
		INNER JOIN dm_stg_emics.dbo.CLAIM_DETAIL CD ON CD.Claim_Number = CPR.Claim_number
		--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS IC on IC.Claim_number = CD.claim_number
		INNER JOIN dm_stg_emics.dbo.CLAIM_ACTIVITY_DETAIL cad on cd.claim_number = cad.Claim_no
		INNER JOIN @paymentRecovery PR on CPR.Claim_number = PR.Claim_No AND CPR.Payment_no = PR.Payment_no
				--# DMIG 3902--selection added to get the garnishee amount and map them as secondary checks
		LEFT JOIN @paymentRecoveryWithGarnishee prGar ON CPR.Claim_number = prGar.Claim_No AND CPR.Payment_no = prGar.Payment_no
		LEFT JOIN @paymentDetails PD ON CPR.Payee_Code = PD.Payee_Code
	WHERE CD.is_NULL= 0 -- Checks are created only for payments
			and isnull(CPR.Cheque_status,'00') <> '3';


	--UPDATE ccst_check
	--SET GSTPaymentClassPEL_Ext = 'dam'
	--		WHERE PublicID in (SELECT CASE WHEN CHEQUE_NO IS  NULL THEN  
	--			'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,PR.ID)
	--			ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+convert(varchar,PR.ID) END as PublicID 
	--		FROM dm_stg_emics.dbo.CLAIM_PAYMENT_RUN CPR
	--			INNER JOIN dm_stg_emics.dbo.CLAIM_DETAIL CD ON CD.Claim_Number = CPR.Claim_number
	--			INNER JOIN dm_stg_emics.dbo.Payment_Recovery PR on CPR.Claim_number = PR.Claim_No AND CPR.Payment_no = PR.Payment_no
	--		where DAM <> 0 AND CD.is_NULL= 0 and isnull(CPR.Cheque_status,'00') <> '3');

	--UPDATE ccst_check
	--SET GSTPaymentClassPEL_Ext = 'itc'
	--		WHERE PublicID in (SELECT CASE WHEN CHEQUE_NO IS  NULL THEN  
	--			'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,PR.ID)
	--			ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+convert(varchar,PR.ID) END as PublicID 
	--		FROM dm_stg_emics.dbo.CLAIM_PAYMENT_RUN CPR
	--			INNER JOIN dm_stg_emics.dbo.CLAIM_DETAIL CD ON CD.Claim_Number = CPR.Claim_number
	--			INNER JOIN dm_stg_emics.dbo.Payment_Recovery PR on CPR.Claim_number = PR.Claim_No AND CPR.Payment_no = PR.Payment_no
	--		where ITC <> 0 AND CD.is_NULL= 0 and isnull(CPR.Cheque_status,'00') <> '3')			



END