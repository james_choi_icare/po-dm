﻿/* =============================================
-- Author:		Ann
-- Create date: 13/06/2019
-- Description:	Claim Management Domain - ccst_theplanstratreview_icare
-- Version: 13/06/2019 v24 initial build
-- ============================================= */
CREATE PROCEDURE [dbo].[SP_REC_XFM_THEPLANSTRATREVIEW]
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_theplanstratreview_icare','Claim Management - v24 13/06/2019'
--drop table ccst_theplanstratreview_icare
Select
	Convert(Datetime2, convert(date,rtwd.Date_Created,103)) as Date,
	rtwp.claim_no as LUWID,
	'Continue with Strategy' as Outcome,
	'continuewithstrategy_icare' as OutcomeType,
	'EMICS:' + RTRIM(rtwp.Claim_no) + '_' + convert(varchar, rtwp.RTwplan_no) + ':PLANREVIEW' as PublicID,
	RTRIM(SUBSTRING(rtwd.Duties,1,512)) as Purpose,
	'EMICS:' + RTRIM(rtwp.Claim_no) + '_' + convert(varchar, rtwp.rtwplan_no) + ':RTWPLAN' as RehabPlan_ExtID

into ccst_theplanstratreview_icare

From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration rtwd --691
Inner join DM_STG_EMICS.dbo.Claim_RTW_Plan rtwp on rtwp.Claim_No = rtwd.Claim_No and rtwd.RTWplan_no = rtwp.rtwplan_no
Inner join (Select Claim_no, RTWplan_no, MAX(Duration_no) as Duration_no From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration as Claim_RTW_Plan_Duration_1
	Group by Claim_No, RTWplan_no) as Max_Duration on rtwd.Claim_No = Max_Duration.Claim_no
	and rtwd.RTWplan_no = Max_Duration.RTWplan_no
	and rtwd.Duration_no = Max_Duration.Duration_no
Inner join DM_STG_EMICS.dbo.claim_detail cd on rtwp.claim_no = cd.claim_number
--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.Claim_Number
Where cd.is_Null = 0
END