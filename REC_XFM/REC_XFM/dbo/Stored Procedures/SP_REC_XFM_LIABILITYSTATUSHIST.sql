﻿CREATE PROC [dbo].[SP_REC_XFM_LIABILITYSTATUSHIST] AS
BEGIN
	--LIABILITY ASSESSMENT v22 ccst_liabilitystatushist_icare

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_liabilitystatushist_icare','Liability Assessment - v22 20/04/2019'

	SELECT 'EMICS:' + cast(DS1.ID as varchar(100)) + ':LIABILITYSTATUSHISTORY' PublicID,
		RTRIM(ds2.Claim_no) as LUWID,
		'EMICS:' + ltrim(rtrim(ds2.Claim_no)) + ':WORKCOMP' as ClaimWorkCompID,
		CASE WHEN DS1.Claim_Liability_Indicator = 8 AND DS3.Segment_Code = 'E' THEN 2
			WHEN DS1.Claim_Liability_Indicator = 8 AND DS3.Segment_Code = 'G' THEN 6
			WHEN DS1.Claim_Liability_Indicator <> 8 Then 0
			WHEN DS1.Claim_Liability_Indicator = 8 AND DS3.Segment_Code not in ('E','G') THEN 12
			ELSE 0 END ProvisionalWeeks
		,
		'Claim_Liability_Indicator:' + cast(DS1.Claim_Liability_Indicator as varchar(50)) + ' Segment_Code:' + DS3.Segment_Code  ProvisionalWeeks_SRC_VALUE,
		
		CASE WHEN DS1.Claim_Liability_Indicator <> 1 AND DS5.Weekly_Benefit_Cease_Date is not null 
				THEN cast(DS5.Weekly_Benefit_Cease_Date AS DATE) 
			WHEN DS1.Claim_Liability_Indicator = 7 THEN
				CASE WHEN DS6.Weekly_benefit_cease_date is not null 
						THEN cast(DS5.Weekly_Benefit_Cease_Date AS DATE) --cast(c.max_period_end_date AS DATE) 
					WHEN DS2.Date_of_liability is not null 
						THEN cast(DS5.Weekly_Benefit_Cease_Date AS DATE)
				--Else NULL
				END
			--WHEN DS2.Date_of_liability is not null THEN cast(DS2.Date_of_liability AS DATE) 
			--ELSE NULL 
			END WeeklyBenefitEndDate,

		DS1.Claim_Liability_Indicator AS LiabilityStatus,
		cast(DS2.Date_of_liability AS DATE) AS LiabilityStatusDate,
		cast(DS1.Transaction_Date AS DATE) AS LiabilityStatusDecisionDate
		
	INTO ccst_liabilitystatushist_icare

	FROM (
			Select * from (
			select CADA.ID,CADA.Claim_no,CADA.Transaction_Date as Transaction_Date,CADA.Claim_Liability_Indicator--,MAX(CADA.ID) as maxID
			,row_number()over (partition by CADA.Claim_no,CADA.Claim_Liability_Indicator order by  Transaction_Date DESC,ID DESC) as ronum
			,MAX(Transaction_Date)over (partition by CADA.Claim_no,CADA.Claim_Liability_Indicator order by Transaction_Date DESC) as MaxTransaction_Date
			from [DM_STG_EMICS].dbo.CAD_AUDIT CADA
			)DS
			Where ronum=1 
			--order by 2,4
		) DS1
		LEFT OUTER JOIN [DM_STG_EMICS].dbo.CAD_AUDIT DS2 on DS1.Claim_no = DS2.Claim_no and DS1.Claim_Liability_Indicator = DS2.Claim_Liability_Indicator
			and DS1.ID = DS2.ID
		LEFT OUTER JOIN [DM_STG_EMICS].dbo.CLAIM_TRIAGE DS3 ON DS1.Claim_no = DS3.Claim_No
		--LEFT JOIN (SELECT DS4.ID, DS4.Claim_no, max(period_end_date) max_period_end_date FROM [DM_STG_EMICS].dbo.Payment_Recovery GROUP BY Claim_No) c ON a.Claim_no = c.Claim_No
		LEFT JOIN (Select DS4.ID,DS4.CLaim_no,max(CDE.Weekly_Benefit_Cease_Date) as Weekly_Benefit_Cease_Date--,DS4.MaxTransaction_Date
						from 
						(
						Select * from (
						select CADA.ID,CADA.Claim_no,Transaction_Date,CADA.Claim_Liability_Indicator--,MAX(CADA.ID) as maxID
						,row_number()over (partition by CADA.Claim_no,CADA.Claim_Liability_Indicator order by  Transaction_Date DESC,ID DESC) as ronum
						,MAX(Transaction_Date)over (partition by CADA.Claim_no,CADA.Claim_Liability_Indicator order by Transaction_Date DESC) as MaxTransaction_Date
						from [DM_STG_EMICS].dbo.CAD_AUDIT CADA
						)DS
						Where ronum=1 )DS4 
						LEFT JOIN [DM_STG_EMICS].dbo.claim_detail_extra_audit CDE on CDE.Weekly_Benefit_Cease_Date is not null 
					and CDE.claim_no = DS4.Claim_no 
					and DS4.MaxTransaction_Date>CDE.create_date
					group by DS4.claim_no, DS4.ID 
				) DS5 on DS1.Claim_no = DS5.claim_no and DS5.ID = DS1.ID
		Left join (Select claim_no, max(period_end_date) as 'Weekly_benefit_cease_date' from DM_STG_EMICS.dbo.Payment_recovery
			group by claim_no) DS6 on DS6.Claim_no = DS1.Claim_no

	END