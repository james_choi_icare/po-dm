﻿-- =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Legal domain - ccst_matterevent
-- Version: Initial build v30 17/06/2019
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_MATTEREVENT]
AS
BEGIN
--drop table ccst_matterevent_icare
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_matterevent_icare','Legal - v32'

Select 
	NULL as EventContactID,
	l.Date_Hearing as EventDate,
	-- JCFIX ----------------
	Case when l.Hearing_type_code = 'TC' then convert(varchar(512), cm.Data)
	--Case when l.Hearing_type_code = 'TC' then convert(varchar, cm.Data)
	-- JCFIX ----------------
		Else NULL
		End as EventDetails,
	NULL as EventLocationID,
	NULL as EventResult,
	Case when l.Hearing_type_code = 'AR' then 'arbitration_icare'
		when l.Hearing_type_code in ('AP','DP','DR','CA') then 'hearing_icare'
		when l.Hearing_type_code = 'MD' then 'mediation_icare'
		when l.Hearing_type_code = 'TC' then 'teleconference_icare'
		End as EventType,
	cd.claim_number as LUWID,
	'EMICS:' + RTRIM(l.Claim_no) + '_' + convert(varchar, l.ID) + ':Litigation' as MatterID,
	'EMICS:' + RTRIM(l.claim_no) + '_' + convert(varchar, l.ID) + ':Litigation' as PublicID

Into ccst_matterevent_icare

From DM_STG_EMICS.dbo.Litigation l
Inner Join DM_STG_EMICS.dbo.Claim_detail cd on l.claim_no = cd.claim_number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

Left outer join (Select id, data = STUFF((Select ' ' + data From DM_STG_EMICS.dbo.CLM_MEMO cm Where cm.id = cm2.id For XML PATH('')),1,1,'')
  From DM_STG_EMICS.dbo.CLM_MEMO cm2
  group by ID) cm on l.Text_ptr = cm.Id 
Where ISNULL(Hearing_Type_code,'') != '' and l.ID <> 101 --93
and cd.is_null = 0
END