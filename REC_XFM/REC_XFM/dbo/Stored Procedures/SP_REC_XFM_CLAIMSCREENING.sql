﻿-- =============================================
-- Author:		Ann
-- Create date: 12/06/2019
-- Description:	Claim Management Domain - ccst_claimscreening_icare
-- Version: v24 initial build
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMSCREENING]
AS
BEGIN
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimscreening_icare','Claim Management - v24 12/06/2019'

	--ccst_claimscreening_icare
Select
	'EMICS:' + cd.claim_number as ClaimID,
	Case when cad.Screening_Action_Code = 0 then NULL
		when cad.Screening_Action_Code = 1 then '01'
		when cad.Screening_Action_Code = 2 then '02'
		when cad.Screening_Action_Code = 3 then '03'
		when cad.Screening_Action_Code = 4 then '04'
		when cad.Screening_Action_Code = 5 then '05'
		when cad.Screening_Action_Code = 6 then '06'
		when cad.Screening_Action_Code = 7 then '07'
		when cad.Screening_Action_Code = 8 then '08'
		End as ClaimScreenActionCode,
	CONVERT(Date, cad.Date_Claim_Screening, 103) as ClaimScreeningDate,
	'1' as Counter,
	cd.Claim_Number as LUWID,
	'EMICS:' + RTRIM(cd.Claim_Number) + ':CLAIMSCREENING' as PublicID

Into ccst_claimscreening_icare

From DM_STG_EMICS.dbo.Claim_detail cd
Inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cd.claim_number = cad.claim_no
--Inner Join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.claim_number
Where cd.is_null = 0
END