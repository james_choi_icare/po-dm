﻿-- =============================================
-- Author:		Saranya Jayaseelan
-- Create date: 12/04/2019
-- Description:	V12.35
--				14/05 checked v12.36 no change
--				1/07/2019 AG optimized to run < 5min
-- <22Jul2019><Satish>: 
-- 1. Modified the Verified PC part to derive the ITCPercentagePEL from pc_policyperiod.ITCEntitlement_icare
-- 2. Ensured we have a simple join to pc_policyperiod as we have already derived the pc_policyperiod.ID for the claim
--    when CoverREF_temptable_consolidated was built.   
-- 3. In Verified CDR part, commented out ccst_interm_policy_PC as this table is not being populated anymore.
--    Instead we now use CoverREF_temptable_consolidated.         
-- 4. Modified Verified PC part to derive PolicyType_icare from pctl_policytype.TYPECODE
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_POLICY]
	
AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_policy','Cover - v12.41 09/07/2019'
--DROP TABLE ccst_policy

CREATE TABLE [dbo].[ccst_policy](
	[EffectiveDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
	[LUWID] [varchar](30) NULL,
	[EmployerCategory_icare] [int] NULL,
	[ITCPercentagePEL] [decimal](18,2) NULL,
	[LabourHire_icare] [varchar](40) NULL,
	[PolicyNumber] [varchar](40) NULL,
	[PolicyType] [varchar](30) NULL,
	[PolicyType_icare] [varchar](50) NULL,
	[Producercode] [varchar](20) NULL,
	[Producername_icare] [varchar](255) NULL,
	[PublicID] [varchar](50) NOT NULL,
	[Status] [varchar](20) NULL,
	[Verified] [int] NULL
) ON [PRIMARY]


--POLICY
Insert into ccst_policy
Select
	PC_PP.PeriodStart as PC_PP_EffectiveDate,
	PC_PP.PeriodEnd as PC_PP_ExpirationDate,
	RTRIM(cd.Claim_Number) as LUWID,
	cctl_EmpCateg.TYPECODE as EmployerCategory_icare,
	cast(cast(PC_PP.ITCEntitlement_icare as float) as int) as ITCPercentagePEL, --22Jul2019 - Corrected this as per James diff report
	Case when pctl_labour.Name='1 - Labour Hire Firm' then 'labour_hire'
		when  pctl_labour.Name='2 - Not a Labour Hire Firm' then 'not_labour_hire'
		when  pctl_labour.Name='3 - Group training aprenticeship scheme' then 'group_training' 
		end as  LabourHire_icare,
	LTRIM(RTRIM(PC_PP.PolicyNumber)) as PolicyNumber,

	tc.PolicyType as PolicyType,
	--tc.PolicyType_icare as PolicyType_icare,
	pctl_policytype.TYPECODE as PolicyType_icare, --22Jul2019 - modified this to match spec
	pc_producercode.code as  ProducerCode,
	pc_producercode.Description as ProducerName_icare,	
	'EMICS:' + LTRIM(RTRIM(tc.LUWID)) + ':POLICY' as publicid,
	case when PD.policy_Status in (50,52) then 'inforce' END as status,
	'1' as Verified
from CoverREF_temptable_consolidated tc --81750
Left join DM_STG_EMICS.dbo.Claim_detail cd on cd.claim_number = tc.LUWID
left outer join dm_stg_pc.dbo.pc_policyperiod PC_PP --22Jul2019 - We can get the details directly without the above SQL
on PC_PP.ID=tc.PC_PP_ID
		--(select ID,policyID,PolicyNumber,LegacyPolicyNumber_icare,PeriodStart, periodend, TermNumber, CancellationDate,EmployerCategory_icare,PolicyType_icare,WCLabourHireCode_icare,ProducerCodeOfRecordID
		--from dm_stg_pc.dbo.pc_policyperiod
		--Where status=9 --bound
		--and MostRecentModel=1
		--and Retired=0
		--and cast(periodstart as date)<>cast(isnull(cancellationdate,'') as date)  
		--) PC_PP	on substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3)=PC_PP.PolicyNumber
		--and CD.Date_of_Injury >= PC_PP.PeriodStart and cd.Date_of_Injury < PC_PP.PeriodEnd

left outer join DM_STG_PC..pctl_employercategory_icare pctl on pctl.ID = PC_PP.EmployerCategory_icare
left outer join  [DM_STG_CC].[dbo].[cctl_employercategory_icare] cctl_EmpCateg on cctl_EmpCateg.name = pctl.name
left outer join [DM_STG_PC].dbo.pctl_wclabourhirecode_icare pctl_labour on pctl_labour.ID=PC_PP.WCLabourHireCode_icare
left outer join [DM_STG_PC].dbo.pc_producercode on pc_producercode.ID=PC_PP.ProducerCodeOfRecordID
left outer join [DM_STG_PC].dbo.pctl_policytype_icare pctl_policytype on pctl_policytype.ID=PC_PP.PolicyType_icare
left outer join  DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PD		on PD.POLICY_NO = CD.POLICY_NO
--5293



--VERIFIED CDR
;WITH temptable_CDR as (
	select Z.* from (
		select row_number () over (partition by luwid,rownum order by RENEWAL_NO desc) latest, Y.* --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with different period start dates
		from (
			select X.* from (
				select distinct
				Case when CD.is_gst_registered=1 then 100 Else 0 end as ITCPercentagePEL,
				'WorkersComp' as PolicyType,
				'Standard' as PolicyType_icare,			
				NULL as PC_PP_ID,	
				PD.PERIOD_START as PD_EffectiveDate,
				PD.PERIOD_EXPIRY as PD_ExpirationDate,	
				concat ('EMICS_', ltrim(rtrim(CD.Policy_No))) as AccountNumber,	
				PD.RENEWAL_NO,
				LTRIM(RTRIM(CD.Claim_Number)) as LUWID,
				CD.Date_of_Injury,
				LTRIM(RTRIM(CD.Policy_No)) as PolicyNumberCD
				,PD.rownum
				,NULL as Status
				from DM_STG_EMICS.[dbo].CLAIM_DETAIL CD 
				left outer join DM_STG_EMICS.[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No
				left outer join  (select row_number () over (partition by PD.POLICY_NO,PD.PERIOD_START order by PD.RENEWAL_NO desc) rownum, PD.POLICY_NO, PD.PERIOD_START,
				PD.PERIOD_EXPIRY,PD.RENEWAL_NO, PD.POLICY_YEAR from DM_STG_EMICS.[dbo].PREMIUM_DETAIL PD) PD --16/11/2018: To ensure you pick the reocrd with LATEST renewal no when there are two records with same period start dates
					on PD.POLICY_NO = PT.POLICY_NO
						--and PD.POLICY_YEAR = CD.Policy_year --Commenting out this join as sometimes the years don't match
				-- CD.Date_of_Injury+'00:01:00' between PD.PERIOD_START and PD.PERIOD_EXPIRY
					  and CD.Date_of_Injury >= PD.PERIOD_START and CD.Date_of_Injury < PD.PERIOD_EXPIRY
				left outer join DM_STG_CDR.[dbo].[DM_STG_CDR_D_POLICY_MASTER] CDR_PM -- CDR Policy Master table
				on ltrim(rtrim(CDR_PM.POLICY_SOURCE_NUMBER))=ltrim(rtrim(CD.Policy_No))
				where CD.is_Null=0
				and ltrim(rtrim(CD.Claim_number)) NOT in 
				--(select LUWID from ccst_interm_policy_PC) --22Jul2019 - this is not being created any anymore
				(select LUWID from [dbo].[CoverREF_temptable_consolidated])  --22Jul2019 - replaced above table with this table as this is the one that is being created now
				and CDR_PM.POLICY_SOURCE_NUMBER is not null
			) X where (X.rownum=1 or X.rownum is null)
		)Y
	)Z	where Z.latest=1
)
,
 temptable_ccst_policy_VerifiedCDR as  
(
-- 1st part of the SQL - This is to fetch all Verified CDR record that have EMICS PREMIUM_DETAIL matching records i.e. we can get cover start and end dates from EMICS
	select 
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,	
		tmp.PD_EffectiveDate as Cover_EffectiveDate,
		tmp.PD_ExpirationDate as Cover_ExpirationDate,	
		tmp.RENEWAL_NO,					
		tmp.LUWID,
		tmp.Date_of_Injury	
		,tmp.LUWID as Claim_Number		
		,tmp.AccountNumber
		,LTRIM(RTRIM(tmp.PolicyNumberCD))	as PolicyNumberCD	
		,tmp.rownum		
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		'EMICS' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		,NULL as Status
		from coverref_temptable_CDR tmp   --2254 2463
		where PD_EffectiveDate is NOT NULL  --picking ony those verified CDR claims for which we DO have Cover dates in EMICS
	union
-- 2nd part of the SQL - This is to fetch all Verified CDR record that DO NOT have EMICS PREMIUM_DETAIL matching records i.e. we CANNOT get cover start and end dates from EMICS
-- so we go and get the cover dates from matching records in CDR instead
	select 
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,		
		CONVERT(DATETIME2, CONVERT(VARCHAR(10),rp.COVER_COMMENCE_DATE_ID,120)) AS Cover_EffectiveDate,
		CONVERT(DATETIME2, CONVERT(VARCHAR(10),ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID),120)) AS Cover_ExpirationDate,
		rp.POLICY_ID as RENEWAL_NO,
		tmp.LUWID,		
		tmp.Date_of_Injury,
		tmp.LUWID as Claim_Number,
		tmp.AccountNumber,
		LTRIM(RTRIM(tmp.PolicyNumberCD)) as PolicyNumberCD			
		,tmp.rownum
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		'CDR' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		,NULL as Status
		from Coverref_temptable_CDR tmp   --2254 2463
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
	union
-- 3rd part of the SQL - This is to fetch all Verified CDR record that DO NOT have either EMICS PREMIUM_DETAIL matching records nor CDR DM_STG_CDR_E_ROLLING_POLICY matching records
-- so we CANNOT get cover dates from either EMICS or CDR. Hence we are defaulting the cover dates to (Date_Of_Injury-1) and (Date_Of_Injury+1).
		select 
		tmp.ITCPercentagePEL,		
		tmp.PolicyType,
		tmp.PolicyType_icare,
		CAST(DATEADD(day,-1,tmp.Date_of_Injury) as DATE) AS Cover_EffectiveDate, --Removed timestamp as otherwise you have issues later with public ids getting duplicated
		CAST(DATEADD(day,1,tmp.Date_of_Injury) as DATE) AS Cover_ExpirationDate, 
		convert(varchar(10),tmp.Date_of_Injury,112) as RENEWAL_NO,
		tmp.LUWID,
		tmp.Date_of_Injury,
		tmp.LUWID as Claim_Number,
		tmp.AccountNumber,
		LTRIM(RTRIM(tmp.PolicyNumberCD)) as PolicyNumberCD	,		
		tmp.rownum	
		,'EMICS:'+LTRIM(RTRIM(tmp.LUWID))+':Policy' as PUBLICID,
		'NOT_IN_EMICS_OR_CDR' as Cover_period_dates_source_flag
		,1 as ManualVerify_icare,
		Case when len(tmp.Tariff_No) = 3 then tmp.Tariff_No else NULL end as TariffRate_icare
		--tmp.POLICY_YEAR		
		,NULL as Status
		from coverref_temptable_CDR tmp   
		inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ltrim(rtrim(cd.claim_number))=tmp.LUWID
		INNER JOIN DM_STG_CDR.dbo.DM_STG_CDR_D_POLICY_MASTER pm ON RTRIM(LTRIM(cd.Policy_No)) = pm.POLICY_SOURCE_NUMBER
		LEFT OUTER JOIN DM_STG_CDR.dbo.DM_STG_CDR_E_ROLLING_POLICY rp ON pm.POLICY_WCA_ID = rp.POLICY_WCA_ID		
		AND rp.WCA_STATUS_ID = 181644
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) >= rp.COVER_COMMENCE_DATE_ID
		AND CONVERT(VARCHAR(8),cd.Date_of_Injury,112) < ISNULL(rp.TERM_PERIOD_EXPIRY_DATE,rp.COVER_EXPIRY_DATE_ID)
		WHERE cd.Is_Null = 0
		and tmp.PD_EffectiveDate is NULL --picking ony those verified CDR claims for which we DO NOT have Cover dates in EMICS and are trying to get it from CDR
		and rp.POLICY_ID is NULL -- no match found for cover dates in CDR too so we then need to go for default cover dates !
)	
Insert into ccst_policy
select 
	Cover_EffectiveDate,
	Cover_ExpirationDate,
	LUWID,
	NULL as EmployerCategory_icare
	,ITCPercentagePEL
	,NULL as LabourHire_icare
	,LTRIM(RTRIM(PolicyNumberCD)) as PolicyNumber
	,policytype
	,policyType_icare	
	,NULL as producercode
	,NULL as producername_icare
	,publicid
	,NULL as status,
	'1' as Verified
from temptable_ccst_policy_VerifiedCDR	

END