﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 09/07/2019
-- Description:	Contacts - Main Contact V002.19
-- 1. This is the child CM Stored Proc for MAIN CONTACT contact that will called by the master stored proc SP_REC_XFM_CONTACT
-- Please note that CM MAIN CONCTACT (MATCHED) CONTACTS are updated/inserted into CM using API by ETL
-- The following data gets updated/inserted -
-- (a) Contact - SourceSystem_icare,  CRM_Version_icare UPDATED
-- (b) ContactTag - Type: ClaimParty is INSERTED
-- 18-Jul-2019<Satish>: 
-- Added an extra check to see if CM and CRM match is there 
-- Added a drop table check for abst_contacr_employer to ensure rerunnability of the script
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_POST_BGL_HOTFIX_SC15_MC]
	
AS
BEGIN


EXEC SP_05_ADM_MAPPING_VERSION 'abst_employer', 'MC_v2.19';

IF OBJECT_ID('temptable_CM_MC_matched_verified_PC_temp1') is not null
drop table dbo.temptable_CM_MC_matched_verified_PC_temp1;

IF OBJECT_ID('temptable_max_claim_number_MC_Matched') is not null
drop table dbo.temptable_max_claim_number_MC_Matched;

IF OBJECT_ID('ContactREF_temptable_maincontact_matched_CM_contact_and_contacttag') is not null
drop table dbo.ContactREF_temptable_maincontact_matched_CM_contact_and_contacttag;

IF OBJECT_ID('abst_contact_employer') is not null
drop table dbo.abst_contact_employer;

SELECT * INTO temptable_max_claim_number_MC_Matched
FROM
(
select AddressBookUID, max(LUWID) as LUWID
from
(
select 
AddressBookUID, LUWID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null and CM_LinkID is not null and CRM_ID is not null
union
select AddressBookUID, LUWID
from [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
where AddressBookUID is not null and CM_LinkID is not null and CRM_ID is not null
union
select AddressBookUID, LUWID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
where AddressBookUID is not null and CM_LinkID is not null and CRM_ID is not null
)X
group by X.AddressBookUID
)Y


--select 
--AddressBookUID, LUWID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
--where AddressBookUID  in
--(
--select AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
--)

--select AddressBookUID from [dbo].[ContactREF_temptable_MC_WOP_PPLC_VerifiedPC]
--where AddressBookUID  in
--(select 
--AddressBookUID from [dbo].[ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched]
--union
--select AddressBookUID from  [dbo].[ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched]
--)
 





--IF OBJECT_ID('ContactREF_temptable_employer_verifiedCDR') is not null
--drop table REC_XFM.dbo.ContactREF_temptable_employer_verifiedCDR;

--IF OBJECT_ID('temptable_EMPLOYER_VerifiedPC') is not null
--drop table temptable_EMPLOYER_VerifiedPC;

--IF OBJECT_ID('temptable_EMPLOYER_VerifiedCDR') is not null
--drop table temptable_EMPLOYER_VerifiedCDR;
--select CRMVersion_icare, SourceSystem_icare from 


SELECT * INTO temptable_CM_MC_matched_verified_PC_temp1
--ContactREF_CM_temptable_employer_verifiedPC
FROM
(
SELECT 
	 --[RowNumber]
	 --,
	 temp.AddressBookUID as [ABContact_LinkID]
	,cast(cm_contact.CRMVersion_icare as int)+1 as [New_CRMVersion_icare]
	,cm_contact.CRMVersion_icare as [Current_CRMVersion_icare]
	,cm_contact.SourceSystem_icare
	,'claimparty' [Type]
	,'EMICS:' + temp.LUWID + ':INSURED_MC:CLAIMPARTY' as  [PublicID]
	--EMICS:1190123:INSURED_MC:CLAIMPARTY
    ,'EMICS:' + temp.LUWID + ':INSURED_MC:CLAIMPARTY' as External_UniqueID
    ,'EMICS:' + temp.LUWID + ':INSURED_MC:CLAIMPARTY' as External_PublicID
	,'EMICS:' + temp.LUWID + ':INSURED_MC' LUWID
--'EMICS:'+ltrim(rtrim(CD.Claim_Number))+':INSURED'+':BUSINESSADDRESS' as PublicID,
from 
temptable_max_claim_number_MC_Matched temp
inner join [DM_XFM].dbo.[DM_LKP_GW_CM_AB_ABCONTACT] cm_contact
on cm_contact.LinkID=temp.AddressBookUID
left outer join [DM_XFM].dbo.[DM_LKP_GW_CM_ABCONTACTTAG] cm_contacttag
on cm_contact.LinkID = cm_contacttag.[ContactLinkID]
--AND (cm_contacttag.[SC_Type] = 'SC15 - Insured Employer Main Contact' OR cm_contacttag.[SC_Type] IS NULL)
AND cm_contacttag.[Type] = 'claimparty'	
where cm_contacttag.PublicID is null -- check claimparty tag does not exist already in CM- only then we go ahead and create
)X


--This table will be used by James script to create the XML and compare with Dev [dbo].[DM_POST_BGL_HOTFIX_SC15_MC_XML]
--select * into [ContactREF_temptable_maincontact_matched_CM_contact_and_contacttag]
select  PublicID, 
		ABContact_LinkID,
		Current_CRMVersion_icare,
		New_CRMVersion_icare,
		SourceSystem_icare,
		LUWID into abst_contact_employer
from
(
select * from
temptable_CM_MC_matched_verified_PC_temp1
)X



IF OBJECT_ID('temptable_CM_MC_matched_verified_PC_temp1') is not null
drop table dbo.temptable_CM_MC_matched_verified_PC_temp1;

IF OBJECT_ID('temptable_max_claim_number_MC_Matched') is not null
drop table dbo.temptable_max_claim_number_MC_Matched;


END