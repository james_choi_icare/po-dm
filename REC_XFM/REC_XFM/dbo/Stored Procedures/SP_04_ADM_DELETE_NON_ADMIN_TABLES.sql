﻿create PROCEDURE  [dbo].[SP_04_ADM_DELETE_NON_ADMIN_TABLES] as
BEGIN
SET NOCOUNT ON;
DECLARE @dbParam varchar(100) = 'DM_AUDIT'


DECLARE @columns varchar(max) = '', @tableName varchar(100), @query nvarchar(max), @userName varchar(100), @roleName varchar(100);

DECLARE @db_cursor CURSOR 

SET @db_cursor = CURSOR FOR select name from sys.tables where name not like 'adm_%' order by 1;

OPEN @db_cursor

FETCH NEXT FROM @db_cursor
INTO @tableName

WHILE @@FETCH_STATUS = 0
BEGIN	

	SET @query = 'drop table '+@tableName+'';
	exec sp_executesql @query

	FETCH NEXT FROM @db_cursor
	INTO @tableName
END
CLOSE @db_cursor
DEALLOCATE @db_cursor

END