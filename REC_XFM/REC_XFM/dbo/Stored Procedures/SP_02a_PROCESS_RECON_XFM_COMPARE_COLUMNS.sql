﻿CREATE PROC [dbo].[SP_02a_PROCESS_RECON_XFM_COMPARE_COLUMNS] 
	@runID int,@xfm_table varchar(100),@xfm_recon_table varchar(100)
AS
BEGIN

	DECLARE @col_name varchar(100), @query nvarchar(max), @vi int;
	
	DECLARE column_cursor CURSOR FOR
	SELECT c.name ColumnName
		FROM [dbo].[ADM_XFM_TABLE_MAPPING] a
			INNER JOIN sys.tables b ON a.xfm_recon_table = b.name
			INNER JOIN sys.columns c ON b.object_id = c.object_id
		WHERE Process = 1 AND c.name <> 'PublicID' AND c.name NOT LIKE '%_SRC_VALUE' AND xfm_table = @xfm_table
		ORDER BY c.name;

	OPEN column_cursor

	FETCH NEXT FROM column_cursor
	INTO @col_name

	WHILE @@FETCH_STATUS = 0
	BEGIN		

		SET @query = 'DELETE FROM [dbo].[ADM_RECON_RESULT] WHERE Tgt_Table = ''' + @xfm_table + ''' AND Tgt_Column = '''+ @col_name + '''';
		exec sp_executesql @query

		SET @query = 'SELECT @vi = count(*)
						FROM sys.tables b
							INNER JOIN sys.columns c ON b.object_id = c.object_id
						WHERE b.name = ''' + @xfm_recon_table + ''' AND c.name = ''' + @col_name + '_SRC_VALUE'''
		EXEC sp_executesql @query, N'@vi int OUTPUT', @vi OUTPUT

		BEGIN TRY	
			IF @vi = 1
			BEGIN
				SET @query = 'INSERT INTO [dbo].[ADM_RECON_RESULT]
								SELECT ''' + @xfm_table + ''' Tgt_Table, ''' + @col_name + ''' Tgt_Column, a.PublicID XFM_Key, b.PublicID Expected_Key,  a.' + @col_name + ' XFM_Value, b.' + @col_name + ' Expected_Value, b.' + @col_name + '_SRC_VALUE, ' + cast(@runID as varchar(10)) + ', a.LUWID
									FROM [DM_XFM].dbo.' + @xfm_table + ' a
										FULL JOIN ' + @xfm_recon_table + ' b ON a.PublicID = b.PublicID
									WHERE a.' + @col_name + ' <> b.' + @col_name + ' AND a.PublicID is not null AND b.PublicID is not null ';
			END
			ELSE
			BEGIN
				SET @query = 'INSERT INTO [dbo].[ADM_RECON_RESULT]
								SELECT ''' + @xfm_table + ''' Tgt_Table, ''' + @col_name + ''' Tgt_Column, a.PublicID XFM_Key, b.PublicID Expected_Key,  a.' + @col_name + ' XFM_Value, b.' + @col_name + ' Expected_Value, null, ' + cast(@runID as varchar(10)) + ', a.LUWID
									FROM [DM_XFM].dbo.' + @xfm_table + ' a
										FULL JOIN ' + @xfm_recon_table + ' b ON a.PublicID = b.PublicID
									WHERE a.' + @col_name + ' <> b.' + @col_name + ' AND a.PublicID is not null AND b.PublicID is not null ';
			END
			
			exec sp_executesql @query

			SET @query = 'DELETE FROM ADM_RECON_RESULT_PASSED WHERE Tgt_Table = ''' + @xfm_table + ''' AND Tgt_Column = ''' + @col_name + ''';
							INSERT INTO ADM_RECON_RESULT_PASSED
								SELECT Tgt_Table, Tgt_Column, count(*),' + cast(@runID as varchar(10)) + '
								FROM (SELECT ''' + @xfm_table + ''' Tgt_Table, ''' + @col_name + ''' Tgt_Column
									FROM [DM_XFM].dbo.' + @xfm_table + ' a
										INNER JOIN ' + @xfm_recon_table + ' b ON a.PublicID = b.PublicID
									WHERE a.' + @col_name + ' = b.' + @col_name + ' OR (a.' + @col_name + ' is null AND b.' + @col_name + ' is null) ) xx 
									GROUP BY Tgt_Table, Tgt_Column ';
			exec sp_executesql @query
	
		END TRY
		BEGIN CATCH
			IF ERROR_NUMBER() is not null
			BEGIN
				INSERT INTO [dbo].[ADM_RECON_RUN_ERROR]
				SELECT 1, @query + ' ' + ERROR_MESSAGE()
			END
		END CATCH

		FETCH NEXT FROM column_cursor
		INTO @col_name
	END
	CLOSE column_cursor
	DEALLOCATE column_cursor

END