﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_EFTDATA]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 20/05
-- Description:	Contacts ccst_eftdata
-- Version:		20/05 initial build to v002.2
--				30/05 Finished EFT builds for Employer (v1) and Vendor (v2.1) and Injured worker (v2.2). NOTE: EXCLUDED isApproved and isPrimary logic for now-need help from mapper
--				14/08 Testing joins for Vendor blocks - see AGTEST comments
--					  Testing filter in MAXP in injuredworker - see AGTEST
--				5/09 DMIG-8121 - Updated isapproved_icare and isprimary logic for injured worker contact to fix QA defect. Added placeholder date for null cases in approved_icare for all contacts
--				12/09 update vendor and employer sc4(CM) join conditions to include eft table details
-- =============================================

/* manual check

Select * from REC_XFM.dbo.ccst_eftdata qa
LEFT JOIN DM_XFM_LD.dbo.DM_XFM_EFTDATA xfm on xfm.publicid = qa.publicid
where ISNULL(xfm.publicid,'') = ISNULL(qa.publicid,'')
and xfm.isprimary != qa.isprimary --ISNULL(xfm.OrdinaryEarnings_icare,'') = ISNULL(qa.OrdinaryEarnings_icare,'') --PIAWEFirst52_icare
and xfm.contactID like '%INJURED%'

*/


	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_eftdata','Contacts - v13 17/06/2019'
	--drop table ccst_Eftdata

	EXEC SP_REC_XFM_EFTDATA_VENDORSELECTION_STAGING
--/*=================== EFTDATA Claim Payment Run - Injured worker ================*/ v004

select * into #temp_max_payment from (
SELECT max(Payment_no) AS PAYMENT_NO,Payee_account_name,Payee_bsb,Payee_account_no,cpr1.Claim_Number from DM_STG_EMICS.dbo.claim_payment_run cpr1
	INNER join DM_STG_EMICS.dbo.BSB bsb ON CPR1.Payee_bsb=BSB.NO 
	inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on cd.Claim_Number=cpr1.Claim_number 
	 and is_Null=0 --This is added ghere to avoid picking up the Payment made of on a non-nullifide claims
	 and cpr1.Payee_account_no is not null
	 and cpr1.payee_type in ('1')
	 Where cheque_status <> 3 --AGTEST 
	  GROUP BY Payee_account_name,Payee_bsb,Payee_account_no,cpr1.Claim_Number)a

select distinct 
	LTRIM(RTRIM(cpr.Payee_account_name)) as AccountName,
	Case when sc4.retired = 1 
		then 
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
		'EMICS:' + convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)) + '_' + convert(varchar, cpr.Payee_account_no) + '_' + '4' + '_' + convert(varchar, Payee_Code) + '_' + convert(varchar, AccountName_rnk) + '_1'
		),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
		Else  
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
		'EMICS:' + convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)) + '_' + convert(varchar, cpr.Payee_account_no) + '_' + '4' + convert(varchar, Payee_Code )+ '_' +convert(varchar,  AccountName_rnk)
		),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')

		End as AddressBookUID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1
			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where --For vendors - need to check cpr1.Payee_code = cpr.Payee_code
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When (sc4.PublicID is null and sc5.PublicID is not null) Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1

			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When  (sc5.PublicID is not null and sc4.PublicID is not null) and cpr.Payee_account_no = isnull(sc4.BankAccountNumber,'') then sc4.Approved_icare 
	End as approved_icare,
	cpr.Payee_account_no as BankAccountNumber,
	NULL as BankAccountType,
		case when sc4.PublicID is not null and sc5.PublicID is not null then sc4.BankBranchName_icare
		  else bsb.branch end
		   as BankBranchName_icare, -- SJ
	sc3.BankName as BankName,
	SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6) as BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:' + RTRIM(cpr.Claim_number) + ':INJURED' as ContactID,
	NULL as ExternalLinkID,
	Case when (sc5.PublicID is null )Then

		Case when ISNULL(cpr.Cheque_Status,00) = 9  and cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment  group by claim_number)  and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9 and Payee_type in (1) 
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1 
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null and ISNULL(cpr.Cheque_Status,00) = 9 and cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment group by claim_number))
						then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,
			When (eft.object is not null  and  cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment group by claim_number))
					 then 1
			Else 0
			End

		When (sc5.PublicID is not null and sc4.PublicID is null  ) Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and con_eft.primary_eft is null and cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment group by claim_number) and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number 
				and ISNULL(cpr1.Cheque_Status,00) = 9 and Payee_type in (1) -- SJ Payee type 
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1 
				) --For a claim, if there is only 1 account paid to
					Then 1

				When (eft.Object is null and con_eft.primary_eft is null  and  ISNULL(cpr.Cheque_Status,00) = 9 and cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment group by claim_number)
				 ) --If claim payment run bank details are more up to date than your EMICS eft bank details
					 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
				when  (eft.Object is not null and con_eft.primary_eft is null and cpr.Payment_no in
					 (select max(payment_no) from #temp_max_payment  group by claim_number) ) then 1
				when( (isnull(cpr.Payee_account_no,'') <> isnull(sc4.BankAccountNumber,'') or sc4.BankAccountNumber is null)
					and isNULL(cpr.Cheque_Status,00 ) = 9) then 0 		
				--when (eft.Object is not null and cpr.paid_date<isnull(eft.create_date, eft.authorised_dte) ) then 1
				Else 0
			End
		When (sc5.PublicID is not null and sc4.PublicID is not null) and cpr.Payee_account_no = isnull(sc4.BankAccountNumber,'') then sc4.IsPrimary
		else 0
		End as IsPrimary,
	LTRIM(RTRIM(cpr.Claim_number)) as LUWID,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
'EMICS:' + ISNULL(RTRIM(cpr.Claim_number)+'_','') +Convert(varchar, cpr.payee_account_no) + '_' + '1' + '_' + Convert(Varchar, maxp.PAYMENT_NO)),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID
	--NULL as WestpacFail_icare

into ccst_eftdata

from 
(select a.*, DENSE_RANK() OVER (PARTITION BY Claim_Number,payee_bsb,Payee_account_no ORDER BY Payee_account_name) as AccountName_rnk from DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN a)  cpr
inner join DM_STG_EMICS.dbo.bsb bsb on cpr.Payee_bsb =bsb.no
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON CPR.Claim_number=CD.Claim_Number
Left join DM_STG_EMICS.dbo.eft  eft ON eft.Object=CD.Claim_Number and eft.bsb =  cpr.Payee_bsb -- SJ
INNER JOIN #temp_max_payment MAXP ON MAXP.Claim_number=CPR.Claim_number
	AND MAXP.Payee_account_name=CPR.Payee_account_name
	AND MAXP.Payee_account_no=CPR.Payee_account_no
	AND MAXP.Payee_bsb=CPR.Payee_bsb
	AND MAXP.PAYMENT_NO=CPR.Payment_no
--sc3
LEFT OUTER JOIN (select a.id,b.PublicID AS ContactID,a.RETIRED__C, a.LastName,a.firstname,E.BankAccountNumber,E.BankBranchName_icare, e.Retired,
	E.BankName,E.BankRoutingNumber,E.BankType_icare,AccountName,e.IsPrimary, e.Approved_icare
	from DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e
	where  b.LinkID=a.id
	and e.ContactID=b.ID
	and e.retired=0 and e.retired=0
and b.PublicID like '%EMICS%') sc3 ON sc3.ContactID='EMICS:'+ convert(varchar, LTRIM(RTRIM(cpr.Claim_number)))+':INJURED' -- SJ to check and confirm with Sahana 
AND sc3.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)
AND sc3.BankAccountNumber=CPR.Payee_account_no
AND sc3.AccountName=cpr.Payee_account_name and eft.Object_type='W'-- TO PICK ONLY BANK ACCOUNT FOR VENDORS

--sc4
left outer join (select b.PublicID as EMICS_ContactID,b.LinkID as Contact_LINKID,crm.id as CRMContactID ,a.* from dm_stg_cm..ab_eftdata a
	inner join dm_stg_cm..ab_abcontact b on a.ContactID=b.ID
	inner join DM_STG_CRM..DM_STG_CRM_CONTACT crm on crm.id=b.LinkID
	) sc4 on sc4.EMICS_ContactID= 'EMICS:' +  convert(varchar, RTRIM(cd.Claim_Number)) + ':INJURED'
		and sc4.BankAccountNumber=cpr.Payee_account_no
		and sc4.BankBranchName_icare=bsb.BRANCH
		and sc4.BankName=bsb.BANK
		and sc4.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_BSB,6),6),4,6)
		and sc4.AccountName=cpr.Payee_account_name
		and --sc4.EMICS_ContactID is  null and -- SJ 9/3/2019
		 cpr.Payee_account_no is not null 
--sc5 9/3/2019
Left outer join (Select b.* from dm_stg_cm..ab_abcontact b where b.retired = 0) sc5 on sc5.PublicID = 'EMICS:' + RTRIM(cpr.claim_number) + ':INJURED' -- and sc5.PublicID is null   -- SJ 9/3/2019
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 
--SJ 
LEft Outer JOIN (select b.publicid  as primary_eft from  DM_STG_CM..ab_eftdata a
	inner join DM_STG_CM..ab_abcontact b on a.ContactID=b.ID
	inner join DM_STG_CRM..DM_STG_CRM_CONTACT crm on crm.id=b.LinkID
	where a.isprimary =1 ) con_eft
	on con_eft.primary_eft = sc5.publicid 
WHERE Payee_TYPE IN (1)-- TO PICK ONLY BANK ACCOUNT FOR INjured Workers
AND ISNULL(CHEQUE_STATUS,'00') <>3-- Consideriing only successful payments
AND CD.IS_NULL=0 -- ONLY INSCOPE CLAIMS
and CPR.Payee_account_no is not null--15311 records

----/*=================== EFTDATA EFT - Injured worker ================*/
Insert into ccst_eftdata

select 
	LTRIM(RTRIM(eft.Account_Name)) as AccountName,
	--REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	--'EMICS:'+convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)) +'_'+convert(varchar, account_no) +'_'+'1'+'_'+ '0' +'_'+ RTRIM(LTRIM(convert(varchar, ISNULL(CRMContactID,Object))))+ '_'+convert(varchar, AccountName_rnk) 
	--),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
	
	-- V6
	CASE WHEN SC4.Retired = 1 
		THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:'+convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)) +'_'+convert(varchar, account_no) +'_'+'4' + '_' + Payee_Code + '_'+convert(varchar, AccountName_rnk) + '_1'
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
		ELSE 
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:'+convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)) +'_'+convert(varchar, account_no) +'_'+'4'+ '_' + Payee_Code + '_'+convert(varchar, AccountName_rnk) 
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') END
		as AddressBookUID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte) then 1
			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(eft.create_date, eft.authorised_dte) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where --For vendors - need to check cpr1.Payee_code = cpr.Payee_code
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When sc4.PublicID is null Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte) then 1
			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(eft.create_date, eft.authorised_dte) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When sc4.PublicID is not null Then sc3.Approved_icare 
		End as approved_icare,
	eft.Account_no as BankAccountNumber,
	NULL as BankAccountType,
		case when sc4.PublicID is not null and sc5.PublicID is not null then sc4.BankBranchName_icare
		  else bsb.branch end
		   as BankBranchName_icare,-- SJ
	sc3.BankName,
	sc3.BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:' + RTRIM(eft.object) + ':INJURED' as ContactID,
	NULL as ExternalLinkID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte))
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When sc4.PublicID is null Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte)) --If claim payment run bank details are more up to date than your EMICS eft bank details
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End
		-- SJ 09/03/2019
		--When (sc5.PublicID is not null or sc4.PublicID is not null) and sc3.IsPrimary = 1 Then 0 --If the contact and it's eftdata is available in CM and has isPrimary already set in CM, no actions required - hence 0		
		When (sc5.PublicID is not null and sc4.PublicID is not null)  then sc4.IsPrimary -- Not necessary to check if bank details exists in CPR -- SJ  09/03/2019
		Else 0
		End as isPrimary,
	cd.claim_number as LUWID,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + ISNULL(RTRIM(eft.Object) + '_','') + convert(varchar, eft.account_no) + '1'+'_' + RTRIM(convert(Varchar,eft.object)) + '_' + '0' ),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID

from (Select a.* , DENSE_RANK() OVER (PARTITION BY Object,BSB,account_no ORDER BY Account_Name) as AccountName_rnk from DM_STG_EMICS.dbo.EFT a where OBJECT_TYPE='W'-- TO PICK ONLY BANK ACCOUNT FOR injured workers
	) eft
inner join DM_STG_EMICS.dbo.BSB bsb on eft.BSB=bsb.NO
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON eft.Object=CD.Claim_Number
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.[CLAIM_PAYMENT_RUN] where payee_type=1 and cheque_status != 3) cpr ON eft.[Object] = cpr.Claim_Number
 AND eft.[BSB] = cpr.[Payee_bsb] 
 AND eft.[Account_no] = cpr.[Payee_account_no]
 AND eft.[Account_name] = cpr.[Payee_account_name]
LEFT OUTER JOIN (select a.id,b.PublicID AS ContactID,a.LastName,a.firstname,E.BankAccountNumber,E.BankBranchName_icare,E.BankName,E.BankRoutingNumber,E.BankType_icare,AccountName,e.IsPrimary,e.Approved_icare
	from DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e
	where  b.LinkID=a.id
	and e.ContactID=b.ID
	and e.retired=0 and e.retired=0
	and b.PublicID like '%EMICS%') sc3 ON sc3.ContactID='EMICS:'+RTRIM(LTRIM(cpr.claim_number))+':INJURED' -- SJ to check and confirm with Sahana 
	AND sc3.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)
	AND sc3.BankAccountNumber=CPR.Payee_account_no
	AND sc3.AccountName=cpr.Payee_account_name

left outer join (select b.PublicID as EMICS_ContactID,b.LinkID as Contact_LINKID,crm.id as CRMContactID ,a.* from dm_stg_cm..ab_eftdata a
	inner join dm_stg_cm..ab_abcontact b on a.ContactID=b.ID
	inner join DM_STG_CRM..DM_STG_CRM_CONTACT crm on crm.id=b.LinkID
	) sc4 on sc4.EMICS_ContactID= 'EMICS:' +  RTRIM(eft.Object) + ':INJURED'
	and sc4.BankAccountNumber=eft.Account_no
	and sc4.BankBranchName_icare=bsb.BRANCH
	and sc4.BankName=bsb.BANK
	and sc4.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(eft.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(eft.BSB,6),6),4,6)
	and sc4.AccountName=eft.Account_name
	-- And sc4.EMICS_ContactID is null -- SJ 09/03/2019

left outer join (select b.* from dm_stg_cm..ab_abcontact b WHERE  B.RETIRED=0 ) sc5
on sc5.PublicID= 'EMICS:' +  RTRIM(eft.Object ) + ':INJURED'
--and sc5.PublicID is null -- SJ 09/03/2019


WHERE CPR.Claim_number IS NULL--EFT RECORDS NOT PULLED AS PART OF CLAIM PAYMENT RUN BLOCK
and cd.is_null=0
 and eft.account_no is not null--3697 records

--/*=================== EFTDATA Claim Payment Run - VENDOR ================*/

Insert into ccst_eftdata
--Vendors CPR

Select Distinct
	cpr.Payee_account_name as AccountName,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + CONVERT(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.PAYEE_BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)
	+LEFT(cpr.PAYEE_BSB,6),6),4,6)) + '_' + CONVERT(varchar, cpr.payee_account_no) + '_' + '4' + '_' + CONVERT(varchar, cpr.payee_code)
	 +'_'+ CONVERT(varchar, AccountNameRank) 
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
	as AddressBookUID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1
			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where --For vendors - need to check cpr1.Payee_code = cpr.Payee_code
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When (sc4.PublicID is null and sc5.PublicID is not null) Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1

			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When  (sc5.PublicID is not null and sc4.PublicID is not null) and cpr.Payee_account_no = isnull(sc4.BankAccountNumber,'') then sc4.Approved_icare 
	End as approved_icare,
	cpr.Payee_account_no as BankAccountNumber,
	NULL as BankAccountType,
	coalesce(sc4.BankBranchName_icare,bsb.branch) as BankBranchName_icare, -- sJ
	Coalesce(bsb.Bank, sc3.bankName) as BankName,
	SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.PAYEE_BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.PAYEE_BSB,6),6),4,6) as BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:' +  RTRIM(cpr.Claim_Number) +'_'+RTRIM(convert(varchar,cpr.Payee_code) ) + ':VENDOR' as ContactID,
	NULL as ExternalLinkID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number, cpr1.payee_account_no  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte))
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When sc4.PublicID is null Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte)) --If claim payment run bank details are more up to date than your EMICS eft bank details
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When (sc5.PublicID is not null or sc4.PublicID is not null) and sc3.IsPrimary = 1 Then 0 --If the contact and it's eftdata is available in CM and has isPrimary already set in CM, no actions required - hence 0		
		
		End as isPrimary,
	cpr.Claim_number as LUWID,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:'+ISNULL(RTRIM(cpr.Claim_Number)+'_','')+CONVERT(VARCHAR,cpr.Payee_account_no)+'4' +'_'+CONVERT(VARCHAR,cpr.Payee_code)+'_'+CONVERT(VARCHAR,MAXP.PAYMENT_NO) ),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID


From (Select cpr.*, DENSE_RANK() OVER (PARTITION BY payee_code,payee_bsb, payee_account_no order by paid_date) as AccountNameRank 
	from DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr) cpr
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD on cpr.Claim_number = cd.Claim_Number
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

INNER join DM_STG_EMICS.dbo.BSB bsb ON cpr.Payee_bsb=BSB.NO 
Left join DM_STG_EMICS.dbo.eft  eft ON eft.Object=CD.Claim_Number
INNER JOIN (SELECT max(Payment_no) AS PAYMENT_NO,CPR.Payee_Code,CPR.Payee_account_no,Payee_account_name,Payee_bsb,CPR.Claim_number from DM_STG_EMICS.dbo.claim_payment_run cpr
	INNER join DM_STG_EMICS.dbo.BSB bsb ON CPR.Payee_bsb=BSB.NO 
	inner join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on cd.Claim_Number=cpr.Claim_number 
	WHERE 
		is_Null = 0
		and	cpr.[Payee_account_no] IS NOT NULL
		and cpr.Payee_type in ('3','4')
		and isnull(cheque_status, '00') <> 3 --AGTEST: Testing this where statement against next run -- cpr.Payee_type in ('3','4')
	GROUP BY CPR.Payee_Code,CPR.Payee_account_no,Payee_account_name,Payee_bsb,CPR.Claim_number) MAXP ON cpr.Payee_Code=MAXP.Payee_Code
		 AND cpr.Payee_account_no=MAXP.Payee_account_no
		 AND cpr.Payee_account_name=MAXP.Payee_account_name
		 AND cpr.Payee_bsb=MAXP.Payee_bsb
		 AND cpr.Claim_number=MAXP.Claim_number
		 AND cpr.PAYMENT_NO=MAXP.PAYMENT_NO
--sc3
LEFT OUTER JOIN (select a.id,b.PublicID AS ContactID,a.LastName,a.firstname,E.BankAccountNumber,E.BankBranchName_icare,E.BankName,E.BankRoutingNumber,E.BankType_icare,AccountName,e.IsPrimary,e.Approved_icare
	from DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e
	where  b.LinkID=a.id
	and e.ContactID=b.ID
	and e.retired=0 and e.retired=0
	and b.PublicID like '%EMICS%') sc3 ON sc3.ContactID='EMICS:'+RTRIM(cpr.PAYEE_CODE)+':VENDOR'
	AND sc3.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.Payee_bsb,6),6),4,6)
	AND sc3.BankAccountNumber=cpr.Payee_account_no
	AND sc3.AccountName=cpr.Payee_account_name

Left outer join (select b.PublicID as EMICS_ContactID,b.LinkID as Contact_LINKID,a.* from dm_stg_cm..ab_eftdata a
inner join dm_stg_cm..ab_abcontact b on a.ContactID=b.ID) sc4 on sc4.EMICS_ContactID= 'EMICS:' +  RTRIM(convert(varchar,cpr.Payee_code) ) + ':VENDOR'
and 'EMICS:'+REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(replace(LinkID,'EMICS:',''),'a','0'),'c','1'),'e','2'),'g','3'),'k','4'),'m','5'),'q','6'),'s','7'),'w','8'),'z','9')=
'EMICS:'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(CONVERT(varchar(10),cpr.Payee_bsb),6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(CONVERT(varchar(10),cpr.Payee_bsb),6),6),4,3) +'_'+convert(varchar,cpr.Payee_account_no)+'_'+'4'+'_'+RTRIM(convert(varchar,cpr.Payee_code) )--+'_'+convert(varchar,AccountName_rnk)
and sc4.EMICS_ContactID is  null
left outer join (select b.* from dm_stg_cm..ab_abcontact b) sc5 on sc5.PublicID= 'EMICS:' +  RTRIM(convert(varchar,cpr.Payee_code) ) + ':VENDOR'
and sc5.PublicID is  null

WHERE Payee_TYPE IN (3,4)-- TO PICK ONLY BANK ACCOUNT FOR VENDORS
AND ISNULL(Cheque_Status,'00') <>3-- Consideriing only successful payments
AND CD.IS_NULL=0 -- ONLY INSCOPE CLAIMS


--/*=================== EFTDATA EFT - VENDOR ================*/

Insert into ccst_eftdata

select 
	eft.Account_name as AccountName,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:'+convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),4,6)) +'_'+convert(varchar, account_no)+'_'+'4'+'_'+RTRIM(Object)+'_'+convert(varchar, eft.AccountNameRank) 
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
	as AddressBookUID,
	ISNULL(eft.Authorised_ind,0) as Approved_icare,
	eft.Account_no as BankAccountNumber,
	NULL as BankAccountType,
	coalesce(sc4.BankBranchName_icare,bsb.branch) as BankBranchName_icare,
	sc3.BankName,
	SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),4,6) as BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:' +  RTRIM(VS.Claim_Number) +'_'+RTRIM(convert(varchar,eft.Object) ) + ':VENDOR' as ContactID,
	NULL as ExternalLinkID,
	

	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte))
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When sc4.PublicID is null Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number, payee_account_no  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte)) --If claim payment run bank details are more up to date than your EMICS eft bank details
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When (sc5.PublicID is not null or sc4.PublicID is not null) and sc3.IsPrimary = 1 Then 0 --If the contact and it's eftdata is available in CM and has isPrimary already set in CM, no actions required - hence 0		
		
		End as isPrimary,
	RTRIM(vs.Claim_Number) as LUWID,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:'+ISNULL(RTRIM(vs.Claim_Number),'')+'_'+CONVERT(VARCHAR,eft.account_no)+'_4' +'_'+RTRIM(LTRIM(CONVERT(VARCHAR,eft.Object)))+'_'+'0' ),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID

from (Select eft.* , DENSE_RANK() OVER (PARTITION BY Object,BSB,account_no ORDER BY eft.Account_Name) as AccountNameRank from DM_STG_EMICS.dbo.EFT eft) eft
inner join DM_STG_EMICS.dbo.BSB bsb on eft.BSB=bsb.NO
LEFT JOIN INTM_EXTRACT_VENDORSELECTION VS ON VS.Creditor_no=EFT.Object 
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON VS.Claim_number=CD.Claim_Number
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

--LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.[CLAIM_PAYMENT_RUN]  where payee_type in (3,4) and isnull(cheque_status,'00')<>3) cpr ON eft.[Object] = cpr.[Payee_Code]
LEFT OUTER JOIN DM_STG_EMICS.dbo.[CLAIM_PAYMENT_RUN] cpr  on payee_type in (3,4) and isnull(cheque_status,'00')<>3
and cpr.claim_number = cd.Claim_Number-- AGTEST: Testing this join

and eft.[Object] = cpr.[Payee_Code] 
 AND eft.[BSB] = cpr.[Payee_bsb] 
 AND eft.[Account_no] = cpr.[Payee_account_no]
 AND eft.[Account_name] = cpr.[Payee_account_name]
LEFT OUTER JOIN (select a.id,b.PublicID AS ContactID,a.LastName,a.firstname,
	E.BankAccountNumber,E.BankBranchName_icare,E.BankName,E.BankRoutingNumber,E.BankType_icare,AccountName,e.IsPrimary,e.Approved_icare
from DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e
where  b.LinkID=a.id
and e.ContactID=b.ID
and e.retired=0 and e.retired=0
and b.PublicID like '%EMICS%') sc3 ON sc3.ContactID='EMICS:'+RTRIM(PAYEE_CODE)+':VENDOR'
AND sc3.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(Payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(Payee_bsb,6),6),4,6)
AND sc3.BankAccountNumber=eft.Account_no--CPR.Payee_account_no
AND sc3.AccountName=eft.Account_name--Payee_account_name

left outer join (select b.PublicID as EMICS_ContactID,b.LinkID as Contact_LINKID,a.* from dm_stg_cm..ab_eftdata a
inner join dm_stg_cm..ab_abcontact b on a.ContactID=b.ID
) sc4 on sc4.EMICS_ContactID= 'EMICS:' +  RTRIM(convert(varchar,eft.Object) ) + ':VENDOR'
-- SJ commented out joins on Linkid and added join conditions on bank details
AND sc4.BankRoutingNumber=SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(eft.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(eft.BSB,6),6),4,6)
AND sc4.BankAccountNumber=eft.Account_no
AND sc4.AccountName=eft.Account_name
--and 'EMICS:'+REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(replace(LinkID,'EMICS:',''),'a','0'),'c','1'),'e','2'),'g','3'),'k','4'),'m','5'),'q','6'),'s','7'),'w','8'),'z','9')=
--'EMICS:'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(CONVERT(varchar(10),eft.BSB),6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(CONVERT(varchar(10),eft.bsb),6),6),4,3) +'_'+convert(varchar,eft.Account_no)+'_'+'4'+'_'+RTRIM(convert(varchar,eft.Object) )--+'_'+convert(varchar,AccountName_rnk)
--and sc4.EMICS_ContactID is  null 

left outer join (select b.* from dm_stg_cm..ab_abcontact b ) sc5 on sc5.PublicID= 'EMICS:' +  RTRIM(convert(varchar,eft.Object) ) + ':VENDOR'
and sc5.PublicID is  null 

WHERE CPR.Claim_number IS NULL--EFT RECORDS NOT PULLED AS PART OF CLAIM PAYMENT RUN BLOCK
and cd.is_null=0
AND eft.OBJECT_TYPE='C'-- TO PICK ONLY BANK ACCOUNT FOR VENDORS/CREDITORS


/*=================== EFTDATA CLAIM PAYMENT RUN - EMPLOYER ================*/
Insert into ccst_eftdata
Select distinct
	--cpr.claim_number, payee_account_name, payee_account_no, payee_bsb, payee_type
	cpr.payee_account_name as AccountName,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),4,6)) + 
	'_' + convert(varchar, cpr.Payee_account_no) + '_2_' + convert(varchar, MP.MaxPayment) + '_' + rtrim(ISNULL(sc4.ID, cpr.claim_number)) + '_' 
	+ convert(varchar, DENSE_RANK() OVER (PARTITION BY cpr.claim_number, cpr.payee_bsb, cpr.payee_account_no order by cpr.payee_account_name)) 
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
		as AddressBookUID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1
			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where --For vendors - need to check cpr1.Payee_code = cpr.Payee_code
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When (sc4.ContactID is null and sc5.PublicID is not null) Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date>=isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then 1

			When ISNULL(cpr.Cheque_Status,00) = 9 and cpr.paid_date<isnull(isnull(eft.create_date, eft.authorised_dte),CONVERT(datetime,'19000101',112)) then eft.Authorised_ind
			When  ISNULL(cpr.Cheque_Status,00) != 9 --Latest payment is unsuccessful
			 and exists --checks has any history of any successful payment
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 Inner join DM_STG_EMICS.dbo.BSB bsb1 on bsb.NO = cpr1.payee_bsb
				Where
				cpr1.Payee_account_no =cpr.Payee_account_no
				and bsb1.BRANCH=bsb.BRANCH
				and bsb1.BANK=bsb.BANK
				and cpr1.Payee_bsb=cpr.payee_BSB
				and cpr1.Payee_account_name=cpr.Payee_account_name
				and ISNULL(cpr1.Cheque_Status,00) = 9
				)
			and eft.Object is null --No data available in EMICS eft table
			Then 1
			Else 0
			End
		When  (sc5.PublicID is not null and sc4.ContactID is not null) and cpr.Payee_account_no = isnull(sc4.BankAccountNumber,'') then sc4.Approved_icare 
	End as approved_icare,

	cpr.payee_account_no as BankAccountNumber,
	NULL as BankAccountType,
	coalesce(sc6.BankBranchName_icare,bsb.branch) as BankBranchName_icare,--SJ
	BSB.Bank as BankName,
	SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),4,6) as BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:'+RTRIM(cpr.claim_number) + ':INSURED' as ContactID,
	NULL as ExternalLinkID,
	Case when sc5.PublicID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and 
			exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
				Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte))
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			
			Else 0
			End
		When (sc5.PublicID is not null or sc4.ContactID is not null) and sc4.IsPrimary = 1 Then 0 --If the contact and it's eftdata is available in CM and has isPrimary already set in CM, no actions required - hence 0		
		
		End as isPrimary,	


	RTRIM(cpr.claim_number) as LUWID,	
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + rtrim(cpr.claim_number) + '_' + convert(varchar, cpr.Payee_account_no) + '_2_' + convert(varchar, MP.MaxPayment)),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID
	--NULL as WestpacFail_icare

From DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr
Inner join DM_STG_EMICS.dbo.claim_Detail cd on cd.claim_number = cpr.claim_number
--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(cd.Claim_Number)) 

Inner join DM_STG_EMICS.dbo.bsb on bsb.no = payee_bsb -- SJ V2  -- and cpr.payee_account_no is not null
 Inner join DM_STG_EMICS.dbo.EFT eft on eft.Object = cd.Policy_no
--SC1 Max payment No
Inner join (Select Payee_account_name,Payee_bsb,Payee_account_no,cpr.Claim_number,max(payment_no) as MaxPayment from DM_STG_EMICS.dbo.claim_payment_run cpr
	Inner join DM_STG_EMICS.dbo.bsb on cpr.payee_bsb = bsb.no
	Inner join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number = cpr.claim_number
	and is_null = 0
	and cpr.payee_account_no is not null
	and cpr.payee_type in ('2')	
   and isnull(cheque_status,00)<>3
	Group by payee_account_name, payee_bsb, Payee_account_no, cpr.claim_number) MP on --SJ V2 mp.MaxPayment = cpr.Payment_no
	MP.Claim_number = cpr.Claim_number 
	and mp.Payee_account_name = cpr.Payee_account_name
	and mp.Payee_bsb = cpr.Payee_bsb
	and mp.Payee_account_no = cpr.Payee_account_no

Left outer join DM_STG_CRM..DM_STG_CRM_POLICY CRM on cd.policy_no = crm.LEGACY_POLICYNO__C

inner join DM_STG_CM.dbo.ab_abcontact sc5 on crm.ACCOUNT__C = sc5.LinkID  

Left outer join (Select e.Approved_icare, e.IsPrimary, a.id, b.PublicID as ContactID, B.LinkID, a.LastName, a.firstname, e.BankAccountNumber, e.BankBranchName_icare, E.BankName, E.BankRoutingNumber, E.BankType_icare, AccountName
	From DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e,DM_STG_CRM..dm_STG_CRM_ACCOUNT ac
	Where b.LinkID = ac.id
	and e.contactID = b.ID
	--and e.ContactID = b.ID -- SJ v 2
	and b.retired = 0 and e.retired =0
	and a.ACCOUNTID = ac.id
	and b.PublicID like 'ab%') sc4 on sc4.LinkID = CRM.ACCOUNT__C and 
	sc4.bankName = bsb.Bank and
	sc4.bankbranchName_icare = bsb.Branch and
	sc4.BankRoutingNumber = SUBSTRING(RIGHT(REPLICATE('0',6)+ LEFT(cpr.Payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),4,6) and --SJ V2 -- added CPR.payee_bsb
	sc4.BankAccountNumber = cpr.payee_account_no and
	sc4.AccountName = cpr.payee_account_name -- SJ v2
	--and sc4.ContactID is null
Left outer join (Select e.Approved_icare, e.IsPrimary, a.id, b.PublicID as ContactID, B.LinkID, a.LastName, a.firstname, e.BankAccountNumber, e.BankBranchName_icare, E.BankName, E.BankRoutingNumber, E.BankType_icare, AccountName
	From DM_STG_CRM..DM_STG_CRM_CONTACT a, dm_stg_cm..ab_abcontact b, dm_stg_cm..ab_eftdata e,DM_STG_CRM..dm_STG_CRM_ACCOUNT ac
	Where b.LinkID = ac.id
	and e.contactID = b.ID
	--and e.ContactID = b.ID -- SJ v 2
	and b.retired = 0 and e.retired =0
	and a.ACCOUNTID = ac.id
	and b.PublicID like 'ab%') sc6 on sc6.LinkID = CRM.ACCOUNT__C and 
	sc6.bankName = bsb.Bank and
	--sc6.BankRoutingNumber = SUBSTRING(RIGHT(REPLICATE('0',6)+ LEFT(cpr.Payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(cpr.payee_bsb,6),6),4,6) and --SJ V2 -- added CPR.payee_bsb
	sc6.BankAccountNumber = cpr.payee_account_no and
	sc6.AccountName = cpr.payee_account_name -- SJ v2

Where payee_type in (2)
and isnull(cheque_status,00)<>3
and cd.is_null =0
  and cpr.payee_account_no is not null -- SJ V2 


/*=================== EFTDATA EFT - EMPLOYER ================*/
Insert into ccst_eftdata
Select distinct
	eft.Account_name as AccountName,
	REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + convert(varchar, SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),4,6)) + '_' + 
	convert(varchar, eft.account_no) + '_1_' + '0' + '_' + convert(varchar, LTRIM(RTRIM(ISNULL(ab.id, cpr.claim_number)))) + '_' 
	+ DENSE_RANK() OVER (PARTITION by cpr.claim_number, cpr.payee_bsb, cpr.payee_account_no order by cpr.payee_account_Name) 
	),'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z')
	as AddressBookUID,
	'1' as Approved_icare, --Case when eft.Authorised_ind is null then 0 		End as Approved_icare,
	eft.Account_no as BankAccountNumber,
	NUll as BankAccountType,
	coalesce(sc6.BankBranchName_icare,bsb.branch) as BankBranchName_icare,
	bsb.Bank as BankName,
	SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),1,3)+'-'+SUBSTRING(RIGHT(REPLICATE('0',6)+LEFT(EFT.BSB,6),6),4,6) as BankRoutingNumber,
	NULL as BankSwiftCode_icare,
	'AU' as BankType_icare,
	'EMICS:' + RTRIM(cd.claim_number) + ':INSURED' as ContactID,
	NULL as ExternalLinkID,
	Case when ab.ContactID is null Then
		Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte))
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			
			When (cpr.claim_number is null and eft.Object is not null)
					then 1 --If we dont have a claim payment run record but has only eft record, then make that as primary

			Else 0
			End


		When ab.ContactID is not null Then 
			Case when ISNULL(cpr.Cheque_Status,00) = 9 and exists
			 (Select 1 from DM_STG_EMICS.dbo.Claim_payment_run cpr1 
				Where
				cpr1.Claim_number = cpr.claim_number
				and ISNULL(cpr1.Cheque_Status,00) = 9
				Group by cpr1.Claim_number  --NOTE: for vendors, also group by account_number 
				Having count(distinct cpr1.Payee_account_no) = 1
				) --For a claim, if there is only 1 account paid to
					Then 1

			When (eft.Object is null or cpr.paid_date>=isnull(eft.create_date, eft.authorised_dte)) --If claim payment run bank details are more up to date than your EMICS eft bank details
					and ISNULL(cpr.Cheque_Status,00) = 9 
					then 1 --If a contact has more than 1 bank account paid to in CPR table and has no data available in EMICS eft table,  
			Else 0
			End

		When (sc5.PublicID is not null or ab.ContactID is not null) and ab.IsPrimary = 1 Then 0 --If the contact and it's eftdata is available in CM and has isPrimary already set in CM, no actions required - hence 0		
		
		End as isPrimary,
	cd.claim_number as LUWID,
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE((
	'EMICS:' + rtrim(cd.claim_number) + '_' + convert(varchar,eft.account_no) + '_2_' + '0' ),
'0','a'),'1','c'),'2','e'),'3','g'),'4','k'),'5','m'),'6','q'),'7','s'),'8','w'),'9','z') as PublicID
	
	--NULL as WestpacFail_icare

FROM DM_STG_EMICS.dbo.EFT eft
Inner join DM_STG_EMICS.dbo.Claim_detail cd on eft.Object = cd.Policy_no
Inner join DM_STG_EMICS.dbo.BSB on eft.bsb = bsb.no
Left outer join (Select * from DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN where payee_type = 2 and isnull(cheque_status,00)<>3) cpr on cpr.claim_number = cd.claim_number
	and eft.BSB = cpr.Payee_bsb
	and eft.account_no = cpr.payee_account_no
	and eft.account_name = cpr.payee_account_name
Left outer join DM_STG_CRM..DM_STG_CRM_POLICY CRM on cd.policy_no = crm.LEGACY_POLICYNO__C
Left outer join (Select e.IsPrimary ,a.id, b.PublicID as ContactID, B.LinkID, a.LastName, a.firstname, E.BankAccountNumber, E.BankBranchName_icare, E.BankName, E.BankRoutingNumber, E.BankType_icare, AccountName
	From DM_STG_CRM..DM_STG_CRM_CONTACT a
	Left join DM_STG_CM..ab_abcontact b on b.LinkID = a.ID
	Left join DM_STG_CM..ab_eftdata e on e.ContactID = b.ID
	Where b.retired =0 and e.retired =0
	and b.PublicID like 'ab%') ab on ab.LinkID = CRM.ACCOUNT__C
	and AB.BankName = BSB.Bank
	and AB.BankBranchName_icare = BSB.BRANCH
	and AB.BankRoutingNumber = SUBSTRING(RIGHT(REPLICATE('0',6)+ LEFT(payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6) + left(PAYEE_BSB,6),6),4,6)
		and ab.BankAccountNumber = EFT.Account_no--CPR.Payee_account_no --sj
		and ab.AccountName = eft.Account_name --Payee_account_name -- SJ
--SJ
Left outer join (Select e.IsPrimary ,a.id, b.PublicID as ContactID, B.LinkID, a.LastName, a.firstname, E.BankAccountNumber, E.BankBranchName_icare, E.BankName, E.BankRoutingNumber, E.BankType_icare, AccountName
	From DM_STG_CRM..DM_STG_CRM_CONTACT a	
	left join DM_STG_CRM..DM_STG_CRM_ACCOUNT ac on a.ACCOUNTID = ac.id
	Left join DM_STG_CM..ab_abcontact b on b.LinkID = ac.ID
	Left join DM_STG_CM..ab_eftdata e on e.ContactID = b.ID
	Where b.retired =0 and e.retired =0
	and b.PublicID like 'ab%') sc6 on sc6.LinkID = CRM.ACCOUNT__C
	and sc6.BankName = BSB.Bank
	--and sc6.BankRoutingNumber = SUBSTRING(RIGHT(REPLICATE('0',6)+ LEFT(payee_bsb,6),6),1,3) + '-' + SUBSTRING(RIGHT(REPLICATE('0',6) + left(PAYEE_BSB,6),6),4,6)
		and sc6.BankAccountNumber = eft.Account_no 
		and sc6.AccountName =eft.Account_name 
--Left outer join (Select ab.PublicID FROM DM_STG_EMICS.dbo.EFT eft
--	Inner join DM_STG_EMICS.dbo.Claim_detail cd on eft.Object = cd.Policy_no and eft.Object_Type = 'E'
--	Inner join DM_STG_CRM..DM_STG_CRM_POLICY CRM on cd.Policy_No = crm.LEGACY_POLICYNO__C
	INNER JOIN DM_STG_CM..ab_abcontact sc5 on crm.ACCOUNT__C = sc5.LinkID and sc5.Retired = 0 --) sc5 on sc5.
--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.claim_number

Where eft.Object_type = 'E'
and cd.is_null=0
and eft.Account_no is not null
and cpr.claim_number is null




END