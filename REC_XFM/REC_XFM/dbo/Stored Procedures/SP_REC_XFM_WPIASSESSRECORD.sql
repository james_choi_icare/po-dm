﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_WPIASSESSRECORD]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 17/05
-- Description:	Work Capacity ccst_WPIAssessRecord_icare
-- Version: 17/05 initial build v8.0
--			24/06/2019 UPDATED to latest SC and xform rules
--			03/07/2019 Updated latest WPIResult_icare xform rules
--			19/07/2019 Updated to new xform rules and sc
-- =============================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_wpiassessrecord_icare','Work Capacity - v15 19/07/2019'

--drop table ccst_WPIAssessRecord_icare
SELECT
	 CASE 
		WHEN s66.Action_Type = 1 THEN '01'
		WHEN s66.Action_Type = 2 THEN '02'
		WHEN s66.Action_Type = 3 THEN '03'
		ELSE NULL
	 END AS ActionType_icare
	,s66.Action_Date AS ApprovalDate_icare
	,CASE 
		WHEN s66.Action_Type = 1 THEN '1'
		WHEN s66.Action_Type = 2 THEN '1'
		WHEN s66.Action_Type = 3 THEN '0'
		ELSE '0'
	 END AS Approved_icare
	,CASE
		--WHEN cad.WPI_Final = '-1' THEN '0'  -- Change 18/07/19
		WHEN cd.Nature_of_Injury IN ('312','771') THEN cad.Paid_PI   -- Change 18/07/19
		ELSE NULL
	 END AS BHLResult_icare
	,CASE
		WHEN cad.WPI = '-1' THEN NULL  -- Change 18/07/19
		WHEN cd.Nature_of_Injury IN ('312','771') THEN cad.WPI
		ELSE NULL
	 END AS ClaimedBHLPercentage_icare --Change from WPI_Final to WPI field
	,CASE 
		WHEN cad.WPI = '-1' THEN NULL
		ELSE cad.WPI
	 END AS ClaimedPercentage_icare --Added new CASE Statement
	,s66.Action_Date AS ComplyingAgreementDate_icare
	,CASE
		WHEN cd.Location_of_Injury IN ('310','311','318','319') THEN 'Yes'
		ELSE 'No'
	 END AS HasBackInjury_icare
	,(SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND pr.Payment_Type = 'WPI002' AND Reversed <> 1) AS Interest
	,CASE 
		WHEN Date_Claim_Ack IS NULL THEN s66.Action_Date
		ELSE Date_Claim_Ack 
	 END AS LetterOfOfferDate_icare
	,cd.Claim_Number AS LUWID
	,'Unknown' AS MaxMedImprovReached_icare
	,CASE 
		WHEN s66.Action_Type = 1 THEN 'Yes'
		WHEN s66.Action_Type = 2 THEN 'Yes'
		WHEN s66.Action_Type = 3 THEN 'No'
		ELSE NULL
	 END AS OfferAccepted_icare
	,CASE 
		WHEN s66.Action_Type = 1 THEN s66.Action_Date
		WHEN s66.Action_Type = 2 THEN s66.Action_Date
		ELSE NULL
	 END AS OfferAcceptedDate_icare
	,CASE
		WHEN cd.Nature_of_Injury IN ('312','771') THEN cad.WPI_Final
		ELSE NULL
	 END AS OfferedBHLPercentage_icare
	,cad.WPI_Final AS OfferedWPIPercentage_icare
	,'EMICS:' + RTRIM(cd.Claim_Number) + ':WPIASSESSRECORD' AS PublicID
	,s66.DORP AS RelevantParticularsDate_icare
	,s66.DORP AS S66ReceivedDate_icare   -- Change 18/07/19
	,(SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) AS SettlementAmount_icare
	,

	CASE 
		WHEN (SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr 
			WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) > 0 THEN 'S66Claim_icare' 
		ELSE NULL
	 END AS SettlementType_icare -- 'S66Claim_icare' AS SettlementType_icare  -- Change 18/07/19


	,'EMICS:' + RTRIM(cd.Claim_Number) + ':WPIASSESSMENT' AS WPIAssessment_icareID
	,CASE
		WHEN s66.Action_Type IN ('1','2') THEN 1
		WHEN s66.Action_Date IS NOT NULL THEN 1
		WHEN (SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) > 0 THEN 1
		ELSE 0
		END AS WPIAssessmentState_icareID,
	CASE WHEN (SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) > 0 
			AND (cad.Paid_PI IS NOT NULL OR cad.Paid_PI > 0)
		THEN cad.Paid_PI
		WHEN (SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) > 0 
			AND (ISNULL(Paid_PI,0) = 0) AND (cad.WPI_Final > 0)
		THEN cad.WPI_Final
		WHEN (SELECT SUM(Trans_Amount) FROM DM_STG_EMICS.dbo.Payment_Recovery pr WHERE pr.Claim_no = cd.Claim_Number AND Payment_Type = 'WPI001' AND Reversed <> 1) > 0 
			AND (ISNULL(Paid_PI,0) = 0 OR ISNULL(WPI_Final,0) = 0)
		THEN cad.WPI
		ELSE NULL
	END AS WPIResult_icare

	into ccst_WPIAssessRecord_icare
FROM DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL cd ON cad.Claim_No = cd.Claim_Number
	INNER JOIN DM_STG_EMICS.dbo.SECTION66 s66 ON cad.Claim_No = s66.Claim_No -- FROM Left Outer Join TO Inner Join 14/05/2019

WHERE cd.is_Null = 0
	AND cd.Result_of_Injury_Code IN ('2','3')


END