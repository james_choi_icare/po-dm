﻿
--=====================================================================
--        Author             : Saranya Jayaseelan
--		  Domain			 : Payments and recoveries
--        Mapping Version    : V38
--	      Note				 :  Sahana to change mapping spec for transaction for reserve and recovery reserve subtype
--							 : v40.3 - 	implemented latest selection criteria from v40.3 mapping spec. changes also included column garnishee_icare,transactionID and postgarnishee_icare
--			18/04/2019				 :v44 - Changes to publicid derivation
--			07/05/2019		 : v45.2 AG - added voided selection criteria and rules
--			14/05/2019		 : v45.6 fixed publicID xform rules for voided reserve records
--			11/06/2019		 : v46.2.1 Duplicate PublicID fix for voided records
--			16/07/2019		 : v46.3.9 updated decimal fields
--			09/09/2019		 : v47.2 updated GSTCalcRate_icare for recovery type xform rules
--=====================================================================

CREATE PROC [dbo].[SP_REC_XFM_TRANSACTIONLINEITEM] AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_transactionlineitem','Payments and Recoveries - v46.3.9 11/06/2019'
--drop table ccst_transactionlineitem
--drop table ccst_transactionlineitem
;with PIAWE_HIS as (
	select distinct id, claim_no,weekly_wage, SHIFT_ALLOWANCES, cd.OT_Allowances, PIAWE_VERIFIED_DATE,PIAWE, create_date from DM_STG_EMICS.dbo.[cd_audit] cd where PIAWE<>0
	and (cd.PIAWE_VERIFIED_DATE is not null 
	or(cd.PIAWE_VERIFIED_DATE is null and  exists (select is_exempt from DM_STG_EMICS.dbo.amendment_exemptions ae where ae.claim_no = cd.Claim_No and is_exempt =  1) )
	)
),
payments as (
SELECT 
	CASE WHEN PR.is_GST=1 THEN convert(decimal (18,2), pr.Trans_Amount-gst )
		ELSE convert(decimal (18,2), pr.Trans_Amount)
		END as AmountWithoutGST,
	convert(decimal (18,2), Trans_Amount) as ClaimAmount,
	DATEDIFF(week,Period_Start_Date,Period_End_Date) as CountofWeeks,
	CASE WHEN ISNULL(PR.Tax_Amt,0)<>0 THEN
		CASE WHEN CPR.CHEQUE_NO IS  NULL THEN 'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,PR.pr_id)+':DED' ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+convert(varchar,PR.pr_id)+':DED' END 
		ELSE NULL 
		END as DeductibleID,
	isnull(TL.Weekly_Earnings,0.00) as DeemedEarningsPerWeek_icare,
	convert(decimal (18,2),PR.EARNINGS) as EarningsE_icare,		
	convert(decimal (18,2),LKP_GARNISHEE.Garnishee_amount) AS Garnishee_icare,
	Case when pr.Estimate_type in (50,72,54,58,59,60,65) then ROUND(his.PIAWE,2) Else NULL 
		End as GrossWeeklyWageRate_icare,
	Case when PR.is_GST=1  then 'Yes' ELSE 'No' 
		END as GSTApplicable_icare,	
	NULL as GSTCalcRate_icare,	 
	GSTD.GSTDecision as GSTDecision,
	CASE WHEN pr.Payment_Type in ('CLP001','COM001','DEC001','DEC003','DOA002',
		'IN7100','IN7200','IN7300','PAS001','PAS002',
		'RCL001','RES001','RES002','RFD001','RFD002',
		'ROP001','RPE001','RSC001','RSC002','SCP001',
		'SCP002','SCP003','SCP004','TRA003','VJC001',
		'VJC002','VJC003','VJC004','WPI001','WPI002',
		'WPP001','WPP002','WPP003','WPP004','WPP005',
		'WPP006','WPP007','WPP008','WPT001','WPT002',
		'WPT003','WPT004','WPT005','WPT006','WPT007') THEN 1 ELSE 0 
		END aS GSTExemptflag,	
	CASE  WHEN PR.ITC<>0 THEN 'itc'
				WHEN PR.DAM<>0 THEN 'dam'
				ELSE 'nogst'
		END as GSTMethodPEL_Ext,
	Case when PR.EARNINGS<> 0 then convert(decimal (18,2), pr.Hours_per_week - convert(decimal(10,2),PR.WC_Hours_Lost + 
		convert(decimal(3,2),convert(decimal,PR.WC_Minutes_lost)/100))) Else 0.00 End
		as HoursPaid_icare,
	CASE WHEN PR.itc<>0 THEN 'Yes' WHEN PR.ITC=0 THEN 'No' ELSE 'Unknown'	
		END as ITCApplicable_icare,		
	ITCD.ITCDecision as ITCDecision, 
	LTRIM(RTRIM(CD.Claim_Number)) as LUWID,	
	cpc.PublicID as Paycode_icareID,
	AGG.AGGTYPECD AS  PaycodeGl_Aggregation,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+CONVERT(VARCHAR,PR.Payment_no)+'_'
		+CONVERT(VARCHAR,PR.Estimate_type)+'_'+convert(varchar,PR.ID)+':PAYMENT' as PublicID,
	convert(decimal (18,2),Trans_Amount) as TransactionAmount,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type+'_'
		+convert (varchar,pr.pr_id)+'_'+case when ISNULL(nullif(PR.Adjust_Trans_Flag,''),'N')='Y' then '1'
		else '0' end+':PAYMENT' as TransactionID,
	convert(decimal (18,2), PR.Trans_Amount) as ReportingAmount,
	convert(decimal (18,2), PR.Trans_Amount) as ReservingAmount,
	CASE WHEN PR.Estimate_type in (50,72,54,58,59,60,65) THEN convert(decimal (18,2),PR.Rate )--updated v46.2.1
			ELSE 0.00 END as WeeklyBenefitRate_icare,		
	0.0 as postgarnishee_icare
	FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from DM_STG_EMICS.dbo.Payment_Recovery) PR-- 15/03:SR-Changed the logic for PR_ID to  have in sync with other payment tables
LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
ON 
CPR.Claim_Number=PR.Claim_NO
AND CPR.Payment_No=PR.Payment_No
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
ON PR.CLAIM_NO=CD.Claim_number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.TIME_LOST_DETAIL where ACTUAL_DEEMED='Deemed' and is_deleted=0) TL
on  PR.Claim_No=TL.Claim_No and PR.Period_Start_Date>=TL.Date_Ceased_Work and PR.Period_End_Date<=TL.Date_Resumed_Work
LEFT OUTER JOIN (select PR_Adj.Claim_No,PR_Adj.Payment_no,sum(PR_Adj.Trans_Amount) as adjustment_amount,sum(PR_Gar.Trans_Amount) as Garnishee_amount from 
(select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
inner join (select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1)PR_Gar
on PR_Adj.Claim_No=PR_Gar.Claim_No
and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
and pr_adj.Garnishee_Payment_id=PR_Gar.id
group by PR_Adj.Claim_No,PR_Adj.Payment_no
) LKP_GARNISHEE
ON PR.Claim_No=LKP_GARNISHEE.Claim_No
AND PR.Payment_no=LKP_GARNISHEE.Payment_no
LEFT OUTER JOIN (select * from DM_STG_CC.dbo.CCX_paycode_icare where Retired=0) cpc
ON case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end =cpc.Paycode
LEFT OUTER JOIN (
select A.Paycode,c.TYPECODE as  GSTDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodegstdecision_icare] c
where a.GSTDecision=c.ID and a.Retired=0 and c.RETIRED=0
)GSTD
ON gstd.Paycode=cpc.ID
LEFT OUTER JOIN (
select a.Paycode,c.TYPECODE as  ITCDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodeitcdecision_icare] c
where a.ITCDecision=c.ID and  a.Retired=0 and c.RETIRED=0
)ITCD
on itcd.Paycode=cpc.ID 
LEFT OUTER JOIN (select pcc.ID,cc.typecode AS COSTCAT,glagg.Description,glagg.typecode AS AGGTYPECD
from DM_STG_CC.[dbo].[ccx_paycodecostcategory_icare] pcc, DM_STG_CC.[dbo].[cctl_costcategory] cc
, DM_STG_CC.[dbo].[cctl_glaggregation_icare] glagg
where  pcc.costcategory=cc.id and glagg.id=pcc.GLaggregation
and pcc.Retired=0 and cc.RETIRED=0 and glagg.RETIRED=0)AGG
ON CPC.ID=AGG.ID
AND PR.Estimate_type=AGG.COSTCAT
/*#added as part of DMIG-5531 to correct the new effective dates of piawe for payments*/
LEFT OUTER JOIN PIAWE_HIS his 
on pr.Claim_No = his.claim_no and his.id = (select max(his2.id) from PIAWE_HIS his2 where his2.claim_no = pr.Claim_No and pr.Transaction_date >= convert(smalldatetime, his2.create_date))
where cd.is_Null=0 AND PR.Estimate_type<70 and Reversed<>1 and PR.Garnishee_no is null 
--DMIG-6263 ADDED TO EXCLUDE OVER PAYMENT RECOVERIES
AND ISNULL(CPR.Cheque_Status,'00') not in (3,6) 
) ,

-----************** Transaction Line Item for reserves*********************-----
reserves as (
select distinct 
	NULL as AmountWithoutGST,
	convert(decimal (18,2), PR.Trans_Amount) as ClaimAmount,
	NULL as  CountofWeeks,
	CASE WHEN ISNULL(PR.Tax_Amt,0)<>0 THEN CASE WHEN CPR.CHEQUE_NO IS  NULL THEN 'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+convert(varchar,PR.pr_id)+':DED'
		ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+convert(varchar,PR.pr_id)+':DED' END 
		ELSE NULL END as DeductibleID,
	NULL as DeemedEarningsPerWeek_icare,
	NULL as EarningsE_icare,
	0.0 as Garnishee_icare,
	NULL as GrossWeeklyWageRate_icare,
	NULL as GSTApplicable_icare,
	NULL as GSTCalcRate_icare,
	NULL as GSTDecision,
	NULL aS GSTExemptflag,
	'nogst' as GSTMethodPEL_Ext,
	NULL as HoursPaid_icare,
	NULL as ITCApplicable_icare,
	NULL as ITCDecision,
	CD.Claim_Number as LUWID,
	NULL as Paycode_icareID,
	NULL AS  PaycodeGl_Aggregation,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+CONVERT(VARCHAR,convert(varchar,PR.Payment_no))+'_'+
		CONVERT(VARCHAR,PR.estimate_type)+'_'+convert(varchar,pr.id)+':RESERVE' as PublicID,	
	convert(decimal (18,2),PR.Trans_Amount) as TransactionAmount,	
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+CONVERT(VARCHAR,convert(varchar,PR.Payment_no))+'_'+
		CONVERT(VARCHAR,PR.estimate_type)+'_'+convert(varchar,pr.pr_id)+'_0'+':RESERVE'  as TransactionID,
	convert(decimal (18,2), PR.Trans_Amount) as ReportingAmount,
	convert(decimal (18,2), PR.Trans_Amount) as ReservingAmount,
	NULL as WeeklyBenefitRate_icare,	
	0.0 as postgarnishee_icare


	FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from DM_STG_EMICS.dbo.Payment_Recovery) PR--15/03:SR: Changed the logic for Pr_ID to sync with other payment tables
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	ON PR.CLAIM_NO=CPR.CLAIM_NUMBER
	AND PR.PAYMENT_NO=CPR.PAYMENT_NO
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	ON PR.CLAIM_NO=CD.Claim_number
	where cd.is_Null=0 and PR.Estimate_type<70 and Reversed<>1
	and PR.Garnishee_no is null
	 --DMIG-6263 ADDED TO EXCLUDE OVER PAYMENT RECOVERIES
	AND ISNULL(CPR.Cheque_Status,'00')<>6
)
,

----****************** Transaction Line Item for Recovery*********************
recovery as (
select distinct 
	NULL as AmountWithoutGST,
	convert(decimal (18,2), -1 * PR.Trans_Amount) as ClaimAmount,
	NULL as  CountofWeeks,
	CASE WHEN ISNULL(PR.Tax_Amt,0)<>0 THEN
		'EMICS:'+convert(varchar,PR.Payment_No)+'_'+convert(varchar,PR.pr_id)+':DED'
		ELSE NULL
		END as DeductibleID,
	NULL as DeemedEarningsPerWeek_icare,
	NULL as EarningsE_icare,
	0.0 as Garnishee_icare,
	NULL as GrossWeeklyWageRate_icare,
	NULL as GSTApplicable_icare,
	Case when PR.payment_type <> 'RPE001' then 10.00 else 0 end as GSTCalcRate_icare,
	NULL as GSTDecision,
	NULL aS GSTExemptflag,
	CASE WHEN PR.ITC<>0 THEN 'itc'
		WHEN PR.DAM<>0 THEN 'dam' ELSE 'nogst'
		END as GSTMethodPEL_Ext,
	NULL as HoursPaid_icare,
	NULL as ITCApplicable_icare,
	NULL as ITCDecision,
	CD.Claim_Number as LUWID,
	cpc.PublicID as Paycode_icareID,
	NULL AS  PaycodeGl_Aggregation,
	'EMICS:'+LTRIM(RTRIM(cd.Claim_Number))+'_'+CONVERT(VARCHAR,convert(varchar,PR.Payment_no))
		+'_'+CONVERT(VARCHAR,PR.estimate_type)+'_'+convert(varchar,PR.ID)+':RECOVERY' as PublicID,
	convert(decimal (18,2),-1*PR.Trans_Amount) as TransactionAmount,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+CONVERT(VARCHAR,convert(varchar,PR.Payment_no)) +'_'+
		CONVERT(VARCHAR,PR.estimate_type)+'_'+convert(varchar,pr.pr_id)+'_0'+':RECOVERY' as TransactionID,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReportingAmount,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReservingAmount,
	NULL as WeeklyBenefitRate_icare,
	0.0 as PostGarnishee_icare

	FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* from DM_STG_EMICS.dbo.Payment_Recovery) PR--15/03:SR: Changed the logic for Pr_ID to sync with other payment tables
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD	ON PR.CLAIM_NO=CD.Claim_number
	LEFT OUTER JOIN (select * from DM_STG_CC.dbo.ccx_paycode_icare where Retired=0) cpc	ON case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end =cpc.Paycode		
	where cd.is_Null=0 and PR.Estimate_type>=70 and Reversed<>1 --10371
),
----**********************Transaction Line Item for Recovery Reserve *************
recoveryreserve as (
select distinct 
	NULL as AmountWithoutGST,
	convert(decimal (18,2), -1 * PR.Trans_Amount) as ClaimAmount,
	NULL as  CountofWeeks,
	CASE WHEN ISNULL(PR.Tax_Amt,0)<>0 THEN
		'EMICS:'+convert(varchar,PR.Payment_No)+'_'+convert(varchar,PR.pr_id)+':DED'
		ELSE NULL
		END as DeductibleID,
	NULL as DeemedEarningsPerWeek_icare,
	NULL as EarningsE_icare,
	0.0 as Garnishee_icare,
	NULL as GrossWeeklyWageRate_icare,
	NULL as GSTApplicable_icare,
	NULL as GSTCalcRate_icare,
	NULL as GSTDecision,
	NULL aS GSTExemptflag,
	'nogst' as GSTMethodPEL_Ext,
	NULL as HoursPaid_icare,
	NULL as ITCApplicable_icare,
	NULL as ITCDecision,
	CD.Claim_Number as LUWID,
	NULL as Paycode_icareID,
	NULL AS  PaycodeGl_Aggregation,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type
		+'_'+convert(varchar,pr.ID)+':RECOVERYRESERVE' as PublicID,	
	convert(decimal (18,2),-1*PR.Trans_Amount) as TransactionAmount,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+CONVERT(VARCHAR,convert(varchar,PR.Payment_no))+
		'_'+CONVERT(VARCHAR,PR.estimate_type)+'_'+convert(varchar,pr.pr_id)+'_0'+':RECOVERYRESERVE' as TransactionID,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReportingAmount,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReservingAmount,
	NULL as WeeklyBenefitRate_icare,
	0.0 as PostGarnishee_icare

	FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from 
		DM_STG_EMICS.dbo.Payment_Recovery) PR--15/03:SR: Changed the logic for Pr_ID to sync with other payment tables
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.CLAIM_NO=CD.Claim_number
--DMIG6362 ADDED FOR INVCLUDING OVER PAYMENT RECOVERY
	LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON CPR.Claim_number=PR.CLAIM_NO
		AND CPR.Payment_no=PR.Payment_no
	where cd.is_Null=0 and (PR.Estimate_type>=70 or isnull(cpr.cheque_status,'00')=6) and Reversed<>1
)
--=====payments_transactiolineitem_voided=================
, payments_voided as (
Select 
	Case when PR.Is_GST =1 then -1*(PR.Trans_Amount-PR.GST)
		Else -1*PR.Trans_Amount 
		END as AmountWithoutGST,
	convert(decimal (18,2), (-1)*PR.Trans_Amount) as ClaimAmount,
	DATEDIFF(Week, pr.period_start_date, pr.period_end_Date) as CountOfWeeks, -- is this Countofweeks or countofweeks_icare?
	NULL as DeductibleID,
	TL.Weekly_Earnings as DeemedEarningsPerWeek_icare,
	Cast(PR.EARNINGS as decimal(18,2)) as EarningsE_icare,
	convert(decimal (18,2), (-1)*gar.Garnishee_amount ) as Garnishee_icare,
	Case when pr.Estimate_type in (50,72,54,58,59,60,65) then ROUND(his.PIAWE,2)
		Else NULL end as GrossWeeklyWageRate_icare,
	Case when PR.is_gst=1 then 'Yes' else 'No'
		End as GSTApplicable_icare,
	10.00 as GSTCalcRate_icare,
	GSTD.GSTDecision as GSTDecision,
	CASE WHEN pr.Payment_Type in ('CLP001','COM001','DEC001','DEC003','DOA002',
		'IN7100','IN7200','IN7300','PAS001','PAS002',
		'RCL001','RES001','RES002','RFD001','RFD002',
		'ROP001','RPE001','RSC001','RSC002','SCP001',
		'SCP002','SCP003','SCP004','TRA003','VJC001',
		'VJC002','VJC003','VJC004','WPI001','WPI002',
		'WPP001','WPP002','WPP003','WPP004','WPP005',
		'WPP006','WPP007','WPP008','WPT001','WPT002',
		'WPT003','WPT004','WPT005','WPT006','WPT007') Then 1 Else 0 
		End as GSTExemptflag,
	Case when pr.ITC<>0 then 'itc' when pr.DAM<>0 then 'dam' ELSE 'nogst'
		End GSTMethodPEL_Ext,
	Case when PR.Earnings <> 0 then 
	convert(decimal (18,2),pr.hours_per_week - convert(Decimal(10,2),WC_Hours_lost+convert(decimal(3,2),convert(decimal,WC_Minutes_lost)/100))) Else 0.00 
		End as HoursPaid_icare,
	CASE When PR.ITC<>0 then 'Yes' 
		When PR.ITC=0 then 'No'
		Else 'Unknown'
		End as ITCApplicable_icare,
	ITCD.ITCDecision as ITCDecision,
	cd.claim_number as LUWID,
	NULL as paycode_icareID, --this is populated in TGT tables, no join provided
	Null as paycodeGl_aggregation, --this is populated from TGT tables, not sure how to join
	'EMICS:' + RTRIM(Cd.claim_number)+'_' + convert(varchar,PR.payment_no) + '_' + pr.estimate_type + '_'+convert(varchar, pr.ID) + ':PAYMENTREV' as PublicID,
	convert(decimal (18,2),(-1)* pr.trans_amount) as TransactionAmount,
	'EMICS:' + RTRIM(cd.claim_number)+'_'+convert(varchar,PR.payment_no)+'_'+PR.Estimate_type+'_'+convert(varchar,PR.pr_id) --which selection criteria?? does pr_id come from
		+ '_'+case when isnull(nullif(PR.Adjust_Trans_flag,''),'N')='Y' then '1' else '0' end + ':PAYMENT' as TransactionID,
	Null as ReportingAmount,
	Null as ReservingAmount,
	Case when pr.estimate_type = 50 then convert(decimal (18,2),PR.Rate) else 0.00 end as WeeklyBenefitRate_icare,
	0.00 as postgarnishee_icare

FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from DM_STG_EMICS.dbo.Payment_Recovery) PR-- 15/03:SR-Changed the logic for PR_ID to  have in sync with other payment tables
Inner join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number = pr.claim_no
Inner join DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr on cpr.claim_number = pr.claim_no	and cpr.payment_no = pr.Payment_no
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.TIME_LOST_DETAIL where ACTUAL_DEEMED='Deemed' and is_deleted=0) TL
	on PR.Claim_No=TL.Claim_No and PR.Period_Start_Date>=TL.Date_Ceased_Work and PR.Period_End_Date<=TL.Date_Resumed_Work

Left outer join (Select distinct ID, claim_no, Weekly_wage, SHIFT_ALLOWANCES, cd.OT_Allowances, PIAWE_Verified_date, piawe, create_date
	From DM_STG_EMICS.dbo.cd_audit cd where PIAWE<>0) his on pr.claim_no = his.claim_no --GrossWeeklyWageRate_icare
		and his.id = (Select max(his2.id) from (Select distinct ID, claim_no, Weekly_wage, SHIFT_ALLOWANCES, cd.OT_Allowances, PIAWE_Verified_date, piawe, create_date
		From DM_STG_EMICS.dbo.cd_audit cd where PIAWE<>0) his2 where his2.claim_no = pr.claim_no and pr.transaction_date >= convert(smalldatetime, his2.create_date))
Left join (Select PR_Adj.Claim_no, PR_Adj.Payment_no, sum(PR_Adj.Trans_amount) as adjustment_amount,
		Sum(PR_Gar.Trans_Amount) as Garnishee_amount 
		from (Select * from DM_STG_EMICS.dbo.Payment_recovery pr where adjust_trans_flag = 'Y' and Garnishee_no is not null and 
			Trans_Amount<0 and reversed<>1) PR_Adj
		Inner join (Select * from DM_STG_EMICS.dbo.Payment_recovery pr where adjust_trans_flag='Y' and Garnishee_no is not null and 
			Trans_Amount>0 and reversed<>1) PR_Gar on PR_Adj.Claim_no = pr_gar.Claim_No
		and pr_adj.Garnishee_No = pr_gar.garnishee_no
		and pr_adj.Period_Start_Date = PR_Gar.Period_Start_Date
		and pr_adj.Period_End_Date = PR_Gar.Period_End_Date
		and PR_Adj.Garnishee_Payment_id = pr_Gar.id
		group by pr_adj.claim_no, pr_adj.payment_no
	) gar on gar.Payment_no = pr.payment_no and gar.claim_no = pr.claim_no
LEFT OUTER JOIN (select * from DM_STG_CC.DBO.ccx_paycode_icare where Retired=0) cpc ON case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end =cpc.Paycode
LEFT OUTER JOIN (select A.Paycode,c.TYPECODE as  GSTDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodegstdecision_icare] c-- DM_STG_CC.DBO.cctl_paycodegstdecision_icare c
	where a.GSTDecision=c.ID and a.Retired=0 and c.RETIRED=0 ) GSTD ON gstd.Paycode=cpc.ID
LEFT OUTER JOIN (select a.Paycode,c.TYPECODE as  ITCDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodeitcdecision_icare] c
	where a.ITCDecision=c.ID and  a.Retired=0 and c.RETIRED=0 ) ITCD on itcd.Paycode=cpc.ID 
Where cd.is_null = 0 and pr.estimate_type < 70 and pr.Reversed <>1 and PR.Garnishee_no is null --50 records
and (cpr.BankChq_status=2 or cpr.Cheque_status = 3)
)
--====reserve_transactionlineitem_voided
, reserve_voided as (
Select 
	NULL as AmountWithoutGST,
	convert(decimal (18,2), (-1)*pr.trans_amount) as ClaimAmount,
	NULL as Countofweeks,
	NULL as DeductibleID,
	NULL as Deemedearningsperweek_icare,
	NULL as EarningsE_icare,
	0.00 as Garnishee_icare,
	NULL as GrossWeeklyWageRate_icare,
	NULL as GSTApplicable_icare,
	NULL as GSTCalcRate_icare,
	NULL as GSTDecision,
	NULL as GSTExemptflag,
	'nogst' as GSTMethodPEL_Ext,
	NULL as HoursPaid_icare,
	NULL as ITCApplicable_icare,
	NULL as ITCDecision,
	cd.claim_number as LUWID,
	NULL as paycode_icareID,
	NULL as paycodeGl_aggregation,
	'EMICS:' + rtrim(ltrim(cd.claim_number)) + '_' + convert(varchar,PR.payment_no) + 
		'_' + convert(varchar,pr.Estimate_type) + '_' + convert(varchar,pr.ID) + ':RESERVEREV' as PublicID,
	convert(decimal (18,2),(-1)* PR.Trans_amount) as TransactionAmount,

	'EMICS:' + RTRIM(CD.claim_number) + '_' + convert(varchar,pr.payment_no) + '_' + convert(varchar,pr.estimate_type)
		+ '_' + convert(varchar,Pr.pr_id) +'_0' + ':RESERVE' as TransactionID,

	convert(decimal (18,2), (-1)*pr.trans_amount) as ReportingAmount,
	convert(decimal (18,2), (-1)*pr.trans_amount) as ReservingAmount,
	NULL as WeeklyBenefitRate_icare,
	0.00 as postgarnishee_icare
	
FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from DM_STG_EMICS.dbo.Payment_Recovery) PR-- 15/03:SR-Changed the logic for PR_ID to  have in sync with other payment tables
Inner join DM_STG_EMICS.dbo.claim_detail cd on cd.claim_number = pr.claim_no
Inner join DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr on cpr.claim_number = pr.claim_no and cpr.payment_no = pr.payment_no
--Left join (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* from DM_STG_EMICS.dbo.Payment_Recovery) PRI on PRI.Claim_no = cd.claim_number--15/03:SR: Changed the logic for Pr_ID to sync with other payment tables
Where cd.is_null = 0 and pr.estimate_type <70 and pr.reversed<>1 and pr.Garnishee_No is null
and (cpr.Bankchq_status = 2 or cpr.Cheque_status = 3)
)
--===========================================OVERPAYMENT_Recovery
,
overpayment_recovery as (
SELECT  
Case when PR.is_GST = 1 then -1*(Trans_Amount-gst) ELSE -1*Trans_Amount End as AmountWithoutGST,
	convert(decimal (18,2), -1*Trans_Amount) as ClaimAmount,
	DATEDIFF(week, period_Start_date, period_end_Date) as CountofWeeks,
	--Date_of_Service as Date_of_Service,
	NULL as DeductibleID,
	isnull(TL.Weekly_earnings,0.00) as DeemedEarningsPerWeek_icare,
	convert(decimal (18,2), PR.Earnings) as EarningsE_icare,
	0 Garnishee_icare,
	Case when pr.estimate_type in (50,72,54,58,59,60,65) then isnull(his.PIAWE,0) else NULL end as GrossWeeklyWage_icare,
	Case when PR.is_GST=1 then 'Yes' Else 'No' end as GSTApplicable_icare,
	10.00 as GSTCalcRate_icare,
	GSTD.GSTDecision as GSTDecision,
	Case when pr.Payment_type in ('CLP001','COM001','DEC001','DEC003','DOA002',
		'IN7100','IN7200','IN7300','PAS001','PAS002',
		'RCL001','RES001','RES002','RFD001','RFD002',
		'ROP001','RPE001','RSC001','RSC002','SCP001',
		'SCP002','SCP003','SCP004','TRA003','VJC001',
		'VJC002','VJC003','VJC004','WPI001','WPI002',
		'WPP001','WPP002','WPP003','WPP004','WPP005',
		'WPP006','WPP007','WPP008','WPT001','WPT002',
		'WPT003','WPT004','WPT005','WPT006','WPT007') THEN 1 ELSE 0 
		end as GSTExemptflag,
	Case when pr.itc<>0 then 'itc' when pr.dam<>0 then 'dam' else 'nogst' end as GSTMethodPEL_Ext,
	Case when pr.earnings<>0 then convert(decimal (18,2),-1*(pr.hours_per_week - convert(decimal(10,2),WC_Hours_Lost + convert(decimal(3,2),convert(decimal,WC_Minutes_lost)/100)))) Else 0.00
		End as HoursPaid_icare,
	Case when PR.itc<>0 then 'Yes' when pr.itc=0 then 'No' Else 'Unknown' End as ITCApplicable_icare,
	ITCD.ITCDecision as ITCDecision,
	cd.claim_number as LWUID,
	cpc.PublicID as Paycode_icareID,
	agg.aggtypecd as paycodeGL_Aggregation,
	'EMICS:' + RTRIM(Cd.claim_number) + '_' + convert(varchar,pr.payment_no)+'_'+Convert(Varchar,pr.estimate_type) + '_' + convert(varchar,pr.id)+':RECOVERY' as PublicID,
	convert(decimal (18,2),-1*Trans_Amount) as TransactionAmount,
	'EMICS:' + RTRIM(cd.claim_number) + '_' + convert(varchar, PR.payment_no) + '_' + Convert(varchar,pr.estimate_type) 
		+ '_' + convert(varchar, pr.pr_id) + '_0:RECOVERY' as TransactionID,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReportingAmount,
	convert(decimal (18,2), -1*PR.Trans_Amount) as ReservingAmount,
	Case when pr.estimate_type in (50,72,54,58,59,60,65) Then convert(decimal (18,2),pr.Rate) else 0.00 end as WeeklyBenefitRate_icare,
	0.00 as PostGarnishee_icare

FROM (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,*  from DM_STG_EMICS.dbo.Payment_Recovery) PR-- 15/03:SR-Changed the logic for PR_ID to  have in sync with other payment tables
LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON CPR.Claim_Number=PR.Claim_NO
AND CPR.Payment_No=PR.Payment_No
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON PR.CLAIM_NO=CD.Claim_number
LEFT OUTER JOIN (select * from DM_STG_EMICS.dbo.TIME_LOST_DETAIL where ACTUAL_DEEMED='Deemed' and is_deleted=0) TL
on  PR.Claim_No=TL.Claim_No and PR.Period_Start_Date>=TL.Date_Ceased_Work and PR.Period_End_Date<=TL.Date_Resumed_Work
LEFT OUTER JOIN (select PR_Adj.Claim_No,PR_Adj.Payment_no,sum(PR_Adj.Trans_Amount) as adjustment_amount,sum(PR_Gar.Trans_Amount) as Garnishee_amount from 
(select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount<0 and   reversed<>1 ) PR_Adj
inner join (select * from DM_STG_EMICS.dbo.payment_recovery  where Adjust_Trans_Flag='Y' and Garnishee_No is not null  and Trans_Amount>0 and reversed<>1)PR_Gar
on PR_Adj.Claim_No=PR_Gar.Claim_No
and PR_Adj.Garnishee_No=PR_Gar.Garnishee_No
and PR_Adj.Period_Start_Date=PR_Gar.Period_Start_Date
and PR_Adj.Period_End_Date=PR_Gar.Period_End_Date
and pr_adj.Garnishee_Payment_id=PR_Gar.id
group by PR_Adj.Claim_No,PR_Adj.Payment_no
) LKP_GARNISHEE
ON PR.Claim_No=LKP_GARNISHEE.Claim_No
AND PR.Payment_no=LKP_GARNISHEE.Payment_no
LEFT OUTER JOIN (select * from DM_STG_CC.dbo.CCX_paycode_icare where Retired=0) cpc
ON case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end =cpc.Paycode
LEFT OUTER JOIN (
select A.Paycode,c.TYPECODE as  GSTDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodegstdecision_icare] c
where a.GSTDecision=c.ID and a.Retired=0 and c.RETIRED=0
)GSTD
ON gstd.Paycode=cpc.ID
LEFT OUTER JOIN (
select a.Paycode,c.TYPECODE as  ITCDecision,c.ID from [DM_STG_CC].[dbo].[ccx_paycodegst_icare] a, DM_STG_CC.[dbo].[cctl_paycodeitcdecision_icare] c
where a.ITCDecision=c.ID and  a.Retired=0 and c.RETIRED=0
)ITCD
on itcd.Paycode=cpc.ID 
LEFT OUTER JOIN (select pcc.ID,cc.typecode AS COSTCAT,glagg.Description,glagg.typecode AS AGGTYPECD
from DM_STG_CC.[dbo].[ccx_paycodecostcategory_icare] pcc, DM_STG_CC.[dbo].[cctl_costcategory] cc
, DM_STG_CC.[dbo].[cctl_glaggregation_icare] glagg
where  pcc.costcategory=cc.id and glagg.id=pcc.GLaggregation
and pcc.Retired=0 and cc.RETIRED=0 and glagg.RETIRED=0)AGG
ON CPC.ID=AGG.ID
AND PR.Estimate_type=AGG.COSTCAT
/*#added as part of DMIG-5531 to correct the new effective dates of piawe for payments*/
LEFT OUTER JOIN (select distinct id, claim_no,weekly_wage, SHIFT_ALLOWANCES, cd.OT_Allowances, PIAWE_VERIFIED_DATE,PIAWE, create_date from DM_STG_EMICS.dbo.[cd_audit] cd where PIAWE<>0
and (cd.PIAWE_VERIFIED_DATE is not null 
or(cd.PIAWE_VERIFIED_DATE is null and  exists (select is_exempt from DM_STG_EMICS.dbo.amendment_exemptions ae where ae.claim_no = cd.Claim_No and is_exempt =  1) ))) his 
on pr.Claim_No = his.claim_no and his.id = (select max(his2.id) from (select distinct id, claim_no,weekly_wage, SHIFT_ALLOWANCES, cd.OT_Allowances, PIAWE_VERIFIED_DATE,PIAWE, create_date from DM_STG_EMICS.dbo.[cd_audit] cd where PIAWE<>0
and (cd.PIAWE_VERIFIED_DATE is not null 
or(cd.PIAWE_VERIFIED_DATE is null and  exists (select is_exempt from DM_STG_EMICS.dbo.amendment_exemptions ae where ae.claim_no = cd.Claim_No and is_exempt =  1) ))) his2 where his2.claim_no = pr.Claim_No and pr.Transaction_date >= convert(smalldatetime, his2.create_date))
where cd.is_Null=0 AND ISNULL(Cheque_Status,'00')=6 and Reversed<>1 and PR.Garnishee_no is null
), all_transactionlineitem as
(
	Select * from payments
	UNION
	Select * from reserves
	UNION 
	Select * from recovery
	UNION
	Select * from recoveryreserve
	UNION
	Select * from payments_voided
	UNION
	Select * from reserve_voided
	UNION
	Select * from overpayment_recovery
	)
Select * into ccst_transactionlineitem
from all_transactionlineitem


END