﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 18/07/2019
-- Description:	Contacts - Main Contact V2.19
-- 1. This SP is only for Main Contacts -E,R Unmatched. To load ADDRESS data for Contact Manager.
--
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CM_ADDRESS]

AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'abst_address', 'MC_v2.19'

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_address_temp1') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_address_temp1;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_address_temp2') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_address_temp2;

IF OBJECT_ID('ContactREF_temptable_maincontact_address_CM_ER_unmatched') is not null
drop table dbo.ContactREF_temptable_maincontact_address_CM_ER_unmatched;

IF OBJECT_ID('abst_address') is not null
drop table dbo.abst_address;






--drop table temptable_CM_MC_ER_unmatched_address_temp1
--Geting the CM address data ready by joining with CM contact data
select * into temptable_CM_MC_ER_unmatched_address_temp1
from
(
select
a.*
from
[dbo].[AddressREF_temptable_Address_maincontact_ER_verifiedPC_unmatched] a
inner join
[dbo].[ContactREF_temptable_maincontact_CM_ER_unmatched] b
on a.PublicID=b.PrimaryAddressID
)X



--drop table temptable_CM_MC_ER_unmatched_address_temp2
--Loading the final temp table to include data for columns that are only present in CM and not in CC
select * into temptable_CM_MC_ER_unmatched_address_temp2
from
(
select 
a.PublicID as AddressBookUID				,
AddressLine1				,
AddressLine1Kanji			,
AddressLine2				,
AddressLine2Kanji			,
AddressLine3				,
AddressType					,
BatchGeocode				,
CEDEX						,
CEDEXBureau					,
City						,
CityKanji					,
Country						,
County						,
DPID_icare					,
Description					,
IsValidated_icare			,
LUWID						,
a.PublicID as LinkID						,
ObfuscatedInternal			,
PostalCode					,
PublicID					,
NULL as ReplacingAddressLinkID		,
Retired						,
--RowNumber					,
StandardizedAddressID_icare ,
State                       ,
Subtype                     ,
ValidUntil
from 
temptable_CM_MC_ER_unmatched_address_temp1 a
)X


--drop table [ContactREF_temptable_maincontact_address_CM_ER_unmatched]
--Storing the data into a REF table before it can be loaded into the final destnation abst_address
select * into [ContactREF_temptable_maincontact_address_CM_ER_unmatched]
from
(
select
*
from
temptable_CM_MC_ER_unmatched_address_temp2
)X

--drop table abst_address
--Loading the data into final destnation abst_address
select * into abst_address
from
(
select
*
from
[ContactREF_temptable_maincontact_address_CM_ER_unmatched]
)X


IF OBJECT_ID('temptable_CM_MC_ER_unmatched_address_temp1') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_address_temp1;

IF OBJECT_ID('temptable_CM_MC_ER_unmatched_address_temp2') is not null
drop table dbo.temptable_CM_MC_ER_unmatched_address_temp2;

END