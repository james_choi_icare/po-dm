﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_TRIAGERISKFACTOR]
AS
BEGIN
	-- =================================
	-- Author: Ann
	-- Create date 06/05/2019
	-- Domain: Triage ccst_triageriskfactor_icare 
	-- Comments: v35 mapping - No join for claim detail in selection criteria which is used in the transformation rules for ClaimID
	--			14/05 checked v36.2 no change
	--			05/07 checked v39 no change
	--			09/07/2019 v39 added is_null = 0
	-- =================================
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_triageriskfactor_icare','Triage - v39 05/07/2019'
--drop table ccst_triageriskfactor_icare
	SELECT 

	'EMICS:' + ctrf.Claim_No as ClaimID,
	ctrf.Claim_No as LUWID,
	'EMICS:' + ltrim(rtrim(ctrf.Claim_No)) + '_' + convert(varchar, ctrf.ID) + ':CLAIM_TRIAGE_RISK_FACTOR' as PublicID,
	Case when rf.TypeCode is null then 1 else 0 end as NewFactor,
	'rf.TypeCode: ' + convert(varchar, rf.typecode) as NewFactor_SRC_VALUE,
	ctrf.Code as RiskFactor

	Into ccst_triageriskfactor_icare

	FROM 
	  [DM_STG_EMICS].dbo.[CLAIM_TRIAGE_RISK_FACTOR] ctrf
	  LEFT OUTER Join [DM_STG_CC].[dbo].[cctl_riskfactors_icare] rf ON ctrf.Code = rf.TYPECODE
	Left join DM_STG_EMICS.dbo.CLAIM_DETAIL cd on ctrf.claim_no = cd.claim_number

	Where cd.is_null = 0
END