﻿-- =============================================
-- Author:		Ann
-- Create date: 09/05
-- Comments: ccst_benefitsaccrual_icare v42
--				v45 updated selection criteria 09/05 ag
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_BENEFITSACCRUAL]
	-- Add the parameters for the stored procedure here

AS
BEGIN
-- drop table ccst_benefitsaccrual_icare
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_benefitsaccrual_icare','Benefits - v42 10/06/2019'

;With CTEx as (
	SELECT claim_no, CountWeeks, sum(trans_amount) trans_amount
	FROM (Select x.claim_no, period_start_date, period_end_date, trans_amount, transaction_date, 
				datediff(week, First_Date, period_start_date) CountWeeks
			From DM_STG_EMICS.dbo.Payment_Recovery  x
				inner join  (select Claim_no, min(period_start_date) First_Date
								FROM DM_STG_EMICS.dbo.Payment_Recovery 
								Where  estimate_type = 50 and trans_amount <> 0  and Reversed <> 1
								GROUP BY Claim_No) y ON x.Claim_No = y.Claim_No
			Where  estimate_type = 50 and trans_amount <> 0  and Reversed <> 1 ) xx
	group by Claim_No, CountWeeks
),CTEx2 as (
	select claim_no, trans_amount, row_number() OVER (partition by claim_no order by countweeks) CountWeeks
	from CTEx
)
,FirstEntitlement as (
	Select 
		claim_no, count(distinct countweeks) as FirstEntitlementWeeks, sum(trans_Amount) as FirstWeekSum--,  CountWeeks --over (partition by CTEx.transaction_date order by claim_no, transaction_date) 
		From CTEx2 
		where countweeks between 1 and 13
		Group by claim_no
),
SecondEntitlement as
(
			Select 
			-- JCFIX ----------------
			claim_no, count(DISTINCT countweeks)  as SecondEntitlementWeeks, sum(trans_Amount) as SecondWeekSum--,  CountWeeks --over (partition by CTEx.transaction_date order by claim_no, transaction_date) 
		From CTEx2 
		where CountWeeks between 14 and 130
		Group by claim_no
),
Totals as 
(
	Select claim_no, max(countweeks) as Totalweekspaid, sum(trans_Amount) as TotalAmountPaid
	From CTEx2
	Group by claim_no
)

Select distinct 
	cd.claim_number as LUWID, 
	'EMICS:' + rtrim(cd.claim_number) + ':BENEFITSACCRUAL' as PublicID,
	'EMICS:' + rtrim(cd.claim_number) + ':INDEMNITY' as ExposureID,
	fe.FirstWeekSum as FirstEntitlementAmount,
	fe.FirstEntitlementWeeks as FirstEntitlementWeeks,
	se.SecondWeekSum as SecondEntitlementAmount,
	se.SecondEntitlementWeeks as SecondEntitlementWeeks,
	tt.TotalAmountPaid as TotalAmountPaid,
	tt.Totalweekspaid as TotalWeeksPaid

	into ccst_benefitsaccrual_icare

	From DM_STG_EMICS.dbo.CLAIM_DETAIL cd
	--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS IC on IC.Claim_number = CD.claim_number
	left join FirstEntitlement fe on fe.Claim_No = cd.Claim_Number
	left join SecondEntitlement se on se.Claim_No = cd.Claim_Number
	left join Totals tt on tt.Claim_No = cd.Claim_Number
	inner join DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad on cad.claim_no = cd.claim_number
	left outer join DM_STG_EMICS.dbo.Payment_Recovery pr on pr.claim_no = cd.claim_number
	Where cd.is_null =0 and cad.Claim_Liability_Indicator <> 12 and (PR.estimate_type 
		in (50,72,54,58,59,60,65) or cd.is_Time_Lost =1 or cd.Date_Deceased is not null)


END