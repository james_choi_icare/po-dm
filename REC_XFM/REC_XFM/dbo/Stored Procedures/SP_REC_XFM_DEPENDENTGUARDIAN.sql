﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_DEPENDENTGUARDIAN]

AS
BEGIN
	-- =============================================
	-- Author:		Ann
	-- Modified date: 09/05
	-- Comments: BENEFITS v39 ccst_dependentguardian_icare
	--				v45 updated 9/05 ag
	--			08/07/02019 checked against v60.1 no changes
	--			12/09/2019 v73, changed Dependent transformation to revert dependent contact FK post batch 1 go-live
	-- =============================================

	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_dependentguardian_icare','Benefits - v73 09/09/2019'
	--drop table ccst_dependentguardian_icare
	Select 
	D.claim_no as LUWID,
	'EMICS:'+ LTRIM(RTRIM(d.Claim_No)) + '_' + convert(varchar, d.ID) + ':DEPENDENTGUARDIAN' PublicID,
	'EMICS:'+ LTRIM(RTRIM(d.Claim_No)) + '_' + LTRIM(RTRIM(convert(varchar, d.ID))) + ':DEPENDENT' [Dependent],
	CASE WHEN estimate_type in (50,54) THEN 'EMICS:' + LTRIM(RTRIM(d.Claim_No)) + ':INDEMNITY' 
		WHEN estimate_type in (55,56) THEN 'EMICS:' + LTRIM(RTRIM(d.Claim_No))  + ':MEDONLYEXP'
		END Exposure,
	Estimate_type as Exposure_SRC_VALUE,
	'EMICS:' + LTRIM(RTRIM(d.Claim_No)) + ':INJURED' Guardian,
	CASE WHEN d.Relationship  in (1, 2) THEN 'spouse'
		WHEN d.Relationship in (3,4) THEN 'parent'
		WHEN d.Relationship in (5,6) THEN 'otherfamilymember'
		WHEN d.Relationship in (7) THEN 'domesticpartner'
		WHEN d.Relationship in (8) THEN 'other'
		WHEN d.Relationship in (9) THEN 'otherfamilymember'
	END AS GuardianRelationship,
	d.Relationship GuardianRelationship_SRC_VALUE

	INTO  ccst_dependentguardian_icare

	From [DM_STG_EMICS].[dbo].Dependents d
	left join ( Select distinct p.claim_no, c.payee_code, p.estimate_type from [DM_STG_EMICS].[dbo].payment_recovery p 
	left join [DM_STG_EMICS].[dbo].claim_payment_run c on c.payment_no = p.payment_no 
		Where p.claim_no in (
			Select distinct claim_no from [DM_STG_EMICS].[dbo].dependents ) 
		and estimate_type not in (61,62,72,73,74,75,76,77)) pr
	on pr.payee_code = d.creditor_no
	--inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(d.claim_no)) 
	
END