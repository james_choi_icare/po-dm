﻿
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMINDICATOR]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 15/07/2019
-- Description: Legal domain - ccst_claimindicator
-- Version: Initial build v32 18/06/2019
--			19/06/2019 v34 updated rules to Name after clarification
-- =============================================
	EXEC SP_05_ADM_MAPPING_VERSION 'ccst_claimindicator','Legal - v34 19/06/0219'


Select Distinct 
	'EMICS:'+RTRIM(cd.Claim_Number)+':ClaimIndicator' as PublicID,
	cd.Claim_Number as LUWID,
	cd.Claim_Number as isOn,
	'EMICS:' + RTRIM(cd.Claim_Number) as ClaimID,
	'LitigationClaimIndicator' as SubType

into ccst_claimindicator

from DM_STG_EMICS.dbo.Litigation l 
inner join dm_STG_EMICS.dbo.claim_detail  cd on cd.Claim_number=l.Claim_No and cd.is_Null=0
--Inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.Claim_Number = cd.Claim_Number
END