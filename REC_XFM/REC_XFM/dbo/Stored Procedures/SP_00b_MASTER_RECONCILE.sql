﻿
CREATE PROC [dbo].[SP_00b_MASTER_RECONCILE] 
	@runID int, @targetSystem varchar(10), @corhort int = 1
AS
BEGIN
	SET NOCOUNT ON;

	IF @targetSystem in ('CC','CM_UPDATE','CM')
	BEGIN

		DECLARE @emailMessage varchar(500), @serverName varchar(100), @dName varchar(100), @errorMessage varchar(100) = '';

		print 'Run ID: ' + cast(@runID as varchar(100))

		SET @dName = DB_NAME();
		SET @serverName = CASE WHEN SERVERPROPERTY('MachineName') = 'IPA2ACLRETLDB04' THEN '10.20.60.65'
							WHEN SERVERPROPERTY('MachineName') = 'IPA2ACLRETLDB05' THEN '10.20.60.105'
							ELSE '' END;

		SET @emailMessage = 'XFM Reconciliation has started at : ' + cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) +
								' Server: ' + @serverName + ' Database: ' + @dName + ' Cohort: ' + cast(@corhort as varchar(100)) + ' Target System: ' + @targetSystem

		--Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'icaredmdevteam@tenzing.co.nz' ,'REC_XFM starting at : '+ cast(getdate() as varchar),@emailMessage,'Q')

		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'james.choi@icare.nsw.gov.au' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')
		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'ann.go@tenzing.co.nz' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')
		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'satish.rajkumar@tenzing.co.nz' ,'REC_XFM starting at : '+ cast(getdate() as varchar), @emailMessage,'Q')

		BEGIN TRY
		
			EXEC [dbo].[SP_02_PROCESS_RECON_XFM_COMPARE] @runID, @targetSystem

			EXEC [dbo].[SP_03_PROCESS_RECON_XFM_SUMMARY] @runID, @targetSystem, 'ADM_RECON_SUMMARY', 'ADM_RECON_RESULT'

			TRUNCATE TABLE [dbo].[ADM_RECON_RESULT_COHORT]

			IF @targetSystem <> 'CM_UPDATE'
			BEGIN 
				INSERT INTO [ADM_RECON_RESULT_COHORT]
				SELECT a.*
				FROM [dbo].[ADM_RECON_RESULT] a 
					INNER JOIN [dbo].[ADM_Claim_Cohort] b ON a.LUWID = b.ClaimNumber
				WHERE b.Cohort = @corhort
			END
			ELSE
			BEGIN
				INSERT INTO [ADM_RECON_RESULT_COHORT]
				SELECT a.*
				FROM [dbo].[ADM_RECON_RESULT] a 
					INNER JOIN [dbo].[ADM_Claim_Cohort] b ON replace(replace(a.LUWID,'EMICS:',''),':INSURED_MC','') = b.ClaimNumber
				WHERE b.Cohort = @corhort
			END			

			EXEC [dbo].[SP_03_PROCESS_RECON_XFM_SUMMARY] @runID, @targetSystem, 'ADM_RECON_SUMMARY_COHORT', 'ADM_RECON_RESULT_COHORT', @corhort
		END TRY
		BEGIN CATCH
		--JA
			IF XACT_STATE()!=0
			BEGIN 
			ROLLBACK TRANSACTION
			END
		--JA
			SET @errorMessage = ERROR_MESSAGE()
		END CATCH

		SET @emailMessage = CASE WHEN @errorMessage <> ''	
								THEN 'XFM Reconciliation has ended with errors at : '+cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) + ' Error Message: ' + @errorMessage
								ELSE 'XFM Reconciliation has ended at : '+cast(getdate() as varchar) + ' Run ID: ' + cast(@runID as varchar(100)) END 
								+ ' Server: ' + @serverName + ' Database: ' + @dName + ' Cohort: ' + cast(@corhort as varchar(100));

		--Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'icaredmdevteam@tenzing.co.nz' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')

		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'james.choi@icare.nsw.gov.au' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')

		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'ann.go@tenzing.co.nz' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')
		Insert into dm_audit.[dbo].[DM_MAILQ] ( crtime, rcp, subject, message, status) values (getdate(),'satish.rajkumar@tenzing.co.nz' ,'REC_XFM ended at : '+ cast(getdate() as varchar), @emailMessage,'Q')
	END
	ELSE
	BEGIN
		print 'Target system must be one of CC, CM or CM_UPDATE'
	END	
END