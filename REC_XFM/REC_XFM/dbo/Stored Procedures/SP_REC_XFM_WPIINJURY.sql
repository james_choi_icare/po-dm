﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_WPIINJURY]
AS
BEGIN
-- =============================================
-- Author:		Ann
-- Create date: 09/05
-- Comments: BENEFITS v36 ccst_wpiinjury_icare
--				v45 checked - updated selection criteria 09/05 ag
--			03/07/2019 - checked against v60.1 no changes
-- =============================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_wpiinjury_icare','Benefits - v60.1 03/07/2019'
--drop table ccst_wpiinjury_icare
	SELECT-- distinct 
	wpiinjury.Claim_number as LUWID,
	'EMICS:' + rtrim(wpiinjury.Claim_Number) + '_' + cast(wpiinjury.Payment_no as varchar(50))+ ':WPIINJURY_ICARE' PublicID,
	areaofbody.TGT_NAME as [AreaOfBody],
	'AreaofBody.TGT_NAME: ' + convert(Varchar, areaofbody.TGT_NAME) as AreaOfBody_SRC_VALUE,
	dpt.GW_DETAIL_TYPECODE as [BodyPart],
	'dpt.GW_DETAIL_TYPECODE: ' + convert(Varchar, dpt.GW_DETAIL_TYPECODE) as BodyPart_SRC_VALUE,
	NULL as InjuryPercent,
	Case 
		WHEN wpiinjury.Location_of_Injury_code = 800 --look up into cctl_cts_injury_Type.Code?
			THEN 'Psychological'
		ELSE 'Physical'
		END AS [InjuryType],
	'Location_of_injury code: ' + convert(varchar, wpiinjury.location_of_injury_code) + ' Location of Injury Description: ' + convert(varchar, wpiinjury.location_of_injury_code_description) as InjuryType_SRC_VALUE, 
	--x.Location_of_Injury as [InjuryType_SRC_VALUE],
	NULL as WPIInput_icare
	
	INTO ccst_wpiinjury_icare
	From (
		Select X.*, Y.location_of_injury_code, y.location_of_injury_code_description
		From 
			( Select distinct cd.claim_number , cpr.cheque_no, cpr.cheque_amount, pr.payment_type, cpr.Payment_no, cd.Location_of_Injury
				From [DM_STG_EMICS].[dbo].CLAIM_PAYMENT_RUN cpr
				Inner join [DM_STG_EMICS].[dbo].CLAIM_DETAIL cd on cpr.claim_number = cd.claim_number
				LEft outer join [DM_STG_EMICS].[dbo].CREDITORS cr on cpr.payee_code = cr.Creditor_no
				Left outer join [DM_STG_EMICS].[dbo].Payment_Recovery pr on pr.claim_no = cpr.claim_number
					and pr.payment_no = cpr.payment_no
				Where pr.payment_type like '%WPI%' and cd.is_null =0
			) x
		Left outer join
			( Select distinct a.claim_number, a.date_of_injury, c_sorted.code as location_of_injury_code,
			c_sorted.description as location_of_injury_code_description
			From [DM_STG_EMICS].[dbo].claim_detail a
			left outer join [DM_STG_EMICS].[dbo].LOCATION_CODES b on a.Location_of_Injury = b.ID
			Left outer join
			(
				Select row_number() over (partition by c.Code order by c.version desc) Latest, c.Code,
				c.Description 
				from [DM_STG_EMICS].[dbo].LOCATION_DESC c
			) c_sorted on c_sorted.Code = b.code and c_sorted.Latest = 1
			Where a.is_null=0
		) Y on x.Claim_Number = y.Claim_Number
	) wpiinjury
	Left join [DM_XFM].[dbo].[DM_LKP_SRC_TGT_BODYPARTTYPE] dpt ON wpiinjury.Location_of_Injury_code = dpt.EMICS_CODE
	Left join [DM_XFM].[dbo].[DM_LKP_SRC_TGT_BODYPARTTYPE] areaofbody on areaofbody.EMICS_CODE = wpiinjury.location_of_injury_code

	
END