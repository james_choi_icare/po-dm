﻿
-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 13/06/2019
-- Description:	Contacts - Named Insured V003.8 Main Contac v2.17
-- 1. This is the master Stored Proc that will call the different contacts type CLAIMCONTACTROLE stored proc such as
--    SP_REC_XFM_CLAIMCONTACTROLE_EMPLOYER_STAGING
-- <24-Jun-2019><Satish>: Added a call to [dbo].[SP_REC_XFM_CLAIMCONTACTROLE_MAINCONTACT_STAGING]
-- <09Aug2019><Satish>: Added code to account for ER_unmatched and VerifiedCDR Main Contacts too.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CLAIMCONTACTROLE]
	
AS
BEGIN
EXEC SP_REC_XFM_CLAIMCONTACTROLE_EMPLOYER_STAGING
EXEC SP_REC_XFM_CLAIMCONTACTROLE_MAINCONTACT_STAGING
--EXEC SP_REC_XFM_CONTACT_INJURED_WORKER
;

IF OBJECT_ID('ccst_claimcontactrole') is not null
drop table dbo.ccst_claimcontactrole;



/****** Object:  Table [dbo].[ccst_claimcontactrole]    Script Date: 5/30/2019 11:41:40 AM ******/
CREATE TABLE [dbo].[ccst_claimcontactrole](
	[Active] [int] NOT NULL,
	[ClaimContactID] [varchar](33) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [Comments] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [CoveredPartyType] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [EvaluationID] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [ExposureID] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [IncidentID] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [LUWID] [varchar](19) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [MatterID] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [NegotiationID] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [PartyNumber] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [PolicyID] [varchar](32) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [PublicID] [varchar](50) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [Retired] [int] NOT NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [Role] [varchar](15) NOT NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [WitnessPerspective] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [WitnessPosition] [varchar](30) NULL
ALTER TABLE [dbo].[ccst_claimcontactrole] ADD [WitnessStatementInd] [varchar](30) NULL
;

INSERT INTO [dbo].ccst_claimcontactrole
(
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
)
SELECT 
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
  FROM [dbo].[ClaimContactRoleREF_temptable_employer_verifiedPC]
  ;

INSERT INTO [dbo].ccst_claimcontactrole
(
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
)
SELECT 
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
  FROM [dbo].[ClaimContactRoleREF_temptable_employer_verifiedCDR]
  ;

INSERT INTO [dbo].ccst_claimcontactrole
(
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
)
SELECT 
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
  FROM [dbo].[ClaimContactRoleREF_temptable_maincontact_matched]
  ;

--Add on 09Aug2019
  INSERT INTO [dbo].ccst_claimcontactrole
(
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
)
SELECT 
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
  FROM [dbo].[ClaimContactRoleREF_temptable_maincontact_unmatched_ER]
  ;

--Add on 09Aug2019
   INSERT INTO [dbo].ccst_claimcontactrole
(
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
)
SELECT 
	[Active],
	[ClaimContactID],
	[Comments],
	[CoveredPartyType],
	[EvaluationID],
	[ExposureID],
	[IncidentID],
	[LUWID],
	[MatterID],
	[NegotiationID],
	[PartyNumber],
	[PolicyID],
	[PublicID],
	[Retired],
	[Role],
	[WitnessPerspective],
	[WitnessPosition],
	[WitnessStatementInd]
  FROM [dbo].[ClaimContactRoleREF_temptable_maincontact_verifiedCDR]
  ;



END