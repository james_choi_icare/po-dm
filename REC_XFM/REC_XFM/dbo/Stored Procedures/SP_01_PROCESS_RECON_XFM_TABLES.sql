﻿
CREATE PROC [dbo].[SP_01_PROCESS_RECON_XFM_TABLES] 
	@runID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @startTime datetime, @endTime datetime;

	SELECT @startTime = CURRENT_TIMESTAMP
	EXEC [dbo].SP_REC_XFM_COVER_REF_STAGING;
	SELECT @endTime = CURRENT_TIMESTAMP

	INSERT INTO [dbo].[ADM_RECON_RUN_AUDIT_DETAIL]
	SELECT @runID, 'SP_REC_XFM_COVER_REF_STAGING completed', @startTime, @endTime

	DECLARE @columns varchar(max) = '', @spocName varchar(100), @targetTableName varchar(100), @query nvarchar(max), @logFileName varchar(100);

	DECLARE sprocs_cursor CURSOR FOR
	SELECT name , xfm_recon_table
	FROM sys.objects  a
		INNER JOIN (	SELECT 'SP_REC_' + replace(replace(replace(xfm_table,'DM_',''),'_ICARE',''),'_EXT','') recon_sp, xx.xfm_recon_table, xx.Running_Order
			FROM [dbo].[ADM_XFM_TABLE_MAPPING] xx
			WHERE Process = 1) b ON a.name = b.recon_sp 
	ORDER BY Running_Order, name

	OPEN sprocs_cursor

	FETCH NEXT FROM sprocs_cursor
	INTO @spocName,@targetTableName

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRY

			SELECT @startTime = CURRENT_TIMESTAMP

			SET @query = 'IF OBJECT_ID('''+@targetTableName+''',''U'') IS NOT NULL
								DROP TABLE '+@targetTableName+'';
			exec sp_executesql @query
		
			SET @query = 'EXEC ' + @spocName;
			exec sp_executesql @query

			SELECT @endTime = CURRENT_TIMESTAMP

			SET @query = 'select top 1 @logFileName = name from sys.database_files where name like ''%log%'' order by name';
			exec sp_executesql @query, N'@logFileName varchar(100) OUTPUT', @logFileName = @logFileName OUTPUT

			SET @query = 'dbcc shrinkfile(' + @logFileName + ',1)'
			exec sp_executesql @query					

		END TRY
		BEGIN CATCH
			INSERT INTO [dbo].[ADM_RECON_RUN_ERROR]
			SELECT @runID, @spocName + ' did not run'
		END CATCH;

		INSERT INTO [dbo].[ADM_RECON_RUN_AUDIT_DETAIL]
		SELECT @runID, @spocName + ' completed', @startTime, @endTime

		FETCH NEXT FROM sprocs_cursor
		INTO @spocName,@targetTableName
	END
	CLOSE sprocs_cursor
	DEALLOCATE sprocs_cursor
END