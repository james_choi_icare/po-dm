﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_GARNISHEEORDER]
	
AS
BEGIN
-- ===============================
-- Author: Ann
-- Domain: PAYMENTS AND RESERVES  ccst_garnisheeorder_icare
-- Version: v42
--			14/05 checked against v45.6 no change
-- =====================================

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_garnisheeorder_icare','Payments and Recoveries - v45.6 14/05/2019'

	SELECT 
	DISTINCT  'EMICS:' + RTRIM(CD.Claim_Number) + '_' + RTRIM(GR.Garnishee_No)+ ':VENDOR' AS AgencyID,
	gr.Weekly_Deduction_Amount as Amount,
	'EMICS:'+RTRIM(GR.Claim_No) AS ClaimID,
	/*CASE WHEN 
	  exists (SELECT 1 FROM Dm_stg_emics.dbo.CLAIM_PAYMENT_RUN CPR1 WHERE CPR.Claim_number=CPR1.Claim_number AND CPR.Payee_Code=CPR1.Payee_Code AND Drawn_date IS NOT NULL )
	   THEN 1
		 ELSE 0
		END as Completed,*/
	gr.End_Date as EndDate,
	'weekly' as Frequency,
	CASE WHEN GR.Garnishee_No='33547'
          THEN 'centrelink_icare'
          WHEN GR.Garnishee_No='85581'
        THEN 'child_support'
		ELSE NULL
		END as GarnisheeType,
	GR.Claim_No as LUWID,
	'weekly_benefit' as paymentType,
	'pre' as PrePostPAYG,
	'EMICS:'+RTRIM(GR.Claim_No)+'_'+RTRIM(GR.Garnishee_No)+'_'+convert(varchar,GR.ID)+':GARNISHEE'  as PublicID,
	gr.Reference as Reference, 
	gr.Start_Date as StartDate, 
	0.00 as TotalAmount

	into ccst_garnisheeorder_icare
	    
	FROM Dm_Stg_emics.dbo.Payment_Recovery PR
	INNER JOIN Dm_Stg_emics.dbo.Garnishees GR ON PR.Claim_No=GR.Claim_No
		AND PR.Garnishee_No=GR.Garnishee_No
	INNER JOIN Dm_Stg_emics.dbo.CLAIM_DETAIL CD ON CD.Claim_Number=PR.Claim_No
	--INNER JOIN DM_XFM.dbo.DM_INSCOPE_CLAIMS ic on ic.claim_number = cd.Claim_Number
	LEFT OUTER JOIN Dm_Stg_emics.dbo.CLAIM_PAYMENT_RUN CPR	ON CPR.Claim_number=PR.Claim_No
		AND CPR.Payment_no=PR.Payment_no
	left outer join (
		select Claim_number,Payee_Code,sum(Cheque_Amount) as cheque_amt from Dm_Stg_emics.dbo.CLAIM_PAYMENT_RUN a, (select distinct claim_no,garnishee_no from Dm_Stg_emics.dbo.Garnishees) b
		where a.Claim_number=b.Claim_No and a.Payee_Code=b.Garnishee_No and a.Cheque_Status<>3
		group by Claim_number,Payee_Code) paidtodate on gr.Claim_No=paidtodate.Claim_number
	and GR.Garnishee_No=paidtodate.Payee_Code
	WHERE CD.is_Null=0 and   PR.Trans_Amount>0 AND REVERSED<>1


END