﻿CREATE PROC [dbo].[SP_REC_XFM_TRANSACTION] AS

BEGIN
--PAYMENTS AND RECOVERIES V40.3 ccst_transaction
--23/04/2019 - V44 - remove reference to XFM policy table 
--15/07/2019 - v46.3.9 updated new SC and xform rules. fixed duplicate publicID
--03/09/2019 - v47.2 updated SC in payments to include Cpc join and checkid xform rules
--****************************************************
--****************************************************
--drop table ccst_transaction
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_transaction','Payments and Recoveries - v47.2 03/09/2019'
--Payments
--1787130
;With closed_claim as (
	select * from (
	SELECT c.Claim_no,b.Payment_no,max(B.Drawn_date) over(partition by c.claim_no ) as  max_drawn_date,Drawn_date,
	Date_Claim_Closed,c.Date_Claim_reopened,PRI.Estimate_type--,max(Paid_Date) over(partition by c.claim_no  )  as Paid_Date
	FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN B
	LEFT JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL C ON B.Claim_number=C.Claim_no
	INNER JOIN DM_STG_EMICS.dbo.Payment_Recovery PRI ON B.Payment_no=PRI.Payment_no
		AND B.Claim_number=PRI.Claim_No) iq
	where Date_Claim_Closed>=iq.max_drawn_date and iq.Drawn_date=iq.max_drawn_date and (Date_Claim_reopened  is  null  OR (Date_Claim_reopened is not null and Date_Claim_reopened<=Date_Claim_Closed ))
) ,
payments_transaction as (
	SELECT distinct 
	
	CASE WHEN ltrim(rtrim(isnull(cpr.Cheque_no,'')))='' THEN  'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+ISNULL(convert(varchar,cpc.id),'00') + ':CHECK'
	ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.cheque_no),'')+'_'+ISNULL(convert(varchar,cpc.id),'00') + ':CHECK' END as CheckID,
	'' as claimcontactID,
	'EMICS:'+RTRIM(PR.Claim_No) as ClaimID,
	CASE WHEN clsd_clm.Payment_no IS NOT NULL THEN 'Y' ELSE 'N' 
		END as Closeclaim,
	case when clsd_expsr.Payment_no is not null then 'Y' else 'N' 
		end as CloseExposure,
	LTRIM(RTRIM(CPR.Payment_desc)) as Comments,
	PR.Estimate_type as Costcategory,
	CASE WHEN PR.Estimate_type ='64' THEN 'aoexpense'
		ELSE 'claimcost'
		END AS COSTTYPE,
	'aud' as Currency,
	0 AS DoesnotErodeReserves,
	CASE WHEN CD.is_Null=0 AND CAD.CLAIM_LIABILITY_INDICATOR<>12
		THEN CASE WHEN PR.ESTIMATE_TYPE IN (51,52,53,55,56,63,64)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':MEDONLYEXP'
		WHEN PR.ESTIMATE_TYPE IN (50,72,54,58,59,60,65)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':INDEMNITY'
		WHEN PR.Estimate_type='57'
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':EMPLIAB'
		ELSE NULL
	   END
		ELSE NULL
		END AS EXPOSUREID, 
	'' as InvoiceNumber_icare,
	CD.Claim_NUmber as LUWID,
	'EMICS:'+convert(varchar,CD.Claim_Number)+convert(varchar,LTG.ID)+':LITIGATION' as MatterID,
	'partial' as Paymenttype,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type+'_'+convert (varchar,pr.pr_id)+'_'+
		case when ISNULL(nullif(PR.Adjust_Trans_Flag,''),'N')='Y' then '1' else '0' 
		end+':PAYMENT' AS PublicID,
	'' as Payeetype_icare,
	'' as RecoveryCategory,
	'' as RecoveryType,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+PR.estimate_type+':RSRVLN' AS ReserveLineId,
	'aud' as Reservingcurrency,
	0 as retired,
	'Payment' as subtype,
	case 
	when Cheque_Status =9 then 'submitted' -- EFT Payments-- cleared
	when Cheque_Status =3 and BankChq_Status is null then 'voided'-- Cheque was cancelled before sending out
	when BankChq_Status =2 then 'voided'-- Cheque was stopped after sending out-- Changing as part of defect DMIG-3792, the stopped cheques are considered as vioded and not stopped
	when Cheque_Status  not in (3,9) and BankChq_Status is null then 'submitted' -- Cheque is requested from-- during migration you should have this status as alll cheques are expected to be cleared
	when BankChq_Status =1 then 'submitted'-- Cheque was cleared by the Bank
	else 'submitted'-- Added to include status for records having null cheque status which is timing issues as per Kim (DMIG-1743), should be fixed during go live
	end as Status,
	'EMICS:'+rtrim(PR.claim_no)+'_' +convert(varchar,pr.Payment_no)+'_'+convert(varchar,pr.Estimate_type)+'_'+
		case when ISNULL(nullif(PR.Adjust_Trans_Flag,''),'N')='Y' then '1' else '0' end +':CHECKSET'  AS  TRANSACTIONSETID

 FROM (select min(id) over(partition  by  Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* 
	from  DM_STG_EMICS.dbo.Payment_Recovery) PR
LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON CPR.Claim_Number=PR.Claim_No
	and CPR.Payment_No=PR.Payment_No 
INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON CD.Claim_Number=PR.Claim_No
INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CAD.CLAIM_NO=CD.Claim_Number
LEFT OUTER JOIN closed_claim clsd_clm on clsd_clm.Claim_no=PR.Claim_No
	and clsd_clm.Payment_no=PR.Payment_no 
LEFT OUTER JOIN closed_claim clsd_expsr on clsd_expsr.Claim_No=PR.Claim_No
	and clsd_expsr.Payment_no=PR.Payment_no
	and clsd_expsr.Estimate_type=PR.Estimate_type
LEFT OUTER JOIN (Select * from (select row_number() over(partition by claim_no order by status_date desc)as row_no,
       Claim_No,status_date,id from DM_STG_EMICS.dbo.Litigation-- where ID <> 101
    ) LTG_1 Where LTG_1.row_no=1) LTG  on LTG.Claim_No=CD.Claim_Number 
LEFT OUTER JOIN (SELECT B.paycode, c.typecode as paymenttype_icare, c.ID from DM_STG_CC.dbo.ccx_paycodepaymenttype_icare A,
	(Select * from DM_STG_CC.dbo.ccx_paycode_icare where Retired =0) B, DM_STG_CC.dbo.cctl_paymenttype_icare C
	Where a.paycode= b.ID and a.paymenttype= c.ID) CPC
	on CPC.paycode = (case when pr.payment_type like 'OR%' then PR.WC_Payment_Type else PR.payment_type end)
where PR.ESTIMATE_TYPE<70 and CD.IS_NULL=0 and Reversed<>1 and Garnishee_No is  null
and isnull(cpr.cheque_status,'00') <>6)
, ORIGINAL_PAYMENT AS 
	(SELECT DISTINCT pr.claim_no ,
		pr.payment_no , 
		prwb.Claim_No as Orig_Claim_no,
		PRWB.Payment_no as orig_payment_no,
		cprwb.Cheque_no as orig_cheq_no,
		cprwb.Cheque_Amount as orig_cheq_amount,
		min(PRWB.id) as orig_pr_id
	from  DM_STG_EMICS.dbo.PAYMENT_RECOVERY PR
	INNER JOIN  DM_STG_EMICS.dbo.Payment_Recovery PRWB ON PR.Claim_No=PRWB.Claim_No and exists(select 1 from  DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN c where pr.payment_no=c.payment_no and isnull(Cheque_Status,'00')=6 )
		and  not exists(select 1 from  DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN c where PRWB.payment_no=c.payment_no and isnull(Cheque_Status,'00') in(3,6) )
	AND ISNULL(PR.Period_Start_Date,'1900-01-01')=ISNULL(PRWB.Period_Start_Date,'1900-01-01')
	AND ISNULL(PR.Period_End_Date,'1900-01-01')=ISNULL(PRWB.Period_End_Date,'1900-01-01')
	AND (ISNULL((nullif(PR.Invoice_no,'')),'DUMMY')=ISNULL((nullif(PRWB.Invoice_no,'')),'DUMMY')
		or ISNULL((PR.Invoice_Date),'1900-01-01')=ISNULL((PRWB.Invoice_Date),'1900-01-01'))
		and PR.Payment_type=PRWB.Payment_Type
		and isnull(PR.wc_payee_id,'DUMMY')=isnull(PRWB.wc_payee_id,'DUMMY')
		and ISNULL(nullif(PRWB.Adjust_Trans_Flag,''),'N') ='N' AND PRWB.Trans_Amount>0
		and PRWB.Trans_Amount=pr.Trans_Amount*-1
	inner join  DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPRWB on prwb.Claim_No=cprwb.Claim_number
		and prwb.Payment_no=cprwb.Payment_no
	group by pr.claim_no,pr.payment_no,PRWB.Claim_no,PRWB.Payment_No,cprwb.Cheque_no,cprwb.Cheque_Amount 
) , recovery_transaction as (
	SELECT distinct -- rp.orig_cheq_no,rp.orig_payment_no ,cpr.cheque_no,cpr.payment_no,rp.payment_no as rp_payment_no,
	CASE WHEN ISNULL(CHEQUE_STATUS,'00')='6' THEN 
	CASE WHEN rp.orig_cheq_no IS  NULL THEN  
	'EMICS:'+convert(varchar,rp.orig_payment_no)+'_'+convert(varchar,rp.orig_pr_id)
	ELSE 'EMICS:'+convert(varchar,rp.orig_payment_no)+ISNULL('_'+RTRIM(rp.orig_cheq_no),'')+'_'+convert(varchar,rp.orig_pr_id) END
	ELSE NULL
	END as CheckID,
	CASE WHEN ISNULL(CPR.cheque_status,'00')=6 
	THEN 
	CASE 
	WHEN CPR.PAYEE_TYPE =1 THEN 
	'EMICS:' + RTRIM(CD.Claim_Number) + ':INJURED'
	WHEN CPR.PAYEE_TYPE in (4,3)
	THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,CPR.Payee_Code)+':VENDOR'
	WHEN PAYEE_TYPE=2
	THEN 'EMICS:' + RTRIM(CD.Claim_Number) + ':INSURED'
	WHEN PAYEE_TYPE=23
	THEN 'EMICS:' + RTRIM(CD.Claim_Number) + '_' + CONVERT(VARCHAR,DP.ID) + ':DEPENDENT'
	ELSE NULL
	End
	ELSE NULL
	END as claimcontactID,
	'EMICS:'+PR.Claim_No as ClaimID,
	''  as Closeclaim,
	'' as CloseExposure,
	LTRIM(RTRIM(CPR.Payment_desc)) as Comments,
	PR.Estimate_type as Costcategory,
	'claimcost' AS COSTTYPE,
	'aud' as Currency,
	'' AS DoesnotErodeReserves,
	 CASE WHEN CD.is_Null=0 AND CAD.CLAIM_LIABILITY_INDICATOR<>12
	 THEN 
	  CASE WHEN PR.ESTIMATE_TYPE IN (51,52,53,55,56,63,64)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':MEDONLYEXP'
		WHEN PR.ESTIMATE_TYPE IN (50,72,54,58,59,60,65)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':INDEMNITY'
		WHEN PR.Estimate_type='57'
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':EMPLIAB'
		ELSE NULL
	   END
	ELSE NULL
	END 
	AS EXPOSUREID,
	PR.Invoice_no as InvoiceNumber_icare,
	CD.Claim_NUmber as LUWID,
	'' as MatterID,
	'' as Paymenttype,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type+'_'+convert

	(varchar,pr.pr_id)+'_0'+':RECOVERY' AS publicid,
	CASE WHEN CPR.Payee_Type='1' THEN 'claimant'
	   WHEN CPR.Payee_Type='2' THEN 'insured'
	   WHEN CPR.Payee_Type IN ('3','4') THEN 'vendor'
	   ELSE 'other'
	END AS Payeetype_icare,
	CASE WHEN PR.Payment_Type ='RPE001' THEN 'write_off_icare'
	WHEN PR.Payment_Type <>'RPE001' AND PR.Estimate_Type >=70 THEN 'subro'
	ELSE 'overpayment_icare'
	END as RecoveryCategory,
	CASE WHEN PR.Estimate_type='72' THEN 'excess'
		 WHEN PR.Estimate_type='73' THEN '151ACommonLaw'
	  WHEN PR.Estimate_type='74' THEN 'SharedNIAgent'
	  WHEN PR.Estimate_type='75' THEN 'SharedInsurer'
	  WHEN PR.Estimate_type='76' THEN '151ZCTP'
	  WHEN PR.Estimate_type='77' THEN '151ZOther'
	  WHEN ISNULL(CPR.Cheque_Status,'00')=6 THEN 'Overpayment'--DMIG-6362-- ADDING CLAIMCONTACT INFO FOR OVERPAYMENT RECOVERY 
	END
	 as RecoveryType,
	 'EMICS:'+RTRIM(cd.Claim_number)+'_'+PR.estimate_type+':RSRVLN' AS ReserveLineId,
	'aud' as Reservingcurrency,
	0 as retired,
	'Recovery' as subtype,
	'submitted' as Status,
	'EMICS:'+RTRIM(PR.Claim_no)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.Estimate_type+'_0'+':RECOVERYSET' AS transactionsetID

	--into #transaction_recovery
	 FROM  (select min(id) over(partition  by  Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as 
		pr_id,* 
		from  DM_STG_EMICS.dbo.Payment_Recovery) PR
	left outer join DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR ON CPR.Claim_Number=PR.Claim_No
		and CPR.Payment_No=PR.Payment_No 
	left outer join ORIGINAL_PAYMENT RP on RP.Claim_No=pr.Claim_No
		and RP.Payment_no=pr.Payment_no
		and (charindex(rp.orig_cheq_no,cpr.Cheque_no,1)<>0 )
	INNER JOIN  DM_STG_EMICS.dbo.CLAIM_DETAIL CD ON CD.Claim_Number=PR.Claim_No
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD ON CAD.CLAIM_NO=CD.Claim_Number
	LEFT OUTER JOIN DM_STG_EMICS.dbo.DEPENDENTS DP ON DP.CLAIM_NO=PR.CLAIM_NO
	where ( ISNULL(CPR.Cheque_Status,'00')=6 or pr.estimate_type>=70) and CD.IS_NULL=0 and Reversed<>1
) , reserves_transaction as (
	SELECT  distinct 
	'' as CheckID,
	'' as claimcontactID,
	'EMICS:'+RTRIM(PR.Claim_No) as ClaimID,
	'' as Closeclaim,
	''  as CloseExposure,
	LTRIM(RTRIM(CPR.Payment_desc)) as Comments,
	PR.Estimate_type as Costcategory,
	CASE WHEN PR.Estimate_type ='64' THEN 'aoexpense'
	ELSE 'claimcost'
	END AS COSTTYPE,
	'aud' as Currency,
	'' AS DoesnotErodeReserves,
	 CASE WHEN CD.is_Null=0 AND CAD.CLAIM_LIABILITY_INDICATOR<>12
	 THEN 
	  CASE WHEN PR.ESTIMATE_TYPE IN (51,52,53,55,56,63,64)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':MEDONLYEXP'
		WHEN PR.ESTIMATE_TYPE IN (50,72,54,58,59,60,65)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':INDEMNITY'
		WHEN PR.Estimate_type='57'
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':EMPLIAB'
		ELSE NULL
	   END
	ELSE NULL
	END 
	AS EXPOSUREID,
	'' as InvoiceNumber_icare,
	CD.Claim_NUmber as LUWID,
	'' as MatterID,
	'' as Paymenttype,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type+'_'+convert

	(varchar,pr.pr_id)+'_0'+':RESERVE' AS publicid,
	'' as Payeetype_icare,
	'' as RecoveryCategory,
	'' as RecoveryType,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+PR.estimate_type+':RSRVLN' AS ReserveLineId,
	'aud' as Reservingcurrency,
	0 as retired,
	'Reserve' as subtype,
	'submitted' as Status,
	'EMICS:'+RTRIM(PR.Claim_no)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.Estimate_type+'_0'+':RESERVESET' AS 
	TRANSACTIONSETID
	--into #transaction_reserve
	 FROM 
	(select min(id) over(partition  by 
	Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* from 
	DM_STG_EMICS.dbo.Payment_Recovery) PR
	LEFT OUTER JOIN 
	DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	ON CPR.Claim_Number=PR.Claim_No
	and CPR.Payment_No=PR.Payment_No
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	ON CD.Claim_Number=PR.Claim_No
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD
	ON CAD.CLAIM_NO=CD.Claim_Number
	LEFT OUTER JOIN DM_STG_EMICS.dbo.Litigation LTG
	ON LTG.Claim_No=CD.Claim_Number and LTG.ID <> 101
	where PR.ESTIMATE_TYPE<70 and CD.IS_NULL=0 and Reversed<>1 and PR.Garnishee_No is null
	AND ISNULL(CPR.Cheque_Status,'00')<>6
) ,  recoveryreserve_transaction as (
	SELECT DISTINCT  '' as CheckID,
	'' as claimcontactID,
	'EMICS:'+PR.Claim_No as ClaimID,
	''  as Closeclaim,
	'' as CloseExposure,
	LTRIM(RTRIM(CPR.Payment_desc)) as Comments,
	PR.Estimate_type as Costcategory,
	'claimcost' AS COSTTYPE,
	'aud' as Currency,
	'' AS DoesnotErodeReserves,
	--CASE WHEN CP.LUWID IS not NULL
	--THEN 
	 CASE WHEN CD.is_Null=0 AND CAD.CLAIM_LIABILITY_INDICATOR<>12
	 THEN 
	  CASE WHEN PR.ESTIMATE_TYPE IN (51,52,53,55,56,63,64)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':MEDONLYEXP'
		WHEN PR.ESTIMATE_TYPE IN (50,72,54,58,59,60,65)
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':INDEMNITY'
		WHEN PR.Estimate_type='57'
		THEN 'EMICS:'+RTRIM(CD.Claim_Number)+':EMPLIAB'
		ELSE NULL
	   END
	-- ELSE NULL
	--  END
	ELSE NULL
	END 
	AS EXPOSUREID,
	'' as InvoiceNumber_icare,
	RTRIM(CD.Claim_Number) AS luwid,
	''  as MatterID,
	'' as Paymenttype,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.estimate_type+'_'+convert

	(varchar,pr.pr_id)+'_0'+':RECOVERYRESERVE' AS publicid,
	''  AS Payeetype_icare,
	CASE WHEN PR.Payment_Type ='RPE001' THEN 'write_off_icare'
	WHEN PR.Payment_Type <>'RPE001' AND PR.Estimate_Type >=70 THEN 'subro'
	ELSE 'overpayment_icare'
	END as RecoveryCategory,
	'' as  RecoveryType,
	'EMICS:'+RTRIM(cd.Claim_number)+'_'+PR.estimate_type+':RSRVLN' AS ReserveLineId,
	'aud' as Reservingcurrency,
	0 as retired,
	'RecoveryReserve' as subtype,
	'submitted' as Status,
	'EMICS:'+RTRIM(PR.Claim_no)+'_'+convert(varchar,PR.Payment_no)+'_'+PR.Estimate_type+'_0'+':RECOVERYRESERVESET' 

	AS TRANSACTIONSETID
	--into #transaction_recoveryreserve
	 FROM 
	 (select min(id) over(partition  by 

	Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,estimate_type ) as pr_id,* from 

	DM_STG_EMICS.dbo.Payment_Recovery) PR
	LEFT OUTER JOIN
	DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
	ON CPR.Claim_Number=PR.Claim_No
	and CPR.Payment_No=PR.Payment_No
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
	ON CD.Claim_Number=PR.Claim_No
	INNER JOIN DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL CAD
	ON CAD.CLAIM_NO=CD.Claim_Number
	LEFT OUTER JOIN DM_STG_EMICS.dbo.Litigation LTG
	ON LTG.Claim_No=CD.Claim_Number --and LTG.ID <> 101
	LEFT OUTER JOIN DM_STG_EMICS.dbo.DEPENDENTS DP
	ON DP.CLAIM_NO=PR.CLAIM_NO
	where (PR.ESTIMATE_TYPE>=70 or ISNULL(CPR.Cheque_Status,'00')=6) and CD.IS_NULL=0 and Reversed<>1

)

, allTransaction as (
	Select * from payments_transaction union 
	Select * from recovery_transaction union
	select * from reserves_transaction union
	Select * from recoveryreserve_transaction
)
Select * into ccst_transaction from allTransaction 


--drop table ccst_transaction

END