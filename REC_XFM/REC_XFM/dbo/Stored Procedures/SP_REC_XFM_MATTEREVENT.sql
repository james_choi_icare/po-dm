﻿-- =============================================
-- Author:		Ann
-- Create date: 17/06/2019
-- Description: Legal domain - ccst_matterevent
-- Version: Initial build v30 17/06/2019
-- =============================================
CREATE PROCEDURE SP_REC_XFM_MATTEREVENT
AS
BEGIN




Select 
	NULL as EventContactID,
	l.Date_Hearing as EventDate,
	Case when l.Hearing_type_code = 'TC' then convert(varchar, cm.Data)
		Else NULL
		End as EventDetails,
	NULL as EventLocationID,
	NULL as EventResult,
	Case when l.Hearing_type_code = 'AR' then 'arbitration_icare'
		when l.Hearing_type_code in ('AP','DP','DR','CA') then 'hearing_icare'
		when l.Hearing_type_code = 'MD' then 'mediation_icare'
		when l.Hearing_type_code = 'TC' then 'teleconference_icare'
		End as EventType,
	cd.claim_number as LUWID,
	'EMICS:' + RTRIM(l.Claim_no) + '_' + convert(varchar, l.ID) + ':Litigation' as MatterID,
	'EMICS:' + RTRIM(l.claim_no) + '_' + convert(varchar, l.ID) + ':Litigation' as PublicID

Into ccst_matterevent

From DM_STG_EMICS.dbo.Litigation l
Inner Join DM_STG_EMICS.dbo.Claim_detail cd on l.claim_no = cd.claim_number
Left outer join DM_STG_EMICS.dbo.CLM_MEMO cm on l.Text_ptr = cm.Id 
Where ISNULL(Hearing_Type_code,'') != '' and l.ID <> 101 --93

END