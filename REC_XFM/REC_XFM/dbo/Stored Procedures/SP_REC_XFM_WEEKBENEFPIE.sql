﻿CREATE PROCEDURE [dbo].[SP_REC_XFM_WEEKBENEFPIE]
AS
BEGIN
/* =============================================
-- Author:		Ann
-- Modified date: 09/05
-- Description: BENEFITS V39 ccst_weekbenefPIE_icare
-- Version: 09/05/2019 v45
			10/06/2019 v51 - AG Updated SC
			08/07/2019 checked v60.1 no changes
-- ============================================= */

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_weekbenefPIE_icare','Benefits - v60.1 10/06/2019'
--drop table ccst_weekbenefPIE_icare
	Select
	tld.Claim_no as LUWID,
	'EMICS:' + RTRIM(tld.Claim_No) + '_' + convert(varchar,tld.ID) + ':WEEKLYBENEFITPOSTINJURYEARNINGS' PublicID,
	'EMICS:'+RTRIM(tld.Claim_no) + ':INDEMNITY' ExposureID,
	NULL as Comments,
	NULL as Deductions,
	convert(datetime2 , tld.Date_Ceased_Work) as StartDate,
	convert(Datetime2, tld.Date_Resumed_Work) as EndDate,
	tld.Actual_HoursPerWeek as HoursWorked,
	tld.Weekly_Earnings as OrdinaryEarnings,
	'0' as ShiftAndOvertime

	INTO ccst_weekBenefPIE_icare

	From [DM_STG_EMICS].[dbo].TIME_LOST_DETAIL tld
	left join [DM_STG_EMICS].[dbo].CLAIM_ACTIVITY_DETAIL cad on cad.claim_no = tld.claim_no
	left join [DM_STG_EMICS].[dbo].CLAIM_DETAIL cd on cd.claim_number = cad.claim_no	
	--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
	Where cd.is_null = 0
	AND cad.claim_liability_indicator <> 12
	AND cd.claim_number in (
		select distinct claim_no from
			(
				Select distinct claim_no, estimate_type from [DM_STG_EMICS].[dbo].payment_recovery
				Where Reversed = 0 and estimate_type in (50,72,54,58,59,60,65) --v51
			) expo
		UNION
		Select claim_number as claim_no from [DM_STG_EMICS].[dbo].claim_detail
		Where date_deceased is not null OR is_time_lost = 1 
	)
	And is_deleted = 0
	And tld.weekly_earnings>0

END