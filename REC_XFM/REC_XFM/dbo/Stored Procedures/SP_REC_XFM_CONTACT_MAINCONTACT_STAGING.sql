﻿

-- =============================================
-- Author:		Satish Rajkumar
-- Create date: 18/06/2019
-- Description:	Contacts - Main Contact V002.17
-- 1. This is the child Stored Proc for MAINCONTACT contact that will called by the master stored proc SP_REC_XFM_CONTACT
-- FYI..The acronym PPLC used in the SP below stands for "Primary Policy Location Contact"
-- For matched there are 3 options -
-- (a) Notifier Type='E,'R' and there is a PPLC contact in PC and the PPLC names match the notifier names
-- (b) Notifier Type='E,'R' and there is a PPLC contact in PC but the PPLC name does not match notifier names
--     instead there is a non-PPLC contact and this non-PPLC name matched the notifier names
-- (c) Notifier Type='W,'O', 'P' - Here we simply get the PPLC contact without looking for any names match
-- 2.  <15Jul-2019><Satish>: Have added UNMATCHED MAINCONTACT code as well
-- (a) This is for Notifier Type='E,'R where the notifier name does NOT match with either the PPLC contact names
--      (or) the non-PPLC contact names. We need to create these contact freshly in CRM/CM.
-- 3. <18-Jul-2019><Satish>: Added CM Link ID and CRM ID to the selection SQLs for ER PPLC matched,
--    ER non-PPLC matched and WOP
-- 4. <20-Jul-2019><Satish>: Modified the join name match criteria for ER PPLC Verified PC & ER non-PPLC
--    on EMICS and PC instead of EMICS and CM
-- 5. <22Jul2019><Satish>: Fixed a bug in temptable_MC_ER_not_PPLC_VerifiedPC_namematched. 
-- 6. <23-Jul-2019><Satish>: As per feedback from James, removed references to LKP tables that start with DM_XFM, instead ensured all LKP tables
--    are based of DM_STG_CM
-- 7. <27Jul2019><Satish>: 
--    (a) Modified logic for "PrimaryPhone" as it was not picking the TYPECODE instead was picking the ID.
--    (b) Modified the logic for all matched code blocks - "communicationpreferences_icare" by adding a new CM lookup table [dbo].[abtl_commmethod_icare] and using 
--        to get the TYPECODE and populating the target with that. Earlier the SP was using just the ID from ab_abcontact.
--    (c) Modified the logic for "Sourcesystem_icare" only for "rest of all Verified CDR" contacts section to set it to "GWCC" - as per spec.
-- 8. <06-Aug-2019><Satish>: 
--    (a) As per difference report for "CellPhoneCountry" identified a QA bug and have now applied the fix to "ER PPLC VerifiedPC namematched", 
--        "ER not PPLC VerifiedPC namematched" and "WOP PPLC VerifiedPC" sections.
--    (b) As per difference report for "FaxPhoneCountry" identified a QA bug and have now applied the fix to "ER PPLC VerifiedPC namematched", 
--        "ER not PPLC VerifiedPC namematched" and "WOP PPLC VerifiedPC" sections.
--    (c) As per difference report for "HomePhoneCountry" identified a QA bug and have now applied the fix to "ER PPLC VerifiedPC namematched", 
--        "ER not PPLC VerifiedPC namematched" and "WOP PPLC VerifiedPC" sections.
--    (d) As per difference report for "WorkPhoneCountry" identified a QA bug and have now applied the fix to "ER PPLC VerifiedPC namematched", 
--        "ER not PPLC VerifiedPC namematched" and "WOP PPLC VerifiedPC" sections.
-- 9. <09Aug2019><Satish>:
--    (a) Modified ER_PPLC_VerifiedPC_namematched, ER_not_PPLC_VerifiedPC_namematched and WOP_PPLC_VerifiedPC sections below for "SourceSystem_icare" by sourcing it 
--        from CRM instead of CM as per discussion with mapper.
-- 10.<20Aug2019><Satish>:
--    (a) Setting PrimaryLanguage to NULL in "ER PPLC", "WOP PPLC" and "ER not PPLC"  sections as part of QA build (DMIG-7905). 
-- 11.<21Aug2019><Satish>:
--    (a) Reverted changes done as part of 9.(a) above. SourceSystem_icare will be sourced from CM only.
-- 12.<29Aug2019><Satish>: As part of DMIG-7772 (DMIG-8073) -
--    (a) Modified ER_nmatched and Rest_of_all_Verified_CDR sections below to ensure that when FirstName and LastName are sourced from EMICS Person_Clm table, we 
--        apply LTRIM and RTRIM on them to ensure any leadding/trailing spaces are removed. Else for ER_unmatched specially we end up with the 2 sets of Main Contact
--        records when in reality it should be only one set.
-- 13.<30Aug2019><Satish>: As part of DMIG-8086 (DMIG-8090) -
--    (a) Within ER PPLC Matched, ER non-PPLC matched and WOP MC sections below, logic for CRMVersion has been changed to pick the highest CRM number where possible. 
--        If CM has higher value then CM CRMVersion should be used. If CRM has higher value, then CRM CRMVersion should be used.
-- 14.<10Sep2019><Satish>: Complete overhaul of this Stored Proc as part of DMIG-7698 (DMIG-8106) DMIG-6151 -
--    (a)  Modified the logic for ER_PPLC_matched, ER_not_PPLC_matched and WOP_PPLC sections below to now include matches to ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup 
--         based on 4 potential match scenarios -
--         (i)   Criteria 1: Getting Contact matches using Policy_term_detail.icare_policy_no against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.policynumber
--         (ii)  Criteria 2: Getting Contact matches using Claim_detail.Policy_No and matching against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.Legacypolicynumber_icare
--         (iii) Criteria 3: Getting Contact matches using Claim_detail.Policy_No and matching against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.policynumber
--         (iv)  Criteria 4: Getting Contact matches by checking to see if we length of CD.PolicyNo is 12 chars and it ends in 701 and if so stripping 701 from CD.PolicyNo and try 
--               and match it against ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup.PolicyNumber
--         This has resulted in moving away from original design of using Date of Injury/Cover domain based logic as part of selection criteria.
--    (b) For ER_unmatched section, had to ensure that we don't tag a Main Contact as ER_unmatched just based on no match being available in the NEW temp tables created as part of
--        point (a) above. Had to also ensure that we check to see if there is a Policy match available in the gw pc contact lookup i.e. temptable_count_gwpc_contact_lkp_1, 
--        temptable_count_gwpc_contact_lkp_2 or temptable_count_gwpc_contact_lkp_3. Only this then guarantees that the Main Contact is unmatched Verified PC Main Contact. 
--    (c) For ER_unmatched section, had to additionally also include a join to [REC_XFM].[dbo].[ADM_Claim_Cohort] and filter on Cohort=2 in order to ensure that the LinkID we set in this section 
--        is set to a max(Claim_Number) that is part of Cohort2. If we don't apply this join and filter, then LinkID sometimes gets set to a Claim_Number that was part of Cohort1 and this gives
--        rise to QA vs XFM differences post QA run.
--    (d) For rest_of_all_VerifiedCDR section, had to additiontally also include a joins to the various NEW temp tables created as part of point (a) above. This is to ensure we don't create a 
--        duplicate Main Contact record if it already exists in one of the NEW temp tables created as part of point (a) above.
-- 15.<16Sep2019><Satish>: Changed the Stored Proc as part of DMIG-8106 -
--    (a) Modified the logic for ER_PPLC_matched and ER_not_PPLC_matched sections (Criteria 1,2,3,4) below to now include matches ensure during name match, it applied REPLACE function on the names
--        coming from EMICS Person_Clm table to remove Char(9) (horizontal tab) - as otherwise we have few contacts where you expect a name match to happen but the QA code does not manage to do it 
--        due to the presence of Char(9)(Horzontal Tab) at the end of LastName coming from EMICS Person_Clm table. Example Claim:1836394.This has been fixed now.
--    (b) Modified the logic for ER_unmatched section to ensure the max(Claim_Number) is set based on getting a GROUP of contacts grouped by concatenated First + LastName plus PC PolicyNumber.
--        Earlier the GROUP BY was by individual First and Last Name and PC Policy PolicyNumber. This earlier logic did not get the max(Claim_Number) i.e LinkID correct for some claims such as 
--        Claim Number:1143924. This has been fixed now.
-- =============================================
CREATE PROCEDURE [dbo].[SP_REC_XFM_CONTACT_MAINCONTACT_STAGING]
	
AS
BEGIN

IF OBJECT_ID('tempdb..#temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table #temptable_MAINCONTACT_gwpc_contacts_lookup

IF OBJECT_ID('ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_1') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_1;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_2') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_2;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_3') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_3;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_4') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_4;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_1') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_1;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_2') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_2;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_3') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_3;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_4') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_4;

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp1') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp1

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp2') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp2

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp3') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp3

IF OBJECT_ID('temptable_MC_rest_of_all_VerifiedCDR_temp') is not null
drop table temptable_MC_rest_of_all_VerifiedCDR_temp

IF OBJECT_ID('temptable_ER_PPLC_N_1') is not null
drop table temptable_ER_PPLC_N_1

IF OBJECT_ID('temptable_ER_PPLC_N_2') is not null
drop table temptable_ER_PPLC_N_2

IF OBJECT_ID('temptable_ER_PPLC_N_3') is not null
drop table temptable_ER_PPLC_N_3

IF OBJECT_ID('temptable_ER_PPLC_N_4') is not null
drop table temptable_ER_PPLC_N_4

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_1') is not null
drop table temptable_count_gwpc_contact_lkp_1

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_2') is not null
drop table temptable_count_gwpc_contact_lkp_2

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_3') is not null
drop table temptable_count_gwpc_contact_lkp_3

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_1') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_1

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_2') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_2

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_3') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_3

IF OBJECT_ID('ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched') is not null
drop table ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched

IF OBJECT_ID('ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched') is not null
drop table ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched

IF OBJECT_ID('ContactREF_temptable_MC_ER_VerifiedPC_unmatched') is not null
drop table ContactREF_temptable_MC_ER_VerifiedPC_unmatched

IF OBJECT_ID('ContactREF_temptable_MC_WOP_PPLC_VerifiedPC') is not null
drop table ContactREF_temptable_MC_WOP_PPLC_VerifiedPC

IF OBJECT_ID('ContactREF_temptable_MC_rest_of_all_VerifiedCDR') is not null
drop table ContactREF_temptable_MC_rest_of_all_VerifiedCDR


--Creating a temp table with all PC contacts (PPLC and non PPLC)
SELECT * INTO ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
FROM
(
SELECT DISTINCT
	 NULL [ID]
	,NULL [PeriodStart]
	,NULL [PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],''))) [PolicyNumber]
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],''))) [LegacyPolicyNumber_icare]
	,a.[CRMUniqueID_icare] [EmployerLinkID]
	,ctl.[TYPECODE] [Subtype]
	,NULL [FirstName]
	,NULL [LastName]
	,NULL [ContactLinkID]
	,'SC5 - Insured Employer' [SC_Type]
	,NULL [IsPPLC]
FROM [DM_STG_PC].[dbo].[pc_policyperiod] pp 
INNER JOIN [DM_STG_PC].[dbo].[pc_policy] p
ON pp.[PolicyID] = p.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_account] a
ON p.[AccountID] = a.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac
ON a.[ID] = ac.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c
ON ac.[Contact] = c.[ID]
AND a.[CRMUniqueID_icare] = c.[AddressBookUID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr
ON ac.[ID] = acr.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl
ON acr.[Subtype] = acrtl.[ID]
AND acrtl.[TYPECODE] = 'NamedInsured'
INNER JOIN [DM_STG_PC].[dbo].[pctl_contact] ctl
ON c.[Subtype] = ctl.[ID]
WHERE pp.[MostRecentModel] = 1 -- the most recent version
AND pp.[Status] = 9 -- Bound
AND CAST(pp.[PeriodStart] AS DATE) <> CAST(ISNULL(pp.[CancellationDate],'') AS DATE)
AND pp.[Retired] = 0
AND p.[Retired] = 0
AND a.[Retired] = 0
AND ac.[Retired] = 0
AND c.[Retired] = 0
AND acr.[Retired] = 0

UNION ALL

SELECT
	 pp.[ID]
	,pp.[PeriodStart]
	,pp.[PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],''))) [PolicyNumber]
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],''))) [LegacyPolicyNumber_icare]
	,a.[CRMUniqueID_icare] [EmployerLinkID]
	,ctl.[TYPECODE] [Subtype]
	,LTRIM(RTRIM(ISNULL(c1.[FirstName],''))) [FirstName]
	,LTRIM(RTRIM(ISNULL(c1.[LastName],''))) [LastName]
	,c1.[AddressBookUID] [ContactLinkID]
	,'SC15 - Insured Employer Main Contact' [SC_Type]
	,CASE WHEN acrtl1.[TYPECODE] = 'LocationContact_icare' THEN 'Y' ELSE 'N' END [IsPPLC]
FROM [DM_STG_PC].[dbo].[pc_policyperiod] pp 
INNER JOIN [DM_STG_PC].[dbo].[pc_policy] p
ON pp.[PolicyID] = p.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_account] a
ON p.[AccountID] = a.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac
ON a.[ID] = ac.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c
ON ac.[Contact] = c.[ID]
AND a.[CRMUniqueID_icare] = c.[AddressBookUID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr
ON ac.[ID] = acr.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl
ON acr.[Subtype] = acrtl.[ID]
AND acrtl.[TYPECODE] = 'NamedInsured'
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontact] ac1
ON a.[ID] = ac1.[Account]
INNER JOIN [DM_STG_PC].[dbo].[pc_contact] c1
ON ac1.[Contact] = c1.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pc_accountcontactrole] acr1
ON ac1.[ID] = acr1.[AccountContact]
INNER JOIN [DM_STG_PC].[dbo].[pctl_accountcontactrole] acrtl1
ON acr1.[Subtype] = acrtl1.[ID]
INNER JOIN [DM_STG_PC].[dbo].[pctl_contact] ctl
ON c1.[Subtype] = ctl.[ID]
AND ctl.[TYPECODE] = 'Person'
WHERE pp.[MostRecentModel] = 1 -- the most recent version
AND pp.[Status] = 9 -- Bound
--AND pp.[CancellationDate] IS NULL
AND CAST(pp.[PeriodStart] AS DATE) <> CAST(ISNULL(pp.[CancellationDate],'') AS DATE)
AND pp.[Retired] = 0
AND p.[Retired] = 0
AND a.[Retired] = 0
AND ac.[Retired] = 0
AND c.[Retired] = 0
AND acr.[Retired] = 0
AND ac1.[Retired] = 0
AND c1.[Retired] = 0
AND acr1.[Retired] = 0
GROUP BY
	 pp.[ID]
	,pp.[PeriodStart]
	,pp.[PeriodEnd]
	,LTRIM(RTRIM(ISNULL(pp.[PolicyNumber],'')))
	,LTRIM(RTRIM(ISNULL(pp.[LegacyPolicyNumber_icare],'')))
	,a.[CRMUniqueID_icare]
	,ctl.[TYPECODE]
	,LTRIM(RTRIM(ISNULL(c1.[FirstName],'')))
	,LTRIM(RTRIM(ISNULL(c1.[LastName],'')))
	,c1.[AddressBookUID]
	,CASE WHEN acrtl1.[TYPECODE] = 'LocationContact_icare' THEN 'Y' ELSE 'N' END
)X


--This temp table is for creating a mini PC contacts lookup table for ER PPLC NOT matched Criteria 1 below
SELECT * INTO temptable_ER_PPLC_N_1
FROM
(
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(CD.Claim_Number)),PC_Con_lkp.policynumber order by PC_Con_lkp.ContactLinkID asc) rownum, 
   ltrim(rtrim(CD.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from 
   (
   select distinct CD.Claim_Number,[Notifier_Id], CD.Policy_No, rlu.Code from [DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
      INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
   ON cd.[Notified_By] = rlu.[Code]
   ) CD
   inner join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT 
   on PT.POLICY_NO = CD.Policy_No --EMICS policy number
   INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
   ON cd.[Notifier_Id] = pc.[ID]
   inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp on PC_Con_lkp.PolicyNumber=PT.icare_policy_no 
   --and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName)
   and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
   --16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
   where SC_Type='SC15 - Insured Employer Main Contact'
   and IsPPLC='N'
   and CD.Code in ('E','R')
)X


--This temp table is for creating a mini PC contacts lookup table for ER PPLC NOT matched Criteria 2 below
SELECT * INTO temptable_ER_PPLC_N_2
FROM
(
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(CD.Claim_Number)),PC_Con_lkp.policynumber order by PC_Con_lkp.ContactLinkID asc) rownum, 
   ltrim(rtrim(CD.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from 
   (
   select distinct CD.Claim_Number,[Notifier_Id], CD.Policy_No, rlu.Code from [DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
      INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
   ON cd.[Notified_By] = rlu.[Code]
   ) CD
   INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
   ON cd.[Notifier_Id] = pc.[ID]
   inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp on PC_Con_lkp.LegacyPolicyNumber_icare=CD.Policy_No
   --and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName)
   and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
   --16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
   where SC_Type='SC15 - Insured Employer Main Contact'
   and IsPPLC='N'
   and CD.Code in ('E','R')
)X

--This temp table is for creating a mini PC contacts lookup table for ER PPLC NOT matched Criteria 3 below
SELECT * INTO temptable_ER_PPLC_N_3
FROM
(
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(CD.Claim_Number)),PC_Con_lkp.policynumber order by PC_Con_lkp.ContactLinkID asc) rownum, 
   ltrim(rtrim(CD.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from 
   (
   select distinct CD.Claim_Number,[Notifier_Id], CD.Policy_No, rlu.Code from [DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
      INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
   ON cd.[Notified_By] = rlu.[Code]
   ) CD
   INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
   ON cd.[Notifier_Id] = pc.[ID]
   inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp on PC_Con_lkp.PolicyNumber=CD.Policy_No
   --and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName)
   and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
   --16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
   where SC_Type='SC15 - Insured Employer Main Contact'
   and IsPPLC='N'
   and CD.Code in ('E','R')
)X


--This temp table is for creating a mini PC contacts lookup table for ER PPLC NOT matched Criteria 4 below
SELECT * INTO temptable_ER_PPLC_N_4
FROM
(
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(CD.Claim_Number)),PC_Con_lkp.policynumber order by PC_Con_lkp.ContactLinkID asc) rownum, 
   ltrim(rtrim(CD.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from 
   (
   select distinct CD.Claim_Number,[Notifier_Id], CD.Policy_No, rlu.Code from [DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
      INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
   ON cd.[Notified_By] = rlu.[Code]
   ) CD
   INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
   ON cd.[Notifier_Id] = pc.[ID]
   inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp on PC_Con_lkp.PolicyNumber=substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3)
   --and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName)
   and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
   --16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
   where SC_Type='SC15 - Insured Employer Main Contact'
   and IsPPLC='N'
   and CD.Code in ('E','R')
   and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701')
)X

--Creating a temp table to get distinct PC PolicyNumber for lookup in ER unmatched section below
select * into temptable_count_gwpc_contact_lkp_1
from
(
select distinct Policynumber
from ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
where SC_Type='SC15 - Insured Employer Main Contact'
)X

--Creating a temp table to get distinct LegacyPolicyNumber and PolicyNumber for lookup in ER unmatched section below
select * into temptable_count_gwpc_contact_lkp_2
from
(
select distinct LegacyPolicyNumber_icare,policynumber
from ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup
where SC_Type='SC15 - Insured Employer Main Contact'
)X

--Creating a temp table to get distinct ClaimNumber and PolicyNumber only for claims where PolicyNo ends in '%701' for lookup in ER unmatched section below
select * into temptable_count_gwpc_contact_lkp_3
from
(
select distinct ltrim(rtrim(CD.Claim_Number)) as Claim_Number, PC_Con_lkp.policynumber
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp
on PC_Con_lkp.PolicyNumber=substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3) 
where SC_Type='SC15 - Insured Employer Main Contact'
and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701')
)X

--Creating a temp table to get distinct PolicyNumber and ContactLinkID for lookup in WOP PPLC (Criteria 1 and 3) section below
select * into temptable_WOP_count_gwpc_contact_lkp_1
from
(
select distinct PolicyNumber, ContactLinkID
from ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp
where IsPPLC='Y'
and SC_Type='SC15 - Insured Employer Main Contact'
and ContactLinkID is not null
)X

--Creating a temp table to get distinct LegacyPolicyNumber_icare and ContactLinkID for lookup in WOP PPLC (Criteria 2) section below
select * into temptable_WOP_count_gwpc_contact_lkp_2
from
(
select distinct LegacyPolicyNumber_icare, ContactLinkID
from ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp
where IsPPLC='Y'
and SC_Type='SC15 - Insured Employer Main Contact'
and ltrim(rtrim(LegacyPolicyNumber_icare)) <> ''
and ContactLinkID is not null
)X

--Creating a temp table to get distinct PolicyNumber and ContactLinkID only for claims where PolicyNo ends in '%701' for lookup in WOP_PPLC (Criteria 4) section below
select * into temptable_WOP_count_gwpc_contact_lkp_3
from
(
select distinct ltrim(rtrim(CD.Claim_Number)) as Claim_Number,policynumber, ContactLinkID
from DM_STG_EMICS.dbo.CLAIM_DETAIL CD
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp
on PC_Con_lkp.PolicyNumber=substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3) 
where IsPPLC='Y'
and SC_Type='SC15 - Insured Employer Main Contact'
and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701')
and ContactLinkID is not null
)X


--(Criteria 1) Creating a temp table with Notifier Type='E,'R' PPLC matched MC contacts
--Using PT.icare_policy_no to match with gwpc contact lookup PolicyNumber
SELECT * INTO temptable_MC_ER_PPLC_VerifiedPC_namematched_1
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--CM_C.CellPhoneExtension,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone,  --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--,UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))
--,ltrim(rtrim(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))))
--,replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')
--,UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName)
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number))  and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--06Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp 
on PC_Con_lkp.PolicyNumber=PT.icare_policy_no 
and PC_Con_lkp.SC_Type='SC15 - Insured Employer Main Contact'
and PC_Con_lkp.isPPLC='Y'
--and ltrim(rtrim(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) --20Jul2019: Changing the compare to PC rather than CM as per spec
and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
--16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
and PC_Con_lkp.ContactLinkID is not null --11/09/2019 There do seem to some NULL contactlinkid records and there is no point picking them as matched PC contact
--06Sep2019: Added the above 5 lines as part of DMIG-7698 (DMIG-8106) DMIG-6151
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
--and ltrim(rtrim(CD.Claim_Number)) in (1598358,1836394) -- (1836394)
--and CM_C.LinkId is not null and crm_c.ID is not  null
)X


--(Criteria 2) Creating a temp table with Notifier Type='E,'R' PPLC matched MC contacts
--Using CD.Policy_No to match with gwpc contact lookup LegacyPolicyNumber_icare
SELECT * INTO temptable_MC_ER_PPLC_VerifiedPC_namematched_2
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--CM_C.CellPhoneExtension,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone,  --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--06Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp 
on PC_Con_lkp.LegacyPolicyNumber_icare=ltrim(rtrim(CD.Policy_No)) 
and PC_Con_lkp.SC_Type='SC15 - Insured Employer Main Contact'
and PC_Con_lkp.isPPLC='Y'
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) --20Jul2019: Changing the compare to PC rather than CM as per spec
and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
--16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
and PC_Con_lkp.ContactLinkID is not null --11/09/2019 There do seem to some NULL contactlinkid records and there is no point picking them as matched PC contact
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 ER_PPLC_matched_1 on ER_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--06Sep2019: Added the above 5 lines as part of DMIG-7698 (DMIG-8106) DMIG-6151
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and ER_PPLC_matched_1.LUWID is null --06Sep2019 Ensuring there is no match existing in the previous search result temp table
--and CM_C.LinkId is not null and crm_c.ID is not  null
)X

--(Criteria 3) Creating a temp table with Notifier Type='E,'R' PPLC matched MC contacts
--Using CD.Policy_No to match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_ER_PPLC_VerifiedPC_namematched_3
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--CM_C.CellPhoneExtension,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone,  --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--06Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp 
on PC_Con_lkp.PolicyNumber=ltrim(rtrim(CD.Policy_No)) 
and PC_Con_lkp.SC_Type='SC15 - Insured Employer Main Contact'
and PC_Con_lkp.isPPLC='Y'
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) --20Jul2019: Changing the compare to PC rather than CM as per spec
and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
--16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
and PC_Con_lkp.ContactLinkID is not null --11/09/2019 There do seem to some NULL contactlinkid records and there is no point picking them as matched PC contact
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 ER_PPLC_matched_1 on ER_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 ER_PPLC_matched_2 on ER_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--06Sep2019: Added the above 5 lines as part of DMIG-7698 (DMIG-8106) DMIG-6151
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and ER_PPLC_matched_1.LUWID is null --06Sep2019 Ensuring there is no match existing in ER_PPLC_matched_1
and ER_PPLC_matched_2.LUWID is null --06Sep2019 Ensuring there is no match existing in ER_PPLC_matched_2
--and CM_C.LinkId is not null and crm_c.ID is not  null
)X

--drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_4
--(Criteria 4) Creating a temp table with Notifier Type='E,'R' PPLC matched MC contacts
--Using CD.Policy_No where PolicyNo ends in '%701'and stripping this away and trying to then match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_ER_PPLC_VerifiedPC_namematched_4
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--CM_C.CellPhoneExtension,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone,  --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--06Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
inner join ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup PC_Con_lkp 
on PC_Con_lkp.PolicyNumber=substring(ltrim(rtrim(CD.Policy_No)),1,len(ltrim(rtrim(CD.Policy_No)))-3) 
and PC_Con_lkp.SC_Type='SC15 - Insured Employer Main Contact'
and PC_Con_lkp.isPPLC='Y'
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) --20Jul2019: Changing the compare to PC rather than CM as per spec
and replace(UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME))),char(9),'')=UPPER(PC_Con_lkp.FirstName) + ' ' + UPPER(PC_Con_lkp.LastName) 
--16Sep2019: Changing the compare to use REPLACE function to eliminate Char(9) (Horizontal Tab) from left hand side of the compare statement.
and PC_Con_lkp.ContactLinkID is not null --11/09/2019 There do seem to some NULL contactlinkid records and there is no point picking them as matched PC contact
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 ER_PPLC_matched_1 on ER_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 ER_PPLC_matched_2 on ER_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 ER_PPLC_matched_3 on ER_PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--06Sep2019: Added the above 5 lines as part of DMIG-7698 (DMIG-8106) DMIG-6151
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and ER_PPLC_matched_1.LUWID is null --06Sep2019 Ensuring there is no match existing in ER_PPLC_matched_1
and ER_PPLC_matched_2.LUWID is null --06Sep2019 Ensuring there is no match existing in ER_PPLC_matched_2
and ER_PPLC_matched_3.LUWID is null --06Sep2019 Ensuring there is no match existing in ER_PPLC_matched_3
and (len(ltrim(rtrim(CD.Policy_No)))=12 and ltrim(rtrim(CD.Policy_No)) like '%701') --06Sep2019 Ensuring we only pick those claims for whom the PolicyNo ends in '%701'
--and CM_C.LinkId is not null and crm_c.ID is not  null
)X

--drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1
--(Criteria 1) Creating a temp table with Notifier Type='E,'R' non-PPLC matched MC contacts
--Using PT.icare_policy_no to match with gwpc contact lookup PolicyNumber
SELECT * INTO temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1
FROM
(
select Y.* from
(
select 
row_number () over (partition by X.LUWID order by X.rownum asc) rownum,
X.PublicID,
X.Person_Clm_FIRSTNAME,
Person_Clm_LASTNAME,
X.Code,
X.LinkID,
X.PPLC_FIRSTNAME,
X.PPLC_LASTNAME,
X.isPPLC,
-- AB contact details to populate in ccst_contact
X.ABNIsActive_icare,
X.ABNValidated_icare,
X.ABN_icare,
X.ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
X.ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
X.AddressBookUID,
X.AdjudicativeDomain,
X.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
X.AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
X.AttorneyLicense,
X.AttorneySpecialty,
X.AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
X.AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
X.AutoRepairLicense,
--'allow' as AutoSync,
X.AutoSync,
X.AutoTowingLicense,
X.BlockedVendor_icare,
X.CellPhone,
X.CellPhoneCountry,
X.CellPhoneExtension,
X.ClaimTaxFreeThreshold_icare,
X.ClaimantIDType,
X.CommunicationPreferences_icare,
X.DateOfBirth,
--'FALSE' as DoNotDestroy,
X.DoNotDestroy as DoNotDestroy,
X.DoctorSpecialty,
X.DoctorSpecialty_icare,
X.EducationLevel,
X.EligibleOffset_icare,
X.EmailAddress1,
X.EmailAddress2,
X.EmployeeNumber,
X.EmployeeSecurityID,
X.FaxPhone,
X.FaxPhoneCountry,
X.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
X.FinancialSupplementDebt_icare,
X.FirstName, --Individuals will have this populated and MC is individual only
X.FirstNameKanji,
X.FormerName,
X.Gender,
X.GreenCardNumber,
X.HelpSslorTslDebt_icare,
X.HomePhone,
X.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
X.HomePhoneExtension,
X.InterpreterRequired_icare,
X.JurisdictionAssignedID,
X.LastName,
X.LastNameKanji,
X.LawFirmSpecialty,
X.LegalSpecialty_icare,
X.LicenseNumber,
X.LicenseState,
X.MaritalStatus,
X.MedicalLicense,
X.MedicalOrgSpecialty,
X.MedicalSpecialty_icare,
X.MedicareIRN_icare,
X.MedicareLevyVariation_icare,
X.MedicareNumber_icare,
X.MedicareValidUntil_icare,
X.MiddleName,
X.Name,  --Companies will have this populated but individuals will have this blank
X.NameKanji,
X.Notes,
X.NumDependents,
X.NumDependentsU18,
X.NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
X.ObfuscatedInternal,
X.Occupation,
X.OccupationDetails_icareID,
X.OffsetAmount_icare,
X.OktaID_icare,
X.OrganizationType,
X.Particle,
X.PassportNumber,
X.PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
X.PendingLinkMessage,
X.PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
X.Preferred,
X.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
X.PreferredPaymentMethod_icare,
X.Prefix,
X.PrimaryAddressID,
X.PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
X.PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
X.PrimaryPhone,
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
X.RegisteredForGST_icare,
--0 as Retired,
X.Retired,
X.SSNReleaseAuthorized,
X.Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
X.SingleRemittancePerDay_icare,
X.SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
X.SourceSystem_icare,
X.SpecialtyType,
X.Subtype,
X.Suffix,
X.TFNDeclarationDate_icare,
X.TaxExemptionsEntitled,
X.TaxFilingStatus,
X.TaxID,
X.TaxStatus,
X.TempCRMUniqueID_icare,
X.TradingName_icare,
X.TrustName_icare,
X.TrusteeName_icare,
X.ValidationLevel,
X.VendorNumber,
X.VendorType,
X.VenueType,
X.W9Received,
X.W9ReceivedDate,
X.W9ValidFrom,
X.W9ValidTo,
X.WithholdingRate,
X.WorkPhone,
X.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
X.WorkPhoneExtension,
X.ACN_icare,
X.CommencementDate_icare,
X.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
X.OtherLanguage_icare,
X.TrustABN_icare,
X.TrustABNIsActive_icare,
X.TrustABNValidated_icare,
X.TrusteeType_icare,
X.TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
X.CRMVersion_icare
,X.LUWID
,X.ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,X.CM_LinkID
,X.CRM_ID
from
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
PC_Con_lkp.rownum,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
--pc.FIRSTNAME as Person_Clm_FIRSTNAME,
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as Person_Clm_FIRSTNAME,
--pc.LASTNAME Person_Clm_LASTNAME,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as Person_Clm_LASTNAME,
rlu.[Code],
CM_C.LinkID,
CM_C.FirstName as PPLC_FIRSTNAME,
CM_C.LastName as PPLC_LASTNAME,
PC_Con_lkp.isPPLC,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
  left outer join
  (
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(PC_Con_lkp.Claim_Number)) order by PC_Con_lkp.rownum asc) rownum, 
   ltrim(rtrim(PC_Con_lkp.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from temptable_ER_PPLC_N_1 PC_Con_lkp
  ) PC_Con_lkp 
  on PC_Con_lkp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
  --10/09/2019 Added this lookup just for this section - this lookup was created somewhere at the top
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--10/09/2019 Added the above joins to check if there is any MC record already present in them. If yes, then we should not be creating any duplicate record via this section.
-- Only if there is no match in these 4 matched temp tables, then we go ahead and create a new MC via this section.
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
-- 22jul2019 - commented out the above line as it is a bug - instead have kept the check at the bottom "where Y.rownum=1"
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --20/06/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_1
and PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_2
and PPLC_matched_3.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_3
and PPLC_matched_4.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_4
and PC_Con_lkp.Claim_Number is not NULL --10/09/2019 FINALLY Ensuring that we DO have a match in non_PPLC_gwpc_lookup
)X
)Y
where Y.rownum=1 --22Jul2019-picking only 1 record when multiple name matches as PC_Con_lkp is already sorted on min(ContactLinkID)
)Z

--drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2
--(Criteria 2) Creating a temp table with Notifier Type='E,'R' non-PPLC matched MC contacts
--Using CD.policy_no to match with gwpc contact lookup LegacyPolicyNumber_icare
SELECT * INTO temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2
FROM
(
select Y.* from
(
select 
row_number () over (partition by X.LUWID order by X.rownum asc) rownum,
X.PublicID,
X.Person_Clm_FIRSTNAME,
Person_Clm_LASTNAME,
X.Code,
X.LinkID,
X.PPLC_FIRSTNAME,
X.PPLC_LASTNAME,
X.isPPLC,
-- AB contact details to populate in ccst_contact
X.ABNIsActive_icare,
X.ABNValidated_icare,
X.ABN_icare,
X.ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
X.ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
X.AddressBookUID,
X.AdjudicativeDomain,
X.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
X.AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
X.AttorneyLicense,
X.AttorneySpecialty,
X.AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
X.AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
X.AutoRepairLicense,
--'allow' as AutoSync,
X.AutoSync,
X.AutoTowingLicense,
X.BlockedVendor_icare,
X.CellPhone,
X.CellPhoneCountry,
X.CellPhoneExtension,
X.ClaimTaxFreeThreshold_icare,
X.ClaimantIDType,
X.CommunicationPreferences_icare,
X.DateOfBirth,
--'FALSE' as DoNotDestroy,
X.DoNotDestroy as DoNotDestroy,
X.DoctorSpecialty,
X.DoctorSpecialty_icare,
X.EducationLevel,
X.EligibleOffset_icare,
X.EmailAddress1,
X.EmailAddress2,
X.EmployeeNumber,
X.EmployeeSecurityID,
X.FaxPhone,
X.FaxPhoneCountry,
X.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
X.FinancialSupplementDebt_icare,
X.FirstName, --Individuals will have this populated and MC is individual only
X.FirstNameKanji,
X.FormerName,
X.Gender,
X.GreenCardNumber,
X.HelpSslorTslDebt_icare,
X.HomePhone,
X.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
X.HomePhoneExtension,
X.InterpreterRequired_icare,
X.JurisdictionAssignedID,
X.LastName,
X.LastNameKanji,
X.LawFirmSpecialty,
X.LegalSpecialty_icare,
X.LicenseNumber,
X.LicenseState,
X.MaritalStatus,
X.MedicalLicense,
X.MedicalOrgSpecialty,
X.MedicalSpecialty_icare,
X.MedicareIRN_icare,
X.MedicareLevyVariation_icare,
X.MedicareNumber_icare,
X.MedicareValidUntil_icare,
X.MiddleName,
X.Name,  --Companies will have this populated but individuals will have this blank
X.NameKanji,
X.Notes,
X.NumDependents,
X.NumDependentsU18,
X.NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
X.ObfuscatedInternal,
X.Occupation,
X.OccupationDetails_icareID,
X.OffsetAmount_icare,
X.OktaID_icare,
X.OrganizationType,
X.Particle,
X.PassportNumber,
X.PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
X.PendingLinkMessage,
X.PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
X.Preferred,
X.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
X.PreferredPaymentMethod_icare,
X.Prefix,
X.PrimaryAddressID,
X.PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
X.PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
X.PrimaryPhone,
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
X.RegisteredForGST_icare,
--0 as Retired,
X.Retired,
X.SSNReleaseAuthorized,
X.Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
X.SingleRemittancePerDay_icare,
X.SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
X.SourceSystem_icare,
X.SpecialtyType,
X.Subtype,
X.Suffix,
X.TFNDeclarationDate_icare,
X.TaxExemptionsEntitled,
X.TaxFilingStatus,
X.TaxID,
X.TaxStatus,
X.TempCRMUniqueID_icare,
X.TradingName_icare,
X.TrustName_icare,
X.TrusteeName_icare,
X.ValidationLevel,
X.VendorNumber,
X.VendorType,
X.VenueType,
X.W9Received,
X.W9ReceivedDate,
X.W9ValidFrom,
X.W9ValidTo,
X.WithholdingRate,
X.WorkPhone,
X.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
X.WorkPhoneExtension,
X.ACN_icare,
X.CommencementDate_icare,
X.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
X.OtherLanguage_icare,
X.TrustABN_icare,
X.TrustABNIsActive_icare,
X.TrustABNValidated_icare,
X.TrusteeType_icare,
X.TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
X.CRMVersion_icare
,X.LUWID
,X.ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,X.CM_LinkID
,X.CRM_ID
from
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
PC_Con_lkp.rownum,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
--pc.FIRSTNAME as Person_Clm_FIRSTNAME,
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as Person_Clm_FIRSTNAME,
--pc.LASTNAME Person_Clm_LASTNAME,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as Person_Clm_LASTNAME,
rlu.[Code],
CM_C.LinkID,
CM_C.FirstName as PPLC_FIRSTNAME,
CM_C.LastName as PPLC_LASTNAME,
PC_Con_lkp.isPPLC,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
  left outer join
  (
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(PC_Con_lkp.Claim_Number)) order by PC_Con_lkp.rownum asc) rownum, 
   ltrim(rtrim(PC_Con_lkp.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from temptable_ER_PPLC_N_2 PC_Con_lkp
  ) PC_Con_lkp 
  on PC_Con_lkp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
  --10/09/2019 Added this lookup just for this section - this lookup was created somewhere at the top
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1 non_PPLC_matched_1
on non_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--10/09/2019 Added the above joins to check if there is any MC record already present in them. If yes, then we should not be creating any duplicate record via this section.
-- Only if there is no match in these 5 matched temp tables, then we go ahead and create a new MC via this section.
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
-- 22jul2019 - commented out the above line as it is a bug - instead have kept the check at the bottom "where Y.rownum=1"
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --20/06/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_1
and PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_2
and PPLC_matched_3.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_3
and PPLC_matched_4.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_4
and non_PPLC_matched_1.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_1
and PC_Con_lkp.Claim_Number is not NULL --10/09/2019 FINALLY Ensuring that we DO have a match in non_PPLC_gwpc_lookup
)X
)Y
where Y.rownum=1 --22Jul2019-picking only 1 record when multiple name matches as PC_Con_lkp is already sorted on min(ContactLinkID)
)Z


--(Criteria 3) Creating a temp table with Notifier Type='E,'R' non-PPLC matched MC contacts
--Using CD.Policy_No to match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3
FROM
(
select Y.* from
(
select 
row_number () over (partition by X.LUWID order by X.rownum asc) rownum,
X.PublicID,
X.Person_Clm_FIRSTNAME,
Person_Clm_LASTNAME,
X.Code,
X.LinkID,
X.PPLC_FIRSTNAME,
X.PPLC_LASTNAME,
X.isPPLC,
-- AB contact details to populate in ccst_contact
X.ABNIsActive_icare,
X.ABNValidated_icare,
X.ABN_icare,
X.ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
X.ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
X.AddressBookUID,
X.AdjudicativeDomain,
X.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
X.AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
X.AttorneyLicense,
X.AttorneySpecialty,
X.AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
X.AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
X.AutoRepairLicense,
--'allow' as AutoSync,
X.AutoSync,
X.AutoTowingLicense,
X.BlockedVendor_icare,
X.CellPhone,
X.CellPhoneCountry,
X.CellPhoneExtension,
X.ClaimTaxFreeThreshold_icare,
X.ClaimantIDType,
X.CommunicationPreferences_icare,
X.DateOfBirth,
--'FALSE' as DoNotDestroy,
X.DoNotDestroy as DoNotDestroy,
X.DoctorSpecialty,
X.DoctorSpecialty_icare,
X.EducationLevel,
X.EligibleOffset_icare,
X.EmailAddress1,
X.EmailAddress2,
X.EmployeeNumber,
X.EmployeeSecurityID,
X.FaxPhone,
X.FaxPhoneCountry,
X.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
X.FinancialSupplementDebt_icare,
X.FirstName, --Individuals will have this populated and MC is individual only
X.FirstNameKanji,
X.FormerName,
X.Gender,
X.GreenCardNumber,
X.HelpSslorTslDebt_icare,
X.HomePhone,
X.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
X.HomePhoneExtension,
X.InterpreterRequired_icare,
X.JurisdictionAssignedID,
X.LastName,
X.LastNameKanji,
X.LawFirmSpecialty,
X.LegalSpecialty_icare,
X.LicenseNumber,
X.LicenseState,
X.MaritalStatus,
X.MedicalLicense,
X.MedicalOrgSpecialty,
X.MedicalSpecialty_icare,
X.MedicareIRN_icare,
X.MedicareLevyVariation_icare,
X.MedicareNumber_icare,
X.MedicareValidUntil_icare,
X.MiddleName,
X.Name,  --Companies will have this populated but individuals will have this blank
X.NameKanji,
X.Notes,
X.NumDependents,
X.NumDependentsU18,
X.NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
X.ObfuscatedInternal,
X.Occupation,
X.OccupationDetails_icareID,
X.OffsetAmount_icare,
X.OktaID_icare,
X.OrganizationType,
X.Particle,
X.PassportNumber,
X.PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
X.PendingLinkMessage,
X.PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
X.Preferred,
X.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
X.PreferredPaymentMethod_icare,
X.Prefix,
X.PrimaryAddressID,
X.PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
X.PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
X.PrimaryPhone,
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
X.RegisteredForGST_icare,
--0 as Retired,
X.Retired,
X.SSNReleaseAuthorized,
X.Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
X.SingleRemittancePerDay_icare,
X.SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
X.SourceSystem_icare,
X.SpecialtyType,
X.Subtype,
X.Suffix,
X.TFNDeclarationDate_icare,
X.TaxExemptionsEntitled,
X.TaxFilingStatus,
X.TaxID,
X.TaxStatus,
X.TempCRMUniqueID_icare,
X.TradingName_icare,
X.TrustName_icare,
X.TrusteeName_icare,
X.ValidationLevel,
X.VendorNumber,
X.VendorType,
X.VenueType,
X.W9Received,
X.W9ReceivedDate,
X.W9ValidFrom,
X.W9ValidTo,
X.WithholdingRate,
X.WorkPhone,
X.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
X.WorkPhoneExtension,
X.ACN_icare,
X.CommencementDate_icare,
X.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
X.OtherLanguage_icare,
X.TrustABN_icare,
X.TrustABNIsActive_icare,
X.TrustABNValidated_icare,
X.TrusteeType_icare,
X.TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
X.CRMVersion_icare
,X.LUWID
,X.ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,X.CM_LinkID
,X.CRM_ID
from
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
PC_Con_lkp.rownum,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
--pc.FIRSTNAME as Person_Clm_FIRSTNAME,
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as Person_Clm_FIRSTNAME,
--pc.LASTNAME Person_Clm_LASTNAME,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as Person_Clm_LASTNAME,
rlu.[Code],
CM_C.LinkID,
CM_C.FirstName as PPLC_FIRSTNAME,
CM_C.LastName as PPLC_LASTNAME,
PC_Con_lkp.isPPLC,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
  left outer join
  (
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(PC_Con_lkp.Claim_Number)) order by PC_Con_lkp.rownum asc) rownum, 
   ltrim(rtrim(PC_Con_lkp.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from temptable_ER_PPLC_N_3 PC_Con_lkp
  ) PC_Con_lkp 
  on PC_Con_lkp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
  --10/09/2019 Added this lookup just for this section - this lookup was created somewhere at the top
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1 non_PPLC_matched_1
on non_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2 non_PPLC_matched_2
on non_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--10/09/2019 Added the above joins to check if there is any MC record already present in them. If yes, then we should not be creating any duplicate record via this section.
-- Only if there is no match in these 6 matched temp tables, then we go ahead and create a new MC via this section.
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
-- 22jul2019 - commented out the above line as it is a bug - instead have kept the check at the bottom "where Y.rownum=1"
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --20/06/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_1
and PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_2
and PPLC_matched_3.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_3
and PPLC_matched_4.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_4
and non_PPLC_matched_1.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_1
and non_PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_2
and PC_Con_lkp.Claim_Number is not NULL --10/09/2019 FINALLY Ensuring that we DO have a match in non_PPLC_gwpc_lookup
)X
)Y
where Y.rownum=1 --22Jul2019-picking only 1 record when multiple name matches as PC_Con_lkp is already sorted on min(ContactLinkID)
)Z

--drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4
--(Criteria 4) Creating a temp table with Notifier Type='E,'R' non-PPLC matched MC contacts
--Using CD.Policy_No where PolicyNo ends in '%701'and stripping this away and trying to then match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4
FROM
(
select Y.* from
(
select 
row_number () over (partition by X.LUWID order by X.rownum asc) rownum,
X.PublicID,
X.Person_Clm_FIRSTNAME,
Person_Clm_LASTNAME,
X.Code,
X.LinkID,
X.PPLC_FIRSTNAME,
X.PPLC_LASTNAME,
X.isPPLC,
-- AB contact details to populate in ccst_contact
X.ABNIsActive_icare,
X.ABNValidated_icare,
X.ABN_icare,
X.ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
X.ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
X.AddressBookUID,
X.AdjudicativeDomain,
X.AdjudicatorLicense,
--0 as AggregateRemittance_icare,
X.AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
X.AttorneyLicense,
X.AttorneySpecialty,
X.AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
X.AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
X.AutoRepairLicense,
--'allow' as AutoSync,
X.AutoSync,
X.AutoTowingLicense,
X.BlockedVendor_icare,
X.CellPhone,
X.CellPhoneCountry,
X.CellPhoneExtension,
X.ClaimTaxFreeThreshold_icare,
X.ClaimantIDType,
X.CommunicationPreferences_icare,
X.DateOfBirth,
--'FALSE' as DoNotDestroy,
X.DoNotDestroy as DoNotDestroy,
X.DoctorSpecialty,
X.DoctorSpecialty_icare,
X.EducationLevel,
X.EligibleOffset_icare,
X.EmailAddress1,
X.EmailAddress2,
X.EmployeeNumber,
X.EmployeeSecurityID,
X.FaxPhone,
X.FaxPhoneCountry,
X.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
X.FinancialSupplementDebt_icare,
X.FirstName, --Individuals will have this populated and MC is individual only
X.FirstNameKanji,
X.FormerName,
X.Gender,
X.GreenCardNumber,
X.HelpSslorTslDebt_icare,
X.HomePhone,
X.HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
X.HomePhoneExtension,
X.InterpreterRequired_icare,
X.JurisdictionAssignedID,
X.LastName,
X.LastNameKanji,
X.LawFirmSpecialty,
X.LegalSpecialty_icare,
X.LicenseNumber,
X.LicenseState,
X.MaritalStatus,
X.MedicalLicense,
X.MedicalOrgSpecialty,
X.MedicalSpecialty_icare,
X.MedicareIRN_icare,
X.MedicareLevyVariation_icare,
X.MedicareNumber_icare,
X.MedicareValidUntil_icare,
X.MiddleName,
X.Name,  --Companies will have this populated but individuals will have this blank
X.NameKanji,
X.Notes,
X.NumDependents,
X.NumDependentsU18,
X.NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
X.ObfuscatedInternal,
X.Occupation,
X.OccupationDetails_icareID,
X.OffsetAmount_icare,
X.OktaID_icare,
X.OrganizationType,
X.Particle,
X.PassportNumber,
X.PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
X.PendingLinkMessage,
X.PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
X.Preferred,
X.PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
X.PreferredPaymentMethod_icare,
X.Prefix,
X.PrimaryAddressID,
X.PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
X.PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
X.PrimaryPhone,
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
X.RegisteredForGST_icare,
--0 as Retired,
X.Retired,
X.SSNReleaseAuthorized,
X.Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
X.SingleRemittancePerDay_icare,
X.SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
X.SourceSystem_icare,
X.SpecialtyType,
X.Subtype,
X.Suffix,
X.TFNDeclarationDate_icare,
X.TaxExemptionsEntitled,
X.TaxFilingStatus,
X.TaxID,
X.TaxStatus,
X.TempCRMUniqueID_icare,
X.TradingName_icare,
X.TrustName_icare,
X.TrusteeName_icare,
X.ValidationLevel,
X.VendorNumber,
X.VendorType,
X.VenueType,
X.W9Received,
X.W9ReceivedDate,
X.W9ValidFrom,
X.W9ValidTo,
X.WithholdingRate,
X.WorkPhone,
X.WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
X.WorkPhoneExtension,
X.ACN_icare,
X.CommencementDate_icare,
X.OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
X.OtherLanguage_icare,
X.TrustABN_icare,
X.TrustABNIsActive_icare,
X.TrustABNValidated_icare,
X.TrusteeType_icare,
X.TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
X.CRMVersion_icare
,X.LUWID
,X.ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,X.CM_LinkID
,X.CRM_ID
from
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
PC_Con_lkp.rownum,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
--pc.FIRSTNAME as Person_Clm_FIRSTNAME,
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as Person_Clm_FIRSTNAME,
--pc.LASTNAME Person_Clm_LASTNAME,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as Person_Clm_LASTNAME,
rlu.[Code],
CM_C.LinkID,
CM_C.FirstName as PPLC_FIRSTNAME,
CM_C.LastName as PPLC_LASTNAME,
PC_Con_lkp.isPPLC,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group
-- of unmatched MCs who have same first name and last name.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
  left outer join
  (
   select 
   --* 
   row_number () over (partition by ltrim(rtrim(PC_Con_lkp.Claim_Number)) order by PC_Con_lkp.rownum asc) rownum, 
   ltrim(rtrim(PC_Con_lkp.Claim_Number)) as Claim_Number,
   PC_Con_lkp.id, 
   PC_Con_lkp.PolicyNumber, 
   PC_Con_lkp.ContactLinkID,
   PC_Con_lkp.FirstName, 
   PC_Con_lkp.LastName, 
   PC_Con_lkp.IsPPLC
   from temptable_ER_PPLC_N_4 PC_Con_lkp
  ) PC_Con_lkp 
  on PC_Con_lkp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
  --10/09/2019 Added this lookup just for this section - this lookup was created somewhere at the top
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1 non_PPLC_matched_1
on non_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2 non_PPLC_matched_2
on non_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3 non_PPLC_matched_3
on non_PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
--10/09/2019 Added the above joins to check if there is any MC record already present in them. If yes, then we should not be creating any duplicate record via this section.
-- Only if there is no match in these 7 matched temp tables, then we go ahead and create a new MC via this section.
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
where CD.is_Null = 0
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
-- 22jul2019 - commented out the above line as it is a bug - instead have kept the check at the bottom "where Y.rownum=1"
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --20/06/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_1
and PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_2
and PPLC_matched_3.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_3
and PPLC_matched_4.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_4
and non_PPLC_matched_1.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_1
and non_PPLC_matched_2.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_2
and non_PPLC_matched_3.LUWID is NULL --10/09/2019 Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_3
and PC_Con_lkp.Claim_Number is not NULL --10/09/2019 FINALLY Ensuring that we DO have a match in non_PPLC_gwpc_lookup
)X
)Y
where Y.rownum=1 --22Jul2019-picking only 1 record when multiple name matches as PC_Con_lkp is already sorted on min(ContactLinkID)
)Z

--Setting up cleansed phone data based on Ibrahim script
IF OBJECT_ID('tempdb.dbo.#tmpNotifierPhoneNumbers', 'U') IS NOT NULL
DROP TABLE #tmpNotifierPhoneNumbers

--1) Clean-up all numbers
SELECT
 Claim_Number
 ,CASE
  WHEN REPLACE(pc.DIRECTPHONE,' ','') IS NOT NULL
   AND (LEN(REPLACE(pc.DIRECTPHONE,' ','')) < 8 OR REPLACE(REPLACE(pc.DIRECTPHONE,' ',''),'0','') = '' OR REPLACE(pc.DIRECTPHONE,' ','') = '')
   THEN NULL
  ELSE REPLACE(pc.DIRECTPHONE,' ','')
 END AS DirectPhone
 ,CASE
  WHEN LTRIM(RTRIM(pc.HOMEPHONE)) IS NOT NULL
   AND (LEN(REPLACE(pc.HOMEPHONE,' ','')) < 8 OR REPLACE(REPLACE(pc.HOMEPHONE,' ',''),'0','') = '' OR REPLACE(pc.HOMEPHONE,' ','') = '')
   THEN NULL
  ELSE REPLACE(pc.HOMEPHONE,' ','')
 END AS HomePhone

 ,CASE
  WHEN LTRIM(RTRIM(pc.MOBILE)) IS NOT NULL
   AND (LEN(REPLACE(pc.MOBILE,' ','')) < 8 OR REPLACE(REPLACE(pc.MOBILE,' ',''),'0','') = '' OR REPLACE(pc.MOBILE,' ','') = '')
   THEN NULL
  ELSE REPLACE(pc.MOBILE,' ','')
 END AS MobilePhone

 ,'xxxxxxx' AS PrimaryNo
INTO #tmpNotifierPhoneNumbers
FROM [DM_STG_EMICS].[dbo].[CLAIM_DETAIL] cd
 INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc ON cd.[Notifier_Id] = pc.[ID]
WHERE cd.is_Null = 0 
 AND pc.DIRECTPHONE <> '137722'

--2) Update the Work No to 02 9999 9999, when all number are NULL
UPDATE #tmpNotifierPhoneNumbers
SET DirectPhone = '0299999999'
WHERE DirectPhone IS NULL 
 AND HomePhone IS NULL
 AND MobilePhone IS NULL

--3) Update Is Primary
--Mobile
UPDATE #tmpNotifierPhoneNumbers
SET PrimaryNo = 'mobile'
WHERE MobilePhone IS NOT NULL
 AND PrimaryNo ='xxxxxxx'

--Direct Phone (Work)
UPDATE #tmpNotifierPhoneNumbers
SET PrimaryNo = 'work'
WHERE DirectPhone IS NOT NULL

--Home
UPDATE #tmpNotifierPhoneNumbers
SET PrimaryNo = 'home'
WHERE HomePhone IS NOT NULL
 AND PrimaryNo = 'xxxxxxx'

--drop table temptable_MC_ER_VerifiedPC_unmatched_temp1
--(ER UNMATCHED):Creating a temp table with Notifier Type='E,'R' UNMATCHED MC contacts
SELECT * INTO temptable_MC_ER_VerifiedPC_unmatched_temp1
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
rlu.[Code],
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
--CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
NULL as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
--CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, 
NULL as ABSLanguageDescription_icare,
--CM_C.LinkID as AddressBookUID,
NULL as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
--CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as AggregateRemittance_icare,
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare,
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
--CM_C.CellPhone,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.MOBILE,''))) <> '' THEN LTRIM(RTRIM(pc.MOBILE)) 
ELSE NULL 
END as CellPhone,
--CM_C.CellPhoneCountry,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.MOBILE,''))) <> '' THEN 'AU' 
ELSE NULL 
END as CellPhoneCountry,
--CM_C.CellPhoneExtension,
NULL as CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.[EMAIL],''))) <> '' THEN 'email' 
WHEN LTRIM(RTRIM(ISNULL(pc.[Address_Line1],''))) <> ''
THEN 'post' 
WHEN QA_PL_add.AddressLine1 is not null then 'post' --checking if we atleast have a policylocation (Verified PC or CDR) named insured address
ELSE NULL
END as CommunicationPreferences_icare,
--CM_C.DateOfBirth,
pc.[DOB] as DateOfBirth,
--'FALSE' as DoNotDestroy,
0 as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
--CM_C.EmailAddress1,
CASE
WHEN  LTRIM(RTRIM(ISNULL(pc.[Email],''))) = '' THEN null
Else  Ltrim(Rtrim(pc.email))
END
as EmailAddress1,
NULL as EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
NULL as FaxPhone,
--CM_C.FaxPhoneCountry,
NULL as FaxPhoneCountry,
NULL as FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
--pc.FIRSTNAME as FirstName, --MC is individual only
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as FirstName,
--29Aug2019:as part of DMIG-7772 (DMIG-8073) applied LTRIM and RTRIM to ensure any leadding/trailing spaces are removed
--Else for ER_unmatched we end with the 2 sets of Main Contact even though technically the names are the same.
NULL as FirstNameKanji,
NULL as FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
--CM_C.HomePhone,
tmp_phone.HomePhone as HomePhone, --coming from Ibrahim cleansed phone number logic
--CM_C.HomePhoneCountry,
CASE 
WHEN LTRIM(RTRIM(ISNULL(tmp_phone.HomePhone,''))) <> '' THEN  'AU' 
ELSE NULL 
END
as HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
NULL as HomePhoneExtension,
NULL as InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
--pc.LASTNAME as LastName,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as LastName,
--29Aug2019:as part of DMIG-7772 (DMIG-8073) applied LTRIM and RTRIM to ensure any leadding/trailing spaces are removed
--Else for ER_unmatched we end with the 2 sets of Main Contact even though technically the names are the same.
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
NULL as MiddleName,
NULL as Name,  --MC will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
NULL as Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
NULL as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
NULL as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,
NULL as PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
tmp_phone.PrimaryNo as PrimaryPhone, --this comes from Ibrahim cleansed phone number logic
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
NULL as RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
NULL as SingleRemittancePerDay_icare,
NULL as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
'Salesforce' as SourceSystem_icare,
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
--CM_C.WorkPhone,
tmp_phone.DirectPhone as WorkPhone, --comes from Ibrahim's cleansed logic
--CM_C.WorkPhoneCountry,
CASE 
WHEN LTRIM(RTRIM(ISNULL(tmp_phone.DirectPhone,''))) <> '' THEN 'AU' 
ELSE NULL 
END
as WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
NULL as WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--CM_C.CRMVersion_icare
1 as CRMVersion_icare
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,NULL as ApprovedPaymentMethod
--,cons_pp.PC_PP_PolicyNumber as PC_PolicyNumber --10/09/2019 Had to comment out this as we have new logic below
,
CASE 
WHEN count_gwpc_lkp_1.PolicyNumber is not null then count_gwpc_lkp_1.PolicyNumber
WHEN count_gwpc_lkp_2.LegacyPolicyNumber_icare is not null then count_gwpc_lkp_2.PolicyNumber
WHEN count_gwpc_lkp_3.policynumber is not null then count_gwpc_lkp_3.PolicyNumber
WHEN count_gwpc_lkp_4.claim_number is not null then count_gwpc_lkp_4.policynumber
ELSE NULL
END
as PC_PolicyNumber
--10/09/2019 Had to add this logic to ensure it picks the PC PolicyNmuber from the one of the newly added lookups
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
inner join [dbo].[ADM_Claim_Cohort] qa_cohort on qa_cohort.ClaimNumber=ltrim(rtrim(CD.Claim_Number)) and qa_cohort.cohort=2
--10/09/2019 Had to add this as other wise we have issues with processing unmatched MCs where max(Claimnumber) is set as LinkID for a group of unmatched
--MCs who have same first name and last name. If we don't add this join to filter Cohort=2, then sometimes we get max(ClaimNumber) set to a Cohort 1 ClaimNumber.
--Then you get to see differences appearing in QA report because XFM has different LinkID to what QA has.
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
inner join #tmpNotifierPhoneNumbers tmp_phone
on tmp_phone.Claim_Number=ltrim(rtrim(CD.Claim_Number))
left outer join [dbo].[ccst_address] QA_PL_add -- ccst_address QA run should have happened before
on  QA_PL_add.LUWID=ltrim(rtrim(CD.Claim_Number)) and QA_PL_add.PublicID like '%POLICYLOCATIONADDRESS'
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1 non_PPLC_matched_1
on non_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2 non_PPLC_matched_2
on non_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3 non_PPLC_matched_3
on non_PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4 non_PPLC_matched_4
on non_PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
 --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
left outer join temptable_count_gwpc_contact_lkp_1 count_gwpc_lkp_1 on ltrim(rtrim(count_gwpc_lkp_1.PolicyNumber))=ltrim(rtrim(PT.icare_policy_no))
left outer join temptable_count_gwpc_contact_lkp_2 count_gwpc_lkp_2 on ltrim(rtrim(count_gwpc_lkp_2.LegacyPolicyNumber_icare))=ltrim(rtrim(CD.Policy_No))
left outer join temptable_count_gwpc_contact_lkp_1 count_gwpc_lkp_3 on ltrim(rtrim(count_gwpc_lkp_3.PolicyNumber))=ltrim(rtrim(CD.Policy_No))
left outer join temptable_count_gwpc_contact_lkp_3 count_gwpc_lkp_4 on ltrim(rtrim(count_gwpc_lkp_4.Claim_Number))=ltrim(rtrim(CD.Claim_Number))
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED
and PPLC_matched_2.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED
and PPLC_matched_3.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED
and PPLC_matched_4.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED
and non_PPLC_matched_1.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED
and non_PPLC_matched_2.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED
and non_PPLC_matched_3.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED
and non_PPLC_matched_4.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED
and 
(
count_gwpc_lkp_1.PolicyNumber is not null
or
count_gwpc_lkp_2.LegacyPolicyNumber_icare is not null
or
count_gwpc_lkp_3.PolicyNumber is not null
or
count_gwpc_lkp_4.Claim_Number is not null
) 
--10/09/2019 Ensuring at least it has a policy match in gwpc_contacts_lookups otherwise we will need to treat the claim as CDR claim
)X


--(ER UNMATCHED): Getting the max(claimnumber) based on grouping by concatenated First + Last Name and PC PolicyNumber
select * into temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber
from
(
select 
--FirstName, LastName,PC_PolicyNumber, max(LUWID) as max_claimnumber
replace(UPPER(FirstName) + ' ' + UPPER(LastName),char(9),'') as concat_name,PC_PolicyNumber, max(LUWID) as max_claimnumber
from
temptable_MC_ER_VerifiedPC_unmatched_temp1
--group by FirstName, LastName,PC_PolicyNumber
group by replace(UPPER(FirstName) + ' ' + UPPER(LastName),char(9),''),PC_PolicyNumber
)X
--16Sep2019: Changing the SELECT and GROUP BY clauses to use concatenated FirstName and LastName as otherwise we dont sometimes get the exact GROUP of expected contacts with same first and last name.


--getting the LinkID to be max(claimnumber) based on matching back on temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber
SELECT * INTO temptable_MC_ER_VerifiedPC_unmatched_temp2
FROM
(
select 
temp_max.max_claimnumber,
--distinct
PublicID,
Code,
-- AB contact details to populate in ccst_contact
ABNIsActive_icare,
ABNValidated_icare,
ABN_icare,
--CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
--CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, 
ABSLanguageDescription_icare,
--CM_C.LinkID as AddressBookUID,
AddressBookUID,
AdjudicativeDomain,
AdjudicatorLicense,
--0 as AggregateRemittance_icare,
--CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
AggregateRemittance_icare,
AttorneyLicense,
AttorneySpecialty,
AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
AuResidentForTaxPurposes_icare,
AutoRepairLicense,
--'allow' as AutoSync,
AutoSync,
AutoTowingLicense,
BlockedVendor_icare,
--CM_C.CellPhone,
CellPhone,
--CM_C.CellPhoneCountry,
CellPhoneCountry,
--CM_C.CellPhoneExtension,
CellPhoneExtension,
ClaimTaxFreeThreshold_icare,
ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
CommunicationPreferences_icare,
--CM_C.DateOfBirth,
DateOfBirth,
--'FALSE' as DoNotDestroy,
DoNotDestroy,
DoctorSpecialty,
DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
EducationLevel,
EligibleOffset_icare,
--CM_C.EmailAddress1,
EmailAddress1,
EmailAddress2,
EmployeeNumber,
EmployeeSecurityID,
FaxPhone,
--CM_C.FaxPhoneCountry,
FaxPhoneCountry,
FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
FinancialSupplementDebt_icare,
temp.FirstName, --MC is individual only
FirstNameKanji,
FormerName,
Gender,
GreenCardNumber,
HelpSslorTslDebt_icare,
--CM_C.HomePhone,
HomePhone, --coming from Ibrahim cleansed phone number logic
--CM_C.HomePhoneCountry,
HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
HomePhoneExtension,
InterpreterRequired_icare,
JurisdictionAssignedID,
temp.LastName,
LastNameKanji,
LawFirmSpecialty,
LegalSpecialty_icare,
LicenseNumber,
LicenseState,
MaritalStatus,
MedicalLicense,
MedicalOrgSpecialty,
MedicalSpecialty_icare,
MedicareIRN_icare,
MedicareLevyVariation_icare,
MedicareNumber_icare,
MedicareValidUntil_icare,
MiddleName,
Name,  --MC will have this blank
NameKanji,
Notes,
NumDependents,
NumDependentsU18,
NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
ObfuscatedInternal,
Occupation,
OccupationDetails_icareID,
OffsetAmount_icare,
OktaID_icare,
OrganizationType,
Particle,
PassportNumber,
PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
PendingLinkMessage,
PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
Preferred,
PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
PreferredPaymentMethod_icare,
Prefix,
PrimaryAddressID,
--'en_AU' as PrimaryLanguage,
PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
PrimaryPhone, --this comes from Ibrahim cleansed phone number logic
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
RegisteredForGST_icare,
Retired,
SSNReleaseAuthorized,
Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
SingleRemittancePerDay_icare,
SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
SourceSystem_icare,
SpecialtyType,
Subtype,
Suffix,
TFNDeclarationDate_icare,
TaxExemptionsEntitled,
TaxFilingStatus,
TaxID,
TaxStatus,
TempCRMUniqueID_icare,
TradingName_icare,
TrustName_icare,
TrusteeName_icare,
ValidationLevel,
VendorNumber,
VendorType,
VenueType,
W9Received,
W9ReceivedDate,
W9ValidFrom,
W9ValidTo,
WithholdingRate,
--CM_C.WorkPhone,
WorkPhone, --comes from Ibrahim's cleansed logic
--CM_C.WorkPhoneCountry,
WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
WorkPhoneExtension,
ACN_icare,
CommencementDate_icare,
OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
OtherLanguage_icare,
TrustABN_icare,
TrustABNIsActive_icare,
TrustABNValidated_icare,
TrusteeType_icare,
TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CRMVersion_icare,
LUWID,
ApprovedPaymentMethod,
temp.PC_PolicyNumber
from temptable_MC_ER_VerifiedPC_unmatched_temp1 temp
inner join temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber temp_max
--on temp.FirstName=temp_max.FirstName and temp.LastName=temp_max.LastName
on temp_max.concat_name=replace(UPPER(temp.FirstName) + ' ' + UPPER(temp.LastName),char(9),'')
and temp.PC_PolicyNumber=temp_max.PC_PolicyNumber
where 
--temp.FirstName=temp_max.FirstName
--and temp.LastName=temp_max.LastName
temp_max.concat_name=replace(UPPER(temp.FirstName) + ' ' + UPPER(temp.LastName),char(9),'')
--16Sep2019: Changing the Compare to use concatenated FirstName and LastName as otherwise we dont sometimes get the exact GROUP of expected contacts with same first and last name.
and temp.PC_PolicyNumber=temp_max.PC_PolicyNumber
)X


--(ER UNMATCHED): Assigning the LinkID to be max(claimnumber)
SELECT * INTO temptable_MC_ER_VerifiedPC_unmatched_temp3
FROM
(
select 
temp.*,
concat (temp2.max_claimnumber,'MC') as LinkID
from temptable_MC_ER_VerifiedPC_unmatched_temp1 temp
inner join temptable_MC_ER_VerifiedPC_unmatched_temp2 temp2
on temp.PublicID=temp2.PublicID
)X


-- (Criteria 1) Creating a temp table with Notifier Type='W','O', 'P' PPLC MC contacts
--Using PT.icare_policy_no to match with gwpc contact lookup PolicyNumber
SELECT * INTO temptable_MC_WOP_PPLC_VerifiedPC_1
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
CD.policy_no,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
LEFT OUTER join [DM_STG_EMICS].[dbo].POLICY_TERM_DETAIL PT on PT.POLICY_NO = CD.Policy_No --EMICS policy number
--11Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
inner join temptable_WOP_count_gwpc_contact_lkp_1 PC_Con_lkp 
on PC_Con_lkp.PolicyNumber=PT.icare_policy_no 
--11Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
-- We do not do any name match for W,O,P notifier types
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))
--= UPPER(LTRIM(RTRIM(CM_C.FirstName))) + ' ' + UPPER(LTRIM(RTRIM(CM_C.LastName )))
and rlu.[Code] in ('W','O', 'P') --Ensuing we filter only W,O,P
--and (CM_C.LinkID is null or CRM_C.ID is null)
)X


-- (Criteria 2) Creating a temp table with Notifier Type='W','O', 'P' PPLC MC contacts
--Using CD.Policy_No to match with gwpc contact lookup LegacyPolicyNumber_icare
SELECT * INTO temptable_MC_WOP_PPLC_VerifiedPC_2
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
CD.policy_no,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
inner join temptable_WOP_count_gwpc_contact_lkp_2 PC_Con_lkp 
on PC_Con_lkp.LegacyPolicyNumber_icare=CD.Policy_No
left outer join temptable_MC_WOP_PPLC_VerifiedPC_1 WOP_PPLC_1
on WOP_PPLC_1.LUWID=ltrim(rtrim(CD.Claim_Number))
--11Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
-- We do not do any name match for W,O,P notifier types
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))
--= UPPER(LTRIM(RTRIM(CM_C.FirstName))) + ' ' + UPPER(LTRIM(RTRIM(CM_C.LastName )))
and rlu.[Code] in ('W','O', 'P') --Ensuing we filter only W,O,P
and WOP_PPLC_1.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_1
--and (CM_C.LinkID is null or CRM_C.ID is null)
)X

--(Criteria 3) Creating a temp table with Notifier Type='W','O', 'P' PPLC MC contacts
--Using CD.Policy_No to match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_WOP_PPLC_VerifiedPC_3
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
CD.policy_no,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
inner join temptable_WOP_count_gwpc_contact_lkp_1 PC_Con_lkp 
on PC_Con_lkp.PolicyNumber=CD.Policy_No
left outer join temptable_MC_WOP_PPLC_VerifiedPC_1 WOP_PPLC_1
on WOP_PPLC_1.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_2 WOP_PPLC_2
on WOP_PPLC_2.LUWID=ltrim(rtrim(CD.Claim_Number))
--11Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
-- We do not do any name match for W,O,P notifier types
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))
--= UPPER(LTRIM(RTRIM(CM_C.FirstName))) + ' ' + UPPER(LTRIM(RTRIM(CM_C.LastName )))
and rlu.[Code] in ('W','O', 'P') --Ensuing we filter only W,O,P
and WOP_PPLC_1.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_1
and WOP_PPLC_2.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_2
--and (CM_C.LinkID is null or CRM_C.ID is null)
)X


--(Criteria 4) Creating a temp table with Notifier Type='W','O', 'P' PPLC MC contacts
--Using CD.Policy_No where PolicyNo ends in '%701'and stripping this away and trying to then match with gwpc_contacts lookup PolicyNumber
SELECT * INTO temptable_MC_WOP_PPLC_VerifiedPC_4
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
CD.policy_no,
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, --19/06/19 Remember to change other SPs too
CM_C.LinkID as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare, --19/06/19 Need to raise a defect against mapper as this attribute is not there in CM
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
CM_C.CellPhone,
--CM_C.CellPhoneCountry, --06Aug2019 Commented this out as we should not use ID directly
CASE WHEN CM_C.CellPhoneCountry is not null then  ABTL_CNY_cell.TYPECODE --'AU'  
ELSE NULL 
End CellPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
CM_C.CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
abtl_comm.TYPECODE as CommunicationPreferences_icare, --27Jul2019 Added this
CM_C.DateOfBirth,
--'FALSE' as DoNotDestroy,
CM_C.DoNotDestroy as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
CM_C.EmailAddress1,
CM_C.EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
CM_C.FaxPhone,
--CM_C.FaxPhoneCountry,
CASE WHEN CM_C.FaxPhone is not null and CM_C.FaxPhoneCountry is null then 'AU' 
--06Aug2019 Changed the above line from  then 16 to  then 'AU'
WHEN CM_C.FaxPhoneCountry  IS NOT NULL then ABTL_CNY_fax.TYPECODE
--06Aug2019 Changed the above line from ELSE CM_C.FaxPhoneCountry to WHEN CM_C.FaxPhoneCountry IS NOT NULL then ABTL_CNY_fax.TYPECODE
ELSE NULL
End FaxPhoneCountry,
CM_C.FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
CM_C.FirstName, --Individuals will have this populated and MC is individual only
NULL as FirstNameKanji,
CM_C.FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
CM_C.HomePhone,
--CM_C.HomePhoneCountry,--06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.HomePhoneCountry is not null then  ABTL_CNY_home.TYPECODE --'AU'  
ELSE NULL 
End HomePhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
CM_C.HomePhoneExtension,
CM_C.InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
CM_C.LastName,
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
CM_C.MiddleName as MiddleName,
CM_C.Name,  --Companies will have this populated but individuals will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
CM_C.Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
0 as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
CM_C.PreferredPaymentMethod_icare as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,--20Aug2019 commenting out this as per DMIG-7905
NULL as PrimaryLanguage,--20Aug2019 setting this to NULL as part of QA build (DMIG-7905). Refer to Mapper's latest spec update DMIG-7749
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
abtl_PrimaryPhoneType.TYPECODE as PrimaryPhone, --27Jul2019 Fixed this
NULL as RegisteredForGST_icare,
--0 as Retired,
CM_C.Retired as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
CM_C.SingleRemittancePerDay_icare as SingleRemittancePerDay_icare,
CM_C.SmsNotification_icare as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
CM_C.SourceSystem_icare,--09/08/2019 Commented this out as it should come from CRM.
--crm_c.LAST_UPDATED_SOURCE__C as SourceSystem_icare,--09/08/2019 Based on QA differences on 105 Cohort1, mapper confirmed it should come from CRM
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
CM_C.WorkPhone,
--CM_C.WorkPhoneCountry, --06Aug2019 Commented this out as this is the ID
CASE WHEN CM_C.WorkPhoneCountry is not null then  ABTL_CNY_work.TYPECODE --'AU'  
ELSE NULL 
End WorkPhoneCountry, --06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
CM_C.WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
CASE 
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) <= isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)	
--CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN isnull(ltrim(rtrim(crm_c.VERSION_NUMBER__C)),0) > isnull(CM_C.CRMVersion_icare,0)
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
WHEN ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is NULL and CM_C.CRMVersion_icare is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(CM_C.CRMVersion_icare AS decimal))+1)
-- CAST(isnull(CM_C.CRMVersion_icare,0) as int)+1
WHEN CM_C.CRMVersion_icare is NULL and ltrim(rtrim(crm_c.VERSION_NUMBER__C)) is not NULL
THEN CONVERT(varchar(20),FLOOR(CAST(ltrim(rtrim(crm_c.VERSION_NUMBER__C)) AS decimal))+1)
--CAST(isnull(ltrim(rtrim(crm_a.VERSION_NUMBER__C)),0) as int)+1
ELSE 1
END as CRMVersion_icare
--30/08/2019 Added this because of DMIG-8086 (DMIG-8090), we need to ensure we now pick the highest CRM number where possible.
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,CM_C.ApprovedPaymentMethod_icare as ApprovedPaymentMethod --19/06/2019 need to look at other SPs
,CM_C.LinkId as CM_LinkID
,crm_c.[ID] as CRM_ID
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
inner join temptable_WOP_count_gwpc_contact_lkp_3 PC_Con_lkp 
on PC_Con_lkp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_1 WOP_PPLC_1
on WOP_PPLC_1.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_2 WOP_PPLC_2
on WOP_PPLC_2.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_3 WOP_PPLC_3
on WOP_PPLC_3.LUWID=ltrim(rtrim(CD.Claim_Number))
--11Sep2019: Added the above join as part of DMIG-7698 (DMIG-8106) DMIG-6151
--inner join  [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS  dic on dic.Claim_Number=ltrim(rtrim(CD.Claim_Number)) --10/07/2019 - Added this in
--inner join DM_XFM.dbo.DM_INSCOPE_CLAIMS dic on cons_pp.Claim_Number=ltrim(rtrim(CD.Claim_Number))
--LEFT OUTER JOIN [DM_STG_PC].[dbo].[pc_policyperiod] PC_PP on PC_PP.ID = cons_pp.PC_PP_ID
LEFT OUTER JOIN [DM_STG_CRM].dbo.[DM_STG_CRM_CONTACT] crm_c --18/06/19 Chaning the table to CRM contact Remember to switch _LD back to without _LD
--ON p.[ACCOUNT__C] = crm_a.[ID]
--ON pc_a.[CRMUniqueID_icare] = crm_a.[ID]  --10/06/2019 -- Adding the modified join based on Ash/Cesar comment on DMIG-6069
ON PC_Con_lkp.ContactLinkID = crm_c.[ID]  --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB contact table] where loadcommandid is null) CM_C on CM_C.LinkId = crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = pc_a.[CRMUniqueID_icare]--crm_a.[ID] --11/02/2019 -- DMIG-3764 - source change from PC to CM
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_abcontact] CM_C on CM_C.LinkID= PC_Con_lkp.ContactLinkID --'0032O000005Bm5AQAS'
--LEFT OUTER JOIN DM_XFM.[dbo].[DM_LKP_GW_CM_AB_ABCONTACT] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_abcontact] CM_C on CM_C.LinkId = PC_Con_lkp.ContactLinkID--crm_a.[ID] --18/06/2019 -- Ensured we lookup using MC CRMID from PC
--LEFT OUTER JOIN (select * from CM Production BackedUp Database.[AB address Table] where loadcommandid is null) CM_A on CM_A.ID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM--
LEFT OUTER JOIN [DM_STG_CM].[dbo].[ab_address] CM_A on CM_A.ID=CM_C.PrimaryAddressID
--LEFT OUTER JOIN  DM_XFM.[dbo].[DM_LKP_GW_CM_ADDRESS] CM_A on CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN [DM_STG_CM_LD].[dbo].[ab_address] CM_A on CM_A.ID = CM_C.PrimaryAddressID--CM_A.PublicID = CM_C.PrimaryAddressID --14/02/2019 -- DMIG-3764 - source change from PC to CM
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType on CM_A.AddressType = CM_AddressType.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_addresstype] CM_AddressType ON CM_A.AddressType = CM_AddressType.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address on CM_A.Subtype = ABTL_Address.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_address] ABTL_Address ON CM_A.Subtype = ABTL_Address.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_cell on ABTL_CNY_cell.Id = CM_C.CellPhoneCountry
--06Aug2019 Added this as otherwise we are seeing ID appearing in QA target instead of TypeCode
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.Id = CM_A.Country
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY on ABTL_CNY.TYPECODE = CM_A.Country
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_home on ABTL_CNY_home.Id = CM_C.HomePhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.ID = CM_C.HomePhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_home on ABTL_CNY_home.TYPECODE = CM_C.HomePhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.Id = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_work on ABTL_CNY_work.TYPECODE = CM_C.WorkPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.Id = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_country] ABTL_CNY_fax on ABTL_CNY_fax.TYPECODE = CM_C.FaxPhoneCountry
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_state] ABTL_State ON CM_A.State = ABTL_State.ID
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State on CM_A.State = ABTL_State.ID
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_state] ABTL_State ON CM_A.State = ABTL_State.TYPECODE
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.Id = CM_C.PrimaryPhone
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.ID = CM_C.PrimaryPhone
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_primaryphonetype] abtl_PrimaryPhoneType on abtl_PrimaryPhoneType.TYPECODE = CM_C.PrimaryPhone
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.Id = CM_C.ABSLanguageCode_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_languagetype] abtl_PrimaryLanguageType on abtl_PrimaryLanguageType.ID = CM_C.ABSLanguageCode_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.Id = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_organizationtype] abtl_organizationtype on abtl_organizationtype.TYPECODE = CM_C.OrganisationType_icare --01/03/2019 added this to map ccst_contact.organizationtype_icare
--LEFT OUTER JOIN CM Production BackedUp Database.[abtl_abcontact] abtl_abcontact on abtl_abcontact.Id = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.ID = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
--LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_abcontact] abtl_abcontact on abtl_abcontact.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
LEFT OUTER JOIN [DM_STG_CM].[dbo].[abtl_commmethod_icare] abtl_comm on abtl_comm.ID = CM_C.CommunicationPreferences_icare --27Jul2019 Added this as SP was inocrrectly loading ID instead of TYPECODE
 -- LEFT OUTER JOIN select * from [DM_STG_CM_LD].[dbo].[abtl_accountorgtype_icare] abtl_orgtype on abtl_orgtype.TYPECODE = CM_C.SubType --01/03/2019 added this to map ccst_contact.subtype
 --left outer join (select * from #temp_ccst_eftdata_emp where rnk=1) eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
--left outer join (select * from REC_XFM.dbo.ccst_eftdata_emp where ContactID like '%:INSURED') eftdata on eftdata.ContactID=concat('EMICS:',ltrim(rtrim(CD.Claim_Number)),':INSURED')
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
-- We do not do any name match for W,O,P notifier types
--and UPPER(LTRIM(RTRIM(pc.FIRSTNAME))) + ' ' + UPPER(LTRIM(RTRIM(pc.LASTNAME)))
--= UPPER(LTRIM(RTRIM(CM_C.FirstName))) + ' ' + UPPER(LTRIM(RTRIM(CM_C.LastName )))
and rlu.[Code] in ('W','O', 'P') --Ensuing we filter only W,O,P
and WOP_PPLC_1.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_1
and WOP_PPLC_2.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_2
and WOP_PPLC_3.LUWID is NULL --Ensuring that we don't create another WOP MC record if it already exists in WOP_PPLC_3
--and (CM_C.LinkID is null or CRM_C.ID is null)
)X

--select * from #temptable_MAINCONTACT_gwpc_contacts_lookup
--where ltrim(rtrim(LegacyPolicyNumber_icare))='A07112016'--'1SF0040701GWC154'   --'WGB940603544122' 

--drop table temptable_MC_rest_of_all_VerifiedCDR_temp

--Creating a temp table with rest of all UNVERIFIED MC contacts i.e verified CDR claims
-- These will remain as Local contacts within CC
-- 11/09/2019: As part of DMIG-7698 (DMIG-8106) DMIG-6151, have modified the joins below to include many more temp tables from the matched MC sections above.
SELECT * INTO temptable_MC_rest_of_all_VerifiedCDR_temp
FROM
(
select distinct
--12Dec2018 Added tables to ensure we can get remittance/payment method details (DMIG-2934)
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC' as PublicID,
rlu.[Code],
-- AB contact details to populate in ccst_contact
NULL as ABNIsActive_icare,
NULL as ABNValidated_icare,
NULL as ABN_icare,
--CM_C.ABSLanguageCode_icare as ABSLanguageCode_icare,
NULL as ABSLanguageCode_icare,
--abtl_PrimaryLanguageType.[DESCRIPTION] as ABSLanguageDescription_icare,
--CM_C.ABSLanguageDescription_icare as ABSLanguageDescription_icare, 
NULL as ABSLanguageDescription_icare,
--CM_C.LinkID as AddressBookUID,
NULL as AddressBookUID,
NULL as AdjudicativeDomain,
NULL as AdjudicatorLicense,
--0 as AggregateRemittance_icare,
--CM_C.AggregateRemittance_icare as AggregateRemittance_icare,--19/06/19 Remember to change other SPs too
NULL as AggregateRemittance_icare,
NULL as  AttorneyLicense,
NULL as  AttorneySpecialty,
NULL as  AttorneySpecialty_icare,
--CM_C.AuResidentForTaxPurposes_icare as AuResidentForTaxPurposes_icare,
NULL as AuResidentForTaxPurposes_icare,
NULL as AutoRepairLicense,
--'allow' as AutoSync,
NULL as AutoSync,
NULL as AutoTowingLicense,
NULL as BlockedVendor_icare,
--CM_C.CellPhone,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.MOBILE,''))) <> '' THEN LTRIM(RTRIM(pc.MOBILE)) 
ELSE NULL 
END as CellPhone,
--CM_C.CellPhoneCountry,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.MOBILE,''))) <> '' THEN 'AU' 
ELSE NULL 
END as CellPhoneCountry,
--CM_C.CellPhoneExtension,
NULL as CellPhoneExtension,
NULL as ClaimTaxFreeThreshold_icare,
NULL as ClaimantIDType,
--CM_C.CommunicationPreferences_icare,
CASE WHEN LTRIM(RTRIM(ISNULL(pc.[EMAIL],''))) <> '' THEN 'email' 
WHEN LTRIM(RTRIM(ISNULL(pc.[Address_Line1],''))) <> ''
THEN 'post' 
WHEN QA_PL_add.AddressLine1 is not null then 'post' --checking if we atleast have a policylocation (Verified PC or CDR) named insured address
ELSE NULL
END as CommunicationPreferences_icare,
--CM_C.DateOfBirth,
pc.[DOB] as DateOfBirth,
--'FALSE' as DoNotDestroy,
0 as DoNotDestroy,
NULL as DoctorSpecialty,
NULL as DoctorSpecialty_icare, --19/06/2019 - need to check other SPs
NULL as EducationLevel,
NULL as EligibleOffset_icare,
--CM_C.EmailAddress1,
CASE
WHEN  LTRIM(RTRIM(ISNULL(pc.[Email],''))) = '' THEN null
Else  Ltrim(Rtrim(pc.email))
END
as EmailAddress1,
NULL as EmailAddress2,
NULL as EmployeeNumber,
NULL as EmployeeSecurityID,
NULL as FaxPhone,
--CM_C.FaxPhoneCountry,
NULL as FaxPhoneCountry,
NULL as FaxPhoneExtension,
--ABTL_CNY_fax.DESCRIPTION as FaxCountry_Description,
NULL as FinancialSupplementDebt_icare,
--pc.FIRSTNAME as FirstName, --MC is individual only
LTRIM(RTRIM(ISNULL(pc.FIRSTNAME,''))) as FirstName,
--29Aug2019:as part of DMIG-7772 (DMIG-8073) applied LTRIM and RTRIM to ensure any leadding/trailing spaces are removed
--Else for ER_unmatched we end with the 2 sets of Main Contact even though technically the names are the same.
NULL as FirstNameKanji,
NULL as FormerName,
NULL as Gender,
NULL as GreenCardNumber,
NULL as HelpSslorTslDebt_icare,
--CM_C.HomePhone,
tmp_phone.HomePhone as HomePhone, --coming from Ibrahim cleansed phone number logic
--CM_C.HomePhoneCountry,
CASE 
WHEN LTRIM(RTRIM(ISNULL(tmp_phone.HomePhone,''))) <> '' THEN  'AU' 
ELSE NULL 
END
as HomePhoneCountry,
--ABTL_CNY_home.DESCRIPTION as HomeCountry_Description,
NULL as HomePhoneExtension,
NULL as InterpreterRequired_icare,
NULL as JurisdictionAssignedID,
--pc.LASTNAME as LastName,
LTRIM(RTRIM(ISNULL(pc.LASTNAME,''))) as LastName,
--29Aug2019:as part of DMIG-7772 (DMIG-8073) applied LTRIM and RTRIM to ensure any leadding/trailing spaces are removed
--Else for ER_unmatched we end with the 2 sets of Main Contact even though technically the names are the same.
NULL as LastNameKanji,
NULL as LawFirmSpecialty,
NULL as LegalSpecialty_icare,
NULL as LicenseNumber,
NULL as LicenseState,
NULL as MaritalStatus,
NULL as MedicalLicense,
NULL as MedicalOrgSpecialty,
NULL as MedicalSpecialty_icare,
NULL as MedicareIRN_icare,
NULL as MedicareLevyVariation_icare,
NULL as MedicareNumber_icare,
NULL as MedicareValidUntil_icare,
NULL as MiddleName,
NULL as Name,  --MC will have this blank
NULL as NameKanji,
NULL as Notes,
NULL as NumDependents,
NULL as NumDependentsU18,
NULL as NumDependentsU25,
--'FALSE' as ObfuscatedInternal,
0 as ObfuscatedInternal,
NULL as Occupation,
NULL as OccupationDetails_icareID,
NULL as OffsetAmount_icare,
NULL as OktaID_icare,
NULL as OrganizationType,
NULL as Particle,
NULL as PassportNumber,
NULL as PayInfoUpdUserName_icare,
--'FALSE' as PendingLinkMessage,
NULL as PendingLinkMessage,
'EMICS' as PolicySystemId,
--CASE WHEN CM_C.Preferred is NULL then 0
--ELSE CM_C.Preferred
--End
--Preferred,
0 as Preferred,
NULL as PreferredCurrency,
--PreferredPaymentMethod_icare,  This is to be added when Ann finished eftdata Employer
NULL as PreferredPaymentMethod_icare,
NULL as Prefix,
'EMICS:' + ltrim(rtrim(cd.Claim_Number)) + ':INSURED_MC:BUSINESSADDRESS' as PrimaryAddressID,
--'en_AU' as PrimaryLanguage,
NULL as PrimaryLanguage,
--CM_C.PrimaryLanguage as PrimaryLanguage,-- 19/06/2019 - need to raise this defect against mapper
NULL as PrimaryLocale,
--CM_C.PrimaryLocale as PrimaryLocale, -- 19/06/2019 - need to raise this defect against mapper
--CM_C.PrimaryPhone,
tmp_phone.PrimaryNo as PrimaryPhone, --this comes from Ibrahim cleansed phone number logic
--abtl_PrimaryPhoneType.TYPECODE as PrimarPhTypeCpde,
NULL as RegisteredForGST_icare,
0 as Retired,
NULL as SSNReleaseAuthorized,
NULL as Score,
--0 as SingleRemittance,  --19/06/2019 : Need to change the attribute name in other SPs
NULL as SingleRemittancePerDay_icare,
NULL as SmsNotification_icare,--19/06/2019 : Need to add this attribute in other SPs
--'Salesforce' as SourceSystem_icare,
'GWCC' as SourceSystem_icare, --27Jul2019 Fixed this as spec says so for Verified CDR
Null as SpecialtyType,
'Person' as Subtype,
NULL as Suffix,
NULL as TFNDeclarationDate_icare,
Null as TaxExemptionsEntitled,
Null as TaxFilingStatus,
Null as TaxID,
NULL as TaxStatus,
Null as TempCRMUniqueID_icare,
NULL as TradingName_icare,
NULL as TrustName_icare,
NULL as TrusteeName_icare,
'loadsave' as ValidationLevel,
NULL as VendorNumber,
NULL as VendorType,
NULL as VenueType,
NULL as W9Received,
NULL as W9ReceivedDate,
NULL as W9ValidFrom,
NULL as W9ValidTo,
NULL as WithholdingRate,
--CM_C.WorkPhone,
tmp_phone.DirectPhone as WorkPhone, --comes from Ibrahim's cleansed logic
--CM_C.WorkPhoneCountry,
CASE 
WHEN LTRIM(RTRIM(ISNULL(tmp_phone.DirectPhone,''))) <> '' THEN 'AU' 
ELSE NULL 
END
as WorkPhoneCountry,
--ABTL_CNY_work.DESCRIPTION as WorkCountry_Description,
NULL as WorkPhoneExtension,
NULL as ACN_icare,
NULL as  CommencementDate_icare,
NULL as  OrganisationType_icare, -- we dont have to go to [dbo].[abtl_organizationtype] to get corresponding TYPECODE as CM_C.OrganisationType_icare itself is the TYPECODE
NULL as OtherLanguage_icare,
NULL as  TrustABN_icare,
NULL as TrustABNIsActive_icare,
NULL as  TrustABNValidated_icare,
NULL as  TrusteeType_icare,
NULL as  TrustRegisteredForGST_icare,
--NULL as DoctorSpecialty_icare,
--CM_C.CRMVersion_icare
NULL as CRMVersion_icare  --As this is verified CDR and so we don't expect these contacts to go into CM/CRM
,ltrim(rtrim(CD.Claim_Number)) as LUWID
,NULL as ApprovedPaymentMethod
--,cons_pp.PC_PP_PolicyNumber as PC_PolicyNumber
,NULL as  PC_PolicyNumber
--0 as AggregaeRemittance--,
--eftdata.bankname
from
[DM_STG_EMICS].[dbo].CLAIM_DETAIL CD
INNER JOIN [DM_STG_EMICS].[dbo].[person_clm] pc
ON cd.[Notifier_Id] = pc.[ID]
INNER JOIN [DM_STG_EMICS].[dbo].[Ref_Look_Up] rlu
ON cd.[Notified_By] = rlu.[Code]
inner join #tmpNotifierPhoneNumbers tmp_phone
on tmp_phone.Claim_Number=ltrim(rtrim(CD.Claim_Number))
left outer join [dbo].[ccst_address] QA_PL_add -- ccst_address QA run should have happened before
on  QA_PL_add.LUWID=ltrim(rtrim(CD.Claim_Number)) and QA_PL_add.PublicID like '%POLICYLOCATIONADDRESS'
--inner join [dbo].[CoverREF_temptable_consolidated] cons_pp on cons_pp.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_1 PPLC_matched_1 --15479
on PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_2 PPLC_matched_2 --471
on PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_3 PPLC_matched_3 --0
on PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_PPLC_VerifiedPC_namematched_4 PPLC_matched_4 --6
on PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1 non_PPLC_matched_1 --4706
on non_PPLC_matched_1.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2 non_PPLC_matched_2 --81
on non_PPLC_matched_2.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3 non_PPLC_matched_3 --0
on non_PPLC_matched_3.LUWID=ltrim(rtrim(CD.Claim_Number)) 
left outer join temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4 non_PPLC_matched_4 --0
on non_PPLC_matched_4.LUWID=ltrim(rtrim(CD.Claim_Number)) 
 --18/06/2019 -- Adding the modified join based on Ash walkthrough of latest MC spec
left outer join temptable_MC_ER_VerifiedPC_unmatched_temp3 unmatched_ER --50573
on unmatched_ER.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_1 MC_WOP_1  --8009
on MC_WOP_1.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_2 MC_WOP_2 --494
on MC_WOP_2.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_3 MC_WOP_3 --0
on MC_WOP_3.LUWID=ltrim(rtrim(CD.Claim_Number))
left outer join temptable_MC_WOP_PPLC_VerifiedPC_4 MC_WOP_4 --7
on MC_WOP_4.LUWID=ltrim(rtrim(CD.Claim_Number))
where CD.is_Null = 0
--and CD.Claim_Number in (1239334,1209899)
--and PC_Con_lkp.rownum=1  --Ensuring we pick only 1 match when there are multiple name matches
--and rlu.[Code] in ('E','R') --Ensuing we filter only E and R
and PPLC_matched_1.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_1
and PPLC_matched_2.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_2
and PPLC_matched_3.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_3
and PPLC_matched_4.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_PPLC_MATCHED_4
and non_PPLC_matched_1.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_1
and non_PPLC_matched_2.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_2
and non_PPLC_matched_3.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_3
and non_PPLC_matched_4.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_not_PPLC_MATCHED_4
and unmatched_ER.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in unmatched_ER temp table
and MC_WOP_1.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_WOP_1
and MC_WOP_2.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_WOP_2
and MC_WOP_3.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_WOP_3
and MC_WOP_4.LUWID is NULL --Ensuring that we don't create another MC record if it already exists in MC_WOP_4
)X


--Populating Notifier Type='E,'R' PPLC matched MC contacts into a table that can be used in the parent Contact SP
select
*
into dbo.ContactREF_temptable_MC_ER_PPLC_VerifiedPC_namematched
from
(
select * 
from temptable_MC_ER_PPLC_VerifiedPC_namematched_1
union
select * 
from temptable_MC_ER_PPLC_VerifiedPC_namematched_2
union
select * 
from temptable_MC_ER_PPLC_VerifiedPC_namematched_3
union
select * 
from temptable_MC_ER_PPLC_VerifiedPC_namematched_4
)x
;

--Populating Notifier Type='E,'R' non-PPLC matched MC contacts into a table that can be used in the parent Contact SP
select
*
into dbo.ContactREF_temptable_MC_ER_not_PPLC_VerifiedPC_namematched
from
(
select * 
from temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1
union
select * 
from temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2
union
select * 
from temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3
union
select * 
from temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4
)x
;

--Populating Notifier Type='E,'R' UNMATCHED MC contacts into a table that can be used in the parent Contact SP
select
*
into dbo.ContactREF_temptable_MC_ER_VerifiedPC_unmatched
from
(
select * 
from temptable_MC_ER_VerifiedPC_unmatched_temp3
)x
;

--Populating Notifier Type='W','O', 'P' PPLC MC contacts into a table that can be used in the parent Contact SP
select
*
into dbo.ContactREF_temptable_MC_WOP_PPLC_VerifiedPC
from
(
select * 
from temptable_MC_WOP_PPLC_VerifiedPC_1
union
select * 
from temptable_MC_WOP_PPLC_VerifiedPC_2
union
select * 
from temptable_MC_WOP_PPLC_VerifiedPC_3
union
select * 
from temptable_MC_WOP_PPLC_VerifiedPC_4
)x
;

--Populating rest of all MC contacts (Verified CDR) into a table that can be used in the parent Contact SP
select
*
into dbo.ContactREF_temptable_MC_rest_of_all_VerifiedCDR
from
(
select * 
from temptable_MC_rest_of_all_VerifiedCDR_temp
)x
;


--Dropping all temp tables
IF OBJECT_ID('tempdb..#temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table #temptable_MAINCONTACT_gwpc_contacts_lookup

IF OBJECT_ID('ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup') is not null
drop table ContactREF_temptable_MAINCONTACT_gwpc_contacts_lookup

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_1') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_1;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_2') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_2;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_3') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_3;

IF OBJECT_ID('temptable_MC_ER_PPLC_VerifiedPC_namematched_4') is not null
drop table temptable_MC_ER_PPLC_VerifiedPC_namematched_4;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_1;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_2;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_3;

IF OBJECT_ID('temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4') is not null
drop table temptable_MC_ER_not_PPLC_VerifiedPC_namematched_4;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_1') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_1;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_2') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_2;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_3') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_3;

IF OBJECT_ID('temptable_MC_WOP_PPLC_VerifiedPC_4') is not null
drop table temptable_MC_WOP_PPLC_VerifiedPC_4;

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp1') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp1

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_max_claimnumber

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp2') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp2

IF OBJECT_ID('temptable_MC_ER_VerifiedPC_unmatched_temp3') is not null
drop table temptable_MC_ER_VerifiedPC_unmatched_temp3

IF OBJECT_ID('temptable_MC_rest_of_all_VerifiedCDR_temp') is not null
drop table temptable_MC_rest_of_all_VerifiedCDR_temp

IF OBJECT_ID('temptable_ER_PPLC_N_1') is not null
drop table temptable_ER_PPLC_N_1

IF OBJECT_ID('temptable_ER_PPLC_N_2') is not null
drop table temptable_ER_PPLC_N_2

IF OBJECT_ID('temptable_ER_PPLC_N_3') is not null
drop table temptable_ER_PPLC_N_3

IF OBJECT_ID('temptable_ER_PPLC_N_4') is not null
drop table temptable_ER_PPLC_N_4

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_1') is not null
drop table temptable_count_gwpc_contact_lkp_1

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_2') is not null
drop table temptable_count_gwpc_contact_lkp_2

IF OBJECT_ID('temptable_count_gwpc_contact_lkp_3') is not null
drop table temptable_count_gwpc_contact_lkp_3

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_1') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_1

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_2') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_2

IF OBJECT_ID('temptable_WOP_count_gwpc_contact_lkp_3') is not null
drop table temptable_WOP_count_gwpc_contact_lkp_3

END