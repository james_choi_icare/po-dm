﻿CREATE PROC [dbo].[SP_05_ADM_MAPPING_VERSION]
	@recXFMtable varchar(300), @mappingVersion varchar(100)
AS
BEGIN
	UPDATE ADM_XFM_TABLE_MAPPING
	SET MAPPING_VERSION = @mappingVersion
	WHERE xfm_recon_table = @recXFMtable;
END