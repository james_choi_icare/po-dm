﻿/* =============================================
-- Author:		Ann
-- Create date: 13/06/2019
-- Description:	Claim Management Domain - ccst_planstratrevparties_icare
-- Version: 13/06/2019 v24 initial build
-- ============================================= */
CREATE PROCEDURE [dbo].[SP_REC_XFM_PLANSTRATREVPARTIES]
AS
BEGIN
EXEC SP_05_ADM_MAPPING_VERSION 'ccst_planstratrevparties_icare','Claim Management - v24 13/06/2019'

Select
	'EMICS:' + RTRIM(LTRIM(cd.Claim_Number)) + ':INJURED' as ContactID,
	NULL as FreeTextParty,
	cd.Claim_Number as LUWID,
	'EMICS:' + RTRIM(rtwp.Claim_No) + '_' + convert(varchar, rtwp.rtwplan_no) + ':PLANSTRATREVPARTY' as PublicID,
	'EMICS:' + RTRIM(rtwp.Claim_No) + '_' + convert(varchar, rtwp.rtwplan_no) + ':PLANREVIEW' ThePlanStratReviewID

into ccst_planstratrevparties_icare

From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration rtwd --691
Inner join DM_STG_EMICS.dbo.Claim_RTW_Plan rtwp on rtwp.Claim_No = rtwd.Claim_No and rtwd.RTWplan_no = rtwp.rtwplan_no
Inner join (Select Claim_no, RTWplan_no, MAX(Duration_no) as Duration_no From DM_STG_EMICS.dbo.Claim_RTW_Plan_Duration as Claim_RTW_Plan_Duration_1
	Group by Claim_No, RTWplan_no) as Max_Duration on rtwd.Claim_No = Max_Duration.Claim_no
	and rtwd.RTWplan_no = Max_Duration.RTWplan_no
	and rtwd.Duration_no = Max_Duration.Duration_no
Inner join DM_STG_EMICS.dbo.claim_detail cd on rtwp.claim_no = cd.claim_number
Where cd.is_Null = 0
END