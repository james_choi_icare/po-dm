﻿/* =============================================
-- Author:	    Saranya Jayaseelan
-- Create date: 26/04/2019
-- Description:	Estimates and Reserves,  ccst_deduction
-- Version: initial build - version v26
--				14/05 checked against v27.2 - no change
--				07/08 v27.3 updated SC 
--				04/09/2019 v28.1 updated checkid and publicid xform, added cpc SC
-- ============================================= */
CREATE PROCEDURE [dbo].[SP_REC_XFM_DEDUCTION] 

AS
BEGIN

EXEC SP_05_ADM_MAPPING_VERSION 'ccst_deduction','Estimates and Reserves - v28.1 04/09/2019'
-- drop table ccst_deduction

;WITH temp_deduction as (
	SELECT 
		CASE WHEN CHEQUE_NO IS  NULL THEN  'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+ISNULL(convert(varchar, cpc.ID),'00')+':CHECK'
			ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+ISNULL(convert(varchar,cpc.ID),'00')+':CHECK'
			END as CheckId,
		sum(PR.Tax_Amt) AS ClaimAmount,
		'paygtax_icare'  as DeductionType,
		CPR.Claim_number as LUWID,
		CASE WHEN CPR.CHEQUE_NO IS  NULL THEN  
			'EMICS:'+convert(varchar,CPR.Payment_No)+'_'+ISNULL(convert(varchar, cpc.ID),'00')+':DED'
			ELSE 'EMICS:'+convert(varchar,CPR.Payment_No)+ISNULL('_'+RTRIM(CPR.Cheque_no),'')+'_'+ISNULL(convert(varchar, cpc.ID),'00')+':DED' END  as PublicID,
		sum(PR.Tax_Amt) AS ReportingAmount,
		sum(PR.Tax_Amt) as  ReservingAmount,
		sum(PR.Tax_Amt) as TransactionAmount
		
		FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN CPR
		INNER JOIN DM_STG_EMICS.dbo.CLAIM_DETAIL CD
		ON CD.Claim_Number=CPR.Claim_number
	--	inner join DM_XFM.dbo.dm_inscope_claims IC on IC.Claim_Number=LTRIM(RTRIM(CD.Claim_Number)) 
		INNER JOIN (select min(id) over(partition  by Claim_no,Payment_No,Invoice_no,Invoice_Date,period_start_date,period_end_date,Estimate_type ) as pr_id,* from DM_STG_EMICS.dbo.Payment_Recovery) PR
		on CPR.Claim_number=PR.Claim_No
		AND CPR.Payment_no=PR.Payment_no
		LEFT OUTER JOIN (
		SELECT B.Paycode,C.TYPECODE AS PAYMENTTYPE_ICARE,c.ID FROM DM_STG_CC..CCX_paycodepaymenttype_icare A,
		 (select * from DM_STG_CC..CCX_paycode_icare where Retired=0) B,DM_STG_CC..cctl_paymenttype_icare C
		WHERE A.Paycode=B.ID AND A.PaymentType=C.ID) CPC
ON CPC.Paycode=(case when pr.Payment_Type like 'OR%' then PR.WC_Payment_Type else PR.Payment_Type end)
where Tax_Amt<>0
		--#DMIG-3813-- applied the filters to get the deduction in sync with the Check records
and PR.Estimate_type<70 and PR.Reversed<>1 and isnull(CPR.cheque_status,'00') not in (3,6) and cd.is_Null=0 --v27.3
		group by CPR.Claim_number,CPR.Payment_No,CPR.CHEQUE_NO, cpc.id
)

Select * into ccst_deduction from temp_deduction


END