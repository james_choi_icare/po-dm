﻿CREATE TABLE [dbo].[ADM_RECON_RESULT_PASSED] (
    [Tgt_Table]  VARCHAR (100) NOT NULL,
    [Tgt_Column] VARCHAR (100) NOT NULL,
    [Pass_Count] INT           NULL,
    [RunID]      INT           NULL
);

