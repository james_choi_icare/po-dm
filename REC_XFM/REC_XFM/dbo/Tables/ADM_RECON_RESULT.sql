﻿CREATE TABLE [dbo].[ADM_RECON_RESULT] (
    [Tgt_Table]      VARCHAR (100)  NOT NULL,
    [Tgt_Column]     VARCHAR (100)  NOT NULL,
    [XFM_Key]        VARCHAR (300)  NULL,
    [Expected_Key]   VARCHAR (300)  NULL,
    [XFM_Value]      NVARCHAR (MAX) NULL,
    [Expected_Value] NVARCHAR (MAX) NULL,
    [Source_Value]   NVARCHAR (MAX) NULL,
    [RunID]          INT            NULL,
    [LUWID]          VARCHAR (100)  NULL
);








GO
CREATE NONCLUSTERED INDEX [CIX_XFM_Key]
    ON [dbo].[ADM_RECON_RESULT]([XFM_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [CIX_Tgt_Table]
    ON [dbo].[ADM_RECON_RESULT]([Tgt_Table] ASC);


GO
CREATE NONCLUSTERED INDEX [CIX_RunID]
    ON [dbo].[ADM_RECON_RESULT]([RunID] ASC);


GO
CREATE NONCLUSTERED INDEX [CIX_LUWID]
    ON [dbo].[ADM_RECON_RESULT]([LUWID] ASC);


GO
CREATE NONCLUSTERED INDEX [CIX_Expected_Key]
    ON [dbo].[ADM_RECON_RESULT]([Expected_Key] ASC);

