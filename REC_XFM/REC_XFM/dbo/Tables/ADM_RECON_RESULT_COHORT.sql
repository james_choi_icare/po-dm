﻿CREATE TABLE [dbo].[ADM_RECON_RESULT_COHORT] (
    [Tgt_Table]      VARCHAR (100)  NOT NULL,
    [Tgt_Column]     VARCHAR (100)  NOT NULL,
    [XFM_Key]        VARCHAR (300)  NULL,
    [Expected_Key]   VARCHAR (300)  NULL,
    [XFM_Value]      NVARCHAR (MAX) NULL,
    [Expected_Value] NVARCHAR (MAX) NULL,
    [Source_Value]   NVARCHAR (MAX) NULL,
    [RunID]          INT            NULL,
    [LUWID]          VARCHAR (100)  NULL
);

