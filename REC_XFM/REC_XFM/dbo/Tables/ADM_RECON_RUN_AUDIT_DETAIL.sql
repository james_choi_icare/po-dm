﻿CREATE TABLE [dbo].[ADM_RECON_RUN_AUDIT_DETAIL] (
    [RunID]     INT           NULL,
    [SPName]    VARCHAR (100) NULL,
    [StartTime] DATETIME      NULL,
    [EndTime]   DATETIME      NULL
);

