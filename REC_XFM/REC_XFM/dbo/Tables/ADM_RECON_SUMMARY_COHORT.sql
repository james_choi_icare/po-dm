﻿CREATE TABLE [dbo].[ADM_RECON_SUMMARY_COHORT] (
    [XFM_TABLE]                 VARCHAR (100) NOT NULL,
    [PASS_RATE (%)]             INT           NULL,
    [MISSING_RECORDS_IN_XFM]    INT           NULL,
    [ADDITIONAL_RECORDS_IN_XFM] INT           NULL,
    [RUN_ID]                    INT           NULL,
    [MAPPING_VERSION]           VARCHAR (100) NULL
);

