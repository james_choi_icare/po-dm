﻿CREATE TABLE [dbo].[ADM_XFM_RULES] (
    [GWTable]            VARCHAR (500)  NULL,
    [GWField]            VARCHAR (500)  NULL,
    [SourceDB]           VARCHAR (500)  NULL,
    [SourceTable]        VARCHAR (500)  NULL,
    [SourceField]        VARCHAR (500)  NULL,
    [TransformationRule] VARCHAR (5000) NULL,
    [MappingDate]        VARCHAR (500)  NULL,
    [Mapper]             VARCHAR (500)  NULL,
    [InScope]            VARCHAR (500)  NULL
);

