﻿CREATE TABLE [dbo].[ADM_Claim_Cohort] (
    [ClaimNumber]  VARCHAR (40) NULL,
    [PolicyNumber] VARCHAR (40) NULL,
    [State]        VARCHAR (50) NULL,
    [Cohort]       INT          NOT NULL
);

