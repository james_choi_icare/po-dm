﻿
CREATE FUNCTION dbo.fn_Claim_note_Text (@clmNo varchar(30), @Type varchar(50))
RETURNS VARCHAR(MAX)

AS 
BEGIN


DECLARE @Body VARCHAR(MAX)
SET @Body = 'X'

DECLARE @Value VARCHAR(MAX)

IF @Type = 'General'
BEGIN
	SET @Body = '* DATA MIGRATION: The claim was migrated from EML on ' + convert(varchar(10), GETDATE(),103) + '.'
	
	SET @Value = (Select cd.PIAWE from DM_STG_EMICS.dbo.CLAIM_DETAIL cd 
					INNER JOIN DM_STG_EMICS.dbo.amendment_exemptions ae on cd.claim_number = ae.claim_no
					WHERE cd.PIAWE is not null 
						and cd.piawe > 1
						and (cd.PIAWE_VERIFIED_DATE IS NOT NULL OR ae.is_exempt = 1)
						and cd.claim_number = @clmNo)
		IF @Value is not null 
		BEGIN
			IF @Body = 'X' SET @Body = '* PIAWE: No unverified PIAWE amounts have been migrated. Latest verified PIAWE is $' + @Value + '. For more details, please refer to the Weekly Benefits & Indemnity tab.'
			ELSE Set @Body = @Body + CHAR(13) + CHAR(10) + '* PIAWE: No unverified PIAWE amounts have been migrated. Latest verified PIAWE is $' + @Value + '. For more details, please refer to the Weekly Benefits & Indemnity tab.'
		END

	SET @Value = (SELECT SUM(Trans_Amount * -1) FROM DM_STG_EMICS.dbo.Payment_Recovery where Payment_type = 'RPE001' and reversed = 0 and claim_no = @clmNo
		Group By Claim_No)

		IF @Value is not null
		BEGIN
			IF @Body = 'X' SET @Body = '* EXCESS: Total Excess on this claim is $' + @Value + '. For more details, please refer to the Summary Screen under the Financials tab.'
			ELSE SET @Body = @Body + CHAR(13) + CHAR(10) + '* EXCESS: TOtal Excess on claim is $' + @Value + '. For more details, please refer to the Summary Screen under the Financials tab.'
		END

	SET @Value = (Select count(*) from DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL cad
					INNER JOIN DM_STG_EMICS.dbo.Claim_Detail cd on cad.claim_no = cd.claim_number
					LEFT OUTER JOIN DM_STG_EMICS.dbo.CLAIM_TRIAGE ct on cad.claim_no = ct.claim_no
					Where cd.is_null = 0
						and cad.Claim_Liability_Indicator = 8
						and ct.Segment_Code NOT IN ('G','E')
						and cad.claim_no = @clmNo)
	SET @Value = (Select count(*) from DM_STG_EMICS.dbo.Claim_RTW_Plan where claim_no = @clmNo)

	IF @Value > 0
	BEGIN
		IF @Body = 'X' SET @Body = '* RETURN TO WORK PLAN: This claim has Returned to WOrk Plans. Please check OnBase Documents System for details.'
		ELSE SET @Body = @body + CHAR(13) + CHAR(10) + '* RETURN TO WORK PLAN: This claim has Return to Work Plans. Please check OnBase Documents System for details.'
	END

	SET @Value = (Select Top 1 r.Description from DM_STG_EMICS.dbo.CLAIM_ACTIVITY_DETAIL c
					INNER JOIN DM_STG_EMICS.dbo.Ref_Look_Up r on c.Dispute_Type = r.code
					Where claim_no = @clmNo
					and c.Dispute_type is not null
					and c.dispute_type <> 0
					and r.name = 'DisputeType')

	IF @Value is not null
	BEGIN
		IF @Body = 'X' SET @Body = '* DISPUTE: This claim has a (' + @Value + ') dispute. Please check CRM for details.'
		ELSE SET @BODY = @BODY + CHAR(13) + CHAR(10) + '* DISPUTE: This claim has a (' + @Value + ') dispute. Please check CRM for details.'
	END

	SET @Value = (SELECT COUNT(*) FROM DM_STG_EMICS.dbo.WORK_CAPACITY_ASSESSMENT 
					WHERE Claim_no = @clmNo)

	IF @Value > 0
	BEGIN
		IF @BODY = 'X' SET @Body = '* WORK CAPACITY: Work Capacity details (formally sotred in Sharepoint) have not been migrated. This claim''s Work Capacity Assessment/Decision details will be entered manually after migration.'
		ELSE SET @Body = @Body + CHAR(13) + CHAR(10) + '* WORK CAPACITY: Work Capacity details (formally stored in Sharepoint) ave not been migrated. This claim''s Work Capacity Assessment/Decision details will be entered manually after migration.'
	END

	SET @Value = (Select STUFF((Select ', ' + LTRIM(RTRIM(cpr.CHEQUE_NO))
					FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr
					WHERE cpr.claim_number = cd.claim_number
					and cpr.cheque_no is not null
					and ltrim(rtrim(cpr.cheque_no)) <> ''
					AND CPR.BankChq_Status = 2
					FOR XML PATH ('')),1 ,1, '') as chqNos
				FROM DM_STG_EMICS.dbo.Claim_detail cd 
				Where cd.is_null = 0
				and STUFF((SELECT ', ' + LTRIM(RTRIM(cpr.CHEQUE_NO))
					FROM DM_STG_EMICS.dbo.CLAIM_PAYMENT_RUN cpr
					Where cpr.Claim_number = cd.claim_number
					and cpr.Cheque_no is not null
					and ltrim(Rtrim(cpr.cheque_no)) <> ''
					and cpr.BankChq_Status = 2
					FOR XML PATH ('')),1 ,1, '') IS NOT NULL
				and cd.claim_number = @clmNo)
	IF @Value is not null
	BEGIN
		IF @Body = 'X' SET @Body = '* STOPPED CHEQUES: The following Cheques where stopped in EMICS - (' + @Value + ').' 
		ELSE SET @Body = @Body + CHAR(13) + CHAR(10) + '* STOPPED CHEQUES: The following Cheques where stopped in EMICS - (' + @Value + ').'
	END

END 
IF @Body = 'X' SET @Body = NULL
ELSE Set @Body = 'Data Migration team notes on the migrated claim:' + CHAR(13) + CHAR(10) + @Body + CHAR(13) + CHAR(10) + CHAR(13) + CHAR(10) + '*** This note was created by the data migration team to document some important decisions, assumptions and instructions related to this migrated claim.'

RETURN @Body
END