﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_DB_Ref_Update
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Directory.GetCurrentDirectory().ToString()).ToString()).ToString()).ToString();
            string[] filePath = Directory.GetFiles(baseDirectory + @"\REC_XFM\dbo\Stored Procedures\");

            foreach (string fileName in filePath)
            {
                string contents = File.ReadAllText(fileName);
                contents = contents.Replace(" [DM_STG_CM].", " [DM_STG_CM_LD].").Replace(" [dm_stg_cm].", " [DM_STG_CM_LD].");
                contents = contents.Replace(" DM_STG_CM.", " [DM_STG_CM_LD].").Replace(" dm_stg_cm.", " [DM_STG_CM_LD].");

                contents = contents.Replace(" [DM_STG_CRM].", " [DM_STG_CRM_LD].").Replace(" [dm_stg_crm].", " [DM_STG_CRM_LD].");
                contents = contents.Replace(" DM_STG_CRM.", " [DM_STG_CRM_LD].").Replace(" dm_stg_crm.", " [DM_STG_CRM_LD].");

                contents = contents.Replace(" [DM_STG_PC].", " [DM_STG_PC_LD].").Replace(" [dm_stg_pc].", " [DM_STG_PC_LD].");
                contents = contents.Replace(" DM_STG_PC.", " [DM_STG_PC_LD].").Replace(" dm_stg_pc.", " [DM_STG_PC_LD].");

                contents = contents.Replace(" [DM_STG_CC].", " [DM_STG_CC_LD].").Replace(" [dm_stg_cc].", " [DM_STG_CC_LD].");
                contents = contents.Replace(" DM_STG_CC.", " [DM_STG_CC_LD].").Replace(" dm_stg_cc.", " [DM_STG_CC_LD].");

                contents = contents.Replace(" [DM_XFM].", " [DM_XFM_LD].").Replace(" [dm_xfm].", " [DM_XFM_LD].");
                contents = contents.Replace(" DM_XFM.", " [DM_XFM_LD].").Replace(" dm_xfm.", " [DM_XFM_LD].");

                File.WriteAllText(fileName, contents);

                int wrongCMref = contents.ToLower().IndexOf("dm_stg_cm.") + contents.ToLower().IndexOf("dm_stg_cm].");

                int wrongCRMref = contents.IndexOf("dm_stg_crm.") + contents.IndexOf("dm_stg_crm].");

                int wrongPCref = contents.IndexOf("dm_stg_pc.") + contents.IndexOf("dm_stg_pc].");

                int wrongCCref = contents.IndexOf("dm_stg_cc.") + contents.IndexOf("dm_stg_cc].");

                int wrongXFMref = contents.IndexOf("dm_xfm.") + contents.IndexOf("dm_xfm].");

                if (wrongCMref > 0 || wrongCRMref > 0 || wrongPCref > 0 || wrongCCref > 0 || wrongXFMref > 0)
                {
                    Console.WriteLine(fileName);
                }
            }

            Console.WriteLine("Database reference updated for the ST run");
            Console.ReadLine();
        }
    }
}
